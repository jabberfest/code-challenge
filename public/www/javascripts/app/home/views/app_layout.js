define([
	'underscore',
	'marionette',
	'templates/app_layout',
	'lib/components/tabs/tab_component_views',
	'entities/abstract/sessions',
	'lib/components/vertical_list/vertical_component_views',
	'lib/shared/session_detail/session_detail_views'
], function(_,Marionette, template,TabComponentViews,SessionEntity,VerticalListComponentViews,SessionDetail){
	
	AppLayout = Marionette.LayoutView.extend({
	  template: template,
	  el: 'body',
	  initialize: function(opts){
	  	this.tabCollections = opts.collection.groupByTitle();
	  	
	  	//Generate Tab configs dynamically based on session title grouping
	  	_self = this;
	  	this.tabConfigs = _.map(Object.keys(this.tabCollections),function(key){
	  		return {
	  			title: key,
	  			collection: new SessionEntity.SessionCollection(_self.tabCollections[key]), 
	  			view: VerticalListComponentViews.View,
	  			viewOpts: {view: SessionDetail.View}  
	  		}
	  	});

	  },
	  regions: {
	  	'tabRegion': '.tab-region'
	  },

	  onRender: function(){
	  	var tabComponent = new TabComponentViews.View({config:this.tabConfigs});
	  	this.tabRegion.show(tabComponent);
	  }

	});

	return AppLayout;
});