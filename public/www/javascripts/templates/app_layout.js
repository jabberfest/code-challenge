define(function(){
  var template = function(__obj) {
  var _safe = function(value) {
    if (typeof value === 'undefined' && value == null)
      value = '';
    var result = new String(value);
    result.ecoSafe = true;
    return result;
  };
  return (function() {
    var __out = [], __self = this, _print = function(value) {
      if (typeof value !== 'undefined' && value != null)
        __out.push(value.ecoSafe ? value : __self.escape(value));
    }, _capture = function(callback) {
      var out = __out, result;
      __out = [];
      callback.call(this);
      result = __out.join('');
      __out = out;
      return _safe(result);
    };
    (function() {
      _print(_safe('<div class="container-fluid">\n  <div class="row nav">\n    <div class="col-md-12">\n      <div class="row body">\n        <img src="images/logo.png"/>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class="container">\n  <div class="row page-header-logo">\n    <div class="col-md-12">\n      <img src="images/summit-logo.png"/>\n    </div>\n  </div>\n\n  <div class="row page-header-logo">\n    <div class="col-md-12">\n      <h3>Video Archive</h3>\n    </div>\n  </div>\n\n  <div class="row body">\n    <div class="col-md-12 tab-region"></div> \n  </div>\n</div>'));
    
    }).call(this);
    
    return __out.join('');
  }).call((function() {
    var obj = {
      escape: function(value) {
        return ('' + value)
          .replace(/&/g, '&amp;')
          .replace(/</g, '&lt;')
          .replace(/>/g, '&gt;')
          .replace(/"/g, '&quot;');
      },
      safe: _safe
    }, key;
    for (key in __obj) obj[key] = __obj[key];
    return obj;
  })());
};
  return template;
});
