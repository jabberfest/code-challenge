define(function(){
  var template = function(__obj) {
  var _safe = function(value) {
    if (typeof value === 'undefined' && value == null)
      value = '';
    var result = new String(value);
    result.ecoSafe = true;
    return result;
  };
  return (function() {
    var __out = [], __self = this, _print = function(value) {
      if (typeof value !== 'undefined' && value != null)
        __out.push(value.ecoSafe ? value : __self.escape(value));
    }, _capture = function(callback) {
      var out = __out, result;
      __out = [];
      callback.call(this);
      result = __out.join('');
      __out = out;
      return _safe(result);
    };
    (function() {
      var i, len, ref, ref1, speaker;
    
      _print(_safe('<h2>'));
    
      _print(this.Title);
    
      _print(_safe('</h2>\n\n'));
    
      if (this.Speakers != null) {
        _print(_safe('\n\t<cite><p>'));
        _print(this.Speakers[0].FirstName + " " + this.Speakers[0].LastName + ", " + this.Speakers[0].Company);
        _print(_safe('</p></cite>\n'));
      }
    
      _print(_safe('\n\n<p>'));
    
      _print(this.Description);
    
      _print(_safe('</p>\n\n'));
    
      if ((this.Speakers != null) && ((ref = this.Speakers[0].Biography) != null ? ref.length : void 0) !== 0) {
        _print(_safe('\n<h3>About the speaker'));
        _print(this.Speakers.length > 1 ? "s" : void 0);
        _print(_safe('</h3>\n\n\t'));
        ref1 = this.Speakers;
        for (i = 0, len = ref1.length; i < len; i++) {
          speaker = ref1[i];
          _print(_safe('\n\t<p>\n\t\t<strong>'));
          _print(speaker.FirstName + " " + speaker.LastName + ",");
          _print(_safe('</strong> \n\t\t'));
          _print("" + speaker.Company);
          _print(_safe('\n\t\t'));
          debugger;
          _print(_safe('\n\t</p>\n\t<p>'));
          _print(speaker.Biography);
          _print(_safe('</p>\n\n\t'));
        }
        _print(_safe('\n\n'));
      }
    
      _print(_safe(' '));
    
    }).call(this);
    
    return __out.join('');
  }).call((function() {
    var obj = {
      escape: function(value) {
        return ('' + value)
          .replace(/&/g, '&amp;')
          .replace(/</g, '&lt;')
          .replace(/>/g, '&gt;')
          .replace(/"/g, '&quot;');
      },
      safe: _safe
    }, key;
    for (key in __obj) obj[key] = __obj[key];
    return obj;
  })());
};
  return template;
});
