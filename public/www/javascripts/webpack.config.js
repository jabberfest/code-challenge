module.exports = {
    entry: "./index.js",
    output: {
        path: __dirname,
        filename: "bundle.js"
    },
    resolve:{
    	modulesDirectories: ['../bower_components','./'],
    	alias: {
    		'jquery':'jquery/dist/jquery.js',
    		'underscore': 'underscore/underscore.js',
    		'backbone': 'backbone/backbone.js',
    		'marionette': 'backbone.marionette/lib/backbone.marionette.js',
            'backbone.chooser': 'lib/vendor/backbone-chooser.js',
            'templates' : '../../../templates',
            'json' : 'json'
    	}
    }
};