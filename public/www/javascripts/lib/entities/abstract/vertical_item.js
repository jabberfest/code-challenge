define([
	'backbone'
], function(Backbone){
	//TODO: Clean up explicit require since backbone.chooser does not appear to support AMD loading
	window._ = require('underscore');
	require('backbone.chooser');

	var VerticalItemEntity = {}

	VerticalItemEntity.Item = Backbone.Model.extend({
		defaults: {},
		initialize: function(){
			new Backbone.Chooser(this);
		}
	});

	VerticalItemEntity.ItemCollection = Backbone.Collection.extend({
		model: VerticalItemEntity.Item,
		initialize: function(){
			new Backbone.SingleChooser(this)
		}
	});

	return VerticalItemEntity;
});