define([
	'marionette',
	'templates/session_detail_view',
], function(Marionette,session_detail_template){
	
	var SessionDetail = {}


	SessionDetail.View = Marionette.LayoutView.extend({
	  template: session_detail_template,
	  className: 'session-detail-view'
	});


	return SessionDetail;
});