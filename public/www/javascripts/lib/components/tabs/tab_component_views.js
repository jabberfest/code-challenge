define([
	'underscore',
	'marionette',
	'templates/tab_component_layout',
	'templates/tab_view',
	'lib/entities/abstract/tab'
], function(_,Marionette, tab_component_layout_template, tab_view_template, TabEntity){
	
	var TabComponent = {}
	var _this = TabComponent;

	TabComponent.View = Marionette.LayoutView.extend({
	  template: tab_component_layout_template,
	  className: 'tab-component',
	  initialize: function(opts){
	  	this.tabs = opts.config
	  },
	  regions:{
	  	tabHeader: '.tab-header-region',
	  	tabContent: '.tab-content-region'
	  },
	  onShow: function(){
	  	tabCollection = new TabEntity.TabCollection(_.map(this.tabs,function(tab){
	  		return new TabEntity.Tab(tab);
	  	}));

	   	var tabCollectionView = new _this.TabCollectionView({collection:tabCollection});
	   	this.listenTo(tabCollection, "collection:chose:one",this.tabChosen);
	   	tabCollection.listenTo(tabCollectionView, "show", this.setDefaultTab);
	   	this.tabHeader.show(tabCollectionView);
	  },
	  tabChosen: function(model){
	  	var tabView = new (model.get('view'))({model: model});
	  	this.tabContent.show(tabView);
	  },
	  setDefaultTab: function(){
	   	//Select first tab by default
	   	this.choose(this.at(0));
	  }
	});

	TabComponent.TabView = Marionette.ItemView.extend({
		template: tab_view_template,
		tagName: 'li',
		modelEvents:{
			'change chosen':'onTabChanged'
		},
		triggers:{
			'click a' : 'tab:chosen'
		},
		onTabChanged: function(model){
			if (model.get('chosen')){
				this.$el.addClass('current');	
			}else{
				this.$el.removeClass('current');
			}
		}
	});

	TabComponent.TabCollectionView = Marionette.CollectionView.extend({
		childView: _this.TabView,
		tagName: 'ul',
		className: 'tabs col-md-12',
		childEvents:{
			'tab:chosen': 'onTabChosen'
		},
		onTabChosen: function(view){
			this.collection.choose(view.model);
		}
	});

	return TabComponent;
});