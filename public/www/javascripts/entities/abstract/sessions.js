define([
	'underscore',
	'backbone'
], function(_,Backbone){

	var SessionEntity = {}

	SessionEntity.Session = Backbone.Model.extend({
		defaults: {}
	});

	SessionEntity.SessionCollection = Backbone.Collection.extend({
		model: SessionEntity.Session,

		groupByTitle: function(){
			//Group sessions by title
			var groups = _.groupBy(this.models,function(session){
				return session.get('Track').Title
			});

			/*
				Lets remove Activities and Training Talks. Ideally, we'd do this more intelligently
				by filtering if certain conditions were met
			*/
			delete groups.Activities;
			delete groups.Training;

			return groups;
		}
	});

	return SessionEntity;

});