define([
	'jquery',
	'underscore',
	'marionette',
	'app/home/views/app_layout',
	'entities/abstract/sessions',
	'json!json/sessions.json'
], function($,_,Marionette, AppLayout,SessionEntity,SessionJSON){
	$(document).ready(function(){

		//Load up the Session Collection
		var sessionCollection = new SessionEntity.SessionCollection(_.map(SessionJSON.Items, function(elem){
			return new SessionEntity.Session(elem)
		}));


		//Create the app layout
		var appLayout = new AppLayout({
			collection:sessionCollection
		});

		//Render the app
		appLayout.render();
	});
});