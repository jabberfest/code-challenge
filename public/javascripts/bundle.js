/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [
		__webpack_require__(1),
		__webpack_require__(2),
		__webpack_require__(3),
		__webpack_require__(5),
		__webpack_require__(12),
		__webpack_require__(19)
	], __WEBPACK_AMD_DEFINE_RESULT__ = function($,_,Marionette, AppLayout,SessionEntity,SessionJSON){
		$(document).ready(function(){

			//Load up the Session Collection
			var sessionCollection = new SessionEntity.SessionCollection(_.map(SessionJSON.Items, function(elem){
				return new SessionEntity.Session(elem)
			}));


			//Create the app layout
			var appLayout = new AppLayout({
				collection:sessionCollection
			});

			//Render the app
			appLayout.render();
		});
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
	 * jQuery JavaScript Library v2.1.4
	 * http://jquery.com/
	 *
	 * Includes Sizzle.js
	 * http://sizzlejs.com/
	 *
	 * Copyright 2005, 2014 jQuery Foundation, Inc. and other contributors
	 * Released under the MIT license
	 * http://jquery.org/license
	 *
	 * Date: 2015-04-28T16:01Z
	 */

	(function( global, factory ) {

		if ( typeof module === "object" && typeof module.exports === "object" ) {
			// For CommonJS and CommonJS-like environments where a proper `window`
			// is present, execute the factory and get jQuery.
			// For environments that do not have a `window` with a `document`
			// (such as Node.js), expose a factory as module.exports.
			// This accentuates the need for the creation of a real `window`.
			// e.g. var jQuery = require("jquery")(window);
			// See ticket #14549 for more info.
			module.exports = global.document ?
				factory( global, true ) :
				function( w ) {
					if ( !w.document ) {
						throw new Error( "jQuery requires a window with a document" );
					}
					return factory( w );
				};
		} else {
			factory( global );
		}

	// Pass this if window is not defined yet
	}(typeof window !== "undefined" ? window : this, function( window, noGlobal ) {

	// Support: Firefox 18+
	// Can't be in strict mode, several libs including ASP.NET trace
	// the stack via arguments.caller.callee and Firefox dies if
	// you try to trace through "use strict" call chains. (#13335)
	//

	var arr = [];

	var slice = arr.slice;

	var concat = arr.concat;

	var push = arr.push;

	var indexOf = arr.indexOf;

	var class2type = {};

	var toString = class2type.toString;

	var hasOwn = class2type.hasOwnProperty;

	var support = {};



	var
		// Use the correct document accordingly with window argument (sandbox)
		document = window.document,

		version = "2.1.4",

		// Define a local copy of jQuery
		jQuery = function( selector, context ) {
			// The jQuery object is actually just the init constructor 'enhanced'
			// Need init if jQuery is called (just allow error to be thrown if not included)
			return new jQuery.fn.init( selector, context );
		},

		// Support: Android<4.1
		// Make sure we trim BOM and NBSP
		rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,

		// Matches dashed string for camelizing
		rmsPrefix = /^-ms-/,
		rdashAlpha = /-([\da-z])/gi,

		// Used by jQuery.camelCase as callback to replace()
		fcamelCase = function( all, letter ) {
			return letter.toUpperCase();
		};

	jQuery.fn = jQuery.prototype = {
		// The current version of jQuery being used
		jquery: version,

		constructor: jQuery,

		// Start with an empty selector
		selector: "",

		// The default length of a jQuery object is 0
		length: 0,

		toArray: function() {
			return slice.call( this );
		},

		// Get the Nth element in the matched element set OR
		// Get the whole matched element set as a clean array
		get: function( num ) {
			return num != null ?

				// Return just the one element from the set
				( num < 0 ? this[ num + this.length ] : this[ num ] ) :

				// Return all the elements in a clean array
				slice.call( this );
		},

		// Take an array of elements and push it onto the stack
		// (returning the new matched element set)
		pushStack: function( elems ) {

			// Build a new jQuery matched element set
			var ret = jQuery.merge( this.constructor(), elems );

			// Add the old object onto the stack (as a reference)
			ret.prevObject = this;
			ret.context = this.context;

			// Return the newly-formed element set
			return ret;
		},

		// Execute a callback for every element in the matched set.
		// (You can seed the arguments with an array of args, but this is
		// only used internally.)
		each: function( callback, args ) {
			return jQuery.each( this, callback, args );
		},

		map: function( callback ) {
			return this.pushStack( jQuery.map(this, function( elem, i ) {
				return callback.call( elem, i, elem );
			}));
		},

		slice: function() {
			return this.pushStack( slice.apply( this, arguments ) );
		},

		first: function() {
			return this.eq( 0 );
		},

		last: function() {
			return this.eq( -1 );
		},

		eq: function( i ) {
			var len = this.length,
				j = +i + ( i < 0 ? len : 0 );
			return this.pushStack( j >= 0 && j < len ? [ this[j] ] : [] );
		},

		end: function() {
			return this.prevObject || this.constructor(null);
		},

		// For internal use only.
		// Behaves like an Array's method, not like a jQuery method.
		push: push,
		sort: arr.sort,
		splice: arr.splice
	};

	jQuery.extend = jQuery.fn.extend = function() {
		var options, name, src, copy, copyIsArray, clone,
			target = arguments[0] || {},
			i = 1,
			length = arguments.length,
			deep = false;

		// Handle a deep copy situation
		if ( typeof target === "boolean" ) {
			deep = target;

			// Skip the boolean and the target
			target = arguments[ i ] || {};
			i++;
		}

		// Handle case when target is a string or something (possible in deep copy)
		if ( typeof target !== "object" && !jQuery.isFunction(target) ) {
			target = {};
		}

		// Extend jQuery itself if only one argument is passed
		if ( i === length ) {
			target = this;
			i--;
		}

		for ( ; i < length; i++ ) {
			// Only deal with non-null/undefined values
			if ( (options = arguments[ i ]) != null ) {
				// Extend the base object
				for ( name in options ) {
					src = target[ name ];
					copy = options[ name ];

					// Prevent never-ending loop
					if ( target === copy ) {
						continue;
					}

					// Recurse if we're merging plain objects or arrays
					if ( deep && copy && ( jQuery.isPlainObject(copy) || (copyIsArray = jQuery.isArray(copy)) ) ) {
						if ( copyIsArray ) {
							copyIsArray = false;
							clone = src && jQuery.isArray(src) ? src : [];

						} else {
							clone = src && jQuery.isPlainObject(src) ? src : {};
						}

						// Never move original objects, clone them
						target[ name ] = jQuery.extend( deep, clone, copy );

					// Don't bring in undefined values
					} else if ( copy !== undefined ) {
						target[ name ] = copy;
					}
				}
			}
		}

		// Return the modified object
		return target;
	};

	jQuery.extend({
		// Unique for each copy of jQuery on the page
		expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),

		// Assume jQuery is ready without the ready module
		isReady: true,

		error: function( msg ) {
			throw new Error( msg );
		},

		noop: function() {},

		isFunction: function( obj ) {
			return jQuery.type(obj) === "function";
		},

		isArray: Array.isArray,

		isWindow: function( obj ) {
			return obj != null && obj === obj.window;
		},

		isNumeric: function( obj ) {
			// parseFloat NaNs numeric-cast false positives (null|true|false|"")
			// ...but misinterprets leading-number strings, particularly hex literals ("0x...")
			// subtraction forces infinities to NaN
			// adding 1 corrects loss of precision from parseFloat (#15100)
			return !jQuery.isArray( obj ) && (obj - parseFloat( obj ) + 1) >= 0;
		},

		isPlainObject: function( obj ) {
			// Not plain objects:
			// - Any object or value whose internal [[Class]] property is not "[object Object]"
			// - DOM nodes
			// - window
			if ( jQuery.type( obj ) !== "object" || obj.nodeType || jQuery.isWindow( obj ) ) {
				return false;
			}

			if ( obj.constructor &&
					!hasOwn.call( obj.constructor.prototype, "isPrototypeOf" ) ) {
				return false;
			}

			// If the function hasn't returned already, we're confident that
			// |obj| is a plain object, created by {} or constructed with new Object
			return true;
		},

		isEmptyObject: function( obj ) {
			var name;
			for ( name in obj ) {
				return false;
			}
			return true;
		},

		type: function( obj ) {
			if ( obj == null ) {
				return obj + "";
			}
			// Support: Android<4.0, iOS<6 (functionish RegExp)
			return typeof obj === "object" || typeof obj === "function" ?
				class2type[ toString.call(obj) ] || "object" :
				typeof obj;
		},

		// Evaluates a script in a global context
		globalEval: function( code ) {
			var script,
				indirect = eval;

			code = jQuery.trim( code );

			if ( code ) {
				// If the code includes a valid, prologue position
				// strict mode pragma, execute code by injecting a
				// script tag into the document.
				if ( code.indexOf("use strict") === 1 ) {
					script = document.createElement("script");
					script.text = code;
					document.head.appendChild( script ).parentNode.removeChild( script );
				} else {
				// Otherwise, avoid the DOM node creation, insertion
				// and removal by using an indirect global eval
					indirect( code );
				}
			}
		},

		// Convert dashed to camelCase; used by the css and data modules
		// Support: IE9-11+
		// Microsoft forgot to hump their vendor prefix (#9572)
		camelCase: function( string ) {
			return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
		},

		nodeName: function( elem, name ) {
			return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
		},

		// args is for internal usage only
		each: function( obj, callback, args ) {
			var value,
				i = 0,
				length = obj.length,
				isArray = isArraylike( obj );

			if ( args ) {
				if ( isArray ) {
					for ( ; i < length; i++ ) {
						value = callback.apply( obj[ i ], args );

						if ( value === false ) {
							break;
						}
					}
				} else {
					for ( i in obj ) {
						value = callback.apply( obj[ i ], args );

						if ( value === false ) {
							break;
						}
					}
				}

			// A special, fast, case for the most common use of each
			} else {
				if ( isArray ) {
					for ( ; i < length; i++ ) {
						value = callback.call( obj[ i ], i, obj[ i ] );

						if ( value === false ) {
							break;
						}
					}
				} else {
					for ( i in obj ) {
						value = callback.call( obj[ i ], i, obj[ i ] );

						if ( value === false ) {
							break;
						}
					}
				}
			}

			return obj;
		},

		// Support: Android<4.1
		trim: function( text ) {
			return text == null ?
				"" :
				( text + "" ).replace( rtrim, "" );
		},

		// results is for internal usage only
		makeArray: function( arr, results ) {
			var ret = results || [];

			if ( arr != null ) {
				if ( isArraylike( Object(arr) ) ) {
					jQuery.merge( ret,
						typeof arr === "string" ?
						[ arr ] : arr
					);
				} else {
					push.call( ret, arr );
				}
			}

			return ret;
		},

		inArray: function( elem, arr, i ) {
			return arr == null ? -1 : indexOf.call( arr, elem, i );
		},

		merge: function( first, second ) {
			var len = +second.length,
				j = 0,
				i = first.length;

			for ( ; j < len; j++ ) {
				first[ i++ ] = second[ j ];
			}

			first.length = i;

			return first;
		},

		grep: function( elems, callback, invert ) {
			var callbackInverse,
				matches = [],
				i = 0,
				length = elems.length,
				callbackExpect = !invert;

			// Go through the array, only saving the items
			// that pass the validator function
			for ( ; i < length; i++ ) {
				callbackInverse = !callback( elems[ i ], i );
				if ( callbackInverse !== callbackExpect ) {
					matches.push( elems[ i ] );
				}
			}

			return matches;
		},

		// arg is for internal usage only
		map: function( elems, callback, arg ) {
			var value,
				i = 0,
				length = elems.length,
				isArray = isArraylike( elems ),
				ret = [];

			// Go through the array, translating each of the items to their new values
			if ( isArray ) {
				for ( ; i < length; i++ ) {
					value = callback( elems[ i ], i, arg );

					if ( value != null ) {
						ret.push( value );
					}
				}

			// Go through every key on the object,
			} else {
				for ( i in elems ) {
					value = callback( elems[ i ], i, arg );

					if ( value != null ) {
						ret.push( value );
					}
				}
			}

			// Flatten any nested arrays
			return concat.apply( [], ret );
		},

		// A global GUID counter for objects
		guid: 1,

		// Bind a function to a context, optionally partially applying any
		// arguments.
		proxy: function( fn, context ) {
			var tmp, args, proxy;

			if ( typeof context === "string" ) {
				tmp = fn[ context ];
				context = fn;
				fn = tmp;
			}

			// Quick check to determine if target is callable, in the spec
			// this throws a TypeError, but we will just return undefined.
			if ( !jQuery.isFunction( fn ) ) {
				return undefined;
			}

			// Simulated bind
			args = slice.call( arguments, 2 );
			proxy = function() {
				return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
			};

			// Set the guid of unique handler to the same of original handler, so it can be removed
			proxy.guid = fn.guid = fn.guid || jQuery.guid++;

			return proxy;
		},

		now: Date.now,

		// jQuery.support is not used in Core but other projects attach their
		// properties to it so it needs to exist.
		support: support
	});

	// Populate the class2type map
	jQuery.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(i, name) {
		class2type[ "[object " + name + "]" ] = name.toLowerCase();
	});

	function isArraylike( obj ) {

		// Support: iOS 8.2 (not reproducible in simulator)
		// `in` check used to prevent JIT error (gh-2145)
		// hasOwn isn't used here due to false negatives
		// regarding Nodelist length in IE
		var length = "length" in obj && obj.length,
			type = jQuery.type( obj );

		if ( type === "function" || jQuery.isWindow( obj ) ) {
			return false;
		}

		if ( obj.nodeType === 1 && length ) {
			return true;
		}

		return type === "array" || length === 0 ||
			typeof length === "number" && length > 0 && ( length - 1 ) in obj;
	}
	var Sizzle =
	/*!
	 * Sizzle CSS Selector Engine v2.2.0-pre
	 * http://sizzlejs.com/
	 *
	 * Copyright 2008, 2014 jQuery Foundation, Inc. and other contributors
	 * Released under the MIT license
	 * http://jquery.org/license
	 *
	 * Date: 2014-12-16
	 */
	(function( window ) {

	var i,
		support,
		Expr,
		getText,
		isXML,
		tokenize,
		compile,
		select,
		outermostContext,
		sortInput,
		hasDuplicate,

		// Local document vars
		setDocument,
		document,
		docElem,
		documentIsHTML,
		rbuggyQSA,
		rbuggyMatches,
		matches,
		contains,

		// Instance-specific data
		expando = "sizzle" + 1 * new Date(),
		preferredDoc = window.document,
		dirruns = 0,
		done = 0,
		classCache = createCache(),
		tokenCache = createCache(),
		compilerCache = createCache(),
		sortOrder = function( a, b ) {
			if ( a === b ) {
				hasDuplicate = true;
			}
			return 0;
		},

		// General-purpose constants
		MAX_NEGATIVE = 1 << 31,

		// Instance methods
		hasOwn = ({}).hasOwnProperty,
		arr = [],
		pop = arr.pop,
		push_native = arr.push,
		push = arr.push,
		slice = arr.slice,
		// Use a stripped-down indexOf as it's faster than native
		// http://jsperf.com/thor-indexof-vs-for/5
		indexOf = function( list, elem ) {
			var i = 0,
				len = list.length;
			for ( ; i < len; i++ ) {
				if ( list[i] === elem ) {
					return i;
				}
			}
			return -1;
		},

		booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",

		// Regular expressions

		// Whitespace characters http://www.w3.org/TR/css3-selectors/#whitespace
		whitespace = "[\\x20\\t\\r\\n\\f]",
		// http://www.w3.org/TR/css3-syntax/#characters
		characterEncoding = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",

		// Loosely modeled on CSS identifier characters
		// An unquoted value should be a CSS identifier http://www.w3.org/TR/css3-selectors/#attribute-selectors
		// Proper syntax: http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
		identifier = characterEncoding.replace( "w", "w#" ),

		// Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
		attributes = "\\[" + whitespace + "*(" + characterEncoding + ")(?:" + whitespace +
			// Operator (capture 2)
			"*([*^$|!~]?=)" + whitespace +
			// "Attribute values must be CSS identifiers [capture 5] or strings [capture 3 or capture 4]"
			"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace +
			"*\\]",

		pseudos = ":(" + characterEncoding + ")(?:\\((" +
			// To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
			// 1. quoted (capture 3; capture 4 or capture 5)
			"('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" +
			// 2. simple (capture 6)
			"((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" +
			// 3. anything else (capture 2)
			".*" +
			")\\)|)",

		// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
		rwhitespace = new RegExp( whitespace + "+", "g" ),
		rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g" ),

		rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
		rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*" ),

		rattributeQuotes = new RegExp( "=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g" ),

		rpseudo = new RegExp( pseudos ),
		ridentifier = new RegExp( "^" + identifier + "$" ),

		matchExpr = {
			"ID": new RegExp( "^#(" + characterEncoding + ")" ),
			"CLASS": new RegExp( "^\\.(" + characterEncoding + ")" ),
			"TAG": new RegExp( "^(" + characterEncoding.replace( "w", "w*" ) + ")" ),
			"ATTR": new RegExp( "^" + attributes ),
			"PSEUDO": new RegExp( "^" + pseudos ),
			"CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
				"*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
				"*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
			"bool": new RegExp( "^(?:" + booleans + ")$", "i" ),
			// For use in libraries implementing .is()
			// We use this for POS matching in `select`
			"needsContext": new RegExp( "^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
				whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
		},

		rinputs = /^(?:input|select|textarea|button)$/i,
		rheader = /^h\d$/i,

		rnative = /^[^{]+\{\s*\[native \w/,

		// Easily-parseable/retrievable ID or TAG or CLASS selectors
		rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,

		rsibling = /[+~]/,
		rescape = /'|\\/g,

		// CSS escapes http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
		runescape = new RegExp( "\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig" ),
		funescape = function( _, escaped, escapedWhitespace ) {
			var high = "0x" + escaped - 0x10000;
			// NaN means non-codepoint
			// Support: Firefox<24
			// Workaround erroneous numeric interpretation of +"0x"
			return high !== high || escapedWhitespace ?
				escaped :
				high < 0 ?
					// BMP codepoint
					String.fromCharCode( high + 0x10000 ) :
					// Supplemental Plane codepoint (surrogate pair)
					String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
		},

		// Used for iframes
		// See setDocument()
		// Removing the function wrapper causes a "Permission Denied"
		// error in IE
		unloadHandler = function() {
			setDocument();
		};

	// Optimize for push.apply( _, NodeList )
	try {
		push.apply(
			(arr = slice.call( preferredDoc.childNodes )),
			preferredDoc.childNodes
		);
		// Support: Android<4.0
		// Detect silently failing push.apply
		arr[ preferredDoc.childNodes.length ].nodeType;
	} catch ( e ) {
		push = { apply: arr.length ?

			// Leverage slice if possible
			function( target, els ) {
				push_native.apply( target, slice.call(els) );
			} :

			// Support: IE<9
			// Otherwise append directly
			function( target, els ) {
				var j = target.length,
					i = 0;
				// Can't trust NodeList.length
				while ( (target[j++] = els[i++]) ) {}
				target.length = j - 1;
			}
		};
	}

	function Sizzle( selector, context, results, seed ) {
		var match, elem, m, nodeType,
			// QSA vars
			i, groups, old, nid, newContext, newSelector;

		if ( ( context ? context.ownerDocument || context : preferredDoc ) !== document ) {
			setDocument( context );
		}

		context = context || document;
		results = results || [];
		nodeType = context.nodeType;

		if ( typeof selector !== "string" || !selector ||
			nodeType !== 1 && nodeType !== 9 && nodeType !== 11 ) {

			return results;
		}

		if ( !seed && documentIsHTML ) {

			// Try to shortcut find operations when possible (e.g., not under DocumentFragment)
			if ( nodeType !== 11 && (match = rquickExpr.exec( selector )) ) {
				// Speed-up: Sizzle("#ID")
				if ( (m = match[1]) ) {
					if ( nodeType === 9 ) {
						elem = context.getElementById( m );
						// Check parentNode to catch when Blackberry 4.6 returns
						// nodes that are no longer in the document (jQuery #6963)
						if ( elem && elem.parentNode ) {
							// Handle the case where IE, Opera, and Webkit return items
							// by name instead of ID
							if ( elem.id === m ) {
								results.push( elem );
								return results;
							}
						} else {
							return results;
						}
					} else {
						// Context is not a document
						if ( context.ownerDocument && (elem = context.ownerDocument.getElementById( m )) &&
							contains( context, elem ) && elem.id === m ) {
							results.push( elem );
							return results;
						}
					}

				// Speed-up: Sizzle("TAG")
				} else if ( match[2] ) {
					push.apply( results, context.getElementsByTagName( selector ) );
					return results;

				// Speed-up: Sizzle(".CLASS")
				} else if ( (m = match[3]) && support.getElementsByClassName ) {
					push.apply( results, context.getElementsByClassName( m ) );
					return results;
				}
			}

			// QSA path
			if ( support.qsa && (!rbuggyQSA || !rbuggyQSA.test( selector )) ) {
				nid = old = expando;
				newContext = context;
				newSelector = nodeType !== 1 && selector;

				// qSA works strangely on Element-rooted queries
				// We can work around this by specifying an extra ID on the root
				// and working up from there (Thanks to Andrew Dupont for the technique)
				// IE 8 doesn't work on object elements
				if ( nodeType === 1 && context.nodeName.toLowerCase() !== "object" ) {
					groups = tokenize( selector );

					if ( (old = context.getAttribute("id")) ) {
						nid = old.replace( rescape, "\\$&" );
					} else {
						context.setAttribute( "id", nid );
					}
					nid = "[id='" + nid + "'] ";

					i = groups.length;
					while ( i-- ) {
						groups[i] = nid + toSelector( groups[i] );
					}
					newContext = rsibling.test( selector ) && testContext( context.parentNode ) || context;
					newSelector = groups.join(",");
				}

				if ( newSelector ) {
					try {
						push.apply( results,
							newContext.querySelectorAll( newSelector )
						);
						return results;
					} catch(qsaError) {
					} finally {
						if ( !old ) {
							context.removeAttribute("id");
						}
					}
				}
			}
		}

		// All others
		return select( selector.replace( rtrim, "$1" ), context, results, seed );
	}

	/**
	 * Create key-value caches of limited size
	 * @returns {Function(string, Object)} Returns the Object data after storing it on itself with
	 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
	 *	deleting the oldest entry
	 */
	function createCache() {
		var keys = [];

		function cache( key, value ) {
			// Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
			if ( keys.push( key + " " ) > Expr.cacheLength ) {
				// Only keep the most recent entries
				delete cache[ keys.shift() ];
			}
			return (cache[ key + " " ] = value);
		}
		return cache;
	}

	/**
	 * Mark a function for special use by Sizzle
	 * @param {Function} fn The function to mark
	 */
	function markFunction( fn ) {
		fn[ expando ] = true;
		return fn;
	}

	/**
	 * Support testing using an element
	 * @param {Function} fn Passed the created div and expects a boolean result
	 */
	function assert( fn ) {
		var div = document.createElement("div");

		try {
			return !!fn( div );
		} catch (e) {
			return false;
		} finally {
			// Remove from its parent by default
			if ( div.parentNode ) {
				div.parentNode.removeChild( div );
			}
			// release memory in IE
			div = null;
		}
	}

	/**
	 * Adds the same handler for all of the specified attrs
	 * @param {String} attrs Pipe-separated list of attributes
	 * @param {Function} handler The method that will be applied
	 */
	function addHandle( attrs, handler ) {
		var arr = attrs.split("|"),
			i = attrs.length;

		while ( i-- ) {
			Expr.attrHandle[ arr[i] ] = handler;
		}
	}

	/**
	 * Checks document order of two siblings
	 * @param {Element} a
	 * @param {Element} b
	 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
	 */
	function siblingCheck( a, b ) {
		var cur = b && a,
			diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
				( ~b.sourceIndex || MAX_NEGATIVE ) -
				( ~a.sourceIndex || MAX_NEGATIVE );

		// Use IE sourceIndex if available on both nodes
		if ( diff ) {
			return diff;
		}

		// Check if b follows a
		if ( cur ) {
			while ( (cur = cur.nextSibling) ) {
				if ( cur === b ) {
					return -1;
				}
			}
		}

		return a ? 1 : -1;
	}

	/**
	 * Returns a function to use in pseudos for input types
	 * @param {String} type
	 */
	function createInputPseudo( type ) {
		return function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return name === "input" && elem.type === type;
		};
	}

	/**
	 * Returns a function to use in pseudos for buttons
	 * @param {String} type
	 */
	function createButtonPseudo( type ) {
		return function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return (name === "input" || name === "button") && elem.type === type;
		};
	}

	/**
	 * Returns a function to use in pseudos for positionals
	 * @param {Function} fn
	 */
	function createPositionalPseudo( fn ) {
		return markFunction(function( argument ) {
			argument = +argument;
			return markFunction(function( seed, matches ) {
				var j,
					matchIndexes = fn( [], seed.length, argument ),
					i = matchIndexes.length;

				// Match elements found at the specified indexes
				while ( i-- ) {
					if ( seed[ (j = matchIndexes[i]) ] ) {
						seed[j] = !(matches[j] = seed[j]);
					}
				}
			});
		});
	}

	/**
	 * Checks a node for validity as a Sizzle context
	 * @param {Element|Object=} context
	 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
	 */
	function testContext( context ) {
		return context && typeof context.getElementsByTagName !== "undefined" && context;
	}

	// Expose support vars for convenience
	support = Sizzle.support = {};

	/**
	 * Detects XML nodes
	 * @param {Element|Object} elem An element or a document
	 * @returns {Boolean} True iff elem is a non-HTML XML node
	 */
	isXML = Sizzle.isXML = function( elem ) {
		// documentElement is verified for cases where it doesn't yet exist
		// (such as loading iframes in IE - #4833)
		var documentElement = elem && (elem.ownerDocument || elem).documentElement;
		return documentElement ? documentElement.nodeName !== "HTML" : false;
	};

	/**
	 * Sets document-related variables once based on the current document
	 * @param {Element|Object} [doc] An element or document object to use to set the document
	 * @returns {Object} Returns the current document
	 */
	setDocument = Sizzle.setDocument = function( node ) {
		var hasCompare, parent,
			doc = node ? node.ownerDocument || node : preferredDoc;

		// If no document and documentElement is available, return
		if ( doc === document || doc.nodeType !== 9 || !doc.documentElement ) {
			return document;
		}

		// Set our document
		document = doc;
		docElem = doc.documentElement;
		parent = doc.defaultView;

		// Support: IE>8
		// If iframe document is assigned to "document" variable and if iframe has been reloaded,
		// IE will throw "permission denied" error when accessing "document" variable, see jQuery #13936
		// IE6-8 do not support the defaultView property so parent will be undefined
		if ( parent && parent !== parent.top ) {
			// IE11 does not have attachEvent, so all must suffer
			if ( parent.addEventListener ) {
				parent.addEventListener( "unload", unloadHandler, false );
			} else if ( parent.attachEvent ) {
				parent.attachEvent( "onunload", unloadHandler );
			}
		}

		/* Support tests
		---------------------------------------------------------------------- */
		documentIsHTML = !isXML( doc );

		/* Attributes
		---------------------------------------------------------------------- */

		// Support: IE<8
		// Verify that getAttribute really returns attributes and not properties
		// (excepting IE8 booleans)
		support.attributes = assert(function( div ) {
			div.className = "i";
			return !div.getAttribute("className");
		});

		/* getElement(s)By*
		---------------------------------------------------------------------- */

		// Check if getElementsByTagName("*") returns only elements
		support.getElementsByTagName = assert(function( div ) {
			div.appendChild( doc.createComment("") );
			return !div.getElementsByTagName("*").length;
		});

		// Support: IE<9
		support.getElementsByClassName = rnative.test( doc.getElementsByClassName );

		// Support: IE<10
		// Check if getElementById returns elements by name
		// The broken getElementById methods don't pick up programatically-set names,
		// so use a roundabout getElementsByName test
		support.getById = assert(function( div ) {
			docElem.appendChild( div ).id = expando;
			return !doc.getElementsByName || !doc.getElementsByName( expando ).length;
		});

		// ID find and filter
		if ( support.getById ) {
			Expr.find["ID"] = function( id, context ) {
				if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
					var m = context.getElementById( id );
					// Check parentNode to catch when Blackberry 4.6 returns
					// nodes that are no longer in the document #6963
					return m && m.parentNode ? [ m ] : [];
				}
			};
			Expr.filter["ID"] = function( id ) {
				var attrId = id.replace( runescape, funescape );
				return function( elem ) {
					return elem.getAttribute("id") === attrId;
				};
			};
		} else {
			// Support: IE6/7
			// getElementById is not reliable as a find shortcut
			delete Expr.find["ID"];

			Expr.filter["ID"] =  function( id ) {
				var attrId = id.replace( runescape, funescape );
				return function( elem ) {
					var node = typeof elem.getAttributeNode !== "undefined" && elem.getAttributeNode("id");
					return node && node.value === attrId;
				};
			};
		}

		// Tag
		Expr.find["TAG"] = support.getElementsByTagName ?
			function( tag, context ) {
				if ( typeof context.getElementsByTagName !== "undefined" ) {
					return context.getElementsByTagName( tag );

				// DocumentFragment nodes don't have gEBTN
				} else if ( support.qsa ) {
					return context.querySelectorAll( tag );
				}
			} :

			function( tag, context ) {
				var elem,
					tmp = [],
					i = 0,
					// By happy coincidence, a (broken) gEBTN appears on DocumentFragment nodes too
					results = context.getElementsByTagName( tag );

				// Filter out possible comments
				if ( tag === "*" ) {
					while ( (elem = results[i++]) ) {
						if ( elem.nodeType === 1 ) {
							tmp.push( elem );
						}
					}

					return tmp;
				}
				return results;
			};

		// Class
		Expr.find["CLASS"] = support.getElementsByClassName && function( className, context ) {
			if ( documentIsHTML ) {
				return context.getElementsByClassName( className );
			}
		};

		/* QSA/matchesSelector
		---------------------------------------------------------------------- */

		// QSA and matchesSelector support

		// matchesSelector(:active) reports false when true (IE9/Opera 11.5)
		rbuggyMatches = [];

		// qSa(:focus) reports false when true (Chrome 21)
		// We allow this because of a bug in IE8/9 that throws an error
		// whenever `document.activeElement` is accessed on an iframe
		// So, we allow :focus to pass through QSA all the time to avoid the IE error
		// See http://bugs.jquery.com/ticket/13378
		rbuggyQSA = [];

		if ( (support.qsa = rnative.test( doc.querySelectorAll )) ) {
			// Build QSA regex
			// Regex strategy adopted from Diego Perini
			assert(function( div ) {
				// Select is set to empty string on purpose
				// This is to test IE's treatment of not explicitly
				// setting a boolean content attribute,
				// since its presence should be enough
				// http://bugs.jquery.com/ticket/12359
				docElem.appendChild( div ).innerHTML = "<a id='" + expando + "'></a>" +
					"<select id='" + expando + "-\f]' msallowcapture=''>" +
					"<option selected=''></option></select>";

				// Support: IE8, Opera 11-12.16
				// Nothing should be selected when empty strings follow ^= or $= or *=
				// The test attribute must be unknown in Opera but "safe" for WinRT
				// http://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section
				if ( div.querySelectorAll("[msallowcapture^='']").length ) {
					rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
				}

				// Support: IE8
				// Boolean attributes and "value" are not treated correctly
				if ( !div.querySelectorAll("[selected]").length ) {
					rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
				}

				// Support: Chrome<29, Android<4.2+, Safari<7.0+, iOS<7.0+, PhantomJS<1.9.7+
				if ( !div.querySelectorAll( "[id~=" + expando + "-]" ).length ) {
					rbuggyQSA.push("~=");
				}

				// Webkit/Opera - :checked should return selected option elements
				// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
				// IE8 throws error here and will not see later tests
				if ( !div.querySelectorAll(":checked").length ) {
					rbuggyQSA.push(":checked");
				}

				// Support: Safari 8+, iOS 8+
				// https://bugs.webkit.org/show_bug.cgi?id=136851
				// In-page `selector#id sibing-combinator selector` fails
				if ( !div.querySelectorAll( "a#" + expando + "+*" ).length ) {
					rbuggyQSA.push(".#.+[+~]");
				}
			});

			assert(function( div ) {
				// Support: Windows 8 Native Apps
				// The type and name attributes are restricted during .innerHTML assignment
				var input = doc.createElement("input");
				input.setAttribute( "type", "hidden" );
				div.appendChild( input ).setAttribute( "name", "D" );

				// Support: IE8
				// Enforce case-sensitivity of name attribute
				if ( div.querySelectorAll("[name=d]").length ) {
					rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
				}

				// FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
				// IE8 throws error here and will not see later tests
				if ( !div.querySelectorAll(":enabled").length ) {
					rbuggyQSA.push( ":enabled", ":disabled" );
				}

				// Opera 10-11 does not throw on post-comma invalid pseudos
				div.querySelectorAll("*,:x");
				rbuggyQSA.push(",.*:");
			});
		}

		if ( (support.matchesSelector = rnative.test( (matches = docElem.matches ||
			docElem.webkitMatchesSelector ||
			docElem.mozMatchesSelector ||
			docElem.oMatchesSelector ||
			docElem.msMatchesSelector) )) ) {

			assert(function( div ) {
				// Check to see if it's possible to do matchesSelector
				// on a disconnected node (IE 9)
				support.disconnectedMatch = matches.call( div, "div" );

				// This should fail with an exception
				// Gecko does not error, returns false instead
				matches.call( div, "[s!='']:x" );
				rbuggyMatches.push( "!=", pseudos );
			});
		}

		rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join("|") );
		rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join("|") );

		/* Contains
		---------------------------------------------------------------------- */
		hasCompare = rnative.test( docElem.compareDocumentPosition );

		// Element contains another
		// Purposefully does not implement inclusive descendent
		// As in, an element does not contain itself
		contains = hasCompare || rnative.test( docElem.contains ) ?
			function( a, b ) {
				var adown = a.nodeType === 9 ? a.documentElement : a,
					bup = b && b.parentNode;
				return a === bup || !!( bup && bup.nodeType === 1 && (
					adown.contains ?
						adown.contains( bup ) :
						a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
				));
			} :
			function( a, b ) {
				if ( b ) {
					while ( (b = b.parentNode) ) {
						if ( b === a ) {
							return true;
						}
					}
				}
				return false;
			};

		/* Sorting
		---------------------------------------------------------------------- */

		// Document order sorting
		sortOrder = hasCompare ?
		function( a, b ) {

			// Flag for duplicate removal
			if ( a === b ) {
				hasDuplicate = true;
				return 0;
			}

			// Sort on method existence if only one input has compareDocumentPosition
			var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
			if ( compare ) {
				return compare;
			}

			// Calculate position if both inputs belong to the same document
			compare = ( a.ownerDocument || a ) === ( b.ownerDocument || b ) ?
				a.compareDocumentPosition( b ) :

				// Otherwise we know they are disconnected
				1;

			// Disconnected nodes
			if ( compare & 1 ||
				(!support.sortDetached && b.compareDocumentPosition( a ) === compare) ) {

				// Choose the first element that is related to our preferred document
				if ( a === doc || a.ownerDocument === preferredDoc && contains(preferredDoc, a) ) {
					return -1;
				}
				if ( b === doc || b.ownerDocument === preferredDoc && contains(preferredDoc, b) ) {
					return 1;
				}

				// Maintain original order
				return sortInput ?
					( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
					0;
			}

			return compare & 4 ? -1 : 1;
		} :
		function( a, b ) {
			// Exit early if the nodes are identical
			if ( a === b ) {
				hasDuplicate = true;
				return 0;
			}

			var cur,
				i = 0,
				aup = a.parentNode,
				bup = b.parentNode,
				ap = [ a ],
				bp = [ b ];

			// Parentless nodes are either documents or disconnected
			if ( !aup || !bup ) {
				return a === doc ? -1 :
					b === doc ? 1 :
					aup ? -1 :
					bup ? 1 :
					sortInput ?
					( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
					0;

			// If the nodes are siblings, we can do a quick check
			} else if ( aup === bup ) {
				return siblingCheck( a, b );
			}

			// Otherwise we need full lists of their ancestors for comparison
			cur = a;
			while ( (cur = cur.parentNode) ) {
				ap.unshift( cur );
			}
			cur = b;
			while ( (cur = cur.parentNode) ) {
				bp.unshift( cur );
			}

			// Walk down the tree looking for a discrepancy
			while ( ap[i] === bp[i] ) {
				i++;
			}

			return i ?
				// Do a sibling check if the nodes have a common ancestor
				siblingCheck( ap[i], bp[i] ) :

				// Otherwise nodes in our document sort first
				ap[i] === preferredDoc ? -1 :
				bp[i] === preferredDoc ? 1 :
				0;
		};

		return doc;
	};

	Sizzle.matches = function( expr, elements ) {
		return Sizzle( expr, null, null, elements );
	};

	Sizzle.matchesSelector = function( elem, expr ) {
		// Set document vars if needed
		if ( ( elem.ownerDocument || elem ) !== document ) {
			setDocument( elem );
		}

		// Make sure that attribute selectors are quoted
		expr = expr.replace( rattributeQuotes, "='$1']" );

		if ( support.matchesSelector && documentIsHTML &&
			( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
			( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {

			try {
				var ret = matches.call( elem, expr );

				// IE 9's matchesSelector returns false on disconnected nodes
				if ( ret || support.disconnectedMatch ||
						// As well, disconnected nodes are said to be in a document
						// fragment in IE 9
						elem.document && elem.document.nodeType !== 11 ) {
					return ret;
				}
			} catch (e) {}
		}

		return Sizzle( expr, document, null, [ elem ] ).length > 0;
	};

	Sizzle.contains = function( context, elem ) {
		// Set document vars if needed
		if ( ( context.ownerDocument || context ) !== document ) {
			setDocument( context );
		}
		return contains( context, elem );
	};

	Sizzle.attr = function( elem, name ) {
		// Set document vars if needed
		if ( ( elem.ownerDocument || elem ) !== document ) {
			setDocument( elem );
		}

		var fn = Expr.attrHandle[ name.toLowerCase() ],
			// Don't get fooled by Object.prototype properties (jQuery #13807)
			val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
				fn( elem, name, !documentIsHTML ) :
				undefined;

		return val !== undefined ?
			val :
			support.attributes || !documentIsHTML ?
				elem.getAttribute( name ) :
				(val = elem.getAttributeNode(name)) && val.specified ?
					val.value :
					null;
	};

	Sizzle.error = function( msg ) {
		throw new Error( "Syntax error, unrecognized expression: " + msg );
	};

	/**
	 * Document sorting and removing duplicates
	 * @param {ArrayLike} results
	 */
	Sizzle.uniqueSort = function( results ) {
		var elem,
			duplicates = [],
			j = 0,
			i = 0;

		// Unless we *know* we can detect duplicates, assume their presence
		hasDuplicate = !support.detectDuplicates;
		sortInput = !support.sortStable && results.slice( 0 );
		results.sort( sortOrder );

		if ( hasDuplicate ) {
			while ( (elem = results[i++]) ) {
				if ( elem === results[ i ] ) {
					j = duplicates.push( i );
				}
			}
			while ( j-- ) {
				results.splice( duplicates[ j ], 1 );
			}
		}

		// Clear input after sorting to release objects
		// See https://github.com/jquery/sizzle/pull/225
		sortInput = null;

		return results;
	};

	/**
	 * Utility function for retrieving the text value of an array of DOM nodes
	 * @param {Array|Element} elem
	 */
	getText = Sizzle.getText = function( elem ) {
		var node,
			ret = "",
			i = 0,
			nodeType = elem.nodeType;

		if ( !nodeType ) {
			// If no nodeType, this is expected to be an array
			while ( (node = elem[i++]) ) {
				// Do not traverse comment nodes
				ret += getText( node );
			}
		} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {
			// Use textContent for elements
			// innerText usage removed for consistency of new lines (jQuery #11153)
			if ( typeof elem.textContent === "string" ) {
				return elem.textContent;
			} else {
				// Traverse its children
				for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
					ret += getText( elem );
				}
			}
		} else if ( nodeType === 3 || nodeType === 4 ) {
			return elem.nodeValue;
		}
		// Do not include comment or processing instruction nodes

		return ret;
	};

	Expr = Sizzle.selectors = {

		// Can be adjusted by the user
		cacheLength: 50,

		createPseudo: markFunction,

		match: matchExpr,

		attrHandle: {},

		find: {},

		relative: {
			">": { dir: "parentNode", first: true },
			" ": { dir: "parentNode" },
			"+": { dir: "previousSibling", first: true },
			"~": { dir: "previousSibling" }
		},

		preFilter: {
			"ATTR": function( match ) {
				match[1] = match[1].replace( runescape, funescape );

				// Move the given value to match[3] whether quoted or unquoted
				match[3] = ( match[3] || match[4] || match[5] || "" ).replace( runescape, funescape );

				if ( match[2] === "~=" ) {
					match[3] = " " + match[3] + " ";
				}

				return match.slice( 0, 4 );
			},

			"CHILD": function( match ) {
				/* matches from matchExpr["CHILD"]
					1 type (only|nth|...)
					2 what (child|of-type)
					3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
					4 xn-component of xn+y argument ([+-]?\d*n|)
					5 sign of xn-component
					6 x of xn-component
					7 sign of y-component
					8 y of y-component
				*/
				match[1] = match[1].toLowerCase();

				if ( match[1].slice( 0, 3 ) === "nth" ) {
					// nth-* requires argument
					if ( !match[3] ) {
						Sizzle.error( match[0] );
					}

					// numeric x and y parameters for Expr.filter.CHILD
					// remember that false/true cast respectively to 0/1
					match[4] = +( match[4] ? match[5] + (match[6] || 1) : 2 * ( match[3] === "even" || match[3] === "odd" ) );
					match[5] = +( ( match[7] + match[8] ) || match[3] === "odd" );

				// other types prohibit arguments
				} else if ( match[3] ) {
					Sizzle.error( match[0] );
				}

				return match;
			},

			"PSEUDO": function( match ) {
				var excess,
					unquoted = !match[6] && match[2];

				if ( matchExpr["CHILD"].test( match[0] ) ) {
					return null;
				}

				// Accept quoted arguments as-is
				if ( match[3] ) {
					match[2] = match[4] || match[5] || "";

				// Strip excess characters from unquoted arguments
				} else if ( unquoted && rpseudo.test( unquoted ) &&
					// Get excess from tokenize (recursively)
					(excess = tokenize( unquoted, true )) &&
					// advance to the next closing parenthesis
					(excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length) ) {

					// excess is a negative index
					match[0] = match[0].slice( 0, excess );
					match[2] = unquoted.slice( 0, excess );
				}

				// Return only captures needed by the pseudo filter method (type and argument)
				return match.slice( 0, 3 );
			}
		},

		filter: {

			"TAG": function( nodeNameSelector ) {
				var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
				return nodeNameSelector === "*" ?
					function() { return true; } :
					function( elem ) {
						return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
					};
			},

			"CLASS": function( className ) {
				var pattern = classCache[ className + " " ];

				return pattern ||
					(pattern = new RegExp( "(^|" + whitespace + ")" + className + "(" + whitespace + "|$)" )) &&
					classCache( className, function( elem ) {
						return pattern.test( typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== "undefined" && elem.getAttribute("class") || "" );
					});
			},

			"ATTR": function( name, operator, check ) {
				return function( elem ) {
					var result = Sizzle.attr( elem, name );

					if ( result == null ) {
						return operator === "!=";
					}
					if ( !operator ) {
						return true;
					}

					result += "";

					return operator === "=" ? result === check :
						operator === "!=" ? result !== check :
						operator === "^=" ? check && result.indexOf( check ) === 0 :
						operator === "*=" ? check && result.indexOf( check ) > -1 :
						operator === "$=" ? check && result.slice( -check.length ) === check :
						operator === "~=" ? ( " " + result.replace( rwhitespace, " " ) + " " ).indexOf( check ) > -1 :
						operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
						false;
				};
			},

			"CHILD": function( type, what, argument, first, last ) {
				var simple = type.slice( 0, 3 ) !== "nth",
					forward = type.slice( -4 ) !== "last",
					ofType = what === "of-type";

				return first === 1 && last === 0 ?

					// Shortcut for :nth-*(n)
					function( elem ) {
						return !!elem.parentNode;
					} :

					function( elem, context, xml ) {
						var cache, outerCache, node, diff, nodeIndex, start,
							dir = simple !== forward ? "nextSibling" : "previousSibling",
							parent = elem.parentNode,
							name = ofType && elem.nodeName.toLowerCase(),
							useCache = !xml && !ofType;

						if ( parent ) {

							// :(first|last|only)-(child|of-type)
							if ( simple ) {
								while ( dir ) {
									node = elem;
									while ( (node = node[ dir ]) ) {
										if ( ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1 ) {
											return false;
										}
									}
									// Reverse direction for :only-* (if we haven't yet done so)
									start = dir = type === "only" && !start && "nextSibling";
								}
								return true;
							}

							start = [ forward ? parent.firstChild : parent.lastChild ];

							// non-xml :nth-child(...) stores cache data on `parent`
							if ( forward && useCache ) {
								// Seek `elem` from a previously-cached index
								outerCache = parent[ expando ] || (parent[ expando ] = {});
								cache = outerCache[ type ] || [];
								nodeIndex = cache[0] === dirruns && cache[1];
								diff = cache[0] === dirruns && cache[2];
								node = nodeIndex && parent.childNodes[ nodeIndex ];

								while ( (node = ++nodeIndex && node && node[ dir ] ||

									// Fallback to seeking `elem` from the start
									(diff = nodeIndex = 0) || start.pop()) ) {

									// When found, cache indexes on `parent` and break
									if ( node.nodeType === 1 && ++diff && node === elem ) {
										outerCache[ type ] = [ dirruns, nodeIndex, diff ];
										break;
									}
								}

							// Use previously-cached element index if available
							} else if ( useCache && (cache = (elem[ expando ] || (elem[ expando ] = {}))[ type ]) && cache[0] === dirruns ) {
								diff = cache[1];

							// xml :nth-child(...) or :nth-last-child(...) or :nth(-last)?-of-type(...)
							} else {
								// Use the same loop as above to seek `elem` from the start
								while ( (node = ++nodeIndex && node && node[ dir ] ||
									(diff = nodeIndex = 0) || start.pop()) ) {

									if ( ( ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1 ) && ++diff ) {
										// Cache the index of each encountered element
										if ( useCache ) {
											(node[ expando ] || (node[ expando ] = {}))[ type ] = [ dirruns, diff ];
										}

										if ( node === elem ) {
											break;
										}
									}
								}
							}

							// Incorporate the offset, then check against cycle size
							diff -= last;
							return diff === first || ( diff % first === 0 && diff / first >= 0 );
						}
					};
			},

			"PSEUDO": function( pseudo, argument ) {
				// pseudo-class names are case-insensitive
				// http://www.w3.org/TR/selectors/#pseudo-classes
				// Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
				// Remember that setFilters inherits from pseudos
				var args,
					fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
						Sizzle.error( "unsupported pseudo: " + pseudo );

				// The user may use createPseudo to indicate that
				// arguments are needed to create the filter function
				// just as Sizzle does
				if ( fn[ expando ] ) {
					return fn( argument );
				}

				// But maintain support for old signatures
				if ( fn.length > 1 ) {
					args = [ pseudo, pseudo, "", argument ];
					return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
						markFunction(function( seed, matches ) {
							var idx,
								matched = fn( seed, argument ),
								i = matched.length;
							while ( i-- ) {
								idx = indexOf( seed, matched[i] );
								seed[ idx ] = !( matches[ idx ] = matched[i] );
							}
						}) :
						function( elem ) {
							return fn( elem, 0, args );
						};
				}

				return fn;
			}
		},

		pseudos: {
			// Potentially complex pseudos
			"not": markFunction(function( selector ) {
				// Trim the selector passed to compile
				// to avoid treating leading and trailing
				// spaces as combinators
				var input = [],
					results = [],
					matcher = compile( selector.replace( rtrim, "$1" ) );

				return matcher[ expando ] ?
					markFunction(function( seed, matches, context, xml ) {
						var elem,
							unmatched = matcher( seed, null, xml, [] ),
							i = seed.length;

						// Match elements unmatched by `matcher`
						while ( i-- ) {
							if ( (elem = unmatched[i]) ) {
								seed[i] = !(matches[i] = elem);
							}
						}
					}) :
					function( elem, context, xml ) {
						input[0] = elem;
						matcher( input, null, xml, results );
						// Don't keep the element (issue #299)
						input[0] = null;
						return !results.pop();
					};
			}),

			"has": markFunction(function( selector ) {
				return function( elem ) {
					return Sizzle( selector, elem ).length > 0;
				};
			}),

			"contains": markFunction(function( text ) {
				text = text.replace( runescape, funescape );
				return function( elem ) {
					return ( elem.textContent || elem.innerText || getText( elem ) ).indexOf( text ) > -1;
				};
			}),

			// "Whether an element is represented by a :lang() selector
			// is based solely on the element's language value
			// being equal to the identifier C,
			// or beginning with the identifier C immediately followed by "-".
			// The matching of C against the element's language value is performed case-insensitively.
			// The identifier C does not have to be a valid language name."
			// http://www.w3.org/TR/selectors/#lang-pseudo
			"lang": markFunction( function( lang ) {
				// lang value must be a valid identifier
				if ( !ridentifier.test(lang || "") ) {
					Sizzle.error( "unsupported lang: " + lang );
				}
				lang = lang.replace( runescape, funescape ).toLowerCase();
				return function( elem ) {
					var elemLang;
					do {
						if ( (elemLang = documentIsHTML ?
							elem.lang :
							elem.getAttribute("xml:lang") || elem.getAttribute("lang")) ) {

							elemLang = elemLang.toLowerCase();
							return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
						}
					} while ( (elem = elem.parentNode) && elem.nodeType === 1 );
					return false;
				};
			}),

			// Miscellaneous
			"target": function( elem ) {
				var hash = window.location && window.location.hash;
				return hash && hash.slice( 1 ) === elem.id;
			},

			"root": function( elem ) {
				return elem === docElem;
			},

			"focus": function( elem ) {
				return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
			},

			// Boolean properties
			"enabled": function( elem ) {
				return elem.disabled === false;
			},

			"disabled": function( elem ) {
				return elem.disabled === true;
			},

			"checked": function( elem ) {
				// In CSS3, :checked should return both checked and selected elements
				// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
				var nodeName = elem.nodeName.toLowerCase();
				return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
			},

			"selected": function( elem ) {
				// Accessing this property makes selected-by-default
				// options in Safari work properly
				if ( elem.parentNode ) {
					elem.parentNode.selectedIndex;
				}

				return elem.selected === true;
			},

			// Contents
			"empty": function( elem ) {
				// http://www.w3.org/TR/selectors/#empty-pseudo
				// :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
				//   but not by others (comment: 8; processing instruction: 7; etc.)
				// nodeType < 6 works because attributes (2) do not appear as children
				for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
					if ( elem.nodeType < 6 ) {
						return false;
					}
				}
				return true;
			},

			"parent": function( elem ) {
				return !Expr.pseudos["empty"]( elem );
			},

			// Element/input types
			"header": function( elem ) {
				return rheader.test( elem.nodeName );
			},

			"input": function( elem ) {
				return rinputs.test( elem.nodeName );
			},

			"button": function( elem ) {
				var name = elem.nodeName.toLowerCase();
				return name === "input" && elem.type === "button" || name === "button";
			},

			"text": function( elem ) {
				var attr;
				return elem.nodeName.toLowerCase() === "input" &&
					elem.type === "text" &&

					// Support: IE<8
					// New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
					( (attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text" );
			},

			// Position-in-collection
			"first": createPositionalPseudo(function() {
				return [ 0 ];
			}),

			"last": createPositionalPseudo(function( matchIndexes, length ) {
				return [ length - 1 ];
			}),

			"eq": createPositionalPseudo(function( matchIndexes, length, argument ) {
				return [ argument < 0 ? argument + length : argument ];
			}),

			"even": createPositionalPseudo(function( matchIndexes, length ) {
				var i = 0;
				for ( ; i < length; i += 2 ) {
					matchIndexes.push( i );
				}
				return matchIndexes;
			}),

			"odd": createPositionalPseudo(function( matchIndexes, length ) {
				var i = 1;
				for ( ; i < length; i += 2 ) {
					matchIndexes.push( i );
				}
				return matchIndexes;
			}),

			"lt": createPositionalPseudo(function( matchIndexes, length, argument ) {
				var i = argument < 0 ? argument + length : argument;
				for ( ; --i >= 0; ) {
					matchIndexes.push( i );
				}
				return matchIndexes;
			}),

			"gt": createPositionalPseudo(function( matchIndexes, length, argument ) {
				var i = argument < 0 ? argument + length : argument;
				for ( ; ++i < length; ) {
					matchIndexes.push( i );
				}
				return matchIndexes;
			})
		}
	};

	Expr.pseudos["nth"] = Expr.pseudos["eq"];

	// Add button/input type pseudos
	for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
		Expr.pseudos[ i ] = createInputPseudo( i );
	}
	for ( i in { submit: true, reset: true } ) {
		Expr.pseudos[ i ] = createButtonPseudo( i );
	}

	// Easy API for creating new setFilters
	function setFilters() {}
	setFilters.prototype = Expr.filters = Expr.pseudos;
	Expr.setFilters = new setFilters();

	tokenize = Sizzle.tokenize = function( selector, parseOnly ) {
		var matched, match, tokens, type,
			soFar, groups, preFilters,
			cached = tokenCache[ selector + " " ];

		if ( cached ) {
			return parseOnly ? 0 : cached.slice( 0 );
		}

		soFar = selector;
		groups = [];
		preFilters = Expr.preFilter;

		while ( soFar ) {

			// Comma and first run
			if ( !matched || (match = rcomma.exec( soFar )) ) {
				if ( match ) {
					// Don't consume trailing commas as valid
					soFar = soFar.slice( match[0].length ) || soFar;
				}
				groups.push( (tokens = []) );
			}

			matched = false;

			// Combinators
			if ( (match = rcombinators.exec( soFar )) ) {
				matched = match.shift();
				tokens.push({
					value: matched,
					// Cast descendant combinators to space
					type: match[0].replace( rtrim, " " )
				});
				soFar = soFar.slice( matched.length );
			}

			// Filters
			for ( type in Expr.filter ) {
				if ( (match = matchExpr[ type ].exec( soFar )) && (!preFilters[ type ] ||
					(match = preFilters[ type ]( match ))) ) {
					matched = match.shift();
					tokens.push({
						value: matched,
						type: type,
						matches: match
					});
					soFar = soFar.slice( matched.length );
				}
			}

			if ( !matched ) {
				break;
			}
		}

		// Return the length of the invalid excess
		// if we're just parsing
		// Otherwise, throw an error or return tokens
		return parseOnly ?
			soFar.length :
			soFar ?
				Sizzle.error( selector ) :
				// Cache the tokens
				tokenCache( selector, groups ).slice( 0 );
	};

	function toSelector( tokens ) {
		var i = 0,
			len = tokens.length,
			selector = "";
		for ( ; i < len; i++ ) {
			selector += tokens[i].value;
		}
		return selector;
	}

	function addCombinator( matcher, combinator, base ) {
		var dir = combinator.dir,
			checkNonElements = base && dir === "parentNode",
			doneName = done++;

		return combinator.first ?
			// Check against closest ancestor/preceding element
			function( elem, context, xml ) {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						return matcher( elem, context, xml );
					}
				}
			} :

			// Check against all ancestor/preceding elements
			function( elem, context, xml ) {
				var oldCache, outerCache,
					newCache = [ dirruns, doneName ];

				// We can't set arbitrary data on XML nodes, so they don't benefit from dir caching
				if ( xml ) {
					while ( (elem = elem[ dir ]) ) {
						if ( elem.nodeType === 1 || checkNonElements ) {
							if ( matcher( elem, context, xml ) ) {
								return true;
							}
						}
					}
				} else {
					while ( (elem = elem[ dir ]) ) {
						if ( elem.nodeType === 1 || checkNonElements ) {
							outerCache = elem[ expando ] || (elem[ expando ] = {});
							if ( (oldCache = outerCache[ dir ]) &&
								oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {

								// Assign to newCache so results back-propagate to previous elements
								return (newCache[ 2 ] = oldCache[ 2 ]);
							} else {
								// Reuse newcache so results back-propagate to previous elements
								outerCache[ dir ] = newCache;

								// A match means we're done; a fail means we have to keep checking
								if ( (newCache[ 2 ] = matcher( elem, context, xml )) ) {
									return true;
								}
							}
						}
					}
				}
			};
	}

	function elementMatcher( matchers ) {
		return matchers.length > 1 ?
			function( elem, context, xml ) {
				var i = matchers.length;
				while ( i-- ) {
					if ( !matchers[i]( elem, context, xml ) ) {
						return false;
					}
				}
				return true;
			} :
			matchers[0];
	}

	function multipleContexts( selector, contexts, results ) {
		var i = 0,
			len = contexts.length;
		for ( ; i < len; i++ ) {
			Sizzle( selector, contexts[i], results );
		}
		return results;
	}

	function condense( unmatched, map, filter, context, xml ) {
		var elem,
			newUnmatched = [],
			i = 0,
			len = unmatched.length,
			mapped = map != null;

		for ( ; i < len; i++ ) {
			if ( (elem = unmatched[i]) ) {
				if ( !filter || filter( elem, context, xml ) ) {
					newUnmatched.push( elem );
					if ( mapped ) {
						map.push( i );
					}
				}
			}
		}

		return newUnmatched;
	}

	function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
		if ( postFilter && !postFilter[ expando ] ) {
			postFilter = setMatcher( postFilter );
		}
		if ( postFinder && !postFinder[ expando ] ) {
			postFinder = setMatcher( postFinder, postSelector );
		}
		return markFunction(function( seed, results, context, xml ) {
			var temp, i, elem,
				preMap = [],
				postMap = [],
				preexisting = results.length,

				// Get initial elements from seed or context
				elems = seed || multipleContexts( selector || "*", context.nodeType ? [ context ] : context, [] ),

				// Prefilter to get matcher input, preserving a map for seed-results synchronization
				matcherIn = preFilter && ( seed || !selector ) ?
					condense( elems, preMap, preFilter, context, xml ) :
					elems,

				matcherOut = matcher ?
					// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
					postFinder || ( seed ? preFilter : preexisting || postFilter ) ?

						// ...intermediate processing is necessary
						[] :

						// ...otherwise use results directly
						results :
					matcherIn;

			// Find primary matches
			if ( matcher ) {
				matcher( matcherIn, matcherOut, context, xml );
			}

			// Apply postFilter
			if ( postFilter ) {
				temp = condense( matcherOut, postMap );
				postFilter( temp, [], context, xml );

				// Un-match failing elements by moving them back to matcherIn
				i = temp.length;
				while ( i-- ) {
					if ( (elem = temp[i]) ) {
						matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ] = elem);
					}
				}
			}

			if ( seed ) {
				if ( postFinder || preFilter ) {
					if ( postFinder ) {
						// Get the final matcherOut by condensing this intermediate into postFinder contexts
						temp = [];
						i = matcherOut.length;
						while ( i-- ) {
							if ( (elem = matcherOut[i]) ) {
								// Restore matcherIn since elem is not yet a final match
								temp.push( (matcherIn[i] = elem) );
							}
						}
						postFinder( null, (matcherOut = []), temp, xml );
					}

					// Move matched elements from seed to results to keep them synchronized
					i = matcherOut.length;
					while ( i-- ) {
						if ( (elem = matcherOut[i]) &&
							(temp = postFinder ? indexOf( seed, elem ) : preMap[i]) > -1 ) {

							seed[temp] = !(results[temp] = elem);
						}
					}
				}

			// Add elements to results, through postFinder if defined
			} else {
				matcherOut = condense(
					matcherOut === results ?
						matcherOut.splice( preexisting, matcherOut.length ) :
						matcherOut
				);
				if ( postFinder ) {
					postFinder( null, results, matcherOut, xml );
				} else {
					push.apply( results, matcherOut );
				}
			}
		});
	}

	function matcherFromTokens( tokens ) {
		var checkContext, matcher, j,
			len = tokens.length,
			leadingRelative = Expr.relative[ tokens[0].type ],
			implicitRelative = leadingRelative || Expr.relative[" "],
			i = leadingRelative ? 1 : 0,

			// The foundational matcher ensures that elements are reachable from top-level context(s)
			matchContext = addCombinator( function( elem ) {
				return elem === checkContext;
			}, implicitRelative, true ),
			matchAnyContext = addCombinator( function( elem ) {
				return indexOf( checkContext, elem ) > -1;
			}, implicitRelative, true ),
			matchers = [ function( elem, context, xml ) {
				var ret = ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
					(checkContext = context).nodeType ?
						matchContext( elem, context, xml ) :
						matchAnyContext( elem, context, xml ) );
				// Avoid hanging onto element (issue #299)
				checkContext = null;
				return ret;
			} ];

		for ( ; i < len; i++ ) {
			if ( (matcher = Expr.relative[ tokens[i].type ]) ) {
				matchers = [ addCombinator(elementMatcher( matchers ), matcher) ];
			} else {
				matcher = Expr.filter[ tokens[i].type ].apply( null, tokens[i].matches );

				// Return special upon seeing a positional matcher
				if ( matcher[ expando ] ) {
					// Find the next relative operator (if any) for proper handling
					j = ++i;
					for ( ; j < len; j++ ) {
						if ( Expr.relative[ tokens[j].type ] ) {
							break;
						}
					}
					return setMatcher(
						i > 1 && elementMatcher( matchers ),
						i > 1 && toSelector(
							// If the preceding token was a descendant combinator, insert an implicit any-element `*`
							tokens.slice( 0, i - 1 ).concat({ value: tokens[ i - 2 ].type === " " ? "*" : "" })
						).replace( rtrim, "$1" ),
						matcher,
						i < j && matcherFromTokens( tokens.slice( i, j ) ),
						j < len && matcherFromTokens( (tokens = tokens.slice( j )) ),
						j < len && toSelector( tokens )
					);
				}
				matchers.push( matcher );
			}
		}

		return elementMatcher( matchers );
	}

	function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
		var bySet = setMatchers.length > 0,
			byElement = elementMatchers.length > 0,
			superMatcher = function( seed, context, xml, results, outermost ) {
				var elem, j, matcher,
					matchedCount = 0,
					i = "0",
					unmatched = seed && [],
					setMatched = [],
					contextBackup = outermostContext,
					// We must always have either seed elements or outermost context
					elems = seed || byElement && Expr.find["TAG"]( "*", outermost ),
					// Use integer dirruns iff this is the outermost matcher
					dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),
					len = elems.length;

				if ( outermost ) {
					outermostContext = context !== document && context;
				}

				// Add elements passing elementMatchers directly to results
				// Keep `i` a string if there are no elements so `matchedCount` will be "00" below
				// Support: IE<9, Safari
				// Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
				for ( ; i !== len && (elem = elems[i]) != null; i++ ) {
					if ( byElement && elem ) {
						j = 0;
						while ( (matcher = elementMatchers[j++]) ) {
							if ( matcher( elem, context, xml ) ) {
								results.push( elem );
								break;
							}
						}
						if ( outermost ) {
							dirruns = dirrunsUnique;
						}
					}

					// Track unmatched elements for set filters
					if ( bySet ) {
						// They will have gone through all possible matchers
						if ( (elem = !matcher && elem) ) {
							matchedCount--;
						}

						// Lengthen the array for every element, matched or not
						if ( seed ) {
							unmatched.push( elem );
						}
					}
				}

				// Apply set filters to unmatched elements
				matchedCount += i;
				if ( bySet && i !== matchedCount ) {
					j = 0;
					while ( (matcher = setMatchers[j++]) ) {
						matcher( unmatched, setMatched, context, xml );
					}

					if ( seed ) {
						// Reintegrate element matches to eliminate the need for sorting
						if ( matchedCount > 0 ) {
							while ( i-- ) {
								if ( !(unmatched[i] || setMatched[i]) ) {
									setMatched[i] = pop.call( results );
								}
							}
						}

						// Discard index placeholder values to get only actual matches
						setMatched = condense( setMatched );
					}

					// Add matches to results
					push.apply( results, setMatched );

					// Seedless set matches succeeding multiple successful matchers stipulate sorting
					if ( outermost && !seed && setMatched.length > 0 &&
						( matchedCount + setMatchers.length ) > 1 ) {

						Sizzle.uniqueSort( results );
					}
				}

				// Override manipulation of globals by nested matchers
				if ( outermost ) {
					dirruns = dirrunsUnique;
					outermostContext = contextBackup;
				}

				return unmatched;
			};

		return bySet ?
			markFunction( superMatcher ) :
			superMatcher;
	}

	compile = Sizzle.compile = function( selector, match /* Internal Use Only */ ) {
		var i,
			setMatchers = [],
			elementMatchers = [],
			cached = compilerCache[ selector + " " ];

		if ( !cached ) {
			// Generate a function of recursive functions that can be used to check each element
			if ( !match ) {
				match = tokenize( selector );
			}
			i = match.length;
			while ( i-- ) {
				cached = matcherFromTokens( match[i] );
				if ( cached[ expando ] ) {
					setMatchers.push( cached );
				} else {
					elementMatchers.push( cached );
				}
			}

			// Cache the compiled function
			cached = compilerCache( selector, matcherFromGroupMatchers( elementMatchers, setMatchers ) );

			// Save selector and tokenization
			cached.selector = selector;
		}
		return cached;
	};

	/**
	 * A low-level selection function that works with Sizzle's compiled
	 *  selector functions
	 * @param {String|Function} selector A selector or a pre-compiled
	 *  selector function built with Sizzle.compile
	 * @param {Element} context
	 * @param {Array} [results]
	 * @param {Array} [seed] A set of elements to match against
	 */
	select = Sizzle.select = function( selector, context, results, seed ) {
		var i, tokens, token, type, find,
			compiled = typeof selector === "function" && selector,
			match = !seed && tokenize( (selector = compiled.selector || selector) );

		results = results || [];

		// Try to minimize operations if there is no seed and only one group
		if ( match.length === 1 ) {

			// Take a shortcut and set the context if the root selector is an ID
			tokens = match[0] = match[0].slice( 0 );
			if ( tokens.length > 2 && (token = tokens[0]).type === "ID" &&
					support.getById && context.nodeType === 9 && documentIsHTML &&
					Expr.relative[ tokens[1].type ] ) {

				context = ( Expr.find["ID"]( token.matches[0].replace(runescape, funescape), context ) || [] )[0];
				if ( !context ) {
					return results;

				// Precompiled matchers will still verify ancestry, so step up a level
				} else if ( compiled ) {
					context = context.parentNode;
				}

				selector = selector.slice( tokens.shift().value.length );
			}

			// Fetch a seed set for right-to-left matching
			i = matchExpr["needsContext"].test( selector ) ? 0 : tokens.length;
			while ( i-- ) {
				token = tokens[i];

				// Abort if we hit a combinator
				if ( Expr.relative[ (type = token.type) ] ) {
					break;
				}
				if ( (find = Expr.find[ type ]) ) {
					// Search, expanding context for leading sibling combinators
					if ( (seed = find(
						token.matches[0].replace( runescape, funescape ),
						rsibling.test( tokens[0].type ) && testContext( context.parentNode ) || context
					)) ) {

						// If seed is empty or no tokens remain, we can return early
						tokens.splice( i, 1 );
						selector = seed.length && toSelector( tokens );
						if ( !selector ) {
							push.apply( results, seed );
							return results;
						}

						break;
					}
				}
			}
		}

		// Compile and execute a filtering function if one is not provided
		// Provide `match` to avoid retokenization if we modified the selector above
		( compiled || compile( selector, match ) )(
			seed,
			context,
			!documentIsHTML,
			results,
			rsibling.test( selector ) && testContext( context.parentNode ) || context
		);
		return results;
	};

	// One-time assignments

	// Sort stability
	support.sortStable = expando.split("").sort( sortOrder ).join("") === expando;

	// Support: Chrome 14-35+
	// Always assume duplicates if they aren't passed to the comparison function
	support.detectDuplicates = !!hasDuplicate;

	// Initialize against the default document
	setDocument();

	// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
	// Detached nodes confoundingly follow *each other*
	support.sortDetached = assert(function( div1 ) {
		// Should return 1, but returns 4 (following)
		return div1.compareDocumentPosition( document.createElement("div") ) & 1;
	});

	// Support: IE<8
	// Prevent attribute/property "interpolation"
	// http://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
	if ( !assert(function( div ) {
		div.innerHTML = "<a href='#'></a>";
		return div.firstChild.getAttribute("href") === "#" ;
	}) ) {
		addHandle( "type|href|height|width", function( elem, name, isXML ) {
			if ( !isXML ) {
				return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
			}
		});
	}

	// Support: IE<9
	// Use defaultValue in place of getAttribute("value")
	if ( !support.attributes || !assert(function( div ) {
		div.innerHTML = "<input/>";
		div.firstChild.setAttribute( "value", "" );
		return div.firstChild.getAttribute( "value" ) === "";
	}) ) {
		addHandle( "value", function( elem, name, isXML ) {
			if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
				return elem.defaultValue;
			}
		});
	}

	// Support: IE<9
	// Use getAttributeNode to fetch booleans when getAttribute lies
	if ( !assert(function( div ) {
		return div.getAttribute("disabled") == null;
	}) ) {
		addHandle( booleans, function( elem, name, isXML ) {
			var val;
			if ( !isXML ) {
				return elem[ name ] === true ? name.toLowerCase() :
						(val = elem.getAttributeNode( name )) && val.specified ?
						val.value :
					null;
			}
		});
	}

	return Sizzle;

	})( window );



	jQuery.find = Sizzle;
	jQuery.expr = Sizzle.selectors;
	jQuery.expr[":"] = jQuery.expr.pseudos;
	jQuery.unique = Sizzle.uniqueSort;
	jQuery.text = Sizzle.getText;
	jQuery.isXMLDoc = Sizzle.isXML;
	jQuery.contains = Sizzle.contains;



	var rneedsContext = jQuery.expr.match.needsContext;

	var rsingleTag = (/^<(\w+)\s*\/?>(?:<\/\1>|)$/);



	var risSimple = /^.[^:#\[\.,]*$/;

	// Implement the identical functionality for filter and not
	function winnow( elements, qualifier, not ) {
		if ( jQuery.isFunction( qualifier ) ) {
			return jQuery.grep( elements, function( elem, i ) {
				/* jshint -W018 */
				return !!qualifier.call( elem, i, elem ) !== not;
			});

		}

		if ( qualifier.nodeType ) {
			return jQuery.grep( elements, function( elem ) {
				return ( elem === qualifier ) !== not;
			});

		}

		if ( typeof qualifier === "string" ) {
			if ( risSimple.test( qualifier ) ) {
				return jQuery.filter( qualifier, elements, not );
			}

			qualifier = jQuery.filter( qualifier, elements );
		}

		return jQuery.grep( elements, function( elem ) {
			return ( indexOf.call( qualifier, elem ) >= 0 ) !== not;
		});
	}

	jQuery.filter = function( expr, elems, not ) {
		var elem = elems[ 0 ];

		if ( not ) {
			expr = ":not(" + expr + ")";
		}

		return elems.length === 1 && elem.nodeType === 1 ?
			jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [] :
			jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
				return elem.nodeType === 1;
			}));
	};

	jQuery.fn.extend({
		find: function( selector ) {
			var i,
				len = this.length,
				ret = [],
				self = this;

			if ( typeof selector !== "string" ) {
				return this.pushStack( jQuery( selector ).filter(function() {
					for ( i = 0; i < len; i++ ) {
						if ( jQuery.contains( self[ i ], this ) ) {
							return true;
						}
					}
				}) );
			}

			for ( i = 0; i < len; i++ ) {
				jQuery.find( selector, self[ i ], ret );
			}

			// Needed because $( selector, context ) becomes $( context ).find( selector )
			ret = this.pushStack( len > 1 ? jQuery.unique( ret ) : ret );
			ret.selector = this.selector ? this.selector + " " + selector : selector;
			return ret;
		},
		filter: function( selector ) {
			return this.pushStack( winnow(this, selector || [], false) );
		},
		not: function( selector ) {
			return this.pushStack( winnow(this, selector || [], true) );
		},
		is: function( selector ) {
			return !!winnow(
				this,

				// If this is a positional/relative selector, check membership in the returned set
				// so $("p:first").is("p:last") won't return true for a doc with two "p".
				typeof selector === "string" && rneedsContext.test( selector ) ?
					jQuery( selector ) :
					selector || [],
				false
			).length;
		}
	});


	// Initialize a jQuery object


	// A central reference to the root jQuery(document)
	var rootjQuery,

		// A simple way to check for HTML strings
		// Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
		// Strict HTML recognition (#11290: must start with <)
		rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,

		init = jQuery.fn.init = function( selector, context ) {
			var match, elem;

			// HANDLE: $(""), $(null), $(undefined), $(false)
			if ( !selector ) {
				return this;
			}

			// Handle HTML strings
			if ( typeof selector === "string" ) {
				if ( selector[0] === "<" && selector[ selector.length - 1 ] === ">" && selector.length >= 3 ) {
					// Assume that strings that start and end with <> are HTML and skip the regex check
					match = [ null, selector, null ];

				} else {
					match = rquickExpr.exec( selector );
				}

				// Match html or make sure no context is specified for #id
				if ( match && (match[1] || !context) ) {

					// HANDLE: $(html) -> $(array)
					if ( match[1] ) {
						context = context instanceof jQuery ? context[0] : context;

						// Option to run scripts is true for back-compat
						// Intentionally let the error be thrown if parseHTML is not present
						jQuery.merge( this, jQuery.parseHTML(
							match[1],
							context && context.nodeType ? context.ownerDocument || context : document,
							true
						) );

						// HANDLE: $(html, props)
						if ( rsingleTag.test( match[1] ) && jQuery.isPlainObject( context ) ) {
							for ( match in context ) {
								// Properties of context are called as methods if possible
								if ( jQuery.isFunction( this[ match ] ) ) {
									this[ match ]( context[ match ] );

								// ...and otherwise set as attributes
								} else {
									this.attr( match, context[ match ] );
								}
							}
						}

						return this;

					// HANDLE: $(#id)
					} else {
						elem = document.getElementById( match[2] );

						// Support: Blackberry 4.6
						// gEBID returns nodes no longer in the document (#6963)
						if ( elem && elem.parentNode ) {
							// Inject the element directly into the jQuery object
							this.length = 1;
							this[0] = elem;
						}

						this.context = document;
						this.selector = selector;
						return this;
					}

				// HANDLE: $(expr, $(...))
				} else if ( !context || context.jquery ) {
					return ( context || rootjQuery ).find( selector );

				// HANDLE: $(expr, context)
				// (which is just equivalent to: $(context).find(expr)
				} else {
					return this.constructor( context ).find( selector );
				}

			// HANDLE: $(DOMElement)
			} else if ( selector.nodeType ) {
				this.context = this[0] = selector;
				this.length = 1;
				return this;

			// HANDLE: $(function)
			// Shortcut for document ready
			} else if ( jQuery.isFunction( selector ) ) {
				return typeof rootjQuery.ready !== "undefined" ?
					rootjQuery.ready( selector ) :
					// Execute immediately if ready is not present
					selector( jQuery );
			}

			if ( selector.selector !== undefined ) {
				this.selector = selector.selector;
				this.context = selector.context;
			}

			return jQuery.makeArray( selector, this );
		};

	// Give the init function the jQuery prototype for later instantiation
	init.prototype = jQuery.fn;

	// Initialize central reference
	rootjQuery = jQuery( document );


	var rparentsprev = /^(?:parents|prev(?:Until|All))/,
		// Methods guaranteed to produce a unique set when starting from a unique set
		guaranteedUnique = {
			children: true,
			contents: true,
			next: true,
			prev: true
		};

	jQuery.extend({
		dir: function( elem, dir, until ) {
			var matched = [],
				truncate = until !== undefined;

			while ( (elem = elem[ dir ]) && elem.nodeType !== 9 ) {
				if ( elem.nodeType === 1 ) {
					if ( truncate && jQuery( elem ).is( until ) ) {
						break;
					}
					matched.push( elem );
				}
			}
			return matched;
		},

		sibling: function( n, elem ) {
			var matched = [];

			for ( ; n; n = n.nextSibling ) {
				if ( n.nodeType === 1 && n !== elem ) {
					matched.push( n );
				}
			}

			return matched;
		}
	});

	jQuery.fn.extend({
		has: function( target ) {
			var targets = jQuery( target, this ),
				l = targets.length;

			return this.filter(function() {
				var i = 0;
				for ( ; i < l; i++ ) {
					if ( jQuery.contains( this, targets[i] ) ) {
						return true;
					}
				}
			});
		},

		closest: function( selectors, context ) {
			var cur,
				i = 0,
				l = this.length,
				matched = [],
				pos = rneedsContext.test( selectors ) || typeof selectors !== "string" ?
					jQuery( selectors, context || this.context ) :
					0;

			for ( ; i < l; i++ ) {
				for ( cur = this[i]; cur && cur !== context; cur = cur.parentNode ) {
					// Always skip document fragments
					if ( cur.nodeType < 11 && (pos ?
						pos.index(cur) > -1 :

						// Don't pass non-elements to Sizzle
						cur.nodeType === 1 &&
							jQuery.find.matchesSelector(cur, selectors)) ) {

						matched.push( cur );
						break;
					}
				}
			}

			return this.pushStack( matched.length > 1 ? jQuery.unique( matched ) : matched );
		},

		// Determine the position of an element within the set
		index: function( elem ) {

			// No argument, return index in parent
			if ( !elem ) {
				return ( this[ 0 ] && this[ 0 ].parentNode ) ? this.first().prevAll().length : -1;
			}

			// Index in selector
			if ( typeof elem === "string" ) {
				return indexOf.call( jQuery( elem ), this[ 0 ] );
			}

			// Locate the position of the desired element
			return indexOf.call( this,

				// If it receives a jQuery object, the first element is used
				elem.jquery ? elem[ 0 ] : elem
			);
		},

		add: function( selector, context ) {
			return this.pushStack(
				jQuery.unique(
					jQuery.merge( this.get(), jQuery( selector, context ) )
				)
			);
		},

		addBack: function( selector ) {
			return this.add( selector == null ?
				this.prevObject : this.prevObject.filter(selector)
			);
		}
	});

	function sibling( cur, dir ) {
		while ( (cur = cur[dir]) && cur.nodeType !== 1 ) {}
		return cur;
	}

	jQuery.each({
		parent: function( elem ) {
			var parent = elem.parentNode;
			return parent && parent.nodeType !== 11 ? parent : null;
		},
		parents: function( elem ) {
			return jQuery.dir( elem, "parentNode" );
		},
		parentsUntil: function( elem, i, until ) {
			return jQuery.dir( elem, "parentNode", until );
		},
		next: function( elem ) {
			return sibling( elem, "nextSibling" );
		},
		prev: function( elem ) {
			return sibling( elem, "previousSibling" );
		},
		nextAll: function( elem ) {
			return jQuery.dir( elem, "nextSibling" );
		},
		prevAll: function( elem ) {
			return jQuery.dir( elem, "previousSibling" );
		},
		nextUntil: function( elem, i, until ) {
			return jQuery.dir( elem, "nextSibling", until );
		},
		prevUntil: function( elem, i, until ) {
			return jQuery.dir( elem, "previousSibling", until );
		},
		siblings: function( elem ) {
			return jQuery.sibling( ( elem.parentNode || {} ).firstChild, elem );
		},
		children: function( elem ) {
			return jQuery.sibling( elem.firstChild );
		},
		contents: function( elem ) {
			return elem.contentDocument || jQuery.merge( [], elem.childNodes );
		}
	}, function( name, fn ) {
		jQuery.fn[ name ] = function( until, selector ) {
			var matched = jQuery.map( this, fn, until );

			if ( name.slice( -5 ) !== "Until" ) {
				selector = until;
			}

			if ( selector && typeof selector === "string" ) {
				matched = jQuery.filter( selector, matched );
			}

			if ( this.length > 1 ) {
				// Remove duplicates
				if ( !guaranteedUnique[ name ] ) {
					jQuery.unique( matched );
				}

				// Reverse order for parents* and prev-derivatives
				if ( rparentsprev.test( name ) ) {
					matched.reverse();
				}
			}

			return this.pushStack( matched );
		};
	});
	var rnotwhite = (/\S+/g);



	// String to Object options format cache
	var optionsCache = {};

	// Convert String-formatted options into Object-formatted ones and store in cache
	function createOptions( options ) {
		var object = optionsCache[ options ] = {};
		jQuery.each( options.match( rnotwhite ) || [], function( _, flag ) {
			object[ flag ] = true;
		});
		return object;
	}

	/*
	 * Create a callback list using the following parameters:
	 *
	 *	options: an optional list of space-separated options that will change how
	 *			the callback list behaves or a more traditional option object
	 *
	 * By default a callback list will act like an event callback list and can be
	 * "fired" multiple times.
	 *
	 * Possible options:
	 *
	 *	once:			will ensure the callback list can only be fired once (like a Deferred)
	 *
	 *	memory:			will keep track of previous values and will call any callback added
	 *					after the list has been fired right away with the latest "memorized"
	 *					values (like a Deferred)
	 *
	 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
	 *
	 *	stopOnFalse:	interrupt callings when a callback returns false
	 *
	 */
	jQuery.Callbacks = function( options ) {

		// Convert options from String-formatted to Object-formatted if needed
		// (we check in cache first)
		options = typeof options === "string" ?
			( optionsCache[ options ] || createOptions( options ) ) :
			jQuery.extend( {}, options );

		var // Last fire value (for non-forgettable lists)
			memory,
			// Flag to know if list was already fired
			fired,
			// Flag to know if list is currently firing
			firing,
			// First callback to fire (used internally by add and fireWith)
			firingStart,
			// End of the loop when firing
			firingLength,
			// Index of currently firing callback (modified by remove if needed)
			firingIndex,
			// Actual callback list
			list = [],
			// Stack of fire calls for repeatable lists
			stack = !options.once && [],
			// Fire callbacks
			fire = function( data ) {
				memory = options.memory && data;
				fired = true;
				firingIndex = firingStart || 0;
				firingStart = 0;
				firingLength = list.length;
				firing = true;
				for ( ; list && firingIndex < firingLength; firingIndex++ ) {
					if ( list[ firingIndex ].apply( data[ 0 ], data[ 1 ] ) === false && options.stopOnFalse ) {
						memory = false; // To prevent further calls using add
						break;
					}
				}
				firing = false;
				if ( list ) {
					if ( stack ) {
						if ( stack.length ) {
							fire( stack.shift() );
						}
					} else if ( memory ) {
						list = [];
					} else {
						self.disable();
					}
				}
			},
			// Actual Callbacks object
			self = {
				// Add a callback or a collection of callbacks to the list
				add: function() {
					if ( list ) {
						// First, we save the current length
						var start = list.length;
						(function add( args ) {
							jQuery.each( args, function( _, arg ) {
								var type = jQuery.type( arg );
								if ( type === "function" ) {
									if ( !options.unique || !self.has( arg ) ) {
										list.push( arg );
									}
								} else if ( arg && arg.length && type !== "string" ) {
									// Inspect recursively
									add( arg );
								}
							});
						})( arguments );
						// Do we need to add the callbacks to the
						// current firing batch?
						if ( firing ) {
							firingLength = list.length;
						// With memory, if we're not firing then
						// we should call right away
						} else if ( memory ) {
							firingStart = start;
							fire( memory );
						}
					}
					return this;
				},
				// Remove a callback from the list
				remove: function() {
					if ( list ) {
						jQuery.each( arguments, function( _, arg ) {
							var index;
							while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
								list.splice( index, 1 );
								// Handle firing indexes
								if ( firing ) {
									if ( index <= firingLength ) {
										firingLength--;
									}
									if ( index <= firingIndex ) {
										firingIndex--;
									}
								}
							}
						});
					}
					return this;
				},
				// Check if a given callback is in the list.
				// If no argument is given, return whether or not list has callbacks attached.
				has: function( fn ) {
					return fn ? jQuery.inArray( fn, list ) > -1 : !!( list && list.length );
				},
				// Remove all callbacks from the list
				empty: function() {
					list = [];
					firingLength = 0;
					return this;
				},
				// Have the list do nothing anymore
				disable: function() {
					list = stack = memory = undefined;
					return this;
				},
				// Is it disabled?
				disabled: function() {
					return !list;
				},
				// Lock the list in its current state
				lock: function() {
					stack = undefined;
					if ( !memory ) {
						self.disable();
					}
					return this;
				},
				// Is it locked?
				locked: function() {
					return !stack;
				},
				// Call all callbacks with the given context and arguments
				fireWith: function( context, args ) {
					if ( list && ( !fired || stack ) ) {
						args = args || [];
						args = [ context, args.slice ? args.slice() : args ];
						if ( firing ) {
							stack.push( args );
						} else {
							fire( args );
						}
					}
					return this;
				},
				// Call all the callbacks with the given arguments
				fire: function() {
					self.fireWith( this, arguments );
					return this;
				},
				// To know if the callbacks have already been called at least once
				fired: function() {
					return !!fired;
				}
			};

		return self;
	};


	jQuery.extend({

		Deferred: function( func ) {
			var tuples = [
					// action, add listener, listener list, final state
					[ "resolve", "done", jQuery.Callbacks("once memory"), "resolved" ],
					[ "reject", "fail", jQuery.Callbacks("once memory"), "rejected" ],
					[ "notify", "progress", jQuery.Callbacks("memory") ]
				],
				state = "pending",
				promise = {
					state: function() {
						return state;
					},
					always: function() {
						deferred.done( arguments ).fail( arguments );
						return this;
					},
					then: function( /* fnDone, fnFail, fnProgress */ ) {
						var fns = arguments;
						return jQuery.Deferred(function( newDefer ) {
							jQuery.each( tuples, function( i, tuple ) {
								var fn = jQuery.isFunction( fns[ i ] ) && fns[ i ];
								// deferred[ done | fail | progress ] for forwarding actions to newDefer
								deferred[ tuple[1] ](function() {
									var returned = fn && fn.apply( this, arguments );
									if ( returned && jQuery.isFunction( returned.promise ) ) {
										returned.promise()
											.done( newDefer.resolve )
											.fail( newDefer.reject )
											.progress( newDefer.notify );
									} else {
										newDefer[ tuple[ 0 ] + "With" ]( this === promise ? newDefer.promise() : this, fn ? [ returned ] : arguments );
									}
								});
							});
							fns = null;
						}).promise();
					},
					// Get a promise for this deferred
					// If obj is provided, the promise aspect is added to the object
					promise: function( obj ) {
						return obj != null ? jQuery.extend( obj, promise ) : promise;
					}
				},
				deferred = {};

			// Keep pipe for back-compat
			promise.pipe = promise.then;

			// Add list-specific methods
			jQuery.each( tuples, function( i, tuple ) {
				var list = tuple[ 2 ],
					stateString = tuple[ 3 ];

				// promise[ done | fail | progress ] = list.add
				promise[ tuple[1] ] = list.add;

				// Handle state
				if ( stateString ) {
					list.add(function() {
						// state = [ resolved | rejected ]
						state = stateString;

					// [ reject_list | resolve_list ].disable; progress_list.lock
					}, tuples[ i ^ 1 ][ 2 ].disable, tuples[ 2 ][ 2 ].lock );
				}

				// deferred[ resolve | reject | notify ]
				deferred[ tuple[0] ] = function() {
					deferred[ tuple[0] + "With" ]( this === deferred ? promise : this, arguments );
					return this;
				};
				deferred[ tuple[0] + "With" ] = list.fireWith;
			});

			// Make the deferred a promise
			promise.promise( deferred );

			// Call given func if any
			if ( func ) {
				func.call( deferred, deferred );
			}

			// All done!
			return deferred;
		},

		// Deferred helper
		when: function( subordinate /* , ..., subordinateN */ ) {
			var i = 0,
				resolveValues = slice.call( arguments ),
				length = resolveValues.length,

				// the count of uncompleted subordinates
				remaining = length !== 1 || ( subordinate && jQuery.isFunction( subordinate.promise ) ) ? length : 0,

				// the master Deferred. If resolveValues consist of only a single Deferred, just use that.
				deferred = remaining === 1 ? subordinate : jQuery.Deferred(),

				// Update function for both resolve and progress values
				updateFunc = function( i, contexts, values ) {
					return function( value ) {
						contexts[ i ] = this;
						values[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
						if ( values === progressValues ) {
							deferred.notifyWith( contexts, values );
						} else if ( !( --remaining ) ) {
							deferred.resolveWith( contexts, values );
						}
					};
				},

				progressValues, progressContexts, resolveContexts;

			// Add listeners to Deferred subordinates; treat others as resolved
			if ( length > 1 ) {
				progressValues = new Array( length );
				progressContexts = new Array( length );
				resolveContexts = new Array( length );
				for ( ; i < length; i++ ) {
					if ( resolveValues[ i ] && jQuery.isFunction( resolveValues[ i ].promise ) ) {
						resolveValues[ i ].promise()
							.done( updateFunc( i, resolveContexts, resolveValues ) )
							.fail( deferred.reject )
							.progress( updateFunc( i, progressContexts, progressValues ) );
					} else {
						--remaining;
					}
				}
			}

			// If we're not waiting on anything, resolve the master
			if ( !remaining ) {
				deferred.resolveWith( resolveContexts, resolveValues );
			}

			return deferred.promise();
		}
	});


	// The deferred used on DOM ready
	var readyList;

	jQuery.fn.ready = function( fn ) {
		// Add the callback
		jQuery.ready.promise().done( fn );

		return this;
	};

	jQuery.extend({
		// Is the DOM ready to be used? Set to true once it occurs.
		isReady: false,

		// A counter to track how many items to wait for before
		// the ready event fires. See #6781
		readyWait: 1,

		// Hold (or release) the ready event
		holdReady: function( hold ) {
			if ( hold ) {
				jQuery.readyWait++;
			} else {
				jQuery.ready( true );
			}
		},

		// Handle when the DOM is ready
		ready: function( wait ) {

			// Abort if there are pending holds or we're already ready
			if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
				return;
			}

			// Remember that the DOM is ready
			jQuery.isReady = true;

			// If a normal DOM Ready event fired, decrement, and wait if need be
			if ( wait !== true && --jQuery.readyWait > 0 ) {
				return;
			}

			// If there are functions bound, to execute
			readyList.resolveWith( document, [ jQuery ] );

			// Trigger any bound ready events
			if ( jQuery.fn.triggerHandler ) {
				jQuery( document ).triggerHandler( "ready" );
				jQuery( document ).off( "ready" );
			}
		}
	});

	/**
	 * The ready event handler and self cleanup method
	 */
	function completed() {
		document.removeEventListener( "DOMContentLoaded", completed, false );
		window.removeEventListener( "load", completed, false );
		jQuery.ready();
	}

	jQuery.ready.promise = function( obj ) {
		if ( !readyList ) {

			readyList = jQuery.Deferred();

			// Catch cases where $(document).ready() is called after the browser event has already occurred.
			// We once tried to use readyState "interactive" here, but it caused issues like the one
			// discovered by ChrisS here: http://bugs.jquery.com/ticket/12282#comment:15
			if ( document.readyState === "complete" ) {
				// Handle it asynchronously to allow scripts the opportunity to delay ready
				setTimeout( jQuery.ready );

			} else {

				// Use the handy event callback
				document.addEventListener( "DOMContentLoaded", completed, false );

				// A fallback to window.onload, that will always work
				window.addEventListener( "load", completed, false );
			}
		}
		return readyList.promise( obj );
	};

	// Kick off the DOM ready check even if the user does not
	jQuery.ready.promise();




	// Multifunctional method to get and set values of a collection
	// The value/s can optionally be executed if it's a function
	var access = jQuery.access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
		var i = 0,
			len = elems.length,
			bulk = key == null;

		// Sets many values
		if ( jQuery.type( key ) === "object" ) {
			chainable = true;
			for ( i in key ) {
				jQuery.access( elems, fn, i, key[i], true, emptyGet, raw );
			}

		// Sets one value
		} else if ( value !== undefined ) {
			chainable = true;

			if ( !jQuery.isFunction( value ) ) {
				raw = true;
			}

			if ( bulk ) {
				// Bulk operations run against the entire set
				if ( raw ) {
					fn.call( elems, value );
					fn = null;

				// ...except when executing function values
				} else {
					bulk = fn;
					fn = function( elem, key, value ) {
						return bulk.call( jQuery( elem ), value );
					};
				}
			}

			if ( fn ) {
				for ( ; i < len; i++ ) {
					fn( elems[i], key, raw ? value : value.call( elems[i], i, fn( elems[i], key ) ) );
				}
			}
		}

		return chainable ?
			elems :

			// Gets
			bulk ?
				fn.call( elems ) :
				len ? fn( elems[0], key ) : emptyGet;
	};


	/**
	 * Determines whether an object can have data
	 */
	jQuery.acceptData = function( owner ) {
		// Accepts only:
		//  - Node
		//    - Node.ELEMENT_NODE
		//    - Node.DOCUMENT_NODE
		//  - Object
		//    - Any
		/* jshint -W018 */
		return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );
	};


	function Data() {
		// Support: Android<4,
		// Old WebKit does not have Object.preventExtensions/freeze method,
		// return new empty object instead with no [[set]] accessor
		Object.defineProperty( this.cache = {}, 0, {
			get: function() {
				return {};
			}
		});

		this.expando = jQuery.expando + Data.uid++;
	}

	Data.uid = 1;
	Data.accepts = jQuery.acceptData;

	Data.prototype = {
		key: function( owner ) {
			// We can accept data for non-element nodes in modern browsers,
			// but we should not, see #8335.
			// Always return the key for a frozen object.
			if ( !Data.accepts( owner ) ) {
				return 0;
			}

			var descriptor = {},
				// Check if the owner object already has a cache key
				unlock = owner[ this.expando ];

			// If not, create one
			if ( !unlock ) {
				unlock = Data.uid++;

				// Secure it in a non-enumerable, non-writable property
				try {
					descriptor[ this.expando ] = { value: unlock };
					Object.defineProperties( owner, descriptor );

				// Support: Android<4
				// Fallback to a less secure definition
				} catch ( e ) {
					descriptor[ this.expando ] = unlock;
					jQuery.extend( owner, descriptor );
				}
			}

			// Ensure the cache object
			if ( !this.cache[ unlock ] ) {
				this.cache[ unlock ] = {};
			}

			return unlock;
		},
		set: function( owner, data, value ) {
			var prop,
				// There may be an unlock assigned to this node,
				// if there is no entry for this "owner", create one inline
				// and set the unlock as though an owner entry had always existed
				unlock = this.key( owner ),
				cache = this.cache[ unlock ];

			// Handle: [ owner, key, value ] args
			if ( typeof data === "string" ) {
				cache[ data ] = value;

			// Handle: [ owner, { properties } ] args
			} else {
				// Fresh assignments by object are shallow copied
				if ( jQuery.isEmptyObject( cache ) ) {
					jQuery.extend( this.cache[ unlock ], data );
				// Otherwise, copy the properties one-by-one to the cache object
				} else {
					for ( prop in data ) {
						cache[ prop ] = data[ prop ];
					}
				}
			}
			return cache;
		},
		get: function( owner, key ) {
			// Either a valid cache is found, or will be created.
			// New caches will be created and the unlock returned,
			// allowing direct access to the newly created
			// empty data object. A valid owner object must be provided.
			var cache = this.cache[ this.key( owner ) ];

			return key === undefined ?
				cache : cache[ key ];
		},
		access: function( owner, key, value ) {
			var stored;
			// In cases where either:
			//
			//   1. No key was specified
			//   2. A string key was specified, but no value provided
			//
			// Take the "read" path and allow the get method to determine
			// which value to return, respectively either:
			//
			//   1. The entire cache object
			//   2. The data stored at the key
			//
			if ( key === undefined ||
					((key && typeof key === "string") && value === undefined) ) {

				stored = this.get( owner, key );

				return stored !== undefined ?
					stored : this.get( owner, jQuery.camelCase(key) );
			}

			// [*]When the key is not a string, or both a key and value
			// are specified, set or extend (existing objects) with either:
			//
			//   1. An object of properties
			//   2. A key and value
			//
			this.set( owner, key, value );

			// Since the "set" path can have two possible entry points
			// return the expected data based on which path was taken[*]
			return value !== undefined ? value : key;
		},
		remove: function( owner, key ) {
			var i, name, camel,
				unlock = this.key( owner ),
				cache = this.cache[ unlock ];

			if ( key === undefined ) {
				this.cache[ unlock ] = {};

			} else {
				// Support array or space separated string of keys
				if ( jQuery.isArray( key ) ) {
					// If "name" is an array of keys...
					// When data is initially created, via ("key", "val") signature,
					// keys will be converted to camelCase.
					// Since there is no way to tell _how_ a key was added, remove
					// both plain key and camelCase key. #12786
					// This will only penalize the array argument path.
					name = key.concat( key.map( jQuery.camelCase ) );
				} else {
					camel = jQuery.camelCase( key );
					// Try the string as a key before any manipulation
					if ( key in cache ) {
						name = [ key, camel ];
					} else {
						// If a key with the spaces exists, use it.
						// Otherwise, create an array by matching non-whitespace
						name = camel;
						name = name in cache ?
							[ name ] : ( name.match( rnotwhite ) || [] );
					}
				}

				i = name.length;
				while ( i-- ) {
					delete cache[ name[ i ] ];
				}
			}
		},
		hasData: function( owner ) {
			return !jQuery.isEmptyObject(
				this.cache[ owner[ this.expando ] ] || {}
			);
		},
		discard: function( owner ) {
			if ( owner[ this.expando ] ) {
				delete this.cache[ owner[ this.expando ] ];
			}
		}
	};
	var data_priv = new Data();

	var data_user = new Data();



	//	Implementation Summary
	//
	//	1. Enforce API surface and semantic compatibility with 1.9.x branch
	//	2. Improve the module's maintainability by reducing the storage
	//		paths to a single mechanism.
	//	3. Use the same single mechanism to support "private" and "user" data.
	//	4. _Never_ expose "private" data to user code (TODO: Drop _data, _removeData)
	//	5. Avoid exposing implementation details on user objects (eg. expando properties)
	//	6. Provide a clear path for implementation upgrade to WeakMap in 2014

	var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
		rmultiDash = /([A-Z])/g;

	function dataAttr( elem, key, data ) {
		var name;

		// If nothing was found internally, try to fetch any
		// data from the HTML5 data-* attribute
		if ( data === undefined && elem.nodeType === 1 ) {
			name = "data-" + key.replace( rmultiDash, "-$1" ).toLowerCase();
			data = elem.getAttribute( name );

			if ( typeof data === "string" ) {
				try {
					data = data === "true" ? true :
						data === "false" ? false :
						data === "null" ? null :
						// Only convert to a number if it doesn't change the string
						+data + "" === data ? +data :
						rbrace.test( data ) ? jQuery.parseJSON( data ) :
						data;
				} catch( e ) {}

				// Make sure we set the data so it isn't changed later
				data_user.set( elem, key, data );
			} else {
				data = undefined;
			}
		}
		return data;
	}

	jQuery.extend({
		hasData: function( elem ) {
			return data_user.hasData( elem ) || data_priv.hasData( elem );
		},

		data: function( elem, name, data ) {
			return data_user.access( elem, name, data );
		},

		removeData: function( elem, name ) {
			data_user.remove( elem, name );
		},

		// TODO: Now that all calls to _data and _removeData have been replaced
		// with direct calls to data_priv methods, these can be deprecated.
		_data: function( elem, name, data ) {
			return data_priv.access( elem, name, data );
		},

		_removeData: function( elem, name ) {
			data_priv.remove( elem, name );
		}
	});

	jQuery.fn.extend({
		data: function( key, value ) {
			var i, name, data,
				elem = this[ 0 ],
				attrs = elem && elem.attributes;

			// Gets all values
			if ( key === undefined ) {
				if ( this.length ) {
					data = data_user.get( elem );

					if ( elem.nodeType === 1 && !data_priv.get( elem, "hasDataAttrs" ) ) {
						i = attrs.length;
						while ( i-- ) {

							// Support: IE11+
							// The attrs elements can be null (#14894)
							if ( attrs[ i ] ) {
								name = attrs[ i ].name;
								if ( name.indexOf( "data-" ) === 0 ) {
									name = jQuery.camelCase( name.slice(5) );
									dataAttr( elem, name, data[ name ] );
								}
							}
						}
						data_priv.set( elem, "hasDataAttrs", true );
					}
				}

				return data;
			}

			// Sets multiple values
			if ( typeof key === "object" ) {
				return this.each(function() {
					data_user.set( this, key );
				});
			}

			return access( this, function( value ) {
				var data,
					camelKey = jQuery.camelCase( key );

				// The calling jQuery object (element matches) is not empty
				// (and therefore has an element appears at this[ 0 ]) and the
				// `value` parameter was not undefined. An empty jQuery object
				// will result in `undefined` for elem = this[ 0 ] which will
				// throw an exception if an attempt to read a data cache is made.
				if ( elem && value === undefined ) {
					// Attempt to get data from the cache
					// with the key as-is
					data = data_user.get( elem, key );
					if ( data !== undefined ) {
						return data;
					}

					// Attempt to get data from the cache
					// with the key camelized
					data = data_user.get( elem, camelKey );
					if ( data !== undefined ) {
						return data;
					}

					// Attempt to "discover" the data in
					// HTML5 custom data-* attrs
					data = dataAttr( elem, camelKey, undefined );
					if ( data !== undefined ) {
						return data;
					}

					// We tried really hard, but the data doesn't exist.
					return;
				}

				// Set the data...
				this.each(function() {
					// First, attempt to store a copy or reference of any
					// data that might've been store with a camelCased key.
					var data = data_user.get( this, camelKey );

					// For HTML5 data-* attribute interop, we have to
					// store property names with dashes in a camelCase form.
					// This might not apply to all properties...*
					data_user.set( this, camelKey, value );

					// *... In the case of properties that might _actually_
					// have dashes, we need to also store a copy of that
					// unchanged property.
					if ( key.indexOf("-") !== -1 && data !== undefined ) {
						data_user.set( this, key, value );
					}
				});
			}, null, value, arguments.length > 1, null, true );
		},

		removeData: function( key ) {
			return this.each(function() {
				data_user.remove( this, key );
			});
		}
	});


	jQuery.extend({
		queue: function( elem, type, data ) {
			var queue;

			if ( elem ) {
				type = ( type || "fx" ) + "queue";
				queue = data_priv.get( elem, type );

				// Speed up dequeue by getting out quickly if this is just a lookup
				if ( data ) {
					if ( !queue || jQuery.isArray( data ) ) {
						queue = data_priv.access( elem, type, jQuery.makeArray(data) );
					} else {
						queue.push( data );
					}
				}
				return queue || [];
			}
		},

		dequeue: function( elem, type ) {
			type = type || "fx";

			var queue = jQuery.queue( elem, type ),
				startLength = queue.length,
				fn = queue.shift(),
				hooks = jQuery._queueHooks( elem, type ),
				next = function() {
					jQuery.dequeue( elem, type );
				};

			// If the fx queue is dequeued, always remove the progress sentinel
			if ( fn === "inprogress" ) {
				fn = queue.shift();
				startLength--;
			}

			if ( fn ) {

				// Add a progress sentinel to prevent the fx queue from being
				// automatically dequeued
				if ( type === "fx" ) {
					queue.unshift( "inprogress" );
				}

				// Clear up the last queue stop function
				delete hooks.stop;
				fn.call( elem, next, hooks );
			}

			if ( !startLength && hooks ) {
				hooks.empty.fire();
			}
		},

		// Not public - generate a queueHooks object, or return the current one
		_queueHooks: function( elem, type ) {
			var key = type + "queueHooks";
			return data_priv.get( elem, key ) || data_priv.access( elem, key, {
				empty: jQuery.Callbacks("once memory").add(function() {
					data_priv.remove( elem, [ type + "queue", key ] );
				})
			});
		}
	});

	jQuery.fn.extend({
		queue: function( type, data ) {
			var setter = 2;

			if ( typeof type !== "string" ) {
				data = type;
				type = "fx";
				setter--;
			}

			if ( arguments.length < setter ) {
				return jQuery.queue( this[0], type );
			}

			return data === undefined ?
				this :
				this.each(function() {
					var queue = jQuery.queue( this, type, data );

					// Ensure a hooks for this queue
					jQuery._queueHooks( this, type );

					if ( type === "fx" && queue[0] !== "inprogress" ) {
						jQuery.dequeue( this, type );
					}
				});
		},
		dequeue: function( type ) {
			return this.each(function() {
				jQuery.dequeue( this, type );
			});
		},
		clearQueue: function( type ) {
			return this.queue( type || "fx", [] );
		},
		// Get a promise resolved when queues of a certain type
		// are emptied (fx is the type by default)
		promise: function( type, obj ) {
			var tmp,
				count = 1,
				defer = jQuery.Deferred(),
				elements = this,
				i = this.length,
				resolve = function() {
					if ( !( --count ) ) {
						defer.resolveWith( elements, [ elements ] );
					}
				};

			if ( typeof type !== "string" ) {
				obj = type;
				type = undefined;
			}
			type = type || "fx";

			while ( i-- ) {
				tmp = data_priv.get( elements[ i ], type + "queueHooks" );
				if ( tmp && tmp.empty ) {
					count++;
					tmp.empty.add( resolve );
				}
			}
			resolve();
			return defer.promise( obj );
		}
	});
	var pnum = (/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/).source;

	var cssExpand = [ "Top", "Right", "Bottom", "Left" ];

	var isHidden = function( elem, el ) {
			// isHidden might be called from jQuery#filter function;
			// in that case, element will be second argument
			elem = el || elem;
			return jQuery.css( elem, "display" ) === "none" || !jQuery.contains( elem.ownerDocument, elem );
		};

	var rcheckableType = (/^(?:checkbox|radio)$/i);



	(function() {
		var fragment = document.createDocumentFragment(),
			div = fragment.appendChild( document.createElement( "div" ) ),
			input = document.createElement( "input" );

		// Support: Safari<=5.1
		// Check state lost if the name is set (#11217)
		// Support: Windows Web Apps (WWA)
		// `name` and `type` must use .setAttribute for WWA (#14901)
		input.setAttribute( "type", "radio" );
		input.setAttribute( "checked", "checked" );
		input.setAttribute( "name", "t" );

		div.appendChild( input );

		// Support: Safari<=5.1, Android<4.2
		// Older WebKit doesn't clone checked state correctly in fragments
		support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;

		// Support: IE<=11+
		// Make sure textarea (and checkbox) defaultValue is properly cloned
		div.innerHTML = "<textarea>x</textarea>";
		support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;
	})();
	var strundefined = typeof undefined;



	support.focusinBubbles = "onfocusin" in window;


	var
		rkeyEvent = /^key/,
		rmouseEvent = /^(?:mouse|pointer|contextmenu)|click/,
		rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
		rtypenamespace = /^([^.]*)(?:\.(.+)|)$/;

	function returnTrue() {
		return true;
	}

	function returnFalse() {
		return false;
	}

	function safeActiveElement() {
		try {
			return document.activeElement;
		} catch ( err ) { }
	}

	/*
	 * Helper functions for managing events -- not part of the public interface.
	 * Props to Dean Edwards' addEvent library for many of the ideas.
	 */
	jQuery.event = {

		global: {},

		add: function( elem, types, handler, data, selector ) {

			var handleObjIn, eventHandle, tmp,
				events, t, handleObj,
				special, handlers, type, namespaces, origType,
				elemData = data_priv.get( elem );

			// Don't attach events to noData or text/comment nodes (but allow plain objects)
			if ( !elemData ) {
				return;
			}

			// Caller can pass in an object of custom data in lieu of the handler
			if ( handler.handler ) {
				handleObjIn = handler;
				handler = handleObjIn.handler;
				selector = handleObjIn.selector;
			}

			// Make sure that the handler has a unique ID, used to find/remove it later
			if ( !handler.guid ) {
				handler.guid = jQuery.guid++;
			}

			// Init the element's event structure and main handler, if this is the first
			if ( !(events = elemData.events) ) {
				events = elemData.events = {};
			}
			if ( !(eventHandle = elemData.handle) ) {
				eventHandle = elemData.handle = function( e ) {
					// Discard the second event of a jQuery.event.trigger() and
					// when an event is called after a page has unloaded
					return typeof jQuery !== strundefined && jQuery.event.triggered !== e.type ?
						jQuery.event.dispatch.apply( elem, arguments ) : undefined;
				};
			}

			// Handle multiple events separated by a space
			types = ( types || "" ).match( rnotwhite ) || [ "" ];
			t = types.length;
			while ( t-- ) {
				tmp = rtypenamespace.exec( types[t] ) || [];
				type = origType = tmp[1];
				namespaces = ( tmp[2] || "" ).split( "." ).sort();

				// There *must* be a type, no attaching namespace-only handlers
				if ( !type ) {
					continue;
				}

				// If event changes its type, use the special event handlers for the changed type
				special = jQuery.event.special[ type ] || {};

				// If selector defined, determine special event api type, otherwise given type
				type = ( selector ? special.delegateType : special.bindType ) || type;

				// Update special based on newly reset type
				special = jQuery.event.special[ type ] || {};

				// handleObj is passed to all event handlers
				handleObj = jQuery.extend({
					type: type,
					origType: origType,
					data: data,
					handler: handler,
					guid: handler.guid,
					selector: selector,
					needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
					namespace: namespaces.join(".")
				}, handleObjIn );

				// Init the event handler queue if we're the first
				if ( !(handlers = events[ type ]) ) {
					handlers = events[ type ] = [];
					handlers.delegateCount = 0;

					// Only use addEventListener if the special events handler returns false
					if ( !special.setup || special.setup.call( elem, data, namespaces, eventHandle ) === false ) {
						if ( elem.addEventListener ) {
							elem.addEventListener( type, eventHandle, false );
						}
					}
				}

				if ( special.add ) {
					special.add.call( elem, handleObj );

					if ( !handleObj.handler.guid ) {
						handleObj.handler.guid = handler.guid;
					}
				}

				// Add to the element's handler list, delegates in front
				if ( selector ) {
					handlers.splice( handlers.delegateCount++, 0, handleObj );
				} else {
					handlers.push( handleObj );
				}

				// Keep track of which events have ever been used, for event optimization
				jQuery.event.global[ type ] = true;
			}

		},

		// Detach an event or set of events from an element
		remove: function( elem, types, handler, selector, mappedTypes ) {

			var j, origCount, tmp,
				events, t, handleObj,
				special, handlers, type, namespaces, origType,
				elemData = data_priv.hasData( elem ) && data_priv.get( elem );

			if ( !elemData || !(events = elemData.events) ) {
				return;
			}

			// Once for each type.namespace in types; type may be omitted
			types = ( types || "" ).match( rnotwhite ) || [ "" ];
			t = types.length;
			while ( t-- ) {
				tmp = rtypenamespace.exec( types[t] ) || [];
				type = origType = tmp[1];
				namespaces = ( tmp[2] || "" ).split( "." ).sort();

				// Unbind all events (on this namespace, if provided) for the element
				if ( !type ) {
					for ( type in events ) {
						jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
					}
					continue;
				}

				special = jQuery.event.special[ type ] || {};
				type = ( selector ? special.delegateType : special.bindType ) || type;
				handlers = events[ type ] || [];
				tmp = tmp[2] && new RegExp( "(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)" );

				// Remove matching events
				origCount = j = handlers.length;
				while ( j-- ) {
					handleObj = handlers[ j ];

					if ( ( mappedTypes || origType === handleObj.origType ) &&
						( !handler || handler.guid === handleObj.guid ) &&
						( !tmp || tmp.test( handleObj.namespace ) ) &&
						( !selector || selector === handleObj.selector || selector === "**" && handleObj.selector ) ) {
						handlers.splice( j, 1 );

						if ( handleObj.selector ) {
							handlers.delegateCount--;
						}
						if ( special.remove ) {
							special.remove.call( elem, handleObj );
						}
					}
				}

				// Remove generic event handler if we removed something and no more handlers exist
				// (avoids potential for endless recursion during removal of special event handlers)
				if ( origCount && !handlers.length ) {
					if ( !special.teardown || special.teardown.call( elem, namespaces, elemData.handle ) === false ) {
						jQuery.removeEvent( elem, type, elemData.handle );
					}

					delete events[ type ];
				}
			}

			// Remove the expando if it's no longer used
			if ( jQuery.isEmptyObject( events ) ) {
				delete elemData.handle;
				data_priv.remove( elem, "events" );
			}
		},

		trigger: function( event, data, elem, onlyHandlers ) {

			var i, cur, tmp, bubbleType, ontype, handle, special,
				eventPath = [ elem || document ],
				type = hasOwn.call( event, "type" ) ? event.type : event,
				namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split(".") : [];

			cur = tmp = elem = elem || document;

			// Don't do events on text and comment nodes
			if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
				return;
			}

			// focus/blur morphs to focusin/out; ensure we're not firing them right now
			if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
				return;
			}

			if ( type.indexOf(".") >= 0 ) {
				// Namespaced trigger; create a regexp to match event type in handle()
				namespaces = type.split(".");
				type = namespaces.shift();
				namespaces.sort();
			}
			ontype = type.indexOf(":") < 0 && "on" + type;

			// Caller can pass in a jQuery.Event object, Object, or just an event type string
			event = event[ jQuery.expando ] ?
				event :
				new jQuery.Event( type, typeof event === "object" && event );

			// Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
			event.isTrigger = onlyHandlers ? 2 : 3;
			event.namespace = namespaces.join(".");
			event.namespace_re = event.namespace ?
				new RegExp( "(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)" ) :
				null;

			// Clean up the event in case it is being reused
			event.result = undefined;
			if ( !event.target ) {
				event.target = elem;
			}

			// Clone any incoming data and prepend the event, creating the handler arg list
			data = data == null ?
				[ event ] :
				jQuery.makeArray( data, [ event ] );

			// Allow special events to draw outside the lines
			special = jQuery.event.special[ type ] || {};
			if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
				return;
			}

			// Determine event propagation path in advance, per W3C events spec (#9951)
			// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
			if ( !onlyHandlers && !special.noBubble && !jQuery.isWindow( elem ) ) {

				bubbleType = special.delegateType || type;
				if ( !rfocusMorph.test( bubbleType + type ) ) {
					cur = cur.parentNode;
				}
				for ( ; cur; cur = cur.parentNode ) {
					eventPath.push( cur );
					tmp = cur;
				}

				// Only add window if we got to document (e.g., not plain obj or detached DOM)
				if ( tmp === (elem.ownerDocument || document) ) {
					eventPath.push( tmp.defaultView || tmp.parentWindow || window );
				}
			}

			// Fire handlers on the event path
			i = 0;
			while ( (cur = eventPath[i++]) && !event.isPropagationStopped() ) {

				event.type = i > 1 ?
					bubbleType :
					special.bindType || type;

				// jQuery handler
				handle = ( data_priv.get( cur, "events" ) || {} )[ event.type ] && data_priv.get( cur, "handle" );
				if ( handle ) {
					handle.apply( cur, data );
				}

				// Native handler
				handle = ontype && cur[ ontype ];
				if ( handle && handle.apply && jQuery.acceptData( cur ) ) {
					event.result = handle.apply( cur, data );
					if ( event.result === false ) {
						event.preventDefault();
					}
				}
			}
			event.type = type;

			// If nobody prevented the default action, do it now
			if ( !onlyHandlers && !event.isDefaultPrevented() ) {

				if ( (!special._default || special._default.apply( eventPath.pop(), data ) === false) &&
					jQuery.acceptData( elem ) ) {

					// Call a native DOM method on the target with the same name name as the event.
					// Don't do default actions on window, that's where global variables be (#6170)
					if ( ontype && jQuery.isFunction( elem[ type ] ) && !jQuery.isWindow( elem ) ) {

						// Don't re-trigger an onFOO event when we call its FOO() method
						tmp = elem[ ontype ];

						if ( tmp ) {
							elem[ ontype ] = null;
						}

						// Prevent re-triggering of the same event, since we already bubbled it above
						jQuery.event.triggered = type;
						elem[ type ]();
						jQuery.event.triggered = undefined;

						if ( tmp ) {
							elem[ ontype ] = tmp;
						}
					}
				}
			}

			return event.result;
		},

		dispatch: function( event ) {

			// Make a writable jQuery.Event from the native event object
			event = jQuery.event.fix( event );

			var i, j, ret, matched, handleObj,
				handlerQueue = [],
				args = slice.call( arguments ),
				handlers = ( data_priv.get( this, "events" ) || {} )[ event.type ] || [],
				special = jQuery.event.special[ event.type ] || {};

			// Use the fix-ed jQuery.Event rather than the (read-only) native event
			args[0] = event;
			event.delegateTarget = this;

			// Call the preDispatch hook for the mapped type, and let it bail if desired
			if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
				return;
			}

			// Determine handlers
			handlerQueue = jQuery.event.handlers.call( this, event, handlers );

			// Run delegates first; they may want to stop propagation beneath us
			i = 0;
			while ( (matched = handlerQueue[ i++ ]) && !event.isPropagationStopped() ) {
				event.currentTarget = matched.elem;

				j = 0;
				while ( (handleObj = matched.handlers[ j++ ]) && !event.isImmediatePropagationStopped() ) {

					// Triggered event must either 1) have no namespace, or 2) have namespace(s)
					// a subset or equal to those in the bound event (both can have no namespace).
					if ( !event.namespace_re || event.namespace_re.test( handleObj.namespace ) ) {

						event.handleObj = handleObj;
						event.data = handleObj.data;

						ret = ( (jQuery.event.special[ handleObj.origType ] || {}).handle || handleObj.handler )
								.apply( matched.elem, args );

						if ( ret !== undefined ) {
							if ( (event.result = ret) === false ) {
								event.preventDefault();
								event.stopPropagation();
							}
						}
					}
				}
			}

			// Call the postDispatch hook for the mapped type
			if ( special.postDispatch ) {
				special.postDispatch.call( this, event );
			}

			return event.result;
		},

		handlers: function( event, handlers ) {
			var i, matches, sel, handleObj,
				handlerQueue = [],
				delegateCount = handlers.delegateCount,
				cur = event.target;

			// Find delegate handlers
			// Black-hole SVG <use> instance trees (#13180)
			// Avoid non-left-click bubbling in Firefox (#3861)
			if ( delegateCount && cur.nodeType && (!event.button || event.type !== "click") ) {

				for ( ; cur !== this; cur = cur.parentNode || this ) {

					// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
					if ( cur.disabled !== true || event.type !== "click" ) {
						matches = [];
						for ( i = 0; i < delegateCount; i++ ) {
							handleObj = handlers[ i ];

							// Don't conflict with Object.prototype properties (#13203)
							sel = handleObj.selector + " ";

							if ( matches[ sel ] === undefined ) {
								matches[ sel ] = handleObj.needsContext ?
									jQuery( sel, this ).index( cur ) >= 0 :
									jQuery.find( sel, this, null, [ cur ] ).length;
							}
							if ( matches[ sel ] ) {
								matches.push( handleObj );
							}
						}
						if ( matches.length ) {
							handlerQueue.push({ elem: cur, handlers: matches });
						}
					}
				}
			}

			// Add the remaining (directly-bound) handlers
			if ( delegateCount < handlers.length ) {
				handlerQueue.push({ elem: this, handlers: handlers.slice( delegateCount ) });
			}

			return handlerQueue;
		},

		// Includes some event props shared by KeyEvent and MouseEvent
		props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),

		fixHooks: {},

		keyHooks: {
			props: "char charCode key keyCode".split(" "),
			filter: function( event, original ) {

				// Add which for key events
				if ( event.which == null ) {
					event.which = original.charCode != null ? original.charCode : original.keyCode;
				}

				return event;
			}
		},

		mouseHooks: {
			props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
			filter: function( event, original ) {
				var eventDoc, doc, body,
					button = original.button;

				// Calculate pageX/Y if missing and clientX/Y available
				if ( event.pageX == null && original.clientX != null ) {
					eventDoc = event.target.ownerDocument || document;
					doc = eventDoc.documentElement;
					body = eventDoc.body;

					event.pageX = original.clientX + ( doc && doc.scrollLeft || body && body.scrollLeft || 0 ) - ( doc && doc.clientLeft || body && body.clientLeft || 0 );
					event.pageY = original.clientY + ( doc && doc.scrollTop  || body && body.scrollTop  || 0 ) - ( doc && doc.clientTop  || body && body.clientTop  || 0 );
				}

				// Add which for click: 1 === left; 2 === middle; 3 === right
				// Note: button is not normalized, so don't use it
				if ( !event.which && button !== undefined ) {
					event.which = ( button & 1 ? 1 : ( button & 2 ? 3 : ( button & 4 ? 2 : 0 ) ) );
				}

				return event;
			}
		},

		fix: function( event ) {
			if ( event[ jQuery.expando ] ) {
				return event;
			}

			// Create a writable copy of the event object and normalize some properties
			var i, prop, copy,
				type = event.type,
				originalEvent = event,
				fixHook = this.fixHooks[ type ];

			if ( !fixHook ) {
				this.fixHooks[ type ] = fixHook =
					rmouseEvent.test( type ) ? this.mouseHooks :
					rkeyEvent.test( type ) ? this.keyHooks :
					{};
			}
			copy = fixHook.props ? this.props.concat( fixHook.props ) : this.props;

			event = new jQuery.Event( originalEvent );

			i = copy.length;
			while ( i-- ) {
				prop = copy[ i ];
				event[ prop ] = originalEvent[ prop ];
			}

			// Support: Cordova 2.5 (WebKit) (#13255)
			// All events should have a target; Cordova deviceready doesn't
			if ( !event.target ) {
				event.target = document;
			}

			// Support: Safari 6.0+, Chrome<28
			// Target should not be a text node (#504, #13143)
			if ( event.target.nodeType === 3 ) {
				event.target = event.target.parentNode;
			}

			return fixHook.filter ? fixHook.filter( event, originalEvent ) : event;
		},

		special: {
			load: {
				// Prevent triggered image.load events from bubbling to window.load
				noBubble: true
			},
			focus: {
				// Fire native event if possible so blur/focus sequence is correct
				trigger: function() {
					if ( this !== safeActiveElement() && this.focus ) {
						this.focus();
						return false;
					}
				},
				delegateType: "focusin"
			},
			blur: {
				trigger: function() {
					if ( this === safeActiveElement() && this.blur ) {
						this.blur();
						return false;
					}
				},
				delegateType: "focusout"
			},
			click: {
				// For checkbox, fire native event so checked state will be right
				trigger: function() {
					if ( this.type === "checkbox" && this.click && jQuery.nodeName( this, "input" ) ) {
						this.click();
						return false;
					}
				},

				// For cross-browser consistency, don't fire native .click() on links
				_default: function( event ) {
					return jQuery.nodeName( event.target, "a" );
				}
			},

			beforeunload: {
				postDispatch: function( event ) {

					// Support: Firefox 20+
					// Firefox doesn't alert if the returnValue field is not set.
					if ( event.result !== undefined && event.originalEvent ) {
						event.originalEvent.returnValue = event.result;
					}
				}
			}
		},

		simulate: function( type, elem, event, bubble ) {
			// Piggyback on a donor event to simulate a different one.
			// Fake originalEvent to avoid donor's stopPropagation, but if the
			// simulated event prevents default then we do the same on the donor.
			var e = jQuery.extend(
				new jQuery.Event(),
				event,
				{
					type: type,
					isSimulated: true,
					originalEvent: {}
				}
			);
			if ( bubble ) {
				jQuery.event.trigger( e, null, elem );
			} else {
				jQuery.event.dispatch.call( elem, e );
			}
			if ( e.isDefaultPrevented() ) {
				event.preventDefault();
			}
		}
	};

	jQuery.removeEvent = function( elem, type, handle ) {
		if ( elem.removeEventListener ) {
			elem.removeEventListener( type, handle, false );
		}
	};

	jQuery.Event = function( src, props ) {
		// Allow instantiation without the 'new' keyword
		if ( !(this instanceof jQuery.Event) ) {
			return new jQuery.Event( src, props );
		}

		// Event object
		if ( src && src.type ) {
			this.originalEvent = src;
			this.type = src.type;

			// Events bubbling up the document may have been marked as prevented
			// by a handler lower down the tree; reflect the correct value.
			this.isDefaultPrevented = src.defaultPrevented ||
					src.defaultPrevented === undefined &&
					// Support: Android<4.0
					src.returnValue === false ?
				returnTrue :
				returnFalse;

		// Event type
		} else {
			this.type = src;
		}

		// Put explicitly provided properties onto the event object
		if ( props ) {
			jQuery.extend( this, props );
		}

		// Create a timestamp if incoming event doesn't have one
		this.timeStamp = src && src.timeStamp || jQuery.now();

		// Mark it as fixed
		this[ jQuery.expando ] = true;
	};

	// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
	// http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
	jQuery.Event.prototype = {
		isDefaultPrevented: returnFalse,
		isPropagationStopped: returnFalse,
		isImmediatePropagationStopped: returnFalse,

		preventDefault: function() {
			var e = this.originalEvent;

			this.isDefaultPrevented = returnTrue;

			if ( e && e.preventDefault ) {
				e.preventDefault();
			}
		},
		stopPropagation: function() {
			var e = this.originalEvent;

			this.isPropagationStopped = returnTrue;

			if ( e && e.stopPropagation ) {
				e.stopPropagation();
			}
		},
		stopImmediatePropagation: function() {
			var e = this.originalEvent;

			this.isImmediatePropagationStopped = returnTrue;

			if ( e && e.stopImmediatePropagation ) {
				e.stopImmediatePropagation();
			}

			this.stopPropagation();
		}
	};

	// Create mouseenter/leave events using mouseover/out and event-time checks
	// Support: Chrome 15+
	jQuery.each({
		mouseenter: "mouseover",
		mouseleave: "mouseout",
		pointerenter: "pointerover",
		pointerleave: "pointerout"
	}, function( orig, fix ) {
		jQuery.event.special[ orig ] = {
			delegateType: fix,
			bindType: fix,

			handle: function( event ) {
				var ret,
					target = this,
					related = event.relatedTarget,
					handleObj = event.handleObj;

				// For mousenter/leave call the handler if related is outside the target.
				// NB: No relatedTarget if the mouse left/entered the browser window
				if ( !related || (related !== target && !jQuery.contains( target, related )) ) {
					event.type = handleObj.origType;
					ret = handleObj.handler.apply( this, arguments );
					event.type = fix;
				}
				return ret;
			}
		};
	});

	// Support: Firefox, Chrome, Safari
	// Create "bubbling" focus and blur events
	if ( !support.focusinBubbles ) {
		jQuery.each({ focus: "focusin", blur: "focusout" }, function( orig, fix ) {

			// Attach a single capturing handler on the document while someone wants focusin/focusout
			var handler = function( event ) {
					jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ), true );
				};

			jQuery.event.special[ fix ] = {
				setup: function() {
					var doc = this.ownerDocument || this,
						attaches = data_priv.access( doc, fix );

					if ( !attaches ) {
						doc.addEventListener( orig, handler, true );
					}
					data_priv.access( doc, fix, ( attaches || 0 ) + 1 );
				},
				teardown: function() {
					var doc = this.ownerDocument || this,
						attaches = data_priv.access( doc, fix ) - 1;

					if ( !attaches ) {
						doc.removeEventListener( orig, handler, true );
						data_priv.remove( doc, fix );

					} else {
						data_priv.access( doc, fix, attaches );
					}
				}
			};
		});
	}

	jQuery.fn.extend({

		on: function( types, selector, data, fn, /*INTERNAL*/ one ) {
			var origFn, type;

			// Types can be a map of types/handlers
			if ( typeof types === "object" ) {
				// ( types-Object, selector, data )
				if ( typeof selector !== "string" ) {
					// ( types-Object, data )
					data = data || selector;
					selector = undefined;
				}
				for ( type in types ) {
					this.on( type, selector, data, types[ type ], one );
				}
				return this;
			}

			if ( data == null && fn == null ) {
				// ( types, fn )
				fn = selector;
				data = selector = undefined;
			} else if ( fn == null ) {
				if ( typeof selector === "string" ) {
					// ( types, selector, fn )
					fn = data;
					data = undefined;
				} else {
					// ( types, data, fn )
					fn = data;
					data = selector;
					selector = undefined;
				}
			}
			if ( fn === false ) {
				fn = returnFalse;
			} else if ( !fn ) {
				return this;
			}

			if ( one === 1 ) {
				origFn = fn;
				fn = function( event ) {
					// Can use an empty set, since event contains the info
					jQuery().off( event );
					return origFn.apply( this, arguments );
				};
				// Use same guid so caller can remove using origFn
				fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
			}
			return this.each( function() {
				jQuery.event.add( this, types, fn, data, selector );
			});
		},
		one: function( types, selector, data, fn ) {
			return this.on( types, selector, data, fn, 1 );
		},
		off: function( types, selector, fn ) {
			var handleObj, type;
			if ( types && types.preventDefault && types.handleObj ) {
				// ( event )  dispatched jQuery.Event
				handleObj = types.handleObj;
				jQuery( types.delegateTarget ).off(
					handleObj.namespace ? handleObj.origType + "." + handleObj.namespace : handleObj.origType,
					handleObj.selector,
					handleObj.handler
				);
				return this;
			}
			if ( typeof types === "object" ) {
				// ( types-object [, selector] )
				for ( type in types ) {
					this.off( type, selector, types[ type ] );
				}
				return this;
			}
			if ( selector === false || typeof selector === "function" ) {
				// ( types [, fn] )
				fn = selector;
				selector = undefined;
			}
			if ( fn === false ) {
				fn = returnFalse;
			}
			return this.each(function() {
				jQuery.event.remove( this, types, fn, selector );
			});
		},

		trigger: function( type, data ) {
			return this.each(function() {
				jQuery.event.trigger( type, data, this );
			});
		},
		triggerHandler: function( type, data ) {
			var elem = this[0];
			if ( elem ) {
				return jQuery.event.trigger( type, data, elem, true );
			}
		}
	});


	var
		rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
		rtagName = /<([\w:]+)/,
		rhtml = /<|&#?\w+;/,
		rnoInnerhtml = /<(?:script|style|link)/i,
		// checked="checked" or checked
		rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
		rscriptType = /^$|\/(?:java|ecma)script/i,
		rscriptTypeMasked = /^true\/(.*)/,
		rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,

		// We have to close these tags to support XHTML (#13200)
		wrapMap = {

			// Support: IE9
			option: [ 1, "<select multiple='multiple'>", "</select>" ],

			thead: [ 1, "<table>", "</table>" ],
			col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
			tr: [ 2, "<table><tbody>", "</tbody></table>" ],
			td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],

			_default: [ 0, "", "" ]
		};

	// Support: IE9
	wrapMap.optgroup = wrapMap.option;

	wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
	wrapMap.th = wrapMap.td;

	// Support: 1.x compatibility
	// Manipulating tables requires a tbody
	function manipulationTarget( elem, content ) {
		return jQuery.nodeName( elem, "table" ) &&
			jQuery.nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ?

			elem.getElementsByTagName("tbody")[0] ||
				elem.appendChild( elem.ownerDocument.createElement("tbody") ) :
			elem;
	}

	// Replace/restore the type attribute of script elements for safe DOM manipulation
	function disableScript( elem ) {
		elem.type = (elem.getAttribute("type") !== null) + "/" + elem.type;
		return elem;
	}
	function restoreScript( elem ) {
		var match = rscriptTypeMasked.exec( elem.type );

		if ( match ) {
			elem.type = match[ 1 ];
		} else {
			elem.removeAttribute("type");
		}

		return elem;
	}

	// Mark scripts as having already been evaluated
	function setGlobalEval( elems, refElements ) {
		var i = 0,
			l = elems.length;

		for ( ; i < l; i++ ) {
			data_priv.set(
				elems[ i ], "globalEval", !refElements || data_priv.get( refElements[ i ], "globalEval" )
			);
		}
	}

	function cloneCopyEvent( src, dest ) {
		var i, l, type, pdataOld, pdataCur, udataOld, udataCur, events;

		if ( dest.nodeType !== 1 ) {
			return;
		}

		// 1. Copy private data: events, handlers, etc.
		if ( data_priv.hasData( src ) ) {
			pdataOld = data_priv.access( src );
			pdataCur = data_priv.set( dest, pdataOld );
			events = pdataOld.events;

			if ( events ) {
				delete pdataCur.handle;
				pdataCur.events = {};

				for ( type in events ) {
					for ( i = 0, l = events[ type ].length; i < l; i++ ) {
						jQuery.event.add( dest, type, events[ type ][ i ] );
					}
				}
			}
		}

		// 2. Copy user data
		if ( data_user.hasData( src ) ) {
			udataOld = data_user.access( src );
			udataCur = jQuery.extend( {}, udataOld );

			data_user.set( dest, udataCur );
		}
	}

	function getAll( context, tag ) {
		var ret = context.getElementsByTagName ? context.getElementsByTagName( tag || "*" ) :
				context.querySelectorAll ? context.querySelectorAll( tag || "*" ) :
				[];

		return tag === undefined || tag && jQuery.nodeName( context, tag ) ?
			jQuery.merge( [ context ], ret ) :
			ret;
	}

	// Fix IE bugs, see support tests
	function fixInput( src, dest ) {
		var nodeName = dest.nodeName.toLowerCase();

		// Fails to persist the checked state of a cloned checkbox or radio button.
		if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
			dest.checked = src.checked;

		// Fails to return the selected option to the default selected state when cloning options
		} else if ( nodeName === "input" || nodeName === "textarea" ) {
			dest.defaultValue = src.defaultValue;
		}
	}

	jQuery.extend({
		clone: function( elem, dataAndEvents, deepDataAndEvents ) {
			var i, l, srcElements, destElements,
				clone = elem.cloneNode( true ),
				inPage = jQuery.contains( elem.ownerDocument, elem );

			// Fix IE cloning issues
			if ( !support.noCloneChecked && ( elem.nodeType === 1 || elem.nodeType === 11 ) &&
					!jQuery.isXMLDoc( elem ) ) {

				// We eschew Sizzle here for performance reasons: http://jsperf.com/getall-vs-sizzle/2
				destElements = getAll( clone );
				srcElements = getAll( elem );

				for ( i = 0, l = srcElements.length; i < l; i++ ) {
					fixInput( srcElements[ i ], destElements[ i ] );
				}
			}

			// Copy the events from the original to the clone
			if ( dataAndEvents ) {
				if ( deepDataAndEvents ) {
					srcElements = srcElements || getAll( elem );
					destElements = destElements || getAll( clone );

					for ( i = 0, l = srcElements.length; i < l; i++ ) {
						cloneCopyEvent( srcElements[ i ], destElements[ i ] );
					}
				} else {
					cloneCopyEvent( elem, clone );
				}
			}

			// Preserve script evaluation history
			destElements = getAll( clone, "script" );
			if ( destElements.length > 0 ) {
				setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
			}

			// Return the cloned set
			return clone;
		},

		buildFragment: function( elems, context, scripts, selection ) {
			var elem, tmp, tag, wrap, contains, j,
				fragment = context.createDocumentFragment(),
				nodes = [],
				i = 0,
				l = elems.length;

			for ( ; i < l; i++ ) {
				elem = elems[ i ];

				if ( elem || elem === 0 ) {

					// Add nodes directly
					if ( jQuery.type( elem ) === "object" ) {
						// Support: QtWebKit, PhantomJS
						// push.apply(_, arraylike) throws on ancient WebKit
						jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );

					// Convert non-html into a text node
					} else if ( !rhtml.test( elem ) ) {
						nodes.push( context.createTextNode( elem ) );

					// Convert html into DOM nodes
					} else {
						tmp = tmp || fragment.appendChild( context.createElement("div") );

						// Deserialize a standard representation
						tag = ( rtagName.exec( elem ) || [ "", "" ] )[ 1 ].toLowerCase();
						wrap = wrapMap[ tag ] || wrapMap._default;
						tmp.innerHTML = wrap[ 1 ] + elem.replace( rxhtmlTag, "<$1></$2>" ) + wrap[ 2 ];

						// Descend through wrappers to the right content
						j = wrap[ 0 ];
						while ( j-- ) {
							tmp = tmp.lastChild;
						}

						// Support: QtWebKit, PhantomJS
						// push.apply(_, arraylike) throws on ancient WebKit
						jQuery.merge( nodes, tmp.childNodes );

						// Remember the top-level container
						tmp = fragment.firstChild;

						// Ensure the created nodes are orphaned (#12392)
						tmp.textContent = "";
					}
				}
			}

			// Remove wrapper from fragment
			fragment.textContent = "";

			i = 0;
			while ( (elem = nodes[ i++ ]) ) {

				// #4087 - If origin and destination elements are the same, and this is
				// that element, do not do anything
				if ( selection && jQuery.inArray( elem, selection ) !== -1 ) {
					continue;
				}

				contains = jQuery.contains( elem.ownerDocument, elem );

				// Append to fragment
				tmp = getAll( fragment.appendChild( elem ), "script" );

				// Preserve script evaluation history
				if ( contains ) {
					setGlobalEval( tmp );
				}

				// Capture executables
				if ( scripts ) {
					j = 0;
					while ( (elem = tmp[ j++ ]) ) {
						if ( rscriptType.test( elem.type || "" ) ) {
							scripts.push( elem );
						}
					}
				}
			}

			return fragment;
		},

		cleanData: function( elems ) {
			var data, elem, type, key,
				special = jQuery.event.special,
				i = 0;

			for ( ; (elem = elems[ i ]) !== undefined; i++ ) {
				if ( jQuery.acceptData( elem ) ) {
					key = elem[ data_priv.expando ];

					if ( key && (data = data_priv.cache[ key ]) ) {
						if ( data.events ) {
							for ( type in data.events ) {
								if ( special[ type ] ) {
									jQuery.event.remove( elem, type );

								// This is a shortcut to avoid jQuery.event.remove's overhead
								} else {
									jQuery.removeEvent( elem, type, data.handle );
								}
							}
						}
						if ( data_priv.cache[ key ] ) {
							// Discard any remaining `private` data
							delete data_priv.cache[ key ];
						}
					}
				}
				// Discard any remaining `user` data
				delete data_user.cache[ elem[ data_user.expando ] ];
			}
		}
	});

	jQuery.fn.extend({
		text: function( value ) {
			return access( this, function( value ) {
				return value === undefined ?
					jQuery.text( this ) :
					this.empty().each(function() {
						if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
							this.textContent = value;
						}
					});
			}, null, value, arguments.length );
		},

		append: function() {
			return this.domManip( arguments, function( elem ) {
				if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
					var target = manipulationTarget( this, elem );
					target.appendChild( elem );
				}
			});
		},

		prepend: function() {
			return this.domManip( arguments, function( elem ) {
				if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
					var target = manipulationTarget( this, elem );
					target.insertBefore( elem, target.firstChild );
				}
			});
		},

		before: function() {
			return this.domManip( arguments, function( elem ) {
				if ( this.parentNode ) {
					this.parentNode.insertBefore( elem, this );
				}
			});
		},

		after: function() {
			return this.domManip( arguments, function( elem ) {
				if ( this.parentNode ) {
					this.parentNode.insertBefore( elem, this.nextSibling );
				}
			});
		},

		remove: function( selector, keepData /* Internal Use Only */ ) {
			var elem,
				elems = selector ? jQuery.filter( selector, this ) : this,
				i = 0;

			for ( ; (elem = elems[i]) != null; i++ ) {
				if ( !keepData && elem.nodeType === 1 ) {
					jQuery.cleanData( getAll( elem ) );
				}

				if ( elem.parentNode ) {
					if ( keepData && jQuery.contains( elem.ownerDocument, elem ) ) {
						setGlobalEval( getAll( elem, "script" ) );
					}
					elem.parentNode.removeChild( elem );
				}
			}

			return this;
		},

		empty: function() {
			var elem,
				i = 0;

			for ( ; (elem = this[i]) != null; i++ ) {
				if ( elem.nodeType === 1 ) {

					// Prevent memory leaks
					jQuery.cleanData( getAll( elem, false ) );

					// Remove any remaining nodes
					elem.textContent = "";
				}
			}

			return this;
		},

		clone: function( dataAndEvents, deepDataAndEvents ) {
			dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
			deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

			return this.map(function() {
				return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
			});
		},

		html: function( value ) {
			return access( this, function( value ) {
				var elem = this[ 0 ] || {},
					i = 0,
					l = this.length;

				if ( value === undefined && elem.nodeType === 1 ) {
					return elem.innerHTML;
				}

				// See if we can take a shortcut and just use innerHTML
				if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
					!wrapMap[ ( rtagName.exec( value ) || [ "", "" ] )[ 1 ].toLowerCase() ] ) {

					value = value.replace( rxhtmlTag, "<$1></$2>" );

					try {
						for ( ; i < l; i++ ) {
							elem = this[ i ] || {};

							// Remove element nodes and prevent memory leaks
							if ( elem.nodeType === 1 ) {
								jQuery.cleanData( getAll( elem, false ) );
								elem.innerHTML = value;
							}
						}

						elem = 0;

					// If using innerHTML throws an exception, use the fallback method
					} catch( e ) {}
				}

				if ( elem ) {
					this.empty().append( value );
				}
			}, null, value, arguments.length );
		},

		replaceWith: function() {
			var arg = arguments[ 0 ];

			// Make the changes, replacing each context element with the new content
			this.domManip( arguments, function( elem ) {
				arg = this.parentNode;

				jQuery.cleanData( getAll( this ) );

				if ( arg ) {
					arg.replaceChild( elem, this );
				}
			});

			// Force removal if there was no new content (e.g., from empty arguments)
			return arg && (arg.length || arg.nodeType) ? this : this.remove();
		},

		detach: function( selector ) {
			return this.remove( selector, true );
		},

		domManip: function( args, callback ) {

			// Flatten any nested arrays
			args = concat.apply( [], args );

			var fragment, first, scripts, hasScripts, node, doc,
				i = 0,
				l = this.length,
				set = this,
				iNoClone = l - 1,
				value = args[ 0 ],
				isFunction = jQuery.isFunction( value );

			// We can't cloneNode fragments that contain checked, in WebKit
			if ( isFunction ||
					( l > 1 && typeof value === "string" &&
						!support.checkClone && rchecked.test( value ) ) ) {
				return this.each(function( index ) {
					var self = set.eq( index );
					if ( isFunction ) {
						args[ 0 ] = value.call( this, index, self.html() );
					}
					self.domManip( args, callback );
				});
			}

			if ( l ) {
				fragment = jQuery.buildFragment( args, this[ 0 ].ownerDocument, false, this );
				first = fragment.firstChild;

				if ( fragment.childNodes.length === 1 ) {
					fragment = first;
				}

				if ( first ) {
					scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
					hasScripts = scripts.length;

					// Use the original fragment for the last item instead of the first because it can end up
					// being emptied incorrectly in certain situations (#8070).
					for ( ; i < l; i++ ) {
						node = fragment;

						if ( i !== iNoClone ) {
							node = jQuery.clone( node, true, true );

							// Keep references to cloned scripts for later restoration
							if ( hasScripts ) {
								// Support: QtWebKit
								// jQuery.merge because push.apply(_, arraylike) throws
								jQuery.merge( scripts, getAll( node, "script" ) );
							}
						}

						callback.call( this[ i ], node, i );
					}

					if ( hasScripts ) {
						doc = scripts[ scripts.length - 1 ].ownerDocument;

						// Reenable scripts
						jQuery.map( scripts, restoreScript );

						// Evaluate executable scripts on first document insertion
						for ( i = 0; i < hasScripts; i++ ) {
							node = scripts[ i ];
							if ( rscriptType.test( node.type || "" ) &&
								!data_priv.access( node, "globalEval" ) && jQuery.contains( doc, node ) ) {

								if ( node.src ) {
									// Optional AJAX dependency, but won't run scripts if not present
									if ( jQuery._evalUrl ) {
										jQuery._evalUrl( node.src );
									}
								} else {
									jQuery.globalEval( node.textContent.replace( rcleanScript, "" ) );
								}
							}
						}
					}
				}
			}

			return this;
		}
	});

	jQuery.each({
		appendTo: "append",
		prependTo: "prepend",
		insertBefore: "before",
		insertAfter: "after",
		replaceAll: "replaceWith"
	}, function( name, original ) {
		jQuery.fn[ name ] = function( selector ) {
			var elems,
				ret = [],
				insert = jQuery( selector ),
				last = insert.length - 1,
				i = 0;

			for ( ; i <= last; i++ ) {
				elems = i === last ? this : this.clone( true );
				jQuery( insert[ i ] )[ original ]( elems );

				// Support: QtWebKit
				// .get() because push.apply(_, arraylike) throws
				push.apply( ret, elems.get() );
			}

			return this.pushStack( ret );
		};
	});


	var iframe,
		elemdisplay = {};

	/**
	 * Retrieve the actual display of a element
	 * @param {String} name nodeName of the element
	 * @param {Object} doc Document object
	 */
	// Called only from within defaultDisplay
	function actualDisplay( name, doc ) {
		var style,
			elem = jQuery( doc.createElement( name ) ).appendTo( doc.body ),

			// getDefaultComputedStyle might be reliably used only on attached element
			display = window.getDefaultComputedStyle && ( style = window.getDefaultComputedStyle( elem[ 0 ] ) ) ?

				// Use of this method is a temporary fix (more like optimization) until something better comes along,
				// since it was removed from specification and supported only in FF
				style.display : jQuery.css( elem[ 0 ], "display" );

		// We don't have any data stored on the element,
		// so use "detach" method as fast way to get rid of the element
		elem.detach();

		return display;
	}

	/**
	 * Try to determine the default display value of an element
	 * @param {String} nodeName
	 */
	function defaultDisplay( nodeName ) {
		var doc = document,
			display = elemdisplay[ nodeName ];

		if ( !display ) {
			display = actualDisplay( nodeName, doc );

			// If the simple way fails, read from inside an iframe
			if ( display === "none" || !display ) {

				// Use the already-created iframe if possible
				iframe = (iframe || jQuery( "<iframe frameborder='0' width='0' height='0'/>" )).appendTo( doc.documentElement );

				// Always write a new HTML skeleton so Webkit and Firefox don't choke on reuse
				doc = iframe[ 0 ].contentDocument;

				// Support: IE
				doc.write();
				doc.close();

				display = actualDisplay( nodeName, doc );
				iframe.detach();
			}

			// Store the correct default display
			elemdisplay[ nodeName ] = display;
		}

		return display;
	}
	var rmargin = (/^margin/);

	var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );

	var getStyles = function( elem ) {
			// Support: IE<=11+, Firefox<=30+ (#15098, #14150)
			// IE throws on elements created in popups
			// FF meanwhile throws on frame elements through "defaultView.getComputedStyle"
			if ( elem.ownerDocument.defaultView.opener ) {
				return elem.ownerDocument.defaultView.getComputedStyle( elem, null );
			}

			return window.getComputedStyle( elem, null );
		};



	function curCSS( elem, name, computed ) {
		var width, minWidth, maxWidth, ret,
			style = elem.style;

		computed = computed || getStyles( elem );

		// Support: IE9
		// getPropertyValue is only needed for .css('filter') (#12537)
		if ( computed ) {
			ret = computed.getPropertyValue( name ) || computed[ name ];
		}

		if ( computed ) {

			if ( ret === "" && !jQuery.contains( elem.ownerDocument, elem ) ) {
				ret = jQuery.style( elem, name );
			}

			// Support: iOS < 6
			// A tribute to the "awesome hack by Dean Edwards"
			// iOS < 6 (at least) returns percentage for a larger set of values, but width seems to be reliably pixels
			// this is against the CSSOM draft spec: http://dev.w3.org/csswg/cssom/#resolved-values
			if ( rnumnonpx.test( ret ) && rmargin.test( name ) ) {

				// Remember the original values
				width = style.width;
				minWidth = style.minWidth;
				maxWidth = style.maxWidth;

				// Put in the new values to get a computed value out
				style.minWidth = style.maxWidth = style.width = ret;
				ret = computed.width;

				// Revert the changed values
				style.width = width;
				style.minWidth = minWidth;
				style.maxWidth = maxWidth;
			}
		}

		return ret !== undefined ?
			// Support: IE
			// IE returns zIndex value as an integer.
			ret + "" :
			ret;
	}


	function addGetHookIf( conditionFn, hookFn ) {
		// Define the hook, we'll check on the first run if it's really needed.
		return {
			get: function() {
				if ( conditionFn() ) {
					// Hook not needed (or it's not possible to use it due
					// to missing dependency), remove it.
					delete this.get;
					return;
				}

				// Hook needed; redefine it so that the support test is not executed again.
				return (this.get = hookFn).apply( this, arguments );
			}
		};
	}


	(function() {
		var pixelPositionVal, boxSizingReliableVal,
			docElem = document.documentElement,
			container = document.createElement( "div" ),
			div = document.createElement( "div" );

		if ( !div.style ) {
			return;
		}

		// Support: IE9-11+
		// Style of cloned element affects source element cloned (#8908)
		div.style.backgroundClip = "content-box";
		div.cloneNode( true ).style.backgroundClip = "";
		support.clearCloneStyle = div.style.backgroundClip === "content-box";

		container.style.cssText = "border:0;width:0;height:0;top:0;left:-9999px;margin-top:1px;" +
			"position:absolute";
		container.appendChild( div );

		// Executing both pixelPosition & boxSizingReliable tests require only one layout
		// so they're executed at the same time to save the second computation.
		function computePixelPositionAndBoxSizingReliable() {
			div.style.cssText =
				// Support: Firefox<29, Android 2.3
				// Vendor-prefix box-sizing
				"-webkit-box-sizing:border-box;-moz-box-sizing:border-box;" +
				"box-sizing:border-box;display:block;margin-top:1%;top:1%;" +
				"border:1px;padding:1px;width:4px;position:absolute";
			div.innerHTML = "";
			docElem.appendChild( container );

			var divStyle = window.getComputedStyle( div, null );
			pixelPositionVal = divStyle.top !== "1%";
			boxSizingReliableVal = divStyle.width === "4px";

			docElem.removeChild( container );
		}

		// Support: node.js jsdom
		// Don't assume that getComputedStyle is a property of the global object
		if ( window.getComputedStyle ) {
			jQuery.extend( support, {
				pixelPosition: function() {

					// This test is executed only once but we still do memoizing
					// since we can use the boxSizingReliable pre-computing.
					// No need to check if the test was already performed, though.
					computePixelPositionAndBoxSizingReliable();
					return pixelPositionVal;
				},
				boxSizingReliable: function() {
					if ( boxSizingReliableVal == null ) {
						computePixelPositionAndBoxSizingReliable();
					}
					return boxSizingReliableVal;
				},
				reliableMarginRight: function() {

					// Support: Android 2.3
					// Check if div with explicit width and no margin-right incorrectly
					// gets computed margin-right based on width of container. (#3333)
					// WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
					// This support function is only executed once so no memoizing is needed.
					var ret,
						marginDiv = div.appendChild( document.createElement( "div" ) );

					// Reset CSS: box-sizing; display; margin; border; padding
					marginDiv.style.cssText = div.style.cssText =
						// Support: Firefox<29, Android 2.3
						// Vendor-prefix box-sizing
						"-webkit-box-sizing:content-box;-moz-box-sizing:content-box;" +
						"box-sizing:content-box;display:block;margin:0;border:0;padding:0";
					marginDiv.style.marginRight = marginDiv.style.width = "0";
					div.style.width = "1px";
					docElem.appendChild( container );

					ret = !parseFloat( window.getComputedStyle( marginDiv, null ).marginRight );

					docElem.removeChild( container );
					div.removeChild( marginDiv );

					return ret;
				}
			});
		}
	})();


	// A method for quickly swapping in/out CSS properties to get correct calculations.
	jQuery.swap = function( elem, options, callback, args ) {
		var ret, name,
			old = {};

		// Remember the old values, and insert the new ones
		for ( name in options ) {
			old[ name ] = elem.style[ name ];
			elem.style[ name ] = options[ name ];
		}

		ret = callback.apply( elem, args || [] );

		// Revert the old values
		for ( name in options ) {
			elem.style[ name ] = old[ name ];
		}

		return ret;
	};


	var
		// Swappable if display is none or starts with table except "table", "table-cell", or "table-caption"
		// See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
		rdisplayswap = /^(none|table(?!-c[ea]).+)/,
		rnumsplit = new RegExp( "^(" + pnum + ")(.*)$", "i" ),
		rrelNum = new RegExp( "^([+-])=(" + pnum + ")", "i" ),

		cssShow = { position: "absolute", visibility: "hidden", display: "block" },
		cssNormalTransform = {
			letterSpacing: "0",
			fontWeight: "400"
		},

		cssPrefixes = [ "Webkit", "O", "Moz", "ms" ];

	// Return a css property mapped to a potentially vendor prefixed property
	function vendorPropName( style, name ) {

		// Shortcut for names that are not vendor prefixed
		if ( name in style ) {
			return name;
		}

		// Check for vendor prefixed names
		var capName = name[0].toUpperCase() + name.slice(1),
			origName = name,
			i = cssPrefixes.length;

		while ( i-- ) {
			name = cssPrefixes[ i ] + capName;
			if ( name in style ) {
				return name;
			}
		}

		return origName;
	}

	function setPositiveNumber( elem, value, subtract ) {
		var matches = rnumsplit.exec( value );
		return matches ?
			// Guard against undefined "subtract", e.g., when used as in cssHooks
			Math.max( 0, matches[ 1 ] - ( subtract || 0 ) ) + ( matches[ 2 ] || "px" ) :
			value;
	}

	function augmentWidthOrHeight( elem, name, extra, isBorderBox, styles ) {
		var i = extra === ( isBorderBox ? "border" : "content" ) ?
			// If we already have the right measurement, avoid augmentation
			4 :
			// Otherwise initialize for horizontal or vertical properties
			name === "width" ? 1 : 0,

			val = 0;

		for ( ; i < 4; i += 2 ) {
			// Both box models exclude margin, so add it if we want it
			if ( extra === "margin" ) {
				val += jQuery.css( elem, extra + cssExpand[ i ], true, styles );
			}

			if ( isBorderBox ) {
				// border-box includes padding, so remove it if we want content
				if ( extra === "content" ) {
					val -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
				}

				// At this point, extra isn't border nor margin, so remove border
				if ( extra !== "margin" ) {
					val -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
				}
			} else {
				// At this point, extra isn't content, so add padding
				val += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );

				// At this point, extra isn't content nor padding, so add border
				if ( extra !== "padding" ) {
					val += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
				}
			}
		}

		return val;
	}

	function getWidthOrHeight( elem, name, extra ) {

		// Start with offset property, which is equivalent to the border-box value
		var valueIsBorderBox = true,
			val = name === "width" ? elem.offsetWidth : elem.offsetHeight,
			styles = getStyles( elem ),
			isBorderBox = jQuery.css( elem, "boxSizing", false, styles ) === "border-box";

		// Some non-html elements return undefined for offsetWidth, so check for null/undefined
		// svg - https://bugzilla.mozilla.org/show_bug.cgi?id=649285
		// MathML - https://bugzilla.mozilla.org/show_bug.cgi?id=491668
		if ( val <= 0 || val == null ) {
			// Fall back to computed then uncomputed css if necessary
			val = curCSS( elem, name, styles );
			if ( val < 0 || val == null ) {
				val = elem.style[ name ];
			}

			// Computed unit is not pixels. Stop here and return.
			if ( rnumnonpx.test(val) ) {
				return val;
			}

			// Check for style in case a browser which returns unreliable values
			// for getComputedStyle silently falls back to the reliable elem.style
			valueIsBorderBox = isBorderBox &&
				( support.boxSizingReliable() || val === elem.style[ name ] );

			// Normalize "", auto, and prepare for extra
			val = parseFloat( val ) || 0;
		}

		// Use the active box-sizing model to add/subtract irrelevant styles
		return ( val +
			augmentWidthOrHeight(
				elem,
				name,
				extra || ( isBorderBox ? "border" : "content" ),
				valueIsBorderBox,
				styles
			)
		) + "px";
	}

	function showHide( elements, show ) {
		var display, elem, hidden,
			values = [],
			index = 0,
			length = elements.length;

		for ( ; index < length; index++ ) {
			elem = elements[ index ];
			if ( !elem.style ) {
				continue;
			}

			values[ index ] = data_priv.get( elem, "olddisplay" );
			display = elem.style.display;
			if ( show ) {
				// Reset the inline display of this element to learn if it is
				// being hidden by cascaded rules or not
				if ( !values[ index ] && display === "none" ) {
					elem.style.display = "";
				}

				// Set elements which have been overridden with display: none
				// in a stylesheet to whatever the default browser style is
				// for such an element
				if ( elem.style.display === "" && isHidden( elem ) ) {
					values[ index ] = data_priv.access( elem, "olddisplay", defaultDisplay(elem.nodeName) );
				}
			} else {
				hidden = isHidden( elem );

				if ( display !== "none" || !hidden ) {
					data_priv.set( elem, "olddisplay", hidden ? display : jQuery.css( elem, "display" ) );
				}
			}
		}

		// Set the display of most of the elements in a second loop
		// to avoid the constant reflow
		for ( index = 0; index < length; index++ ) {
			elem = elements[ index ];
			if ( !elem.style ) {
				continue;
			}
			if ( !show || elem.style.display === "none" || elem.style.display === "" ) {
				elem.style.display = show ? values[ index ] || "" : "none";
			}
		}

		return elements;
	}

	jQuery.extend({

		// Add in style property hooks for overriding the default
		// behavior of getting and setting a style property
		cssHooks: {
			opacity: {
				get: function( elem, computed ) {
					if ( computed ) {

						// We should always get a number back from opacity
						var ret = curCSS( elem, "opacity" );
						return ret === "" ? "1" : ret;
					}
				}
			}
		},

		// Don't automatically add "px" to these possibly-unitless properties
		cssNumber: {
			"columnCount": true,
			"fillOpacity": true,
			"flexGrow": true,
			"flexShrink": true,
			"fontWeight": true,
			"lineHeight": true,
			"opacity": true,
			"order": true,
			"orphans": true,
			"widows": true,
			"zIndex": true,
			"zoom": true
		},

		// Add in properties whose names you wish to fix before
		// setting or getting the value
		cssProps: {
			"float": "cssFloat"
		},

		// Get and set the style property on a DOM Node
		style: function( elem, name, value, extra ) {

			// Don't set styles on text and comment nodes
			if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
				return;
			}

			// Make sure that we're working with the right name
			var ret, type, hooks,
				origName = jQuery.camelCase( name ),
				style = elem.style;

			name = jQuery.cssProps[ origName ] || ( jQuery.cssProps[ origName ] = vendorPropName( style, origName ) );

			// Gets hook for the prefixed version, then unprefixed version
			hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

			// Check if we're setting a value
			if ( value !== undefined ) {
				type = typeof value;

				// Convert "+=" or "-=" to relative numbers (#7345)
				if ( type === "string" && (ret = rrelNum.exec( value )) ) {
					value = ( ret[1] + 1 ) * ret[2] + parseFloat( jQuery.css( elem, name ) );
					// Fixes bug #9237
					type = "number";
				}

				// Make sure that null and NaN values aren't set (#7116)
				if ( value == null || value !== value ) {
					return;
				}

				// If a number, add 'px' to the (except for certain CSS properties)
				if ( type === "number" && !jQuery.cssNumber[ origName ] ) {
					value += "px";
				}

				// Support: IE9-11+
				// background-* props affect original clone's values
				if ( !support.clearCloneStyle && value === "" && name.indexOf( "background" ) === 0 ) {
					style[ name ] = "inherit";
				}

				// If a hook was provided, use that value, otherwise just set the specified value
				if ( !hooks || !("set" in hooks) || (value = hooks.set( elem, value, extra )) !== undefined ) {
					style[ name ] = value;
				}

			} else {
				// If a hook was provided get the non-computed value from there
				if ( hooks && "get" in hooks && (ret = hooks.get( elem, false, extra )) !== undefined ) {
					return ret;
				}

				// Otherwise just get the value from the style object
				return style[ name ];
			}
		},

		css: function( elem, name, extra, styles ) {
			var val, num, hooks,
				origName = jQuery.camelCase( name );

			// Make sure that we're working with the right name
			name = jQuery.cssProps[ origName ] || ( jQuery.cssProps[ origName ] = vendorPropName( elem.style, origName ) );

			// Try prefixed name followed by the unprefixed name
			hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

			// If a hook was provided get the computed value from there
			if ( hooks && "get" in hooks ) {
				val = hooks.get( elem, true, extra );
			}

			// Otherwise, if a way to get the computed value exists, use that
			if ( val === undefined ) {
				val = curCSS( elem, name, styles );
			}

			// Convert "normal" to computed value
			if ( val === "normal" && name in cssNormalTransform ) {
				val = cssNormalTransform[ name ];
			}

			// Make numeric if forced or a qualifier was provided and val looks numeric
			if ( extra === "" || extra ) {
				num = parseFloat( val );
				return extra === true || jQuery.isNumeric( num ) ? num || 0 : val;
			}
			return val;
		}
	});

	jQuery.each([ "height", "width" ], function( i, name ) {
		jQuery.cssHooks[ name ] = {
			get: function( elem, computed, extra ) {
				if ( computed ) {

					// Certain elements can have dimension info if we invisibly show them
					// but it must have a current display style that would benefit
					return rdisplayswap.test( jQuery.css( elem, "display" ) ) && elem.offsetWidth === 0 ?
						jQuery.swap( elem, cssShow, function() {
							return getWidthOrHeight( elem, name, extra );
						}) :
						getWidthOrHeight( elem, name, extra );
				}
			},

			set: function( elem, value, extra ) {
				var styles = extra && getStyles( elem );
				return setPositiveNumber( elem, value, extra ?
					augmentWidthOrHeight(
						elem,
						name,
						extra,
						jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
						styles
					) : 0
				);
			}
		};
	});

	// Support: Android 2.3
	jQuery.cssHooks.marginRight = addGetHookIf( support.reliableMarginRight,
		function( elem, computed ) {
			if ( computed ) {
				return jQuery.swap( elem, { "display": "inline-block" },
					curCSS, [ elem, "marginRight" ] );
			}
		}
	);

	// These hooks are used by animate to expand properties
	jQuery.each({
		margin: "",
		padding: "",
		border: "Width"
	}, function( prefix, suffix ) {
		jQuery.cssHooks[ prefix + suffix ] = {
			expand: function( value ) {
				var i = 0,
					expanded = {},

					// Assumes a single number if not a string
					parts = typeof value === "string" ? value.split(" ") : [ value ];

				for ( ; i < 4; i++ ) {
					expanded[ prefix + cssExpand[ i ] + suffix ] =
						parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
				}

				return expanded;
			}
		};

		if ( !rmargin.test( prefix ) ) {
			jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
		}
	});

	jQuery.fn.extend({
		css: function( name, value ) {
			return access( this, function( elem, name, value ) {
				var styles, len,
					map = {},
					i = 0;

				if ( jQuery.isArray( name ) ) {
					styles = getStyles( elem );
					len = name.length;

					for ( ; i < len; i++ ) {
						map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
					}

					return map;
				}

				return value !== undefined ?
					jQuery.style( elem, name, value ) :
					jQuery.css( elem, name );
			}, name, value, arguments.length > 1 );
		},
		show: function() {
			return showHide( this, true );
		},
		hide: function() {
			return showHide( this );
		},
		toggle: function( state ) {
			if ( typeof state === "boolean" ) {
				return state ? this.show() : this.hide();
			}

			return this.each(function() {
				if ( isHidden( this ) ) {
					jQuery( this ).show();
				} else {
					jQuery( this ).hide();
				}
			});
		}
	});


	function Tween( elem, options, prop, end, easing ) {
		return new Tween.prototype.init( elem, options, prop, end, easing );
	}
	jQuery.Tween = Tween;

	Tween.prototype = {
		constructor: Tween,
		init: function( elem, options, prop, end, easing, unit ) {
			this.elem = elem;
			this.prop = prop;
			this.easing = easing || "swing";
			this.options = options;
			this.start = this.now = this.cur();
			this.end = end;
			this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
		},
		cur: function() {
			var hooks = Tween.propHooks[ this.prop ];

			return hooks && hooks.get ?
				hooks.get( this ) :
				Tween.propHooks._default.get( this );
		},
		run: function( percent ) {
			var eased,
				hooks = Tween.propHooks[ this.prop ];

			if ( this.options.duration ) {
				this.pos = eased = jQuery.easing[ this.easing ](
					percent, this.options.duration * percent, 0, 1, this.options.duration
				);
			} else {
				this.pos = eased = percent;
			}
			this.now = ( this.end - this.start ) * eased + this.start;

			if ( this.options.step ) {
				this.options.step.call( this.elem, this.now, this );
			}

			if ( hooks && hooks.set ) {
				hooks.set( this );
			} else {
				Tween.propHooks._default.set( this );
			}
			return this;
		}
	};

	Tween.prototype.init.prototype = Tween.prototype;

	Tween.propHooks = {
		_default: {
			get: function( tween ) {
				var result;

				if ( tween.elem[ tween.prop ] != null &&
					(!tween.elem.style || tween.elem.style[ tween.prop ] == null) ) {
					return tween.elem[ tween.prop ];
				}

				// Passing an empty string as a 3rd parameter to .css will automatically
				// attempt a parseFloat and fallback to a string if the parse fails.
				// Simple values such as "10px" are parsed to Float;
				// complex values such as "rotate(1rad)" are returned as-is.
				result = jQuery.css( tween.elem, tween.prop, "" );
				// Empty strings, null, undefined and "auto" are converted to 0.
				return !result || result === "auto" ? 0 : result;
			},
			set: function( tween ) {
				// Use step hook for back compat.
				// Use cssHook if its there.
				// Use .style if available and use plain properties where available.
				if ( jQuery.fx.step[ tween.prop ] ) {
					jQuery.fx.step[ tween.prop ]( tween );
				} else if ( tween.elem.style && ( tween.elem.style[ jQuery.cssProps[ tween.prop ] ] != null || jQuery.cssHooks[ tween.prop ] ) ) {
					jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
				} else {
					tween.elem[ tween.prop ] = tween.now;
				}
			}
		}
	};

	// Support: IE9
	// Panic based approach to setting things on disconnected nodes
	Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
		set: function( tween ) {
			if ( tween.elem.nodeType && tween.elem.parentNode ) {
				tween.elem[ tween.prop ] = tween.now;
			}
		}
	};

	jQuery.easing = {
		linear: function( p ) {
			return p;
		},
		swing: function( p ) {
			return 0.5 - Math.cos( p * Math.PI ) / 2;
		}
	};

	jQuery.fx = Tween.prototype.init;

	// Back Compat <1.8 extension point
	jQuery.fx.step = {};




	var
		fxNow, timerId,
		rfxtypes = /^(?:toggle|show|hide)$/,
		rfxnum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" ),
		rrun = /queueHooks$/,
		animationPrefilters = [ defaultPrefilter ],
		tweeners = {
			"*": [ function( prop, value ) {
				var tween = this.createTween( prop, value ),
					target = tween.cur(),
					parts = rfxnum.exec( value ),
					unit = parts && parts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),

					// Starting value computation is required for potential unit mismatches
					start = ( jQuery.cssNumber[ prop ] || unit !== "px" && +target ) &&
						rfxnum.exec( jQuery.css( tween.elem, prop ) ),
					scale = 1,
					maxIterations = 20;

				if ( start && start[ 3 ] !== unit ) {
					// Trust units reported by jQuery.css
					unit = unit || start[ 3 ];

					// Make sure we update the tween properties later on
					parts = parts || [];

					// Iteratively approximate from a nonzero starting point
					start = +target || 1;

					do {
						// If previous iteration zeroed out, double until we get *something*.
						// Use string for doubling so we don't accidentally see scale as unchanged below
						scale = scale || ".5";

						// Adjust and apply
						start = start / scale;
						jQuery.style( tween.elem, prop, start + unit );

					// Update scale, tolerating zero or NaN from tween.cur(),
					// break the loop if scale is unchanged or perfect, or if we've just had enough
					} while ( scale !== (scale = tween.cur() / target) && scale !== 1 && --maxIterations );
				}

				// Update tween properties
				if ( parts ) {
					start = tween.start = +start || +target || 0;
					tween.unit = unit;
					// If a +=/-= token was provided, we're doing a relative animation
					tween.end = parts[ 1 ] ?
						start + ( parts[ 1 ] + 1 ) * parts[ 2 ] :
						+parts[ 2 ];
				}

				return tween;
			} ]
		};

	// Animations created synchronously will run synchronously
	function createFxNow() {
		setTimeout(function() {
			fxNow = undefined;
		});
		return ( fxNow = jQuery.now() );
	}

	// Generate parameters to create a standard animation
	function genFx( type, includeWidth ) {
		var which,
			i = 0,
			attrs = { height: type };

		// If we include width, step value is 1 to do all cssExpand values,
		// otherwise step value is 2 to skip over Left and Right
		includeWidth = includeWidth ? 1 : 0;
		for ( ; i < 4 ; i += 2 - includeWidth ) {
			which = cssExpand[ i ];
			attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
		}

		if ( includeWidth ) {
			attrs.opacity = attrs.width = type;
		}

		return attrs;
	}

	function createTween( value, prop, animation ) {
		var tween,
			collection = ( tweeners[ prop ] || [] ).concat( tweeners[ "*" ] ),
			index = 0,
			length = collection.length;
		for ( ; index < length; index++ ) {
			if ( (tween = collection[ index ].call( animation, prop, value )) ) {

				// We're done with this property
				return tween;
			}
		}
	}

	function defaultPrefilter( elem, props, opts ) {
		/* jshint validthis: true */
		var prop, value, toggle, tween, hooks, oldfire, display, checkDisplay,
			anim = this,
			orig = {},
			style = elem.style,
			hidden = elem.nodeType && isHidden( elem ),
			dataShow = data_priv.get( elem, "fxshow" );

		// Handle queue: false promises
		if ( !opts.queue ) {
			hooks = jQuery._queueHooks( elem, "fx" );
			if ( hooks.unqueued == null ) {
				hooks.unqueued = 0;
				oldfire = hooks.empty.fire;
				hooks.empty.fire = function() {
					if ( !hooks.unqueued ) {
						oldfire();
					}
				};
			}
			hooks.unqueued++;

			anim.always(function() {
				// Ensure the complete handler is called before this completes
				anim.always(function() {
					hooks.unqueued--;
					if ( !jQuery.queue( elem, "fx" ).length ) {
						hooks.empty.fire();
					}
				});
			});
		}

		// Height/width overflow pass
		if ( elem.nodeType === 1 && ( "height" in props || "width" in props ) ) {
			// Make sure that nothing sneaks out
			// Record all 3 overflow attributes because IE9-10 do not
			// change the overflow attribute when overflowX and
			// overflowY are set to the same value
			opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];

			// Set display property to inline-block for height/width
			// animations on inline elements that are having width/height animated
			display = jQuery.css( elem, "display" );

			// Test default display if display is currently "none"
			checkDisplay = display === "none" ?
				data_priv.get( elem, "olddisplay" ) || defaultDisplay( elem.nodeName ) : display;

			if ( checkDisplay === "inline" && jQuery.css( elem, "float" ) === "none" ) {
				style.display = "inline-block";
			}
		}

		if ( opts.overflow ) {
			style.overflow = "hidden";
			anim.always(function() {
				style.overflow = opts.overflow[ 0 ];
				style.overflowX = opts.overflow[ 1 ];
				style.overflowY = opts.overflow[ 2 ];
			});
		}

		// show/hide pass
		for ( prop in props ) {
			value = props[ prop ];
			if ( rfxtypes.exec( value ) ) {
				delete props[ prop ];
				toggle = toggle || value === "toggle";
				if ( value === ( hidden ? "hide" : "show" ) ) {

					// If there is dataShow left over from a stopped hide or show and we are going to proceed with show, we should pretend to be hidden
					if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
						hidden = true;
					} else {
						continue;
					}
				}
				orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );

			// Any non-fx value stops us from restoring the original display value
			} else {
				display = undefined;
			}
		}

		if ( !jQuery.isEmptyObject( orig ) ) {
			if ( dataShow ) {
				if ( "hidden" in dataShow ) {
					hidden = dataShow.hidden;
				}
			} else {
				dataShow = data_priv.access( elem, "fxshow", {} );
			}

			// Store state if its toggle - enables .stop().toggle() to "reverse"
			if ( toggle ) {
				dataShow.hidden = !hidden;
			}
			if ( hidden ) {
				jQuery( elem ).show();
			} else {
				anim.done(function() {
					jQuery( elem ).hide();
				});
			}
			anim.done(function() {
				var prop;

				data_priv.remove( elem, "fxshow" );
				for ( prop in orig ) {
					jQuery.style( elem, prop, orig[ prop ] );
				}
			});
			for ( prop in orig ) {
				tween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );

				if ( !( prop in dataShow ) ) {
					dataShow[ prop ] = tween.start;
					if ( hidden ) {
						tween.end = tween.start;
						tween.start = prop === "width" || prop === "height" ? 1 : 0;
					}
				}
			}

		// If this is a noop like .hide().hide(), restore an overwritten display value
		} else if ( (display === "none" ? defaultDisplay( elem.nodeName ) : display) === "inline" ) {
			style.display = display;
		}
	}

	function propFilter( props, specialEasing ) {
		var index, name, easing, value, hooks;

		// camelCase, specialEasing and expand cssHook pass
		for ( index in props ) {
			name = jQuery.camelCase( index );
			easing = specialEasing[ name ];
			value = props[ index ];
			if ( jQuery.isArray( value ) ) {
				easing = value[ 1 ];
				value = props[ index ] = value[ 0 ];
			}

			if ( index !== name ) {
				props[ name ] = value;
				delete props[ index ];
			}

			hooks = jQuery.cssHooks[ name ];
			if ( hooks && "expand" in hooks ) {
				value = hooks.expand( value );
				delete props[ name ];

				// Not quite $.extend, this won't overwrite existing keys.
				// Reusing 'index' because we have the correct "name"
				for ( index in value ) {
					if ( !( index in props ) ) {
						props[ index ] = value[ index ];
						specialEasing[ index ] = easing;
					}
				}
			} else {
				specialEasing[ name ] = easing;
			}
		}
	}

	function Animation( elem, properties, options ) {
		var result,
			stopped,
			index = 0,
			length = animationPrefilters.length,
			deferred = jQuery.Deferred().always( function() {
				// Don't match elem in the :animated selector
				delete tick.elem;
			}),
			tick = function() {
				if ( stopped ) {
					return false;
				}
				var currentTime = fxNow || createFxNow(),
					remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),
					// Support: Android 2.3
					// Archaic crash bug won't allow us to use `1 - ( 0.5 || 0 )` (#12497)
					temp = remaining / animation.duration || 0,
					percent = 1 - temp,
					index = 0,
					length = animation.tweens.length;

				for ( ; index < length ; index++ ) {
					animation.tweens[ index ].run( percent );
				}

				deferred.notifyWith( elem, [ animation, percent, remaining ]);

				if ( percent < 1 && length ) {
					return remaining;
				} else {
					deferred.resolveWith( elem, [ animation ] );
					return false;
				}
			},
			animation = deferred.promise({
				elem: elem,
				props: jQuery.extend( {}, properties ),
				opts: jQuery.extend( true, { specialEasing: {} }, options ),
				originalProperties: properties,
				originalOptions: options,
				startTime: fxNow || createFxNow(),
				duration: options.duration,
				tweens: [],
				createTween: function( prop, end ) {
					var tween = jQuery.Tween( elem, animation.opts, prop, end,
							animation.opts.specialEasing[ prop ] || animation.opts.easing );
					animation.tweens.push( tween );
					return tween;
				},
				stop: function( gotoEnd ) {
					var index = 0,
						// If we are going to the end, we want to run all the tweens
						// otherwise we skip this part
						length = gotoEnd ? animation.tweens.length : 0;
					if ( stopped ) {
						return this;
					}
					stopped = true;
					for ( ; index < length ; index++ ) {
						animation.tweens[ index ].run( 1 );
					}

					// Resolve when we played the last frame; otherwise, reject
					if ( gotoEnd ) {
						deferred.resolveWith( elem, [ animation, gotoEnd ] );
					} else {
						deferred.rejectWith( elem, [ animation, gotoEnd ] );
					}
					return this;
				}
			}),
			props = animation.props;

		propFilter( props, animation.opts.specialEasing );

		for ( ; index < length ; index++ ) {
			result = animationPrefilters[ index ].call( animation, elem, props, animation.opts );
			if ( result ) {
				return result;
			}
		}

		jQuery.map( props, createTween, animation );

		if ( jQuery.isFunction( animation.opts.start ) ) {
			animation.opts.start.call( elem, animation );
		}

		jQuery.fx.timer(
			jQuery.extend( tick, {
				elem: elem,
				anim: animation,
				queue: animation.opts.queue
			})
		);

		// attach callbacks from options
		return animation.progress( animation.opts.progress )
			.done( animation.opts.done, animation.opts.complete )
			.fail( animation.opts.fail )
			.always( animation.opts.always );
	}

	jQuery.Animation = jQuery.extend( Animation, {

		tweener: function( props, callback ) {
			if ( jQuery.isFunction( props ) ) {
				callback = props;
				props = [ "*" ];
			} else {
				props = props.split(" ");
			}

			var prop,
				index = 0,
				length = props.length;

			for ( ; index < length ; index++ ) {
				prop = props[ index ];
				tweeners[ prop ] = tweeners[ prop ] || [];
				tweeners[ prop ].unshift( callback );
			}
		},

		prefilter: function( callback, prepend ) {
			if ( prepend ) {
				animationPrefilters.unshift( callback );
			} else {
				animationPrefilters.push( callback );
			}
		}
	});

	jQuery.speed = function( speed, easing, fn ) {
		var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
			complete: fn || !fn && easing ||
				jQuery.isFunction( speed ) && speed,
			duration: speed,
			easing: fn && easing || easing && !jQuery.isFunction( easing ) && easing
		};

		opt.duration = jQuery.fx.off ? 0 : typeof opt.duration === "number" ? opt.duration :
			opt.duration in jQuery.fx.speeds ? jQuery.fx.speeds[ opt.duration ] : jQuery.fx.speeds._default;

		// Normalize opt.queue - true/undefined/null -> "fx"
		if ( opt.queue == null || opt.queue === true ) {
			opt.queue = "fx";
		}

		// Queueing
		opt.old = opt.complete;

		opt.complete = function() {
			if ( jQuery.isFunction( opt.old ) ) {
				opt.old.call( this );
			}

			if ( opt.queue ) {
				jQuery.dequeue( this, opt.queue );
			}
		};

		return opt;
	};

	jQuery.fn.extend({
		fadeTo: function( speed, to, easing, callback ) {

			// Show any hidden elements after setting opacity to 0
			return this.filter( isHidden ).css( "opacity", 0 ).show()

				// Animate to the value specified
				.end().animate({ opacity: to }, speed, easing, callback );
		},
		animate: function( prop, speed, easing, callback ) {
			var empty = jQuery.isEmptyObject( prop ),
				optall = jQuery.speed( speed, easing, callback ),
				doAnimation = function() {
					// Operate on a copy of prop so per-property easing won't be lost
					var anim = Animation( this, jQuery.extend( {}, prop ), optall );

					// Empty animations, or finishing resolves immediately
					if ( empty || data_priv.get( this, "finish" ) ) {
						anim.stop( true );
					}
				};
				doAnimation.finish = doAnimation;

			return empty || optall.queue === false ?
				this.each( doAnimation ) :
				this.queue( optall.queue, doAnimation );
		},
		stop: function( type, clearQueue, gotoEnd ) {
			var stopQueue = function( hooks ) {
				var stop = hooks.stop;
				delete hooks.stop;
				stop( gotoEnd );
			};

			if ( typeof type !== "string" ) {
				gotoEnd = clearQueue;
				clearQueue = type;
				type = undefined;
			}
			if ( clearQueue && type !== false ) {
				this.queue( type || "fx", [] );
			}

			return this.each(function() {
				var dequeue = true,
					index = type != null && type + "queueHooks",
					timers = jQuery.timers,
					data = data_priv.get( this );

				if ( index ) {
					if ( data[ index ] && data[ index ].stop ) {
						stopQueue( data[ index ] );
					}
				} else {
					for ( index in data ) {
						if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
							stopQueue( data[ index ] );
						}
					}
				}

				for ( index = timers.length; index--; ) {
					if ( timers[ index ].elem === this && (type == null || timers[ index ].queue === type) ) {
						timers[ index ].anim.stop( gotoEnd );
						dequeue = false;
						timers.splice( index, 1 );
					}
				}

				// Start the next in the queue if the last step wasn't forced.
				// Timers currently will call their complete callbacks, which
				// will dequeue but only if they were gotoEnd.
				if ( dequeue || !gotoEnd ) {
					jQuery.dequeue( this, type );
				}
			});
		},
		finish: function( type ) {
			if ( type !== false ) {
				type = type || "fx";
			}
			return this.each(function() {
				var index,
					data = data_priv.get( this ),
					queue = data[ type + "queue" ],
					hooks = data[ type + "queueHooks" ],
					timers = jQuery.timers,
					length = queue ? queue.length : 0;

				// Enable finishing flag on private data
				data.finish = true;

				// Empty the queue first
				jQuery.queue( this, type, [] );

				if ( hooks && hooks.stop ) {
					hooks.stop.call( this, true );
				}

				// Look for any active animations, and finish them
				for ( index = timers.length; index--; ) {
					if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
						timers[ index ].anim.stop( true );
						timers.splice( index, 1 );
					}
				}

				// Look for any animations in the old queue and finish them
				for ( index = 0; index < length; index++ ) {
					if ( queue[ index ] && queue[ index ].finish ) {
						queue[ index ].finish.call( this );
					}
				}

				// Turn off finishing flag
				delete data.finish;
			});
		}
	});

	jQuery.each([ "toggle", "show", "hide" ], function( i, name ) {
		var cssFn = jQuery.fn[ name ];
		jQuery.fn[ name ] = function( speed, easing, callback ) {
			return speed == null || typeof speed === "boolean" ?
				cssFn.apply( this, arguments ) :
				this.animate( genFx( name, true ), speed, easing, callback );
		};
	});

	// Generate shortcuts for custom animations
	jQuery.each({
		slideDown: genFx("show"),
		slideUp: genFx("hide"),
		slideToggle: genFx("toggle"),
		fadeIn: { opacity: "show" },
		fadeOut: { opacity: "hide" },
		fadeToggle: { opacity: "toggle" }
	}, function( name, props ) {
		jQuery.fn[ name ] = function( speed, easing, callback ) {
			return this.animate( props, speed, easing, callback );
		};
	});

	jQuery.timers = [];
	jQuery.fx.tick = function() {
		var timer,
			i = 0,
			timers = jQuery.timers;

		fxNow = jQuery.now();

		for ( ; i < timers.length; i++ ) {
			timer = timers[ i ];
			// Checks the timer has not already been removed
			if ( !timer() && timers[ i ] === timer ) {
				timers.splice( i--, 1 );
			}
		}

		if ( !timers.length ) {
			jQuery.fx.stop();
		}
		fxNow = undefined;
	};

	jQuery.fx.timer = function( timer ) {
		jQuery.timers.push( timer );
		if ( timer() ) {
			jQuery.fx.start();
		} else {
			jQuery.timers.pop();
		}
	};

	jQuery.fx.interval = 13;

	jQuery.fx.start = function() {
		if ( !timerId ) {
			timerId = setInterval( jQuery.fx.tick, jQuery.fx.interval );
		}
	};

	jQuery.fx.stop = function() {
		clearInterval( timerId );
		timerId = null;
	};

	jQuery.fx.speeds = {
		slow: 600,
		fast: 200,
		// Default speed
		_default: 400
	};


	// Based off of the plugin by Clint Helfers, with permission.
	// http://blindsignals.com/index.php/2009/07/jquery-delay/
	jQuery.fn.delay = function( time, type ) {
		time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
		type = type || "fx";

		return this.queue( type, function( next, hooks ) {
			var timeout = setTimeout( next, time );
			hooks.stop = function() {
				clearTimeout( timeout );
			};
		});
	};


	(function() {
		var input = document.createElement( "input" ),
			select = document.createElement( "select" ),
			opt = select.appendChild( document.createElement( "option" ) );

		input.type = "checkbox";

		// Support: iOS<=5.1, Android<=4.2+
		// Default value for a checkbox should be "on"
		support.checkOn = input.value !== "";

		// Support: IE<=11+
		// Must access selectedIndex to make default options select
		support.optSelected = opt.selected;

		// Support: Android<=2.3
		// Options inside disabled selects are incorrectly marked as disabled
		select.disabled = true;
		support.optDisabled = !opt.disabled;

		// Support: IE<=11+
		// An input loses its value after becoming a radio
		input = document.createElement( "input" );
		input.value = "t";
		input.type = "radio";
		support.radioValue = input.value === "t";
	})();


	var nodeHook, boolHook,
		attrHandle = jQuery.expr.attrHandle;

	jQuery.fn.extend({
		attr: function( name, value ) {
			return access( this, jQuery.attr, name, value, arguments.length > 1 );
		},

		removeAttr: function( name ) {
			return this.each(function() {
				jQuery.removeAttr( this, name );
			});
		}
	});

	jQuery.extend({
		attr: function( elem, name, value ) {
			var hooks, ret,
				nType = elem.nodeType;

			// don't get/set attributes on text, comment and attribute nodes
			if ( !elem || nType === 3 || nType === 8 || nType === 2 ) {
				return;
			}

			// Fallback to prop when attributes are not supported
			if ( typeof elem.getAttribute === strundefined ) {
				return jQuery.prop( elem, name, value );
			}

			// All attributes are lowercase
			// Grab necessary hook if one is defined
			if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
				name = name.toLowerCase();
				hooks = jQuery.attrHooks[ name ] ||
					( jQuery.expr.match.bool.test( name ) ? boolHook : nodeHook );
			}

			if ( value !== undefined ) {

				if ( value === null ) {
					jQuery.removeAttr( elem, name );

				} else if ( hooks && "set" in hooks && (ret = hooks.set( elem, value, name )) !== undefined ) {
					return ret;

				} else {
					elem.setAttribute( name, value + "" );
					return value;
				}

			} else if ( hooks && "get" in hooks && (ret = hooks.get( elem, name )) !== null ) {
				return ret;

			} else {
				ret = jQuery.find.attr( elem, name );

				// Non-existent attributes return null, we normalize to undefined
				return ret == null ?
					undefined :
					ret;
			}
		},

		removeAttr: function( elem, value ) {
			var name, propName,
				i = 0,
				attrNames = value && value.match( rnotwhite );

			if ( attrNames && elem.nodeType === 1 ) {
				while ( (name = attrNames[i++]) ) {
					propName = jQuery.propFix[ name ] || name;

					// Boolean attributes get special treatment (#10870)
					if ( jQuery.expr.match.bool.test( name ) ) {
						// Set corresponding property to false
						elem[ propName ] = false;
					}

					elem.removeAttribute( name );
				}
			}
		},

		attrHooks: {
			type: {
				set: function( elem, value ) {
					if ( !support.radioValue && value === "radio" &&
						jQuery.nodeName( elem, "input" ) ) {
						var val = elem.value;
						elem.setAttribute( "type", value );
						if ( val ) {
							elem.value = val;
						}
						return value;
					}
				}
			}
		}
	});

	// Hooks for boolean attributes
	boolHook = {
		set: function( elem, value, name ) {
			if ( value === false ) {
				// Remove boolean attributes when set to false
				jQuery.removeAttr( elem, name );
			} else {
				elem.setAttribute( name, name );
			}
			return name;
		}
	};
	jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( i, name ) {
		var getter = attrHandle[ name ] || jQuery.find.attr;

		attrHandle[ name ] = function( elem, name, isXML ) {
			var ret, handle;
			if ( !isXML ) {
				// Avoid an infinite loop by temporarily removing this function from the getter
				handle = attrHandle[ name ];
				attrHandle[ name ] = ret;
				ret = getter( elem, name, isXML ) != null ?
					name.toLowerCase() :
					null;
				attrHandle[ name ] = handle;
			}
			return ret;
		};
	});




	var rfocusable = /^(?:input|select|textarea|button)$/i;

	jQuery.fn.extend({
		prop: function( name, value ) {
			return access( this, jQuery.prop, name, value, arguments.length > 1 );
		},

		removeProp: function( name ) {
			return this.each(function() {
				delete this[ jQuery.propFix[ name ] || name ];
			});
		}
	});

	jQuery.extend({
		propFix: {
			"for": "htmlFor",
			"class": "className"
		},

		prop: function( elem, name, value ) {
			var ret, hooks, notxml,
				nType = elem.nodeType;

			// Don't get/set properties on text, comment and attribute nodes
			if ( !elem || nType === 3 || nType === 8 || nType === 2 ) {
				return;
			}

			notxml = nType !== 1 || !jQuery.isXMLDoc( elem );

			if ( notxml ) {
				// Fix name and attach hooks
				name = jQuery.propFix[ name ] || name;
				hooks = jQuery.propHooks[ name ];
			}

			if ( value !== undefined ) {
				return hooks && "set" in hooks && (ret = hooks.set( elem, value, name )) !== undefined ?
					ret :
					( elem[ name ] = value );

			} else {
				return hooks && "get" in hooks && (ret = hooks.get( elem, name )) !== null ?
					ret :
					elem[ name ];
			}
		},

		propHooks: {
			tabIndex: {
				get: function( elem ) {
					return elem.hasAttribute( "tabindex" ) || rfocusable.test( elem.nodeName ) || elem.href ?
						elem.tabIndex :
						-1;
				}
			}
		}
	});

	if ( !support.optSelected ) {
		jQuery.propHooks.selected = {
			get: function( elem ) {
				var parent = elem.parentNode;
				if ( parent && parent.parentNode ) {
					parent.parentNode.selectedIndex;
				}
				return null;
			}
		};
	}

	jQuery.each([
		"tabIndex",
		"readOnly",
		"maxLength",
		"cellSpacing",
		"cellPadding",
		"rowSpan",
		"colSpan",
		"useMap",
		"frameBorder",
		"contentEditable"
	], function() {
		jQuery.propFix[ this.toLowerCase() ] = this;
	});




	var rclass = /[\t\r\n\f]/g;

	jQuery.fn.extend({
		addClass: function( value ) {
			var classes, elem, cur, clazz, j, finalValue,
				proceed = typeof value === "string" && value,
				i = 0,
				len = this.length;

			if ( jQuery.isFunction( value ) ) {
				return this.each(function( j ) {
					jQuery( this ).addClass( value.call( this, j, this.className ) );
				});
			}

			if ( proceed ) {
				// The disjunction here is for better compressibility (see removeClass)
				classes = ( value || "" ).match( rnotwhite ) || [];

				for ( ; i < len; i++ ) {
					elem = this[ i ];
					cur = elem.nodeType === 1 && ( elem.className ?
						( " " + elem.className + " " ).replace( rclass, " " ) :
						" "
					);

					if ( cur ) {
						j = 0;
						while ( (clazz = classes[j++]) ) {
							if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
								cur += clazz + " ";
							}
						}

						// only assign if different to avoid unneeded rendering.
						finalValue = jQuery.trim( cur );
						if ( elem.className !== finalValue ) {
							elem.className = finalValue;
						}
					}
				}
			}

			return this;
		},

		removeClass: function( value ) {
			var classes, elem, cur, clazz, j, finalValue,
				proceed = arguments.length === 0 || typeof value === "string" && value,
				i = 0,
				len = this.length;

			if ( jQuery.isFunction( value ) ) {
				return this.each(function( j ) {
					jQuery( this ).removeClass( value.call( this, j, this.className ) );
				});
			}
			if ( proceed ) {
				classes = ( value || "" ).match( rnotwhite ) || [];

				for ( ; i < len; i++ ) {
					elem = this[ i ];
					// This expression is here for better compressibility (see addClass)
					cur = elem.nodeType === 1 && ( elem.className ?
						( " " + elem.className + " " ).replace( rclass, " " ) :
						""
					);

					if ( cur ) {
						j = 0;
						while ( (clazz = classes[j++]) ) {
							// Remove *all* instances
							while ( cur.indexOf( " " + clazz + " " ) >= 0 ) {
								cur = cur.replace( " " + clazz + " ", " " );
							}
						}

						// Only assign if different to avoid unneeded rendering.
						finalValue = value ? jQuery.trim( cur ) : "";
						if ( elem.className !== finalValue ) {
							elem.className = finalValue;
						}
					}
				}
			}

			return this;
		},

		toggleClass: function( value, stateVal ) {
			var type = typeof value;

			if ( typeof stateVal === "boolean" && type === "string" ) {
				return stateVal ? this.addClass( value ) : this.removeClass( value );
			}

			if ( jQuery.isFunction( value ) ) {
				return this.each(function( i ) {
					jQuery( this ).toggleClass( value.call(this, i, this.className, stateVal), stateVal );
				});
			}

			return this.each(function() {
				if ( type === "string" ) {
					// Toggle individual class names
					var className,
						i = 0,
						self = jQuery( this ),
						classNames = value.match( rnotwhite ) || [];

					while ( (className = classNames[ i++ ]) ) {
						// Check each className given, space separated list
						if ( self.hasClass( className ) ) {
							self.removeClass( className );
						} else {
							self.addClass( className );
						}
					}

				// Toggle whole class name
				} else if ( type === strundefined || type === "boolean" ) {
					if ( this.className ) {
						// store className if set
						data_priv.set( this, "__className__", this.className );
					}

					// If the element has a class name or if we're passed `false`,
					// then remove the whole classname (if there was one, the above saved it).
					// Otherwise bring back whatever was previously saved (if anything),
					// falling back to the empty string if nothing was stored.
					this.className = this.className || value === false ? "" : data_priv.get( this, "__className__" ) || "";
				}
			});
		},

		hasClass: function( selector ) {
			var className = " " + selector + " ",
				i = 0,
				l = this.length;
			for ( ; i < l; i++ ) {
				if ( this[i].nodeType === 1 && (" " + this[i].className + " ").replace(rclass, " ").indexOf( className ) >= 0 ) {
					return true;
				}
			}

			return false;
		}
	});




	var rreturn = /\r/g;

	jQuery.fn.extend({
		val: function( value ) {
			var hooks, ret, isFunction,
				elem = this[0];

			if ( !arguments.length ) {
				if ( elem ) {
					hooks = jQuery.valHooks[ elem.type ] || jQuery.valHooks[ elem.nodeName.toLowerCase() ];

					if ( hooks && "get" in hooks && (ret = hooks.get( elem, "value" )) !== undefined ) {
						return ret;
					}

					ret = elem.value;

					return typeof ret === "string" ?
						// Handle most common string cases
						ret.replace(rreturn, "") :
						// Handle cases where value is null/undef or number
						ret == null ? "" : ret;
				}

				return;
			}

			isFunction = jQuery.isFunction( value );

			return this.each(function( i ) {
				var val;

				if ( this.nodeType !== 1 ) {
					return;
				}

				if ( isFunction ) {
					val = value.call( this, i, jQuery( this ).val() );
				} else {
					val = value;
				}

				// Treat null/undefined as ""; convert numbers to string
				if ( val == null ) {
					val = "";

				} else if ( typeof val === "number" ) {
					val += "";

				} else if ( jQuery.isArray( val ) ) {
					val = jQuery.map( val, function( value ) {
						return value == null ? "" : value + "";
					});
				}

				hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

				// If set returns undefined, fall back to normal setting
				if ( !hooks || !("set" in hooks) || hooks.set( this, val, "value" ) === undefined ) {
					this.value = val;
				}
			});
		}
	});

	jQuery.extend({
		valHooks: {
			option: {
				get: function( elem ) {
					var val = jQuery.find.attr( elem, "value" );
					return val != null ?
						val :
						// Support: IE10-11+
						// option.text throws exceptions (#14686, #14858)
						jQuery.trim( jQuery.text( elem ) );
				}
			},
			select: {
				get: function( elem ) {
					var value, option,
						options = elem.options,
						index = elem.selectedIndex,
						one = elem.type === "select-one" || index < 0,
						values = one ? null : [],
						max = one ? index + 1 : options.length,
						i = index < 0 ?
							max :
							one ? index : 0;

					// Loop through all the selected options
					for ( ; i < max; i++ ) {
						option = options[ i ];

						// IE6-9 doesn't update selected after form reset (#2551)
						if ( ( option.selected || i === index ) &&
								// Don't return options that are disabled or in a disabled optgroup
								( support.optDisabled ? !option.disabled : option.getAttribute( "disabled" ) === null ) &&
								( !option.parentNode.disabled || !jQuery.nodeName( option.parentNode, "optgroup" ) ) ) {

							// Get the specific value for the option
							value = jQuery( option ).val();

							// We don't need an array for one selects
							if ( one ) {
								return value;
							}

							// Multi-Selects return an array
							values.push( value );
						}
					}

					return values;
				},

				set: function( elem, value ) {
					var optionSet, option,
						options = elem.options,
						values = jQuery.makeArray( value ),
						i = options.length;

					while ( i-- ) {
						option = options[ i ];
						if ( (option.selected = jQuery.inArray( option.value, values ) >= 0) ) {
							optionSet = true;
						}
					}

					// Force browsers to behave consistently when non-matching value is set
					if ( !optionSet ) {
						elem.selectedIndex = -1;
					}
					return values;
				}
			}
		}
	});

	// Radios and checkboxes getter/setter
	jQuery.each([ "radio", "checkbox" ], function() {
		jQuery.valHooks[ this ] = {
			set: function( elem, value ) {
				if ( jQuery.isArray( value ) ) {
					return ( elem.checked = jQuery.inArray( jQuery(elem).val(), value ) >= 0 );
				}
			}
		};
		if ( !support.checkOn ) {
			jQuery.valHooks[ this ].get = function( elem ) {
				return elem.getAttribute("value") === null ? "on" : elem.value;
			};
		}
	});




	// Return jQuery for attributes-only inclusion


	jQuery.each( ("blur focus focusin focusout load resize scroll unload click dblclick " +
		"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
		"change select submit keydown keypress keyup error contextmenu").split(" "), function( i, name ) {

		// Handle event binding
		jQuery.fn[ name ] = function( data, fn ) {
			return arguments.length > 0 ?
				this.on( name, null, data, fn ) :
				this.trigger( name );
		};
	});

	jQuery.fn.extend({
		hover: function( fnOver, fnOut ) {
			return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
		},

		bind: function( types, data, fn ) {
			return this.on( types, null, data, fn );
		},
		unbind: function( types, fn ) {
			return this.off( types, null, fn );
		},

		delegate: function( selector, types, data, fn ) {
			return this.on( types, selector, data, fn );
		},
		undelegate: function( selector, types, fn ) {
			// ( namespace ) or ( selector, types [, fn] )
			return arguments.length === 1 ? this.off( selector, "**" ) : this.off( types, selector || "**", fn );
		}
	});


	var nonce = jQuery.now();

	var rquery = (/\?/);



	// Support: Android 2.3
	// Workaround failure to string-cast null input
	jQuery.parseJSON = function( data ) {
		return JSON.parse( data + "" );
	};


	// Cross-browser xml parsing
	jQuery.parseXML = function( data ) {
		var xml, tmp;
		if ( !data || typeof data !== "string" ) {
			return null;
		}

		// Support: IE9
		try {
			tmp = new DOMParser();
			xml = tmp.parseFromString( data, "text/xml" );
		} catch ( e ) {
			xml = undefined;
		}

		if ( !xml || xml.getElementsByTagName( "parsererror" ).length ) {
			jQuery.error( "Invalid XML: " + data );
		}
		return xml;
	};


	var
		rhash = /#.*$/,
		rts = /([?&])_=[^&]*/,
		rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,
		// #7653, #8125, #8152: local protocol detection
		rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
		rnoContent = /^(?:GET|HEAD)$/,
		rprotocol = /^\/\//,
		rurl = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,

		/* Prefilters
		 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
		 * 2) These are called:
		 *    - BEFORE asking for a transport
		 *    - AFTER param serialization (s.data is a string if s.processData is true)
		 * 3) key is the dataType
		 * 4) the catchall symbol "*" can be used
		 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
		 */
		prefilters = {},

		/* Transports bindings
		 * 1) key is the dataType
		 * 2) the catchall symbol "*" can be used
		 * 3) selection will start with transport dataType and THEN go to "*" if needed
		 */
		transports = {},

		// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
		allTypes = "*/".concat( "*" ),

		// Document location
		ajaxLocation = window.location.href,

		// Segment location into parts
		ajaxLocParts = rurl.exec( ajaxLocation.toLowerCase() ) || [];

	// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
	function addToPrefiltersOrTransports( structure ) {

		// dataTypeExpression is optional and defaults to "*"
		return function( dataTypeExpression, func ) {

			if ( typeof dataTypeExpression !== "string" ) {
				func = dataTypeExpression;
				dataTypeExpression = "*";
			}

			var dataType,
				i = 0,
				dataTypes = dataTypeExpression.toLowerCase().match( rnotwhite ) || [];

			if ( jQuery.isFunction( func ) ) {
				// For each dataType in the dataTypeExpression
				while ( (dataType = dataTypes[i++]) ) {
					// Prepend if requested
					if ( dataType[0] === "+" ) {
						dataType = dataType.slice( 1 ) || "*";
						(structure[ dataType ] = structure[ dataType ] || []).unshift( func );

					// Otherwise append
					} else {
						(structure[ dataType ] = structure[ dataType ] || []).push( func );
					}
				}
			}
		};
	}

	// Base inspection function for prefilters and transports
	function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {

		var inspected = {},
			seekingTransport = ( structure === transports );

		function inspect( dataType ) {
			var selected;
			inspected[ dataType ] = true;
			jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
				var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
				if ( typeof dataTypeOrTransport === "string" && !seekingTransport && !inspected[ dataTypeOrTransport ] ) {
					options.dataTypes.unshift( dataTypeOrTransport );
					inspect( dataTypeOrTransport );
					return false;
				} else if ( seekingTransport ) {
					return !( selected = dataTypeOrTransport );
				}
			});
			return selected;
		}

		return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
	}

	// A special extend for ajax options
	// that takes "flat" options (not to be deep extended)
	// Fixes #9887
	function ajaxExtend( target, src ) {
		var key, deep,
			flatOptions = jQuery.ajaxSettings.flatOptions || {};

		for ( key in src ) {
			if ( src[ key ] !== undefined ) {
				( flatOptions[ key ] ? target : ( deep || (deep = {}) ) )[ key ] = src[ key ];
			}
		}
		if ( deep ) {
			jQuery.extend( true, target, deep );
		}

		return target;
	}

	/* Handles responses to an ajax request:
	 * - finds the right dataType (mediates between content-type and expected dataType)
	 * - returns the corresponding response
	 */
	function ajaxHandleResponses( s, jqXHR, responses ) {

		var ct, type, finalDataType, firstDataType,
			contents = s.contents,
			dataTypes = s.dataTypes;

		// Remove auto dataType and get content-type in the process
		while ( dataTypes[ 0 ] === "*" ) {
			dataTypes.shift();
			if ( ct === undefined ) {
				ct = s.mimeType || jqXHR.getResponseHeader("Content-Type");
			}
		}

		// Check if we're dealing with a known content-type
		if ( ct ) {
			for ( type in contents ) {
				if ( contents[ type ] && contents[ type ].test( ct ) ) {
					dataTypes.unshift( type );
					break;
				}
			}
		}

		// Check to see if we have a response for the expected dataType
		if ( dataTypes[ 0 ] in responses ) {
			finalDataType = dataTypes[ 0 ];
		} else {
			// Try convertible dataTypes
			for ( type in responses ) {
				if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[0] ] ) {
					finalDataType = type;
					break;
				}
				if ( !firstDataType ) {
					firstDataType = type;
				}
			}
			// Or just use first one
			finalDataType = finalDataType || firstDataType;
		}

		// If we found a dataType
		// We add the dataType to the list if needed
		// and return the corresponding response
		if ( finalDataType ) {
			if ( finalDataType !== dataTypes[ 0 ] ) {
				dataTypes.unshift( finalDataType );
			}
			return responses[ finalDataType ];
		}
	}

	/* Chain conversions given the request and the original response
	 * Also sets the responseXXX fields on the jqXHR instance
	 */
	function ajaxConvert( s, response, jqXHR, isSuccess ) {
		var conv2, current, conv, tmp, prev,
			converters = {},
			// Work with a copy of dataTypes in case we need to modify it for conversion
			dataTypes = s.dataTypes.slice();

		// Create converters map with lowercased keys
		if ( dataTypes[ 1 ] ) {
			for ( conv in s.converters ) {
				converters[ conv.toLowerCase() ] = s.converters[ conv ];
			}
		}

		current = dataTypes.shift();

		// Convert to each sequential dataType
		while ( current ) {

			if ( s.responseFields[ current ] ) {
				jqXHR[ s.responseFields[ current ] ] = response;
			}

			// Apply the dataFilter if provided
			if ( !prev && isSuccess && s.dataFilter ) {
				response = s.dataFilter( response, s.dataType );
			}

			prev = current;
			current = dataTypes.shift();

			if ( current ) {

			// There's only work to do if current dataType is non-auto
				if ( current === "*" ) {

					current = prev;

				// Convert response if prev dataType is non-auto and differs from current
				} else if ( prev !== "*" && prev !== current ) {

					// Seek a direct converter
					conv = converters[ prev + " " + current ] || converters[ "* " + current ];

					// If none found, seek a pair
					if ( !conv ) {
						for ( conv2 in converters ) {

							// If conv2 outputs current
							tmp = conv2.split( " " );
							if ( tmp[ 1 ] === current ) {

								// If prev can be converted to accepted input
								conv = converters[ prev + " " + tmp[ 0 ] ] ||
									converters[ "* " + tmp[ 0 ] ];
								if ( conv ) {
									// Condense equivalence converters
									if ( conv === true ) {
										conv = converters[ conv2 ];

									// Otherwise, insert the intermediate dataType
									} else if ( converters[ conv2 ] !== true ) {
										current = tmp[ 0 ];
										dataTypes.unshift( tmp[ 1 ] );
									}
									break;
								}
							}
						}
					}

					// Apply converter (if not an equivalence)
					if ( conv !== true ) {

						// Unless errors are allowed to bubble, catch and return them
						if ( conv && s[ "throws" ] ) {
							response = conv( response );
						} else {
							try {
								response = conv( response );
							} catch ( e ) {
								return { state: "parsererror", error: conv ? e : "No conversion from " + prev + " to " + current };
							}
						}
					}
				}
			}
		}

		return { state: "success", data: response };
	}

	jQuery.extend({

		// Counter for holding the number of active queries
		active: 0,

		// Last-Modified header cache for next request
		lastModified: {},
		etag: {},

		ajaxSettings: {
			url: ajaxLocation,
			type: "GET",
			isLocal: rlocalProtocol.test( ajaxLocParts[ 1 ] ),
			global: true,
			processData: true,
			async: true,
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			/*
			timeout: 0,
			data: null,
			dataType: null,
			username: null,
			password: null,
			cache: null,
			throws: false,
			traditional: false,
			headers: {},
			*/

			accepts: {
				"*": allTypes,
				text: "text/plain",
				html: "text/html",
				xml: "application/xml, text/xml",
				json: "application/json, text/javascript"
			},

			contents: {
				xml: /xml/,
				html: /html/,
				json: /json/
			},

			responseFields: {
				xml: "responseXML",
				text: "responseText",
				json: "responseJSON"
			},

			// Data converters
			// Keys separate source (or catchall "*") and destination types with a single space
			converters: {

				// Convert anything to text
				"* text": String,

				// Text to html (true = no transformation)
				"text html": true,

				// Evaluate text as a json expression
				"text json": jQuery.parseJSON,

				// Parse text as xml
				"text xml": jQuery.parseXML
			},

			// For options that shouldn't be deep extended:
			// you can add your own custom options here if
			// and when you create one that shouldn't be
			// deep extended (see ajaxExtend)
			flatOptions: {
				url: true,
				context: true
			}
		},

		// Creates a full fledged settings object into target
		// with both ajaxSettings and settings fields.
		// If target is omitted, writes into ajaxSettings.
		ajaxSetup: function( target, settings ) {
			return settings ?

				// Building a settings object
				ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :

				// Extending ajaxSettings
				ajaxExtend( jQuery.ajaxSettings, target );
		},

		ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
		ajaxTransport: addToPrefiltersOrTransports( transports ),

		// Main method
		ajax: function( url, options ) {

			// If url is an object, simulate pre-1.5 signature
			if ( typeof url === "object" ) {
				options = url;
				url = undefined;
			}

			// Force options to be an object
			options = options || {};

			var transport,
				// URL without anti-cache param
				cacheURL,
				// Response headers
				responseHeadersString,
				responseHeaders,
				// timeout handle
				timeoutTimer,
				// Cross-domain detection vars
				parts,
				// To know if global events are to be dispatched
				fireGlobals,
				// Loop variable
				i,
				// Create the final options object
				s = jQuery.ajaxSetup( {}, options ),
				// Callbacks context
				callbackContext = s.context || s,
				// Context for global events is callbackContext if it is a DOM node or jQuery collection
				globalEventContext = s.context && ( callbackContext.nodeType || callbackContext.jquery ) ?
					jQuery( callbackContext ) :
					jQuery.event,
				// Deferreds
				deferred = jQuery.Deferred(),
				completeDeferred = jQuery.Callbacks("once memory"),
				// Status-dependent callbacks
				statusCode = s.statusCode || {},
				// Headers (they are sent all at once)
				requestHeaders = {},
				requestHeadersNames = {},
				// The jqXHR state
				state = 0,
				// Default abort message
				strAbort = "canceled",
				// Fake xhr
				jqXHR = {
					readyState: 0,

					// Builds headers hashtable if needed
					getResponseHeader: function( key ) {
						var match;
						if ( state === 2 ) {
							if ( !responseHeaders ) {
								responseHeaders = {};
								while ( (match = rheaders.exec( responseHeadersString )) ) {
									responseHeaders[ match[1].toLowerCase() ] = match[ 2 ];
								}
							}
							match = responseHeaders[ key.toLowerCase() ];
						}
						return match == null ? null : match;
					},

					// Raw string
					getAllResponseHeaders: function() {
						return state === 2 ? responseHeadersString : null;
					},

					// Caches the header
					setRequestHeader: function( name, value ) {
						var lname = name.toLowerCase();
						if ( !state ) {
							name = requestHeadersNames[ lname ] = requestHeadersNames[ lname ] || name;
							requestHeaders[ name ] = value;
						}
						return this;
					},

					// Overrides response content-type header
					overrideMimeType: function( type ) {
						if ( !state ) {
							s.mimeType = type;
						}
						return this;
					},

					// Status-dependent callbacks
					statusCode: function( map ) {
						var code;
						if ( map ) {
							if ( state < 2 ) {
								for ( code in map ) {
									// Lazy-add the new callback in a way that preserves old ones
									statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
								}
							} else {
								// Execute the appropriate callbacks
								jqXHR.always( map[ jqXHR.status ] );
							}
						}
						return this;
					},

					// Cancel the request
					abort: function( statusText ) {
						var finalText = statusText || strAbort;
						if ( transport ) {
							transport.abort( finalText );
						}
						done( 0, finalText );
						return this;
					}
				};

			// Attach deferreds
			deferred.promise( jqXHR ).complete = completeDeferred.add;
			jqXHR.success = jqXHR.done;
			jqXHR.error = jqXHR.fail;

			// Remove hash character (#7531: and string promotion)
			// Add protocol if not provided (prefilters might expect it)
			// Handle falsy url in the settings object (#10093: consistency with old signature)
			// We also use the url parameter if available
			s.url = ( ( url || s.url || ajaxLocation ) + "" ).replace( rhash, "" )
				.replace( rprotocol, ajaxLocParts[ 1 ] + "//" );

			// Alias method option to type as per ticket #12004
			s.type = options.method || options.type || s.method || s.type;

			// Extract dataTypes list
			s.dataTypes = jQuery.trim( s.dataType || "*" ).toLowerCase().match( rnotwhite ) || [ "" ];

			// A cross-domain request is in order when we have a protocol:host:port mismatch
			if ( s.crossDomain == null ) {
				parts = rurl.exec( s.url.toLowerCase() );
				s.crossDomain = !!( parts &&
					( parts[ 1 ] !== ajaxLocParts[ 1 ] || parts[ 2 ] !== ajaxLocParts[ 2 ] ||
						( parts[ 3 ] || ( parts[ 1 ] === "http:" ? "80" : "443" ) ) !==
							( ajaxLocParts[ 3 ] || ( ajaxLocParts[ 1 ] === "http:" ? "80" : "443" ) ) )
				);
			}

			// Convert data if not already a string
			if ( s.data && s.processData && typeof s.data !== "string" ) {
				s.data = jQuery.param( s.data, s.traditional );
			}

			// Apply prefilters
			inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

			// If request was aborted inside a prefilter, stop there
			if ( state === 2 ) {
				return jqXHR;
			}

			// We can fire global events as of now if asked to
			// Don't fire events if jQuery.event is undefined in an AMD-usage scenario (#15118)
			fireGlobals = jQuery.event && s.global;

			// Watch for a new set of requests
			if ( fireGlobals && jQuery.active++ === 0 ) {
				jQuery.event.trigger("ajaxStart");
			}

			// Uppercase the type
			s.type = s.type.toUpperCase();

			// Determine if request has content
			s.hasContent = !rnoContent.test( s.type );

			// Save the URL in case we're toying with the If-Modified-Since
			// and/or If-None-Match header later on
			cacheURL = s.url;

			// More options handling for requests with no content
			if ( !s.hasContent ) {

				// If data is available, append data to url
				if ( s.data ) {
					cacheURL = ( s.url += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data );
					// #9682: remove data so that it's not used in an eventual retry
					delete s.data;
				}

				// Add anti-cache in url if needed
				if ( s.cache === false ) {
					s.url = rts.test( cacheURL ) ?

						// If there is already a '_' parameter, set its value
						cacheURL.replace( rts, "$1_=" + nonce++ ) :

						// Otherwise add one to the end
						cacheURL + ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + nonce++;
				}
			}

			// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
			if ( s.ifModified ) {
				if ( jQuery.lastModified[ cacheURL ] ) {
					jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
				}
				if ( jQuery.etag[ cacheURL ] ) {
					jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
				}
			}

			// Set the correct header, if data is being sent
			if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
				jqXHR.setRequestHeader( "Content-Type", s.contentType );
			}

			// Set the Accepts header for the server, depending on the dataType
			jqXHR.setRequestHeader(
				"Accept",
				s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[0] ] ?
					s.accepts[ s.dataTypes[0] ] + ( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
					s.accepts[ "*" ]
			);

			// Check for headers option
			for ( i in s.headers ) {
				jqXHR.setRequestHeader( i, s.headers[ i ] );
			}

			// Allow custom headers/mimetypes and early abort
			if ( s.beforeSend && ( s.beforeSend.call( callbackContext, jqXHR, s ) === false || state === 2 ) ) {
				// Abort if not done already and return
				return jqXHR.abort();
			}

			// Aborting is no longer a cancellation
			strAbort = "abort";

			// Install callbacks on deferreds
			for ( i in { success: 1, error: 1, complete: 1 } ) {
				jqXHR[ i ]( s[ i ] );
			}

			// Get transport
			transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

			// If no transport, we auto-abort
			if ( !transport ) {
				done( -1, "No Transport" );
			} else {
				jqXHR.readyState = 1;

				// Send global event
				if ( fireGlobals ) {
					globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
				}
				// Timeout
				if ( s.async && s.timeout > 0 ) {
					timeoutTimer = setTimeout(function() {
						jqXHR.abort("timeout");
					}, s.timeout );
				}

				try {
					state = 1;
					transport.send( requestHeaders, done );
				} catch ( e ) {
					// Propagate exception as error if not done
					if ( state < 2 ) {
						done( -1, e );
					// Simply rethrow otherwise
					} else {
						throw e;
					}
				}
			}

			// Callback for when everything is done
			function done( status, nativeStatusText, responses, headers ) {
				var isSuccess, success, error, response, modified,
					statusText = nativeStatusText;

				// Called once
				if ( state === 2 ) {
					return;
				}

				// State is "done" now
				state = 2;

				// Clear timeout if it exists
				if ( timeoutTimer ) {
					clearTimeout( timeoutTimer );
				}

				// Dereference transport for early garbage collection
				// (no matter how long the jqXHR object will be used)
				transport = undefined;

				// Cache response headers
				responseHeadersString = headers || "";

				// Set readyState
				jqXHR.readyState = status > 0 ? 4 : 0;

				// Determine if successful
				isSuccess = status >= 200 && status < 300 || status === 304;

				// Get response data
				if ( responses ) {
					response = ajaxHandleResponses( s, jqXHR, responses );
				}

				// Convert no matter what (that way responseXXX fields are always set)
				response = ajaxConvert( s, response, jqXHR, isSuccess );

				// If successful, handle type chaining
				if ( isSuccess ) {

					// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
					if ( s.ifModified ) {
						modified = jqXHR.getResponseHeader("Last-Modified");
						if ( modified ) {
							jQuery.lastModified[ cacheURL ] = modified;
						}
						modified = jqXHR.getResponseHeader("etag");
						if ( modified ) {
							jQuery.etag[ cacheURL ] = modified;
						}
					}

					// if no content
					if ( status === 204 || s.type === "HEAD" ) {
						statusText = "nocontent";

					// if not modified
					} else if ( status === 304 ) {
						statusText = "notmodified";

					// If we have data, let's convert it
					} else {
						statusText = response.state;
						success = response.data;
						error = response.error;
						isSuccess = !error;
					}
				} else {
					// Extract error from statusText and normalize for non-aborts
					error = statusText;
					if ( status || !statusText ) {
						statusText = "error";
						if ( status < 0 ) {
							status = 0;
						}
					}
				}

				// Set data for the fake xhr object
				jqXHR.status = status;
				jqXHR.statusText = ( nativeStatusText || statusText ) + "";

				// Success/Error
				if ( isSuccess ) {
					deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
				} else {
					deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
				}

				// Status-dependent callbacks
				jqXHR.statusCode( statusCode );
				statusCode = undefined;

				if ( fireGlobals ) {
					globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
						[ jqXHR, s, isSuccess ? success : error ] );
				}

				// Complete
				completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

				if ( fireGlobals ) {
					globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );
					// Handle the global AJAX counter
					if ( !( --jQuery.active ) ) {
						jQuery.event.trigger("ajaxStop");
					}
				}
			}

			return jqXHR;
		},

		getJSON: function( url, data, callback ) {
			return jQuery.get( url, data, callback, "json" );
		},

		getScript: function( url, callback ) {
			return jQuery.get( url, undefined, callback, "script" );
		}
	});

	jQuery.each( [ "get", "post" ], function( i, method ) {
		jQuery[ method ] = function( url, data, callback, type ) {
			// Shift arguments if data argument was omitted
			if ( jQuery.isFunction( data ) ) {
				type = type || callback;
				callback = data;
				data = undefined;
			}

			return jQuery.ajax({
				url: url,
				type: method,
				dataType: type,
				data: data,
				success: callback
			});
		};
	});


	jQuery._evalUrl = function( url ) {
		return jQuery.ajax({
			url: url,
			type: "GET",
			dataType: "script",
			async: false,
			global: false,
			"throws": true
		});
	};


	jQuery.fn.extend({
		wrapAll: function( html ) {
			var wrap;

			if ( jQuery.isFunction( html ) ) {
				return this.each(function( i ) {
					jQuery( this ).wrapAll( html.call(this, i) );
				});
			}

			if ( this[ 0 ] ) {

				// The elements to wrap the target around
				wrap = jQuery( html, this[ 0 ].ownerDocument ).eq( 0 ).clone( true );

				if ( this[ 0 ].parentNode ) {
					wrap.insertBefore( this[ 0 ] );
				}

				wrap.map(function() {
					var elem = this;

					while ( elem.firstElementChild ) {
						elem = elem.firstElementChild;
					}

					return elem;
				}).append( this );
			}

			return this;
		},

		wrapInner: function( html ) {
			if ( jQuery.isFunction( html ) ) {
				return this.each(function( i ) {
					jQuery( this ).wrapInner( html.call(this, i) );
				});
			}

			return this.each(function() {
				var self = jQuery( this ),
					contents = self.contents();

				if ( contents.length ) {
					contents.wrapAll( html );

				} else {
					self.append( html );
				}
			});
		},

		wrap: function( html ) {
			var isFunction = jQuery.isFunction( html );

			return this.each(function( i ) {
				jQuery( this ).wrapAll( isFunction ? html.call(this, i) : html );
			});
		},

		unwrap: function() {
			return this.parent().each(function() {
				if ( !jQuery.nodeName( this, "body" ) ) {
					jQuery( this ).replaceWith( this.childNodes );
				}
			}).end();
		}
	});


	jQuery.expr.filters.hidden = function( elem ) {
		// Support: Opera <= 12.12
		// Opera reports offsetWidths and offsetHeights less than zero on some elements
		return elem.offsetWidth <= 0 && elem.offsetHeight <= 0;
	};
	jQuery.expr.filters.visible = function( elem ) {
		return !jQuery.expr.filters.hidden( elem );
	};




	var r20 = /%20/g,
		rbracket = /\[\]$/,
		rCRLF = /\r?\n/g,
		rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
		rsubmittable = /^(?:input|select|textarea|keygen)/i;

	function buildParams( prefix, obj, traditional, add ) {
		var name;

		if ( jQuery.isArray( obj ) ) {
			// Serialize array item.
			jQuery.each( obj, function( i, v ) {
				if ( traditional || rbracket.test( prefix ) ) {
					// Treat each array item as a scalar.
					add( prefix, v );

				} else {
					// Item is non-scalar (array or object), encode its numeric index.
					buildParams( prefix + "[" + ( typeof v === "object" ? i : "" ) + "]", v, traditional, add );
				}
			});

		} else if ( !traditional && jQuery.type( obj ) === "object" ) {
			// Serialize object item.
			for ( name in obj ) {
				buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
			}

		} else {
			// Serialize scalar item.
			add( prefix, obj );
		}
	}

	// Serialize an array of form elements or a set of
	// key/values into a query string
	jQuery.param = function( a, traditional ) {
		var prefix,
			s = [],
			add = function( key, value ) {
				// If value is a function, invoke it and return its value
				value = jQuery.isFunction( value ) ? value() : ( value == null ? "" : value );
				s[ s.length ] = encodeURIComponent( key ) + "=" + encodeURIComponent( value );
			};

		// Set traditional to true for jQuery <= 1.3.2 behavior.
		if ( traditional === undefined ) {
			traditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional;
		}

		// If an array was passed in, assume that it is an array of form elements.
		if ( jQuery.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {
			// Serialize the form elements
			jQuery.each( a, function() {
				add( this.name, this.value );
			});

		} else {
			// If traditional, encode the "old" way (the way 1.3.2 or older
			// did it), otherwise encode params recursively.
			for ( prefix in a ) {
				buildParams( prefix, a[ prefix ], traditional, add );
			}
		}

		// Return the resulting serialization
		return s.join( "&" ).replace( r20, "+" );
	};

	jQuery.fn.extend({
		serialize: function() {
			return jQuery.param( this.serializeArray() );
		},
		serializeArray: function() {
			return this.map(function() {
				// Can add propHook for "elements" to filter or add form elements
				var elements = jQuery.prop( this, "elements" );
				return elements ? jQuery.makeArray( elements ) : this;
			})
			.filter(function() {
				var type = this.type;

				// Use .is( ":disabled" ) so that fieldset[disabled] works
				return this.name && !jQuery( this ).is( ":disabled" ) &&
					rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
					( this.checked || !rcheckableType.test( type ) );
			})
			.map(function( i, elem ) {
				var val = jQuery( this ).val();

				return val == null ?
					null :
					jQuery.isArray( val ) ?
						jQuery.map( val, function( val ) {
							return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
						}) :
						{ name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
			}).get();
		}
	});


	jQuery.ajaxSettings.xhr = function() {
		try {
			return new XMLHttpRequest();
		} catch( e ) {}
	};

	var xhrId = 0,
		xhrCallbacks = {},
		xhrSuccessStatus = {
			// file protocol always yields status code 0, assume 200
			0: 200,
			// Support: IE9
			// #1450: sometimes IE returns 1223 when it should be 204
			1223: 204
		},
		xhrSupported = jQuery.ajaxSettings.xhr();

	// Support: IE9
	// Open requests must be manually aborted on unload (#5280)
	// See https://support.microsoft.com/kb/2856746 for more info
	if ( window.attachEvent ) {
		window.attachEvent( "onunload", function() {
			for ( var key in xhrCallbacks ) {
				xhrCallbacks[ key ]();
			}
		});
	}

	support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
	support.ajax = xhrSupported = !!xhrSupported;

	jQuery.ajaxTransport(function( options ) {
		var callback;

		// Cross domain only allowed if supported through XMLHttpRequest
		if ( support.cors || xhrSupported && !options.crossDomain ) {
			return {
				send: function( headers, complete ) {
					var i,
						xhr = options.xhr(),
						id = ++xhrId;

					xhr.open( options.type, options.url, options.async, options.username, options.password );

					// Apply custom fields if provided
					if ( options.xhrFields ) {
						for ( i in options.xhrFields ) {
							xhr[ i ] = options.xhrFields[ i ];
						}
					}

					// Override mime type if needed
					if ( options.mimeType && xhr.overrideMimeType ) {
						xhr.overrideMimeType( options.mimeType );
					}

					// X-Requested-With header
					// For cross-domain requests, seeing as conditions for a preflight are
					// akin to a jigsaw puzzle, we simply never set it to be sure.
					// (it can always be set on a per-request basis or even using ajaxSetup)
					// For same-domain requests, won't change header if already provided.
					if ( !options.crossDomain && !headers["X-Requested-With"] ) {
						headers["X-Requested-With"] = "XMLHttpRequest";
					}

					// Set headers
					for ( i in headers ) {
						xhr.setRequestHeader( i, headers[ i ] );
					}

					// Callback
					callback = function( type ) {
						return function() {
							if ( callback ) {
								delete xhrCallbacks[ id ];
								callback = xhr.onload = xhr.onerror = null;

								if ( type === "abort" ) {
									xhr.abort();
								} else if ( type === "error" ) {
									complete(
										// file: protocol always yields status 0; see #8605, #14207
										xhr.status,
										xhr.statusText
									);
								} else {
									complete(
										xhrSuccessStatus[ xhr.status ] || xhr.status,
										xhr.statusText,
										// Support: IE9
										// Accessing binary-data responseText throws an exception
										// (#11426)
										typeof xhr.responseText === "string" ? {
											text: xhr.responseText
										} : undefined,
										xhr.getAllResponseHeaders()
									);
								}
							}
						};
					};

					// Listen to events
					xhr.onload = callback();
					xhr.onerror = callback("error");

					// Create the abort callback
					callback = xhrCallbacks[ id ] = callback("abort");

					try {
						// Do send the request (this may raise an exception)
						xhr.send( options.hasContent && options.data || null );
					} catch ( e ) {
						// #14683: Only rethrow if this hasn't been notified as an error yet
						if ( callback ) {
							throw e;
						}
					}
				},

				abort: function() {
					if ( callback ) {
						callback();
					}
				}
			};
		}
	});




	// Install script dataType
	jQuery.ajaxSetup({
		accepts: {
			script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
		},
		contents: {
			script: /(?:java|ecma)script/
		},
		converters: {
			"text script": function( text ) {
				jQuery.globalEval( text );
				return text;
			}
		}
	});

	// Handle cache's special case and crossDomain
	jQuery.ajaxPrefilter( "script", function( s ) {
		if ( s.cache === undefined ) {
			s.cache = false;
		}
		if ( s.crossDomain ) {
			s.type = "GET";
		}
	});

	// Bind script tag hack transport
	jQuery.ajaxTransport( "script", function( s ) {
		// This transport only deals with cross domain requests
		if ( s.crossDomain ) {
			var script, callback;
			return {
				send: function( _, complete ) {
					script = jQuery("<script>").prop({
						async: true,
						charset: s.scriptCharset,
						src: s.url
					}).on(
						"load error",
						callback = function( evt ) {
							script.remove();
							callback = null;
							if ( evt ) {
								complete( evt.type === "error" ? 404 : 200, evt.type );
							}
						}
					);
					document.head.appendChild( script[ 0 ] );
				},
				abort: function() {
					if ( callback ) {
						callback();
					}
				}
			};
		}
	});




	var oldCallbacks = [],
		rjsonp = /(=)\?(?=&|$)|\?\?/;

	// Default jsonp settings
	jQuery.ajaxSetup({
		jsonp: "callback",
		jsonpCallback: function() {
			var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce++ ) );
			this[ callback ] = true;
			return callback;
		}
	});

	// Detect, normalize options and install callbacks for jsonp requests
	jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

		var callbackName, overwritten, responseContainer,
			jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
				"url" :
				typeof s.data === "string" && !( s.contentType || "" ).indexOf("application/x-www-form-urlencoded") && rjsonp.test( s.data ) && "data"
			);

		// Handle iff the expected data type is "jsonp" or we have a parameter to set
		if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {

			// Get callback name, remembering preexisting value associated with it
			callbackName = s.jsonpCallback = jQuery.isFunction( s.jsonpCallback ) ?
				s.jsonpCallback() :
				s.jsonpCallback;

			// Insert callback into url or form data
			if ( jsonProp ) {
				s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
			} else if ( s.jsonp !== false ) {
				s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
			}

			// Use data converter to retrieve json after script execution
			s.converters["script json"] = function() {
				if ( !responseContainer ) {
					jQuery.error( callbackName + " was not called" );
				}
				return responseContainer[ 0 ];
			};

			// force json dataType
			s.dataTypes[ 0 ] = "json";

			// Install callback
			overwritten = window[ callbackName ];
			window[ callbackName ] = function() {
				responseContainer = arguments;
			};

			// Clean-up function (fires after converters)
			jqXHR.always(function() {
				// Restore preexisting value
				window[ callbackName ] = overwritten;

				// Save back as free
				if ( s[ callbackName ] ) {
					// make sure that re-using the options doesn't screw things around
					s.jsonpCallback = originalSettings.jsonpCallback;

					// save the callback name for future use
					oldCallbacks.push( callbackName );
				}

				// Call if it was a function and we have a response
				if ( responseContainer && jQuery.isFunction( overwritten ) ) {
					overwritten( responseContainer[ 0 ] );
				}

				responseContainer = overwritten = undefined;
			});

			// Delegate to script
			return "script";
		}
	});




	// data: string of html
	// context (optional): If specified, the fragment will be created in this context, defaults to document
	// keepScripts (optional): If true, will include scripts passed in the html string
	jQuery.parseHTML = function( data, context, keepScripts ) {
		if ( !data || typeof data !== "string" ) {
			return null;
		}
		if ( typeof context === "boolean" ) {
			keepScripts = context;
			context = false;
		}
		context = context || document;

		var parsed = rsingleTag.exec( data ),
			scripts = !keepScripts && [];

		// Single tag
		if ( parsed ) {
			return [ context.createElement( parsed[1] ) ];
		}

		parsed = jQuery.buildFragment( [ data ], context, scripts );

		if ( scripts && scripts.length ) {
			jQuery( scripts ).remove();
		}

		return jQuery.merge( [], parsed.childNodes );
	};


	// Keep a copy of the old load method
	var _load = jQuery.fn.load;

	/**
	 * Load a url into a page
	 */
	jQuery.fn.load = function( url, params, callback ) {
		if ( typeof url !== "string" && _load ) {
			return _load.apply( this, arguments );
		}

		var selector, type, response,
			self = this,
			off = url.indexOf(" ");

		if ( off >= 0 ) {
			selector = jQuery.trim( url.slice( off ) );
			url = url.slice( 0, off );
		}

		// If it's a function
		if ( jQuery.isFunction( params ) ) {

			// We assume that it's the callback
			callback = params;
			params = undefined;

		// Otherwise, build a param string
		} else if ( params && typeof params === "object" ) {
			type = "POST";
		}

		// If we have elements to modify, make the request
		if ( self.length > 0 ) {
			jQuery.ajax({
				url: url,

				// if "type" variable is undefined, then "GET" method will be used
				type: type,
				dataType: "html",
				data: params
			}).done(function( responseText ) {

				// Save response for use in complete callback
				response = arguments;

				self.html( selector ?

					// If a selector was specified, locate the right elements in a dummy div
					// Exclude scripts to avoid IE 'Permission Denied' errors
					jQuery("<div>").append( jQuery.parseHTML( responseText ) ).find( selector ) :

					// Otherwise use the full result
					responseText );

			}).complete( callback && function( jqXHR, status ) {
				self.each( callback, response || [ jqXHR.responseText, status, jqXHR ] );
			});
		}

		return this;
	};




	// Attach a bunch of functions for handling common AJAX events
	jQuery.each( [ "ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend" ], function( i, type ) {
		jQuery.fn[ type ] = function( fn ) {
			return this.on( type, fn );
		};
	});




	jQuery.expr.filters.animated = function( elem ) {
		return jQuery.grep(jQuery.timers, function( fn ) {
			return elem === fn.elem;
		}).length;
	};




	var docElem = window.document.documentElement;

	/**
	 * Gets a window from an element
	 */
	function getWindow( elem ) {
		return jQuery.isWindow( elem ) ? elem : elem.nodeType === 9 && elem.defaultView;
	}

	jQuery.offset = {
		setOffset: function( elem, options, i ) {
			var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
				position = jQuery.css( elem, "position" ),
				curElem = jQuery( elem ),
				props = {};

			// Set position first, in-case top/left are set even on static elem
			if ( position === "static" ) {
				elem.style.position = "relative";
			}

			curOffset = curElem.offset();
			curCSSTop = jQuery.css( elem, "top" );
			curCSSLeft = jQuery.css( elem, "left" );
			calculatePosition = ( position === "absolute" || position === "fixed" ) &&
				( curCSSTop + curCSSLeft ).indexOf("auto") > -1;

			// Need to be able to calculate position if either
			// top or left is auto and position is either absolute or fixed
			if ( calculatePosition ) {
				curPosition = curElem.position();
				curTop = curPosition.top;
				curLeft = curPosition.left;

			} else {
				curTop = parseFloat( curCSSTop ) || 0;
				curLeft = parseFloat( curCSSLeft ) || 0;
			}

			if ( jQuery.isFunction( options ) ) {
				options = options.call( elem, i, curOffset );
			}

			if ( options.top != null ) {
				props.top = ( options.top - curOffset.top ) + curTop;
			}
			if ( options.left != null ) {
				props.left = ( options.left - curOffset.left ) + curLeft;
			}

			if ( "using" in options ) {
				options.using.call( elem, props );

			} else {
				curElem.css( props );
			}
		}
	};

	jQuery.fn.extend({
		offset: function( options ) {
			if ( arguments.length ) {
				return options === undefined ?
					this :
					this.each(function( i ) {
						jQuery.offset.setOffset( this, options, i );
					});
			}

			var docElem, win,
				elem = this[ 0 ],
				box = { top: 0, left: 0 },
				doc = elem && elem.ownerDocument;

			if ( !doc ) {
				return;
			}

			docElem = doc.documentElement;

			// Make sure it's not a disconnected DOM node
			if ( !jQuery.contains( docElem, elem ) ) {
				return box;
			}

			// Support: BlackBerry 5, iOS 3 (original iPhone)
			// If we don't have gBCR, just use 0,0 rather than error
			if ( typeof elem.getBoundingClientRect !== strundefined ) {
				box = elem.getBoundingClientRect();
			}
			win = getWindow( doc );
			return {
				top: box.top + win.pageYOffset - docElem.clientTop,
				left: box.left + win.pageXOffset - docElem.clientLeft
			};
		},

		position: function() {
			if ( !this[ 0 ] ) {
				return;
			}

			var offsetParent, offset,
				elem = this[ 0 ],
				parentOffset = { top: 0, left: 0 };

			// Fixed elements are offset from window (parentOffset = {top:0, left: 0}, because it is its only offset parent
			if ( jQuery.css( elem, "position" ) === "fixed" ) {
				// Assume getBoundingClientRect is there when computed position is fixed
				offset = elem.getBoundingClientRect();

			} else {
				// Get *real* offsetParent
				offsetParent = this.offsetParent();

				// Get correct offsets
				offset = this.offset();
				if ( !jQuery.nodeName( offsetParent[ 0 ], "html" ) ) {
					parentOffset = offsetParent.offset();
				}

				// Add offsetParent borders
				parentOffset.top += jQuery.css( offsetParent[ 0 ], "borderTopWidth", true );
				parentOffset.left += jQuery.css( offsetParent[ 0 ], "borderLeftWidth", true );
			}

			// Subtract parent offsets and element margins
			return {
				top: offset.top - parentOffset.top - jQuery.css( elem, "marginTop", true ),
				left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true )
			};
		},

		offsetParent: function() {
			return this.map(function() {
				var offsetParent = this.offsetParent || docElem;

				while ( offsetParent && ( !jQuery.nodeName( offsetParent, "html" ) && jQuery.css( offsetParent, "position" ) === "static" ) ) {
					offsetParent = offsetParent.offsetParent;
				}

				return offsetParent || docElem;
			});
		}
	});

	// Create scrollLeft and scrollTop methods
	jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
		var top = "pageYOffset" === prop;

		jQuery.fn[ method ] = function( val ) {
			return access( this, function( elem, method, val ) {
				var win = getWindow( elem );

				if ( val === undefined ) {
					return win ? win[ prop ] : elem[ method ];
				}

				if ( win ) {
					win.scrollTo(
						!top ? val : window.pageXOffset,
						top ? val : window.pageYOffset
					);

				} else {
					elem[ method ] = val;
				}
			}, method, val, arguments.length, null );
		};
	});

	// Support: Safari<7+, Chrome<37+
	// Add the top/left cssHooks using jQuery.fn.position
	// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
	// Blink bug: https://code.google.com/p/chromium/issues/detail?id=229280
	// getComputedStyle returns percent when specified for top/left/bottom/right;
	// rather than make the css module depend on the offset module, just check for it here
	jQuery.each( [ "top", "left" ], function( i, prop ) {
		jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
			function( elem, computed ) {
				if ( computed ) {
					computed = curCSS( elem, prop );
					// If curCSS returns percentage, fallback to offset
					return rnumnonpx.test( computed ) ?
						jQuery( elem ).position()[ prop ] + "px" :
						computed;
				}
			}
		);
	});


	// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
	jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
		jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name }, function( defaultExtra, funcName ) {
			// Margin is only for outerHeight, outerWidth
			jQuery.fn[ funcName ] = function( margin, value ) {
				var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
					extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );

				return access( this, function( elem, type, value ) {
					var doc;

					if ( jQuery.isWindow( elem ) ) {
						// As of 5/8/2012 this will yield incorrect results for Mobile Safari, but there
						// isn't a whole lot we can do. See pull request at this URL for discussion:
						// https://github.com/jquery/jquery/pull/764
						return elem.document.documentElement[ "client" + name ];
					}

					// Get document width or height
					if ( elem.nodeType === 9 ) {
						doc = elem.documentElement;

						// Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
						// whichever is greatest
						return Math.max(
							elem.body[ "scroll" + name ], doc[ "scroll" + name ],
							elem.body[ "offset" + name ], doc[ "offset" + name ],
							doc[ "client" + name ]
						);
					}

					return value === undefined ?
						// Get width or height on the element, requesting but not forcing parseFloat
						jQuery.css( elem, type, extra ) :

						// Set width or height on the element
						jQuery.style( elem, type, value, extra );
				}, type, chainable ? margin : undefined, chainable, null );
			};
		});
	});


	// The number of elements contained in the matched element set
	jQuery.fn.size = function() {
		return this.length;
	};

	jQuery.fn.andSelf = jQuery.fn.addBack;




	// Register as a named AMD module, since jQuery can be concatenated with other
	// files that may use define, but not via a proper concatenation script that
	// understands anonymous AMD modules. A named AMD is safest and most robust
	// way to register. Lowercase jquery is used because AMD module names are
	// derived from file names, and jQuery is normally delivered in a lowercase
	// file name. Do this after creating the global so that if an AMD module wants
	// to call noConflict to hide this version of jQuery, it will work.

	// Note that for maximum portability, libraries that are not jQuery should
	// declare themselves as anonymous modules, and avoid setting a global if an
	// AMD loader is present. jQuery is a special case. For more information, see
	// https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon

	if ( true ) {
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = function() {
			return jQuery;
		}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	}




	var
		// Map over jQuery in case of overwrite
		_jQuery = window.jQuery,

		// Map over the $ in case of overwrite
		_$ = window.$;

	jQuery.noConflict = function( deep ) {
		if ( window.$ === jQuery ) {
			window.$ = _$;
		}

		if ( deep && window.jQuery === jQuery ) {
			window.jQuery = _jQuery;
		}

		return jQuery;
	};

	// Expose jQuery and $ identifiers, even in AMD
	// (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
	// and CommonJS for browser emulators (#13566)
	if ( typeof noGlobal === strundefined ) {
		window.jQuery = window.$ = jQuery;
	}




	return jQuery;

	}));


/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;//     Underscore.js 1.8.3
	//     http://underscorejs.org
	//     (c) 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
	//     Underscore may be freely distributed under the MIT license.

	(function() {

	  // Baseline setup
	  // --------------

	  // Establish the root object, `window` in the browser, or `exports` on the server.
	  var root = this;

	  // Save the previous value of the `_` variable.
	  var previousUnderscore = root._;

	  // Save bytes in the minified (but not gzipped) version:
	  var ArrayProto = Array.prototype, ObjProto = Object.prototype, FuncProto = Function.prototype;

	  // Create quick reference variables for speed access to core prototypes.
	  var
	    push             = ArrayProto.push,
	    slice            = ArrayProto.slice,
	    toString         = ObjProto.toString,
	    hasOwnProperty   = ObjProto.hasOwnProperty;

	  // All **ECMAScript 5** native function implementations that we hope to use
	  // are declared here.
	  var
	    nativeIsArray      = Array.isArray,
	    nativeKeys         = Object.keys,
	    nativeBind         = FuncProto.bind,
	    nativeCreate       = Object.create;

	  // Naked function reference for surrogate-prototype-swapping.
	  var Ctor = function(){};

	  // Create a safe reference to the Underscore object for use below.
	  var _ = function(obj) {
	    if (obj instanceof _) return obj;
	    if (!(this instanceof _)) return new _(obj);
	    this._wrapped = obj;
	  };

	  // Export the Underscore object for **Node.js**, with
	  // backwards-compatibility for the old `require()` API. If we're in
	  // the browser, add `_` as a global object.
	  if (true) {
	    if (typeof module !== 'undefined' && module.exports) {
	      exports = module.exports = _;
	    }
	    exports._ = _;
	  } else {
	    root._ = _;
	  }

	  // Current version.
	  _.VERSION = '1.8.3';

	  // Internal function that returns an efficient (for current engines) version
	  // of the passed-in callback, to be repeatedly applied in other Underscore
	  // functions.
	  var optimizeCb = function(func, context, argCount) {
	    if (context === void 0) return func;
	    switch (argCount == null ? 3 : argCount) {
	      case 1: return function(value) {
	        return func.call(context, value);
	      };
	      case 2: return function(value, other) {
	        return func.call(context, value, other);
	      };
	      case 3: return function(value, index, collection) {
	        return func.call(context, value, index, collection);
	      };
	      case 4: return function(accumulator, value, index, collection) {
	        return func.call(context, accumulator, value, index, collection);
	      };
	    }
	    return function() {
	      return func.apply(context, arguments);
	    };
	  };

	  // A mostly-internal function to generate callbacks that can be applied
	  // to each element in a collection, returning the desired result — either
	  // identity, an arbitrary callback, a property matcher, or a property accessor.
	  var cb = function(value, context, argCount) {
	    if (value == null) return _.identity;
	    if (_.isFunction(value)) return optimizeCb(value, context, argCount);
	    if (_.isObject(value)) return _.matcher(value);
	    return _.property(value);
	  };
	  _.iteratee = function(value, context) {
	    return cb(value, context, Infinity);
	  };

	  // An internal function for creating assigner functions.
	  var createAssigner = function(keysFunc, undefinedOnly) {
	    return function(obj) {
	      var length = arguments.length;
	      if (length < 2 || obj == null) return obj;
	      for (var index = 1; index < length; index++) {
	        var source = arguments[index],
	            keys = keysFunc(source),
	            l = keys.length;
	        for (var i = 0; i < l; i++) {
	          var key = keys[i];
	          if (!undefinedOnly || obj[key] === void 0) obj[key] = source[key];
	        }
	      }
	      return obj;
	    };
	  };

	  // An internal function for creating a new object that inherits from another.
	  var baseCreate = function(prototype) {
	    if (!_.isObject(prototype)) return {};
	    if (nativeCreate) return nativeCreate(prototype);
	    Ctor.prototype = prototype;
	    var result = new Ctor;
	    Ctor.prototype = null;
	    return result;
	  };

	  var property = function(key) {
	    return function(obj) {
	      return obj == null ? void 0 : obj[key];
	    };
	  };

	  // Helper for collection methods to determine whether a collection
	  // should be iterated as an array or as an object
	  // Related: http://people.mozilla.org/~jorendorff/es6-draft.html#sec-tolength
	  // Avoids a very nasty iOS 8 JIT bug on ARM-64. #2094
	  var MAX_ARRAY_INDEX = Math.pow(2, 53) - 1;
	  var getLength = property('length');
	  var isArrayLike = function(collection) {
	    var length = getLength(collection);
	    return typeof length == 'number' && length >= 0 && length <= MAX_ARRAY_INDEX;
	  };

	  // Collection Functions
	  // --------------------

	  // The cornerstone, an `each` implementation, aka `forEach`.
	  // Handles raw objects in addition to array-likes. Treats all
	  // sparse array-likes as if they were dense.
	  _.each = _.forEach = function(obj, iteratee, context) {
	    iteratee = optimizeCb(iteratee, context);
	    var i, length;
	    if (isArrayLike(obj)) {
	      for (i = 0, length = obj.length; i < length; i++) {
	        iteratee(obj[i], i, obj);
	      }
	    } else {
	      var keys = _.keys(obj);
	      for (i = 0, length = keys.length; i < length; i++) {
	        iteratee(obj[keys[i]], keys[i], obj);
	      }
	    }
	    return obj;
	  };

	  // Return the results of applying the iteratee to each element.
	  _.map = _.collect = function(obj, iteratee, context) {
	    iteratee = cb(iteratee, context);
	    var keys = !isArrayLike(obj) && _.keys(obj),
	        length = (keys || obj).length,
	        results = Array(length);
	    for (var index = 0; index < length; index++) {
	      var currentKey = keys ? keys[index] : index;
	      results[index] = iteratee(obj[currentKey], currentKey, obj);
	    }
	    return results;
	  };

	  // Create a reducing function iterating left or right.
	  function createReduce(dir) {
	    // Optimized iterator function as using arguments.length
	    // in the main function will deoptimize the, see #1991.
	    function iterator(obj, iteratee, memo, keys, index, length) {
	      for (; index >= 0 && index < length; index += dir) {
	        var currentKey = keys ? keys[index] : index;
	        memo = iteratee(memo, obj[currentKey], currentKey, obj);
	      }
	      return memo;
	    }

	    return function(obj, iteratee, memo, context) {
	      iteratee = optimizeCb(iteratee, context, 4);
	      var keys = !isArrayLike(obj) && _.keys(obj),
	          length = (keys || obj).length,
	          index = dir > 0 ? 0 : length - 1;
	      // Determine the initial value if none is provided.
	      if (arguments.length < 3) {
	        memo = obj[keys ? keys[index] : index];
	        index += dir;
	      }
	      return iterator(obj, iteratee, memo, keys, index, length);
	    };
	  }

	  // **Reduce** builds up a single result from a list of values, aka `inject`,
	  // or `foldl`.
	  _.reduce = _.foldl = _.inject = createReduce(1);

	  // The right-associative version of reduce, also known as `foldr`.
	  _.reduceRight = _.foldr = createReduce(-1);

	  // Return the first value which passes a truth test. Aliased as `detect`.
	  _.find = _.detect = function(obj, predicate, context) {
	    var key;
	    if (isArrayLike(obj)) {
	      key = _.findIndex(obj, predicate, context);
	    } else {
	      key = _.findKey(obj, predicate, context);
	    }
	    if (key !== void 0 && key !== -1) return obj[key];
	  };

	  // Return all the elements that pass a truth test.
	  // Aliased as `select`.
	  _.filter = _.select = function(obj, predicate, context) {
	    var results = [];
	    predicate = cb(predicate, context);
	    _.each(obj, function(value, index, list) {
	      if (predicate(value, index, list)) results.push(value);
	    });
	    return results;
	  };

	  // Return all the elements for which a truth test fails.
	  _.reject = function(obj, predicate, context) {
	    return _.filter(obj, _.negate(cb(predicate)), context);
	  };

	  // Determine whether all of the elements match a truth test.
	  // Aliased as `all`.
	  _.every = _.all = function(obj, predicate, context) {
	    predicate = cb(predicate, context);
	    var keys = !isArrayLike(obj) && _.keys(obj),
	        length = (keys || obj).length;
	    for (var index = 0; index < length; index++) {
	      var currentKey = keys ? keys[index] : index;
	      if (!predicate(obj[currentKey], currentKey, obj)) return false;
	    }
	    return true;
	  };

	  // Determine if at least one element in the object matches a truth test.
	  // Aliased as `any`.
	  _.some = _.any = function(obj, predicate, context) {
	    predicate = cb(predicate, context);
	    var keys = !isArrayLike(obj) && _.keys(obj),
	        length = (keys || obj).length;
	    for (var index = 0; index < length; index++) {
	      var currentKey = keys ? keys[index] : index;
	      if (predicate(obj[currentKey], currentKey, obj)) return true;
	    }
	    return false;
	  };

	  // Determine if the array or object contains a given item (using `===`).
	  // Aliased as `includes` and `include`.
	  _.contains = _.includes = _.include = function(obj, item, fromIndex, guard) {
	    if (!isArrayLike(obj)) obj = _.values(obj);
	    if (typeof fromIndex != 'number' || guard) fromIndex = 0;
	    return _.indexOf(obj, item, fromIndex) >= 0;
	  };

	  // Invoke a method (with arguments) on every item in a collection.
	  _.invoke = function(obj, method) {
	    var args = slice.call(arguments, 2);
	    var isFunc = _.isFunction(method);
	    return _.map(obj, function(value) {
	      var func = isFunc ? method : value[method];
	      return func == null ? func : func.apply(value, args);
	    });
	  };

	  // Convenience version of a common use case of `map`: fetching a property.
	  _.pluck = function(obj, key) {
	    return _.map(obj, _.property(key));
	  };

	  // Convenience version of a common use case of `filter`: selecting only objects
	  // containing specific `key:value` pairs.
	  _.where = function(obj, attrs) {
	    return _.filter(obj, _.matcher(attrs));
	  };

	  // Convenience version of a common use case of `find`: getting the first object
	  // containing specific `key:value` pairs.
	  _.findWhere = function(obj, attrs) {
	    return _.find(obj, _.matcher(attrs));
	  };

	  // Return the maximum element (or element-based computation).
	  _.max = function(obj, iteratee, context) {
	    var result = -Infinity, lastComputed = -Infinity,
	        value, computed;
	    if (iteratee == null && obj != null) {
	      obj = isArrayLike(obj) ? obj : _.values(obj);
	      for (var i = 0, length = obj.length; i < length; i++) {
	        value = obj[i];
	        if (value > result) {
	          result = value;
	        }
	      }
	    } else {
	      iteratee = cb(iteratee, context);
	      _.each(obj, function(value, index, list) {
	        computed = iteratee(value, index, list);
	        if (computed > lastComputed || computed === -Infinity && result === -Infinity) {
	          result = value;
	          lastComputed = computed;
	        }
	      });
	    }
	    return result;
	  };

	  // Return the minimum element (or element-based computation).
	  _.min = function(obj, iteratee, context) {
	    var result = Infinity, lastComputed = Infinity,
	        value, computed;
	    if (iteratee == null && obj != null) {
	      obj = isArrayLike(obj) ? obj : _.values(obj);
	      for (var i = 0, length = obj.length; i < length; i++) {
	        value = obj[i];
	        if (value < result) {
	          result = value;
	        }
	      }
	    } else {
	      iteratee = cb(iteratee, context);
	      _.each(obj, function(value, index, list) {
	        computed = iteratee(value, index, list);
	        if (computed < lastComputed || computed === Infinity && result === Infinity) {
	          result = value;
	          lastComputed = computed;
	        }
	      });
	    }
	    return result;
	  };

	  // Shuffle a collection, using the modern version of the
	  // [Fisher-Yates shuffle](http://en.wikipedia.org/wiki/Fisher–Yates_shuffle).
	  _.shuffle = function(obj) {
	    var set = isArrayLike(obj) ? obj : _.values(obj);
	    var length = set.length;
	    var shuffled = Array(length);
	    for (var index = 0, rand; index < length; index++) {
	      rand = _.random(0, index);
	      if (rand !== index) shuffled[index] = shuffled[rand];
	      shuffled[rand] = set[index];
	    }
	    return shuffled;
	  };

	  // Sample **n** random values from a collection.
	  // If **n** is not specified, returns a single random element.
	  // The internal `guard` argument allows it to work with `map`.
	  _.sample = function(obj, n, guard) {
	    if (n == null || guard) {
	      if (!isArrayLike(obj)) obj = _.values(obj);
	      return obj[_.random(obj.length - 1)];
	    }
	    return _.shuffle(obj).slice(0, Math.max(0, n));
	  };

	  // Sort the object's values by a criterion produced by an iteratee.
	  _.sortBy = function(obj, iteratee, context) {
	    iteratee = cb(iteratee, context);
	    return _.pluck(_.map(obj, function(value, index, list) {
	      return {
	        value: value,
	        index: index,
	        criteria: iteratee(value, index, list)
	      };
	    }).sort(function(left, right) {
	      var a = left.criteria;
	      var b = right.criteria;
	      if (a !== b) {
	        if (a > b || a === void 0) return 1;
	        if (a < b || b === void 0) return -1;
	      }
	      return left.index - right.index;
	    }), 'value');
	  };

	  // An internal function used for aggregate "group by" operations.
	  var group = function(behavior) {
	    return function(obj, iteratee, context) {
	      var result = {};
	      iteratee = cb(iteratee, context);
	      _.each(obj, function(value, index) {
	        var key = iteratee(value, index, obj);
	        behavior(result, value, key);
	      });
	      return result;
	    };
	  };

	  // Groups the object's values by a criterion. Pass either a string attribute
	  // to group by, or a function that returns the criterion.
	  _.groupBy = group(function(result, value, key) {
	    if (_.has(result, key)) result[key].push(value); else result[key] = [value];
	  });

	  // Indexes the object's values by a criterion, similar to `groupBy`, but for
	  // when you know that your index values will be unique.
	  _.indexBy = group(function(result, value, key) {
	    result[key] = value;
	  });

	  // Counts instances of an object that group by a certain criterion. Pass
	  // either a string attribute to count by, or a function that returns the
	  // criterion.
	  _.countBy = group(function(result, value, key) {
	    if (_.has(result, key)) result[key]++; else result[key] = 1;
	  });

	  // Safely create a real, live array from anything iterable.
	  _.toArray = function(obj) {
	    if (!obj) return [];
	    if (_.isArray(obj)) return slice.call(obj);
	    if (isArrayLike(obj)) return _.map(obj, _.identity);
	    return _.values(obj);
	  };

	  // Return the number of elements in an object.
	  _.size = function(obj) {
	    if (obj == null) return 0;
	    return isArrayLike(obj) ? obj.length : _.keys(obj).length;
	  };

	  // Split a collection into two arrays: one whose elements all satisfy the given
	  // predicate, and one whose elements all do not satisfy the predicate.
	  _.partition = function(obj, predicate, context) {
	    predicate = cb(predicate, context);
	    var pass = [], fail = [];
	    _.each(obj, function(value, key, obj) {
	      (predicate(value, key, obj) ? pass : fail).push(value);
	    });
	    return [pass, fail];
	  };

	  // Array Functions
	  // ---------------

	  // Get the first element of an array. Passing **n** will return the first N
	  // values in the array. Aliased as `head` and `take`. The **guard** check
	  // allows it to work with `_.map`.
	  _.first = _.head = _.take = function(array, n, guard) {
	    if (array == null) return void 0;
	    if (n == null || guard) return array[0];
	    return _.initial(array, array.length - n);
	  };

	  // Returns everything but the last entry of the array. Especially useful on
	  // the arguments object. Passing **n** will return all the values in
	  // the array, excluding the last N.
	  _.initial = function(array, n, guard) {
	    return slice.call(array, 0, Math.max(0, array.length - (n == null || guard ? 1 : n)));
	  };

	  // Get the last element of an array. Passing **n** will return the last N
	  // values in the array.
	  _.last = function(array, n, guard) {
	    if (array == null) return void 0;
	    if (n == null || guard) return array[array.length - 1];
	    return _.rest(array, Math.max(0, array.length - n));
	  };

	  // Returns everything but the first entry of the array. Aliased as `tail` and `drop`.
	  // Especially useful on the arguments object. Passing an **n** will return
	  // the rest N values in the array.
	  _.rest = _.tail = _.drop = function(array, n, guard) {
	    return slice.call(array, n == null || guard ? 1 : n);
	  };

	  // Trim out all falsy values from an array.
	  _.compact = function(array) {
	    return _.filter(array, _.identity);
	  };

	  // Internal implementation of a recursive `flatten` function.
	  var flatten = function(input, shallow, strict, startIndex) {
	    var output = [], idx = 0;
	    for (var i = startIndex || 0, length = getLength(input); i < length; i++) {
	      var value = input[i];
	      if (isArrayLike(value) && (_.isArray(value) || _.isArguments(value))) {
	        //flatten current level of array or arguments object
	        if (!shallow) value = flatten(value, shallow, strict);
	        var j = 0, len = value.length;
	        output.length += len;
	        while (j < len) {
	          output[idx++] = value[j++];
	        }
	      } else if (!strict) {
	        output[idx++] = value;
	      }
	    }
	    return output;
	  };

	  // Flatten out an array, either recursively (by default), or just one level.
	  _.flatten = function(array, shallow) {
	    return flatten(array, shallow, false);
	  };

	  // Return a version of the array that does not contain the specified value(s).
	  _.without = function(array) {
	    return _.difference(array, slice.call(arguments, 1));
	  };

	  // Produce a duplicate-free version of the array. If the array has already
	  // been sorted, you have the option of using a faster algorithm.
	  // Aliased as `unique`.
	  _.uniq = _.unique = function(array, isSorted, iteratee, context) {
	    if (!_.isBoolean(isSorted)) {
	      context = iteratee;
	      iteratee = isSorted;
	      isSorted = false;
	    }
	    if (iteratee != null) iteratee = cb(iteratee, context);
	    var result = [];
	    var seen = [];
	    for (var i = 0, length = getLength(array); i < length; i++) {
	      var value = array[i],
	          computed = iteratee ? iteratee(value, i, array) : value;
	      if (isSorted) {
	        if (!i || seen !== computed) result.push(value);
	        seen = computed;
	      } else if (iteratee) {
	        if (!_.contains(seen, computed)) {
	          seen.push(computed);
	          result.push(value);
	        }
	      } else if (!_.contains(result, value)) {
	        result.push(value);
	      }
	    }
	    return result;
	  };

	  // Produce an array that contains the union: each distinct element from all of
	  // the passed-in arrays.
	  _.union = function() {
	    return _.uniq(flatten(arguments, true, true));
	  };

	  // Produce an array that contains every item shared between all the
	  // passed-in arrays.
	  _.intersection = function(array) {
	    var result = [];
	    var argsLength = arguments.length;
	    for (var i = 0, length = getLength(array); i < length; i++) {
	      var item = array[i];
	      if (_.contains(result, item)) continue;
	      for (var j = 1; j < argsLength; j++) {
	        if (!_.contains(arguments[j], item)) break;
	      }
	      if (j === argsLength) result.push(item);
	    }
	    return result;
	  };

	  // Take the difference between one array and a number of other arrays.
	  // Only the elements present in just the first array will remain.
	  _.difference = function(array) {
	    var rest = flatten(arguments, true, true, 1);
	    return _.filter(array, function(value){
	      return !_.contains(rest, value);
	    });
	  };

	  // Zip together multiple lists into a single array -- elements that share
	  // an index go together.
	  _.zip = function() {
	    return _.unzip(arguments);
	  };

	  // Complement of _.zip. Unzip accepts an array of arrays and groups
	  // each array's elements on shared indices
	  _.unzip = function(array) {
	    var length = array && _.max(array, getLength).length || 0;
	    var result = Array(length);

	    for (var index = 0; index < length; index++) {
	      result[index] = _.pluck(array, index);
	    }
	    return result;
	  };

	  // Converts lists into objects. Pass either a single array of `[key, value]`
	  // pairs, or two parallel arrays of the same length -- one of keys, and one of
	  // the corresponding values.
	  _.object = function(list, values) {
	    var result = {};
	    for (var i = 0, length = getLength(list); i < length; i++) {
	      if (values) {
	        result[list[i]] = values[i];
	      } else {
	        result[list[i][0]] = list[i][1];
	      }
	    }
	    return result;
	  };

	  // Generator function to create the findIndex and findLastIndex functions
	  function createPredicateIndexFinder(dir) {
	    return function(array, predicate, context) {
	      predicate = cb(predicate, context);
	      var length = getLength(array);
	      var index = dir > 0 ? 0 : length - 1;
	      for (; index >= 0 && index < length; index += dir) {
	        if (predicate(array[index], index, array)) return index;
	      }
	      return -1;
	    };
	  }

	  // Returns the first index on an array-like that passes a predicate test
	  _.findIndex = createPredicateIndexFinder(1);
	  _.findLastIndex = createPredicateIndexFinder(-1);

	  // Use a comparator function to figure out the smallest index at which
	  // an object should be inserted so as to maintain order. Uses binary search.
	  _.sortedIndex = function(array, obj, iteratee, context) {
	    iteratee = cb(iteratee, context, 1);
	    var value = iteratee(obj);
	    var low = 0, high = getLength(array);
	    while (low < high) {
	      var mid = Math.floor((low + high) / 2);
	      if (iteratee(array[mid]) < value) low = mid + 1; else high = mid;
	    }
	    return low;
	  };

	  // Generator function to create the indexOf and lastIndexOf functions
	  function createIndexFinder(dir, predicateFind, sortedIndex) {
	    return function(array, item, idx) {
	      var i = 0, length = getLength(array);
	      if (typeof idx == 'number') {
	        if (dir > 0) {
	            i = idx >= 0 ? idx : Math.max(idx + length, i);
	        } else {
	            length = idx >= 0 ? Math.min(idx + 1, length) : idx + length + 1;
	        }
	      } else if (sortedIndex && idx && length) {
	        idx = sortedIndex(array, item);
	        return array[idx] === item ? idx : -1;
	      }
	      if (item !== item) {
	        idx = predicateFind(slice.call(array, i, length), _.isNaN);
	        return idx >= 0 ? idx + i : -1;
	      }
	      for (idx = dir > 0 ? i : length - 1; idx >= 0 && idx < length; idx += dir) {
	        if (array[idx] === item) return idx;
	      }
	      return -1;
	    };
	  }

	  // Return the position of the first occurrence of an item in an array,
	  // or -1 if the item is not included in the array.
	  // If the array is large and already in sort order, pass `true`
	  // for **isSorted** to use binary search.
	  _.indexOf = createIndexFinder(1, _.findIndex, _.sortedIndex);
	  _.lastIndexOf = createIndexFinder(-1, _.findLastIndex);

	  // Generate an integer Array containing an arithmetic progression. A port of
	  // the native Python `range()` function. See
	  // [the Python documentation](http://docs.python.org/library/functions.html#range).
	  _.range = function(start, stop, step) {
	    if (stop == null) {
	      stop = start || 0;
	      start = 0;
	    }
	    step = step || 1;

	    var length = Math.max(Math.ceil((stop - start) / step), 0);
	    var range = Array(length);

	    for (var idx = 0; idx < length; idx++, start += step) {
	      range[idx] = start;
	    }

	    return range;
	  };

	  // Function (ahem) Functions
	  // ------------------

	  // Determines whether to execute a function as a constructor
	  // or a normal function with the provided arguments
	  var executeBound = function(sourceFunc, boundFunc, context, callingContext, args) {
	    if (!(callingContext instanceof boundFunc)) return sourceFunc.apply(context, args);
	    var self = baseCreate(sourceFunc.prototype);
	    var result = sourceFunc.apply(self, args);
	    if (_.isObject(result)) return result;
	    return self;
	  };

	  // Create a function bound to a given object (assigning `this`, and arguments,
	  // optionally). Delegates to **ECMAScript 5**'s native `Function.bind` if
	  // available.
	  _.bind = function(func, context) {
	    if (nativeBind && func.bind === nativeBind) return nativeBind.apply(func, slice.call(arguments, 1));
	    if (!_.isFunction(func)) throw new TypeError('Bind must be called on a function');
	    var args = slice.call(arguments, 2);
	    var bound = function() {
	      return executeBound(func, bound, context, this, args.concat(slice.call(arguments)));
	    };
	    return bound;
	  };

	  // Partially apply a function by creating a version that has had some of its
	  // arguments pre-filled, without changing its dynamic `this` context. _ acts
	  // as a placeholder, allowing any combination of arguments to be pre-filled.
	  _.partial = function(func) {
	    var boundArgs = slice.call(arguments, 1);
	    var bound = function() {
	      var position = 0, length = boundArgs.length;
	      var args = Array(length);
	      for (var i = 0; i < length; i++) {
	        args[i] = boundArgs[i] === _ ? arguments[position++] : boundArgs[i];
	      }
	      while (position < arguments.length) args.push(arguments[position++]);
	      return executeBound(func, bound, this, this, args);
	    };
	    return bound;
	  };

	  // Bind a number of an object's methods to that object. Remaining arguments
	  // are the method names to be bound. Useful for ensuring that all callbacks
	  // defined on an object belong to it.
	  _.bindAll = function(obj) {
	    var i, length = arguments.length, key;
	    if (length <= 1) throw new Error('bindAll must be passed function names');
	    for (i = 1; i < length; i++) {
	      key = arguments[i];
	      obj[key] = _.bind(obj[key], obj);
	    }
	    return obj;
	  };

	  // Memoize an expensive function by storing its results.
	  _.memoize = function(func, hasher) {
	    var memoize = function(key) {
	      var cache = memoize.cache;
	      var address = '' + (hasher ? hasher.apply(this, arguments) : key);
	      if (!_.has(cache, address)) cache[address] = func.apply(this, arguments);
	      return cache[address];
	    };
	    memoize.cache = {};
	    return memoize;
	  };

	  // Delays a function for the given number of milliseconds, and then calls
	  // it with the arguments supplied.
	  _.delay = function(func, wait) {
	    var args = slice.call(arguments, 2);
	    return setTimeout(function(){
	      return func.apply(null, args);
	    }, wait);
	  };

	  // Defers a function, scheduling it to run after the current call stack has
	  // cleared.
	  _.defer = _.partial(_.delay, _, 1);

	  // Returns a function, that, when invoked, will only be triggered at most once
	  // during a given window of time. Normally, the throttled function will run
	  // as much as it can, without ever going more than once per `wait` duration;
	  // but if you'd like to disable the execution on the leading edge, pass
	  // `{leading: false}`. To disable execution on the trailing edge, ditto.
	  _.throttle = function(func, wait, options) {
	    var context, args, result;
	    var timeout = null;
	    var previous = 0;
	    if (!options) options = {};
	    var later = function() {
	      previous = options.leading === false ? 0 : _.now();
	      timeout = null;
	      result = func.apply(context, args);
	      if (!timeout) context = args = null;
	    };
	    return function() {
	      var now = _.now();
	      if (!previous && options.leading === false) previous = now;
	      var remaining = wait - (now - previous);
	      context = this;
	      args = arguments;
	      if (remaining <= 0 || remaining > wait) {
	        if (timeout) {
	          clearTimeout(timeout);
	          timeout = null;
	        }
	        previous = now;
	        result = func.apply(context, args);
	        if (!timeout) context = args = null;
	      } else if (!timeout && options.trailing !== false) {
	        timeout = setTimeout(later, remaining);
	      }
	      return result;
	    };
	  };

	  // Returns a function, that, as long as it continues to be invoked, will not
	  // be triggered. The function will be called after it stops being called for
	  // N milliseconds. If `immediate` is passed, trigger the function on the
	  // leading edge, instead of the trailing.
	  _.debounce = function(func, wait, immediate) {
	    var timeout, args, context, timestamp, result;

	    var later = function() {
	      var last = _.now() - timestamp;

	      if (last < wait && last >= 0) {
	        timeout = setTimeout(later, wait - last);
	      } else {
	        timeout = null;
	        if (!immediate) {
	          result = func.apply(context, args);
	          if (!timeout) context = args = null;
	        }
	      }
	    };

	    return function() {
	      context = this;
	      args = arguments;
	      timestamp = _.now();
	      var callNow = immediate && !timeout;
	      if (!timeout) timeout = setTimeout(later, wait);
	      if (callNow) {
	        result = func.apply(context, args);
	        context = args = null;
	      }

	      return result;
	    };
	  };

	  // Returns the first function passed as an argument to the second,
	  // allowing you to adjust arguments, run code before and after, and
	  // conditionally execute the original function.
	  _.wrap = function(func, wrapper) {
	    return _.partial(wrapper, func);
	  };

	  // Returns a negated version of the passed-in predicate.
	  _.negate = function(predicate) {
	    return function() {
	      return !predicate.apply(this, arguments);
	    };
	  };

	  // Returns a function that is the composition of a list of functions, each
	  // consuming the return value of the function that follows.
	  _.compose = function() {
	    var args = arguments;
	    var start = args.length - 1;
	    return function() {
	      var i = start;
	      var result = args[start].apply(this, arguments);
	      while (i--) result = args[i].call(this, result);
	      return result;
	    };
	  };

	  // Returns a function that will only be executed on and after the Nth call.
	  _.after = function(times, func) {
	    return function() {
	      if (--times < 1) {
	        return func.apply(this, arguments);
	      }
	    };
	  };

	  // Returns a function that will only be executed up to (but not including) the Nth call.
	  _.before = function(times, func) {
	    var memo;
	    return function() {
	      if (--times > 0) {
	        memo = func.apply(this, arguments);
	      }
	      if (times <= 1) func = null;
	      return memo;
	    };
	  };

	  // Returns a function that will be executed at most one time, no matter how
	  // often you call it. Useful for lazy initialization.
	  _.once = _.partial(_.before, 2);

	  // Object Functions
	  // ----------------

	  // Keys in IE < 9 that won't be iterated by `for key in ...` and thus missed.
	  var hasEnumBug = !{toString: null}.propertyIsEnumerable('toString');
	  var nonEnumerableProps = ['valueOf', 'isPrototypeOf', 'toString',
	                      'propertyIsEnumerable', 'hasOwnProperty', 'toLocaleString'];

	  function collectNonEnumProps(obj, keys) {
	    var nonEnumIdx = nonEnumerableProps.length;
	    var constructor = obj.constructor;
	    var proto = (_.isFunction(constructor) && constructor.prototype) || ObjProto;

	    // Constructor is a special case.
	    var prop = 'constructor';
	    if (_.has(obj, prop) && !_.contains(keys, prop)) keys.push(prop);

	    while (nonEnumIdx--) {
	      prop = nonEnumerableProps[nonEnumIdx];
	      if (prop in obj && obj[prop] !== proto[prop] && !_.contains(keys, prop)) {
	        keys.push(prop);
	      }
	    }
	  }

	  // Retrieve the names of an object's own properties.
	  // Delegates to **ECMAScript 5**'s native `Object.keys`
	  _.keys = function(obj) {
	    if (!_.isObject(obj)) return [];
	    if (nativeKeys) return nativeKeys(obj);
	    var keys = [];
	    for (var key in obj) if (_.has(obj, key)) keys.push(key);
	    // Ahem, IE < 9.
	    if (hasEnumBug) collectNonEnumProps(obj, keys);
	    return keys;
	  };

	  // Retrieve all the property names of an object.
	  _.allKeys = function(obj) {
	    if (!_.isObject(obj)) return [];
	    var keys = [];
	    for (var key in obj) keys.push(key);
	    // Ahem, IE < 9.
	    if (hasEnumBug) collectNonEnumProps(obj, keys);
	    return keys;
	  };

	  // Retrieve the values of an object's properties.
	  _.values = function(obj) {
	    var keys = _.keys(obj);
	    var length = keys.length;
	    var values = Array(length);
	    for (var i = 0; i < length; i++) {
	      values[i] = obj[keys[i]];
	    }
	    return values;
	  };

	  // Returns the results of applying the iteratee to each element of the object
	  // In contrast to _.map it returns an object
	  _.mapObject = function(obj, iteratee, context) {
	    iteratee = cb(iteratee, context);
	    var keys =  _.keys(obj),
	          length = keys.length,
	          results = {},
	          currentKey;
	      for (var index = 0; index < length; index++) {
	        currentKey = keys[index];
	        results[currentKey] = iteratee(obj[currentKey], currentKey, obj);
	      }
	      return results;
	  };

	  // Convert an object into a list of `[key, value]` pairs.
	  _.pairs = function(obj) {
	    var keys = _.keys(obj);
	    var length = keys.length;
	    var pairs = Array(length);
	    for (var i = 0; i < length; i++) {
	      pairs[i] = [keys[i], obj[keys[i]]];
	    }
	    return pairs;
	  };

	  // Invert the keys and values of an object. The values must be serializable.
	  _.invert = function(obj) {
	    var result = {};
	    var keys = _.keys(obj);
	    for (var i = 0, length = keys.length; i < length; i++) {
	      result[obj[keys[i]]] = keys[i];
	    }
	    return result;
	  };

	  // Return a sorted list of the function names available on the object.
	  // Aliased as `methods`
	  _.functions = _.methods = function(obj) {
	    var names = [];
	    for (var key in obj) {
	      if (_.isFunction(obj[key])) names.push(key);
	    }
	    return names.sort();
	  };

	  // Extend a given object with all the properties in passed-in object(s).
	  _.extend = createAssigner(_.allKeys);

	  // Assigns a given object with all the own properties in the passed-in object(s)
	  // (https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)
	  _.extendOwn = _.assign = createAssigner(_.keys);

	  // Returns the first key on an object that passes a predicate test
	  _.findKey = function(obj, predicate, context) {
	    predicate = cb(predicate, context);
	    var keys = _.keys(obj), key;
	    for (var i = 0, length = keys.length; i < length; i++) {
	      key = keys[i];
	      if (predicate(obj[key], key, obj)) return key;
	    }
	  };

	  // Return a copy of the object only containing the whitelisted properties.
	  _.pick = function(object, oiteratee, context) {
	    var result = {}, obj = object, iteratee, keys;
	    if (obj == null) return result;
	    if (_.isFunction(oiteratee)) {
	      keys = _.allKeys(obj);
	      iteratee = optimizeCb(oiteratee, context);
	    } else {
	      keys = flatten(arguments, false, false, 1);
	      iteratee = function(value, key, obj) { return key in obj; };
	      obj = Object(obj);
	    }
	    for (var i = 0, length = keys.length; i < length; i++) {
	      var key = keys[i];
	      var value = obj[key];
	      if (iteratee(value, key, obj)) result[key] = value;
	    }
	    return result;
	  };

	   // Return a copy of the object without the blacklisted properties.
	  _.omit = function(obj, iteratee, context) {
	    if (_.isFunction(iteratee)) {
	      iteratee = _.negate(iteratee);
	    } else {
	      var keys = _.map(flatten(arguments, false, false, 1), String);
	      iteratee = function(value, key) {
	        return !_.contains(keys, key);
	      };
	    }
	    return _.pick(obj, iteratee, context);
	  };

	  // Fill in a given object with default properties.
	  _.defaults = createAssigner(_.allKeys, true);

	  // Creates an object that inherits from the given prototype object.
	  // If additional properties are provided then they will be added to the
	  // created object.
	  _.create = function(prototype, props) {
	    var result = baseCreate(prototype);
	    if (props) _.extendOwn(result, props);
	    return result;
	  };

	  // Create a (shallow-cloned) duplicate of an object.
	  _.clone = function(obj) {
	    if (!_.isObject(obj)) return obj;
	    return _.isArray(obj) ? obj.slice() : _.extend({}, obj);
	  };

	  // Invokes interceptor with the obj, and then returns obj.
	  // The primary purpose of this method is to "tap into" a method chain, in
	  // order to perform operations on intermediate results within the chain.
	  _.tap = function(obj, interceptor) {
	    interceptor(obj);
	    return obj;
	  };

	  // Returns whether an object has a given set of `key:value` pairs.
	  _.isMatch = function(object, attrs) {
	    var keys = _.keys(attrs), length = keys.length;
	    if (object == null) return !length;
	    var obj = Object(object);
	    for (var i = 0; i < length; i++) {
	      var key = keys[i];
	      if (attrs[key] !== obj[key] || !(key in obj)) return false;
	    }
	    return true;
	  };


	  // Internal recursive comparison function for `isEqual`.
	  var eq = function(a, b, aStack, bStack) {
	    // Identical objects are equal. `0 === -0`, but they aren't identical.
	    // See the [Harmony `egal` proposal](http://wiki.ecmascript.org/doku.php?id=harmony:egal).
	    if (a === b) return a !== 0 || 1 / a === 1 / b;
	    // A strict comparison is necessary because `null == undefined`.
	    if (a == null || b == null) return a === b;
	    // Unwrap any wrapped objects.
	    if (a instanceof _) a = a._wrapped;
	    if (b instanceof _) b = b._wrapped;
	    // Compare `[[Class]]` names.
	    var className = toString.call(a);
	    if (className !== toString.call(b)) return false;
	    switch (className) {
	      // Strings, numbers, regular expressions, dates, and booleans are compared by value.
	      case '[object RegExp]':
	      // RegExps are coerced to strings for comparison (Note: '' + /a/i === '/a/i')
	      case '[object String]':
	        // Primitives and their corresponding object wrappers are equivalent; thus, `"5"` is
	        // equivalent to `new String("5")`.
	        return '' + a === '' + b;
	      case '[object Number]':
	        // `NaN`s are equivalent, but non-reflexive.
	        // Object(NaN) is equivalent to NaN
	        if (+a !== +a) return +b !== +b;
	        // An `egal` comparison is performed for other numeric values.
	        return +a === 0 ? 1 / +a === 1 / b : +a === +b;
	      case '[object Date]':
	      case '[object Boolean]':
	        // Coerce dates and booleans to numeric primitive values. Dates are compared by their
	        // millisecond representations. Note that invalid dates with millisecond representations
	        // of `NaN` are not equivalent.
	        return +a === +b;
	    }

	    var areArrays = className === '[object Array]';
	    if (!areArrays) {
	      if (typeof a != 'object' || typeof b != 'object') return false;

	      // Objects with different constructors are not equivalent, but `Object`s or `Array`s
	      // from different frames are.
	      var aCtor = a.constructor, bCtor = b.constructor;
	      if (aCtor !== bCtor && !(_.isFunction(aCtor) && aCtor instanceof aCtor &&
	                               _.isFunction(bCtor) && bCtor instanceof bCtor)
	                          && ('constructor' in a && 'constructor' in b)) {
	        return false;
	      }
	    }
	    // Assume equality for cyclic structures. The algorithm for detecting cyclic
	    // structures is adapted from ES 5.1 section 15.12.3, abstract operation `JO`.

	    // Initializing stack of traversed objects.
	    // It's done here since we only need them for objects and arrays comparison.
	    aStack = aStack || [];
	    bStack = bStack || [];
	    var length = aStack.length;
	    while (length--) {
	      // Linear search. Performance is inversely proportional to the number of
	      // unique nested structures.
	      if (aStack[length] === a) return bStack[length] === b;
	    }

	    // Add the first object to the stack of traversed objects.
	    aStack.push(a);
	    bStack.push(b);

	    // Recursively compare objects and arrays.
	    if (areArrays) {
	      // Compare array lengths to determine if a deep comparison is necessary.
	      length = a.length;
	      if (length !== b.length) return false;
	      // Deep compare the contents, ignoring non-numeric properties.
	      while (length--) {
	        if (!eq(a[length], b[length], aStack, bStack)) return false;
	      }
	    } else {
	      // Deep compare objects.
	      var keys = _.keys(a), key;
	      length = keys.length;
	      // Ensure that both objects contain the same number of properties before comparing deep equality.
	      if (_.keys(b).length !== length) return false;
	      while (length--) {
	        // Deep compare each member
	        key = keys[length];
	        if (!(_.has(b, key) && eq(a[key], b[key], aStack, bStack))) return false;
	      }
	    }
	    // Remove the first object from the stack of traversed objects.
	    aStack.pop();
	    bStack.pop();
	    return true;
	  };

	  // Perform a deep comparison to check if two objects are equal.
	  _.isEqual = function(a, b) {
	    return eq(a, b);
	  };

	  // Is a given array, string, or object empty?
	  // An "empty" object has no enumerable own-properties.
	  _.isEmpty = function(obj) {
	    if (obj == null) return true;
	    if (isArrayLike(obj) && (_.isArray(obj) || _.isString(obj) || _.isArguments(obj))) return obj.length === 0;
	    return _.keys(obj).length === 0;
	  };

	  // Is a given value a DOM element?
	  _.isElement = function(obj) {
	    return !!(obj && obj.nodeType === 1);
	  };

	  // Is a given value an array?
	  // Delegates to ECMA5's native Array.isArray
	  _.isArray = nativeIsArray || function(obj) {
	    return toString.call(obj) === '[object Array]';
	  };

	  // Is a given variable an object?
	  _.isObject = function(obj) {
	    var type = typeof obj;
	    return type === 'function' || type === 'object' && !!obj;
	  };

	  // Add some isType methods: isArguments, isFunction, isString, isNumber, isDate, isRegExp, isError.
	  _.each(['Arguments', 'Function', 'String', 'Number', 'Date', 'RegExp', 'Error'], function(name) {
	    _['is' + name] = function(obj) {
	      return toString.call(obj) === '[object ' + name + ']';
	    };
	  });

	  // Define a fallback version of the method in browsers (ahem, IE < 9), where
	  // there isn't any inspectable "Arguments" type.
	  if (!_.isArguments(arguments)) {
	    _.isArguments = function(obj) {
	      return _.has(obj, 'callee');
	    };
	  }

	  // Optimize `isFunction` if appropriate. Work around some typeof bugs in old v8,
	  // IE 11 (#1621), and in Safari 8 (#1929).
	  if (typeof /./ != 'function' && typeof Int8Array != 'object') {
	    _.isFunction = function(obj) {
	      return typeof obj == 'function' || false;
	    };
	  }

	  // Is a given object a finite number?
	  _.isFinite = function(obj) {
	    return isFinite(obj) && !isNaN(parseFloat(obj));
	  };

	  // Is the given value `NaN`? (NaN is the only number which does not equal itself).
	  _.isNaN = function(obj) {
	    return _.isNumber(obj) && obj !== +obj;
	  };

	  // Is a given value a boolean?
	  _.isBoolean = function(obj) {
	    return obj === true || obj === false || toString.call(obj) === '[object Boolean]';
	  };

	  // Is a given value equal to null?
	  _.isNull = function(obj) {
	    return obj === null;
	  };

	  // Is a given variable undefined?
	  _.isUndefined = function(obj) {
	    return obj === void 0;
	  };

	  // Shortcut function for checking if an object has a given property directly
	  // on itself (in other words, not on a prototype).
	  _.has = function(obj, key) {
	    return obj != null && hasOwnProperty.call(obj, key);
	  };

	  // Utility Functions
	  // -----------------

	  // Run Underscore.js in *noConflict* mode, returning the `_` variable to its
	  // previous owner. Returns a reference to the Underscore object.
	  _.noConflict = function() {
	    root._ = previousUnderscore;
	    return this;
	  };

	  // Keep the identity function around for default iteratees.
	  _.identity = function(value) {
	    return value;
	  };

	  // Predicate-generating functions. Often useful outside of Underscore.
	  _.constant = function(value) {
	    return function() {
	      return value;
	    };
	  };

	  _.noop = function(){};

	  _.property = property;

	  // Generates a function for a given object that returns a given property.
	  _.propertyOf = function(obj) {
	    return obj == null ? function(){} : function(key) {
	      return obj[key];
	    };
	  };

	  // Returns a predicate for checking whether an object has a given set of
	  // `key:value` pairs.
	  _.matcher = _.matches = function(attrs) {
	    attrs = _.extendOwn({}, attrs);
	    return function(obj) {
	      return _.isMatch(obj, attrs);
	    };
	  };

	  // Run a function **n** times.
	  _.times = function(n, iteratee, context) {
	    var accum = Array(Math.max(0, n));
	    iteratee = optimizeCb(iteratee, context, 1);
	    for (var i = 0; i < n; i++) accum[i] = iteratee(i);
	    return accum;
	  };

	  // Return a random integer between min and max (inclusive).
	  _.random = function(min, max) {
	    if (max == null) {
	      max = min;
	      min = 0;
	    }
	    return min + Math.floor(Math.random() * (max - min + 1));
	  };

	  // A (possibly faster) way to get the current timestamp as an integer.
	  _.now = Date.now || function() {
	    return new Date().getTime();
	  };

	   // List of HTML entities for escaping.
	  var escapeMap = {
	    '&': '&amp;',
	    '<': '&lt;',
	    '>': '&gt;',
	    '"': '&quot;',
	    "'": '&#x27;',
	    '`': '&#x60;'
	  };
	  var unescapeMap = _.invert(escapeMap);

	  // Functions for escaping and unescaping strings to/from HTML interpolation.
	  var createEscaper = function(map) {
	    var escaper = function(match) {
	      return map[match];
	    };
	    // Regexes for identifying a key that needs to be escaped
	    var source = '(?:' + _.keys(map).join('|') + ')';
	    var testRegexp = RegExp(source);
	    var replaceRegexp = RegExp(source, 'g');
	    return function(string) {
	      string = string == null ? '' : '' + string;
	      return testRegexp.test(string) ? string.replace(replaceRegexp, escaper) : string;
	    };
	  };
	  _.escape = createEscaper(escapeMap);
	  _.unescape = createEscaper(unescapeMap);

	  // If the value of the named `property` is a function then invoke it with the
	  // `object` as context; otherwise, return it.
	  _.result = function(object, property, fallback) {
	    var value = object == null ? void 0 : object[property];
	    if (value === void 0) {
	      value = fallback;
	    }
	    return _.isFunction(value) ? value.call(object) : value;
	  };

	  // Generate a unique integer id (unique within the entire client session).
	  // Useful for temporary DOM ids.
	  var idCounter = 0;
	  _.uniqueId = function(prefix) {
	    var id = ++idCounter + '';
	    return prefix ? prefix + id : id;
	  };

	  // By default, Underscore uses ERB-style template delimiters, change the
	  // following template settings to use alternative delimiters.
	  _.templateSettings = {
	    evaluate    : /<%([\s\S]+?)%>/g,
	    interpolate : /<%=([\s\S]+?)%>/g,
	    escape      : /<%-([\s\S]+?)%>/g
	  };

	  // When customizing `templateSettings`, if you don't want to define an
	  // interpolation, evaluation or escaping regex, we need one that is
	  // guaranteed not to match.
	  var noMatch = /(.)^/;

	  // Certain characters need to be escaped so that they can be put into a
	  // string literal.
	  var escapes = {
	    "'":      "'",
	    '\\':     '\\',
	    '\r':     'r',
	    '\n':     'n',
	    '\u2028': 'u2028',
	    '\u2029': 'u2029'
	  };

	  var escaper = /\\|'|\r|\n|\u2028|\u2029/g;

	  var escapeChar = function(match) {
	    return '\\' + escapes[match];
	  };

	  // JavaScript micro-templating, similar to John Resig's implementation.
	  // Underscore templating handles arbitrary delimiters, preserves whitespace,
	  // and correctly escapes quotes within interpolated code.
	  // NB: `oldSettings` only exists for backwards compatibility.
	  _.template = function(text, settings, oldSettings) {
	    if (!settings && oldSettings) settings = oldSettings;
	    settings = _.defaults({}, settings, _.templateSettings);

	    // Combine delimiters into one regular expression via alternation.
	    var matcher = RegExp([
	      (settings.escape || noMatch).source,
	      (settings.interpolate || noMatch).source,
	      (settings.evaluate || noMatch).source
	    ].join('|') + '|$', 'g');

	    // Compile the template source, escaping string literals appropriately.
	    var index = 0;
	    var source = "__p+='";
	    text.replace(matcher, function(match, escape, interpolate, evaluate, offset) {
	      source += text.slice(index, offset).replace(escaper, escapeChar);
	      index = offset + match.length;

	      if (escape) {
	        source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";
	      } else if (interpolate) {
	        source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
	      } else if (evaluate) {
	        source += "';\n" + evaluate + "\n__p+='";
	      }

	      // Adobe VMs need the match returned to produce the correct offest.
	      return match;
	    });
	    source += "';\n";

	    // If a variable is not specified, place data values in local scope.
	    if (!settings.variable) source = 'with(obj||{}){\n' + source + '}\n';

	    source = "var __t,__p='',__j=Array.prototype.join," +
	      "print=function(){__p+=__j.call(arguments,'');};\n" +
	      source + 'return __p;\n';

	    try {
	      var render = new Function(settings.variable || 'obj', '_', source);
	    } catch (e) {
	      e.source = source;
	      throw e;
	    }

	    var template = function(data) {
	      return render.call(this, data, _);
	    };

	    // Provide the compiled source as a convenience for precompilation.
	    var argument = settings.variable || 'obj';
	    template.source = 'function(' + argument + '){\n' + source + '}';

	    return template;
	  };

	  // Add a "chain" function. Start chaining a wrapped Underscore object.
	  _.chain = function(obj) {
	    var instance = _(obj);
	    instance._chain = true;
	    return instance;
	  };

	  // OOP
	  // ---------------
	  // If Underscore is called as a function, it returns a wrapped object that
	  // can be used OO-style. This wrapper holds altered versions of all the
	  // underscore functions. Wrapped objects may be chained.

	  // Helper function to continue chaining intermediate results.
	  var result = function(instance, obj) {
	    return instance._chain ? _(obj).chain() : obj;
	  };

	  // Add your own custom functions to the Underscore object.
	  _.mixin = function(obj) {
	    _.each(_.functions(obj), function(name) {
	      var func = _[name] = obj[name];
	      _.prototype[name] = function() {
	        var args = [this._wrapped];
	        push.apply(args, arguments);
	        return result(this, func.apply(_, args));
	      };
	    });
	  };

	  // Add all of the Underscore functions to the wrapper object.
	  _.mixin(_);

	  // Add all mutator Array functions to the wrapper.
	  _.each(['pop', 'push', 'reverse', 'shift', 'sort', 'splice', 'unshift'], function(name) {
	    var method = ArrayProto[name];
	    _.prototype[name] = function() {
	      var obj = this._wrapped;
	      method.apply(obj, arguments);
	      if ((name === 'shift' || name === 'splice') && obj.length === 0) delete obj[0];
	      return result(this, obj);
	    };
	  });

	  // Add all accessor Array functions to the wrapper.
	  _.each(['concat', 'join', 'slice'], function(name) {
	    var method = ArrayProto[name];
	    _.prototype[name] = function() {
	      return result(this, method.apply(this._wrapped, arguments));
	    };
	  });

	  // Extracts the result from a wrapped and chained object.
	  _.prototype.value = function() {
	    return this._wrapped;
	  };

	  // Provide unwrapping proxy for some methods used in engine operations
	  // such as arithmetic and JSON stringification.
	  _.prototype.valueOf = _.prototype.toJSON = _.prototype.value;

	  _.prototype.toString = function() {
	    return '' + this._wrapped;
	  };

	  // AMD registration happens at the end for compatibility with AMD loaders
	  // that may not enforce next-turn semantics on modules. Even though general
	  // practice for AMD registration is to be anonymous, underscore registers
	  // as a named module because, like jQuery, it is a base library that is
	  // popular enough to be bundled in a third party lib, but not be part of
	  // an AMD load request. Those cases could generate an error when an
	  // anonymous define() is called outside of a loader request.
	  if (true) {
	    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = function() {
	      return _;
	    }.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	  }
	}.call(this));


/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;// MarionetteJS (Backbone.Marionette)
	// ----------------------------------
	// v2.4.3
	//
	// Copyright (c)2015 Derick Bailey, Muted Solutions, LLC.
	// Distributed under MIT license
	//
	// http://marionettejs.com


	/*!
	 * Includes BabySitter
	 * https://github.com/marionettejs/backbone.babysitter/
	 *
	 * Includes Wreqr
	 * https://github.com/marionettejs/backbone.wreqr/
	 */


	(function(root, factory) {

	  /* istanbul ignore next */
	  if (true) {
	    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(4), __webpack_require__(2)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Backbone, _) {
	      return (root.Marionette = root.Mn = factory(root, Backbone, _));
	    }.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	  } else if (typeof exports !== 'undefined') {
	    var Backbone = require('backbone');
	    var _ = require('underscore');
	    module.exports = factory(root, Backbone, _);
	  } else {
	    root.Marionette = root.Mn = factory(root, root.Backbone, root._);
	  }

	}(this, function(root, Backbone, _) {
	  'use strict';

	  /* istanbul ignore next */
	  // Backbone.BabySitter
	  // -------------------
	  // v0.1.10
	  //
	  // Copyright (c)2015 Derick Bailey, Muted Solutions, LLC.
	  // Distributed under MIT license
	  //
	  // http://github.com/marionettejs/backbone.babysitter
	  (function(Backbone, _) {
	    "use strict";
	    var previousChildViewContainer = Backbone.ChildViewContainer;
	    // BabySitter.ChildViewContainer
	    // -----------------------------
	    //
	    // Provide a container to store, retrieve and
	    // shut down child views.
	    Backbone.ChildViewContainer = function(Backbone, _) {
	      // Container Constructor
	      // ---------------------
	      var Container = function(views) {
	        this._views = {};
	        this._indexByModel = {};
	        this._indexByCustom = {};
	        this._updateLength();
	        _.each(views, this.add, this);
	      };
	      // Container Methods
	      // -----------------
	      _.extend(Container.prototype, {
	        // Add a view to this container. Stores the view
	        // by `cid` and makes it searchable by the model
	        // cid (and model itself). Optionally specify
	        // a custom key to store an retrieve the view.
	        add: function(view, customIndex) {
	          var viewCid = view.cid;
	          // store the view
	          this._views[viewCid] = view;
	          // index it by model
	          if (view.model) {
	            this._indexByModel[view.model.cid] = viewCid;
	          }
	          // index by custom
	          if (customIndex) {
	            this._indexByCustom[customIndex] = viewCid;
	          }
	          this._updateLength();
	          return this;
	        },
	        // Find a view by the model that was attached to
	        // it. Uses the model's `cid` to find it.
	        findByModel: function(model) {
	          return this.findByModelCid(model.cid);
	        },
	        // Find a view by the `cid` of the model that was attached to
	        // it. Uses the model's `cid` to find the view `cid` and
	        // retrieve the view using it.
	        findByModelCid: function(modelCid) {
	          var viewCid = this._indexByModel[modelCid];
	          return this.findByCid(viewCid);
	        },
	        // Find a view by a custom indexer.
	        findByCustom: function(index) {
	          var viewCid = this._indexByCustom[index];
	          return this.findByCid(viewCid);
	        },
	        // Find by index. This is not guaranteed to be a
	        // stable index.
	        findByIndex: function(index) {
	          return _.values(this._views)[index];
	        },
	        // retrieve a view by its `cid` directly
	        findByCid: function(cid) {
	          return this._views[cid];
	        },
	        // Remove a view
	        remove: function(view) {
	          var viewCid = view.cid;
	          // delete model index
	          if (view.model) {
	            delete this._indexByModel[view.model.cid];
	          }
	          // delete custom index
	          _.any(this._indexByCustom, function(cid, key) {
	            if (cid === viewCid) {
	              delete this._indexByCustom[key];
	              return true;
	            }
	          }, this);
	          // remove the view from the container
	          delete this._views[viewCid];
	          // update the length
	          this._updateLength();
	          return this;
	        },
	        // Call a method on every view in the container,
	        // passing parameters to the call method one at a
	        // time, like `function.call`.
	        call: function(method) {
	          this.apply(method, _.tail(arguments));
	        },
	        // Apply a method on every view in the container,
	        // passing parameters to the call method one at a
	        // time, like `function.apply`.
	        apply: function(method, args) {
	          _.each(this._views, function(view) {
	            if (_.isFunction(view[method])) {
	              view[method].apply(view, args || []);
	            }
	          });
	        },
	        // Update the `.length` attribute on this container
	        _updateLength: function() {
	          this.length = _.size(this._views);
	        }
	      });
	      // Borrowing this code from Backbone.Collection:
	      // http://backbonejs.org/docs/backbone.html#section-106
	      //
	      // Mix in methods from Underscore, for iteration, and other
	      // collection related features.
	      var methods = [ "forEach", "each", "map", "find", "detect", "filter", "select", "reject", "every", "all", "some", "any", "include", "contains", "invoke", "toArray", "first", "initial", "rest", "last", "without", "isEmpty", "pluck", "reduce" ];
	      _.each(methods, function(method) {
	        Container.prototype[method] = function() {
	          var views = _.values(this._views);
	          var args = [ views ].concat(_.toArray(arguments));
	          return _[method].apply(_, args);
	        };
	      });
	      // return the public API
	      return Container;
	    }(Backbone, _);
	    Backbone.ChildViewContainer.VERSION = "0.1.10";
	    Backbone.ChildViewContainer.noConflict = function() {
	      Backbone.ChildViewContainer = previousChildViewContainer;
	      return this;
	    };
	    return Backbone.ChildViewContainer;
	  })(Backbone, _);

	  /* istanbul ignore next */
	  // Backbone.Wreqr (Backbone.Marionette)
	  // ----------------------------------
	  // v1.3.5
	  //
	  // Copyright (c)2015 Derick Bailey, Muted Solutions, LLC.
	  // Distributed under MIT license
	  //
	  // http://github.com/marionettejs/backbone.wreqr
	  (function(Backbone, _) {
	    "use strict";
	    var previousWreqr = Backbone.Wreqr;
	    var Wreqr = Backbone.Wreqr = {};
	    Backbone.Wreqr.VERSION = "1.3.5";
	    Backbone.Wreqr.noConflict = function() {
	      Backbone.Wreqr = previousWreqr;
	      return this;
	    };
	    // Handlers
	    // --------
	    // A registry of functions to call, given a name
	    Wreqr.Handlers = function(Backbone, _) {
	      "use strict";
	      // Constructor
	      // -----------
	      var Handlers = function(options) {
	        this.options = options;
	        this._wreqrHandlers = {};
	        if (_.isFunction(this.initialize)) {
	          this.initialize(options);
	        }
	      };
	      Handlers.extend = Backbone.Model.extend;
	      // Instance Members
	      // ----------------
	      _.extend(Handlers.prototype, Backbone.Events, {
	        // Add multiple handlers using an object literal configuration
	        setHandlers: function(handlers) {
	          _.each(handlers, function(handler, name) {
	            var context = null;
	            if (_.isObject(handler) && !_.isFunction(handler)) {
	              context = handler.context;
	              handler = handler.callback;
	            }
	            this.setHandler(name, handler, context);
	          }, this);
	        },
	        // Add a handler for the given name, with an
	        // optional context to run the handler within
	        setHandler: function(name, handler, context) {
	          var config = {
	            callback: handler,
	            context: context
	          };
	          this._wreqrHandlers[name] = config;
	          this.trigger("handler:add", name, handler, context);
	        },
	        // Determine whether or not a handler is registered
	        hasHandler: function(name) {
	          return !!this._wreqrHandlers[name];
	        },
	        // Get the currently registered handler for
	        // the specified name. Throws an exception if
	        // no handler is found.
	        getHandler: function(name) {
	          var config = this._wreqrHandlers[name];
	          if (!config) {
	            return;
	          }
	          return function() {
	            return config.callback.apply(config.context, arguments);
	          };
	        },
	        // Remove a handler for the specified name
	        removeHandler: function(name) {
	          delete this._wreqrHandlers[name];
	        },
	        // Remove all handlers from this registry
	        removeAllHandlers: function() {
	          this._wreqrHandlers = {};
	        }
	      });
	      return Handlers;
	    }(Backbone, _);
	    // Wreqr.CommandStorage
	    // --------------------
	    //
	    // Store and retrieve commands for execution.
	    Wreqr.CommandStorage = function() {
	      "use strict";
	      // Constructor function
	      var CommandStorage = function(options) {
	        this.options = options;
	        this._commands = {};
	        if (_.isFunction(this.initialize)) {
	          this.initialize(options);
	        }
	      };
	      // Instance methods
	      _.extend(CommandStorage.prototype, Backbone.Events, {
	        // Get an object literal by command name, that contains
	        // the `commandName` and the `instances` of all commands
	        // represented as an array of arguments to process
	        getCommands: function(commandName) {
	          var commands = this._commands[commandName];
	          // we don't have it, so add it
	          if (!commands) {
	            // build the configuration
	            commands = {
	              command: commandName,
	              instances: []
	            };
	            // store it
	            this._commands[commandName] = commands;
	          }
	          return commands;
	        },
	        // Add a command by name, to the storage and store the
	        // args for the command
	        addCommand: function(commandName, args) {
	          var command = this.getCommands(commandName);
	          command.instances.push(args);
	        },
	        // Clear all commands for the given `commandName`
	        clearCommands: function(commandName) {
	          var command = this.getCommands(commandName);
	          command.instances = [];
	        }
	      });
	      return CommandStorage;
	    }();
	    // Wreqr.Commands
	    // --------------
	    //
	    // A simple command pattern implementation. Register a command
	    // handler and execute it.
	    Wreqr.Commands = function(Wreqr, _) {
	      "use strict";
	      return Wreqr.Handlers.extend({
	        // default storage type
	        storageType: Wreqr.CommandStorage,
	        constructor: function(options) {
	          this.options = options || {};
	          this._initializeStorage(this.options);
	          this.on("handler:add", this._executeCommands, this);
	          Wreqr.Handlers.prototype.constructor.apply(this, arguments);
	        },
	        // Execute a named command with the supplied args
	        execute: function(name) {
	          name = arguments[0];
	          var args = _.rest(arguments);
	          if (this.hasHandler(name)) {
	            this.getHandler(name).apply(this, args);
	          } else {
	            this.storage.addCommand(name, args);
	          }
	        },
	        // Internal method to handle bulk execution of stored commands
	        _executeCommands: function(name, handler, context) {
	          var command = this.storage.getCommands(name);
	          // loop through and execute all the stored command instances
	          _.each(command.instances, function(args) {
	            handler.apply(context, args);
	          });
	          this.storage.clearCommands(name);
	        },
	        // Internal method to initialize storage either from the type's
	        // `storageType` or the instance `options.storageType`.
	        _initializeStorage: function(options) {
	          var storage;
	          var StorageType = options.storageType || this.storageType;
	          if (_.isFunction(StorageType)) {
	            storage = new StorageType();
	          } else {
	            storage = StorageType;
	          }
	          this.storage = storage;
	        }
	      });
	    }(Wreqr, _);
	    // Wreqr.RequestResponse
	    // ---------------------
	    //
	    // A simple request/response implementation. Register a
	    // request handler, and return a response from it
	    Wreqr.RequestResponse = function(Wreqr, _) {
	      "use strict";
	      return Wreqr.Handlers.extend({
	        request: function(name) {
	          if (this.hasHandler(name)) {
	            return this.getHandler(name).apply(this, _.rest(arguments));
	          }
	        }
	      });
	    }(Wreqr, _);
	    // Event Aggregator
	    // ----------------
	    // A pub-sub object that can be used to decouple various parts
	    // of an application through event-driven architecture.
	    Wreqr.EventAggregator = function(Backbone, _) {
	      "use strict";
	      var EA = function() {};
	      // Copy the `extend` function used by Backbone's classes
	      EA.extend = Backbone.Model.extend;
	      // Copy the basic Backbone.Events on to the event aggregator
	      _.extend(EA.prototype, Backbone.Events);
	      return EA;
	    }(Backbone, _);
	    // Wreqr.Channel
	    // --------------
	    //
	    // An object that wraps the three messaging systems:
	    // EventAggregator, RequestResponse, Commands
	    Wreqr.Channel = function(Wreqr) {
	      "use strict";
	      var Channel = function(channelName) {
	        this.vent = new Backbone.Wreqr.EventAggregator();
	        this.reqres = new Backbone.Wreqr.RequestResponse();
	        this.commands = new Backbone.Wreqr.Commands();
	        this.channelName = channelName;
	      };
	      _.extend(Channel.prototype, {
	        // Remove all handlers from the messaging systems of this channel
	        reset: function() {
	          this.vent.off();
	          this.vent.stopListening();
	          this.reqres.removeAllHandlers();
	          this.commands.removeAllHandlers();
	          return this;
	        },
	        // Connect a hash of events; one for each messaging system
	        connectEvents: function(hash, context) {
	          this._connect("vent", hash, context);
	          return this;
	        },
	        connectCommands: function(hash, context) {
	          this._connect("commands", hash, context);
	          return this;
	        },
	        connectRequests: function(hash, context) {
	          this._connect("reqres", hash, context);
	          return this;
	        },
	        // Attach the handlers to a given message system `type`
	        _connect: function(type, hash, context) {
	          if (!hash) {
	            return;
	          }
	          context = context || this;
	          var method = type === "vent" ? "on" : "setHandler";
	          _.each(hash, function(fn, eventName) {
	            this[type][method](eventName, _.bind(fn, context));
	          }, this);
	        }
	      });
	      return Channel;
	    }(Wreqr);
	    // Wreqr.Radio
	    // --------------
	    //
	    // An object that lets you communicate with many channels.
	    Wreqr.radio = function(Wreqr, _) {
	      "use strict";
	      var Radio = function() {
	        this._channels = {};
	        this.vent = {};
	        this.commands = {};
	        this.reqres = {};
	        this._proxyMethods();
	      };
	      _.extend(Radio.prototype, {
	        channel: function(channelName) {
	          if (!channelName) {
	            throw new Error("Channel must receive a name");
	          }
	          return this._getChannel(channelName);
	        },
	        _getChannel: function(channelName) {
	          var channel = this._channels[channelName];
	          if (!channel) {
	            channel = new Wreqr.Channel(channelName);
	            this._channels[channelName] = channel;
	          }
	          return channel;
	        },
	        _proxyMethods: function() {
	          _.each([ "vent", "commands", "reqres" ], function(system) {
	            _.each(messageSystems[system], function(method) {
	              this[system][method] = proxyMethod(this, system, method);
	            }, this);
	          }, this);
	        }
	      });
	      var messageSystems = {
	        vent: [ "on", "off", "trigger", "once", "stopListening", "listenTo", "listenToOnce" ],
	        commands: [ "execute", "setHandler", "setHandlers", "removeHandler", "removeAllHandlers" ],
	        reqres: [ "request", "setHandler", "setHandlers", "removeHandler", "removeAllHandlers" ]
	      };
	      var proxyMethod = function(radio, system, method) {
	        return function(channelName) {
	          var messageSystem = radio._getChannel(channelName)[system];
	          return messageSystem[method].apply(messageSystem, _.rest(arguments));
	        };
	      };
	      return new Radio();
	    }(Wreqr, _);
	    return Backbone.Wreqr;
	  })(Backbone, _);

	  var previousMarionette = root.Marionette;
	  var previousMn = root.Mn;

	  var Marionette = Backbone.Marionette = {};

	  Marionette.VERSION = '2.4.3';

	  Marionette.noConflict = function() {
	    root.Marionette = previousMarionette;
	    root.Mn = previousMn;
	    return this;
	  };

	  Backbone.Marionette = Marionette;

	  // Get the Deferred creator for later use
	  Marionette.Deferred = Backbone.$.Deferred;

	  /* jshint unused: false *//* global console */
	  
	  // Helpers
	  // -------
	  
	  // Marionette.extend
	  // -----------------
	  
	  // Borrow the Backbone `extend` method so we can use it as needed
	  Marionette.extend = Backbone.Model.extend;
	  
	  // Marionette.isNodeAttached
	  // -------------------------
	  
	  // Determine if `el` is a child of the document
	  Marionette.isNodeAttached = function(el) {
	    return Backbone.$.contains(document.documentElement, el);
	  };
	  
	  // Merge `keys` from `options` onto `this`
	  Marionette.mergeOptions = function(options, keys) {
	    if (!options) { return; }
	    _.extend(this, _.pick(options, keys));
	  };
	  
	  // Marionette.getOption
	  // --------------------
	  
	  // Retrieve an object, function or other value from a target
	  // object or its `options`, with `options` taking precedence.
	  Marionette.getOption = function(target, optionName) {
	    if (!target || !optionName) { return; }
	    if (target.options && (target.options[optionName] !== undefined)) {
	      return target.options[optionName];
	    } else {
	      return target[optionName];
	    }
	  };
	  
	  // Proxy `Marionette.getOption`
	  Marionette.proxyGetOption = function(optionName) {
	    return Marionette.getOption(this, optionName);
	  };
	  
	  // Similar to `_.result`, this is a simple helper
	  // If a function is provided we call it with context
	  // otherwise just return the value. If the value is
	  // undefined return a default value
	  Marionette._getValue = function(value, context, params) {
	    if (_.isFunction(value)) {
	      value = params ? value.apply(context, params) : value.call(context);
	    }
	    return value;
	  };
	  
	  // Marionette.normalizeMethods
	  // ----------------------
	  
	  // Pass in a mapping of events => functions or function names
	  // and return a mapping of events => functions
	  Marionette.normalizeMethods = function(hash) {
	    return _.reduce(hash, function(normalizedHash, method, name) {
	      if (!_.isFunction(method)) {
	        method = this[method];
	      }
	      if (method) {
	        normalizedHash[name] = method;
	      }
	      return normalizedHash;
	    }, {}, this);
	  };
	  
	  // utility method for parsing @ui. syntax strings
	  // into associated selector
	  Marionette.normalizeUIString = function(uiString, ui) {
	    return uiString.replace(/@ui\.[a-zA-Z_$0-9]*/g, function(r) {
	      return ui[r.slice(4)];
	    });
	  };
	  
	  // allows for the use of the @ui. syntax within
	  // a given key for triggers and events
	  // swaps the @ui with the associated selector.
	  // Returns a new, non-mutated, parsed events hash.
	  Marionette.normalizeUIKeys = function(hash, ui) {
	    return _.reduce(hash, function(memo, val, key) {
	      var normalizedKey = Marionette.normalizeUIString(key, ui);
	      memo[normalizedKey] = val;
	      return memo;
	    }, {});
	  };
	  
	  // allows for the use of the @ui. syntax within
	  // a given value for regions
	  // swaps the @ui with the associated selector
	  Marionette.normalizeUIValues = function(hash, ui, properties) {
	    _.each(hash, function(val, key) {
	      if (_.isString(val)) {
	        hash[key] = Marionette.normalizeUIString(val, ui);
	      } else if (_.isObject(val) && _.isArray(properties)) {
	        _.extend(val, Marionette.normalizeUIValues(_.pick(val, properties), ui));
	        /* Value is an object, and we got an array of embedded property names to normalize. */
	        _.each(properties, function(property) {
	          var propertyVal = val[property];
	          if (_.isString(propertyVal)) {
	            val[property] = Marionette.normalizeUIString(propertyVal, ui);
	          }
	        });
	      }
	    });
	    return hash;
	  };
	  
	  // Mix in methods from Underscore, for iteration, and other
	  // collection related features.
	  // Borrowing this code from Backbone.Collection:
	  // http://backbonejs.org/docs/backbone.html#section-121
	  Marionette.actAsCollection = function(object, listProperty) {
	    var methods = ['forEach', 'each', 'map', 'find', 'detect', 'filter',
	      'select', 'reject', 'every', 'all', 'some', 'any', 'include',
	      'contains', 'invoke', 'toArray', 'first', 'initial', 'rest',
	      'last', 'without', 'isEmpty', 'pluck'];
	  
	    _.each(methods, function(method) {
	      object[method] = function() {
	        var list = _.values(_.result(this, listProperty));
	        var args = [list].concat(_.toArray(arguments));
	        return _[method].apply(_, args);
	      };
	    });
	  };
	  
	  var deprecate = Marionette.deprecate = function(message, test) {
	    if (_.isObject(message)) {
	      message = (
	        message.prev + ' is going to be removed in the future. ' +
	        'Please use ' + message.next + ' instead.' +
	        (message.url ? ' See: ' + message.url : '')
	      );
	    }
	  
	    if ((test === undefined || !test) && !deprecate._cache[message]) {
	      deprecate._warn('Deprecation warning: ' + message);
	      deprecate._cache[message] = true;
	    }
	  };
	  
	  deprecate._warn = typeof console !== 'undefined' && (console.warn || console.log) || function() {};
	  deprecate._cache = {};
	  
	  /* jshint maxstatements: 14, maxcomplexity: 7 */
	  
	  // Trigger Method
	  // --------------
	  
	  Marionette._triggerMethod = (function() {
	    // split the event name on the ":"
	    var splitter = /(^|:)(\w)/gi;
	  
	    // take the event section ("section1:section2:section3")
	    // and turn it in to uppercase name
	    function getEventName(match, prefix, eventName) {
	      return eventName.toUpperCase();
	    }
	  
	    return function(context, event, args) {
	      var noEventArg = arguments.length < 3;
	      if (noEventArg) {
	        args = event;
	        event = args[0];
	      }
	  
	      // get the method name from the event name
	      var methodName = 'on' + event.replace(splitter, getEventName);
	      var method = context[methodName];
	      var result;
	  
	      // call the onMethodName if it exists
	      if (_.isFunction(method)) {
	        // pass all args, except the event name
	        result = method.apply(context, noEventArg ? _.rest(args) : args);
	      }
	  
	      // trigger the event, if a trigger method exists
	      if (_.isFunction(context.trigger)) {
	        if (noEventArg + args.length > 1) {
	          context.trigger.apply(context, noEventArg ? args : [event].concat(_.drop(args, 0)));
	        } else {
	          context.trigger(event);
	        }
	      }
	  
	      return result;
	    };
	  })();
	  
	  // Trigger an event and/or a corresponding method name. Examples:
	  //
	  // `this.triggerMethod("foo")` will trigger the "foo" event and
	  // call the "onFoo" method.
	  //
	  // `this.triggerMethod("foo:bar")` will trigger the "foo:bar" event and
	  // call the "onFooBar" method.
	  Marionette.triggerMethod = function(event) {
	    return Marionette._triggerMethod(this, arguments);
	  };
	  
	  // triggerMethodOn invokes triggerMethod on a specific context
	  //
	  // e.g. `Marionette.triggerMethodOn(view, 'show')`
	  // will trigger a "show" event or invoke onShow the view.
	  Marionette.triggerMethodOn = function(context) {
	    var fnc = _.isFunction(context.triggerMethod) ?
	                  context.triggerMethod :
	                  Marionette.triggerMethod;
	  
	    return fnc.apply(context, _.rest(arguments));
	  };
	  
	  // DOM Refresh
	  // -----------
	  
	  // Monitor a view's state, and after it has been rendered and shown
	  // in the DOM, trigger a "dom:refresh" event every time it is
	  // re-rendered.
	  
	  Marionette.MonitorDOMRefresh = function(view) {
	    if (view._isDomRefreshMonitored) { return; }
	    view._isDomRefreshMonitored = true;
	  
	    // track when the view has been shown in the DOM,
	    // using a Marionette.Region (or by other means of triggering "show")
	    function handleShow() {
	      view._isShown = true;
	      triggerDOMRefresh();
	    }
	  
	    // track when the view has been rendered
	    function handleRender() {
	      view._isRendered = true;
	      triggerDOMRefresh();
	    }
	  
	    // Trigger the "dom:refresh" event and corresponding "onDomRefresh" method
	    function triggerDOMRefresh() {
	      if (view._isShown && view._isRendered && Marionette.isNodeAttached(view.el)) {
	        Marionette.triggerMethodOn(view, 'dom:refresh', view);
	      }
	    }
	  
	    view.on({
	      show: handleShow,
	      render: handleRender
	    });
	  };
	  
	  /* jshint maxparams: 5 */
	  
	  // Bind Entity Events & Unbind Entity Events
	  // -----------------------------------------
	  //
	  // These methods are used to bind/unbind a backbone "entity" (e.g. collection/model)
	  // to methods on a target object.
	  //
	  // The first parameter, `target`, must have the Backbone.Events module mixed in.
	  //
	  // The second parameter is the `entity` (Backbone.Model, Backbone.Collection or
	  // any object that has Backbone.Events mixed in) to bind the events from.
	  //
	  // The third parameter is a hash of { "event:name": "eventHandler" }
	  // configuration. Multiple handlers can be separated by a space. A
	  // function can be supplied instead of a string handler name.
	  
	  (function(Marionette) {
	    'use strict';
	  
	    // Bind the event to handlers specified as a string of
	    // handler names on the target object
	    function bindFromStrings(target, entity, evt, methods) {
	      var methodNames = methods.split(/\s+/);
	  
	      _.each(methodNames, function(methodName) {
	  
	        var method = target[methodName];
	        if (!method) {
	          throw new Marionette.Error('Method "' + methodName +
	            '" was configured as an event handler, but does not exist.');
	        }
	  
	        target.listenTo(entity, evt, method);
	      });
	    }
	  
	    // Bind the event to a supplied callback function
	    function bindToFunction(target, entity, evt, method) {
	      target.listenTo(entity, evt, method);
	    }
	  
	    // Bind the event to handlers specified as a string of
	    // handler names on the target object
	    function unbindFromStrings(target, entity, evt, methods) {
	      var methodNames = methods.split(/\s+/);
	  
	      _.each(methodNames, function(methodName) {
	        var method = target[methodName];
	        target.stopListening(entity, evt, method);
	      });
	    }
	  
	    // Bind the event to a supplied callback function
	    function unbindToFunction(target, entity, evt, method) {
	      target.stopListening(entity, evt, method);
	    }
	  
	    // generic looping function
	    function iterateEvents(target, entity, bindings, functionCallback, stringCallback) {
	      if (!entity || !bindings) { return; }
	  
	      // type-check bindings
	      if (!_.isObject(bindings)) {
	        throw new Marionette.Error({
	          message: 'Bindings must be an object or function.',
	          url: 'marionette.functions.html#marionettebindentityevents'
	        });
	      }
	  
	      // allow the bindings to be a function
	      bindings = Marionette._getValue(bindings, target);
	  
	      // iterate the bindings and bind them
	      _.each(bindings, function(methods, evt) {
	  
	        // allow for a function as the handler,
	        // or a list of event names as a string
	        if (_.isFunction(methods)) {
	          functionCallback(target, entity, evt, methods);
	        } else {
	          stringCallback(target, entity, evt, methods);
	        }
	  
	      });
	    }
	  
	    // Export Public API
	    Marionette.bindEntityEvents = function(target, entity, bindings) {
	      iterateEvents(target, entity, bindings, bindToFunction, bindFromStrings);
	    };
	  
	    Marionette.unbindEntityEvents = function(target, entity, bindings) {
	      iterateEvents(target, entity, bindings, unbindToFunction, unbindFromStrings);
	    };
	  
	    // Proxy `bindEntityEvents`
	    Marionette.proxyBindEntityEvents = function(entity, bindings) {
	      return Marionette.bindEntityEvents(this, entity, bindings);
	    };
	  
	    // Proxy `unbindEntityEvents`
	    Marionette.proxyUnbindEntityEvents = function(entity, bindings) {
	      return Marionette.unbindEntityEvents(this, entity, bindings);
	    };
	  })(Marionette);
	  

	  // Error
	  // -----
	  
	  var errorProps = ['description', 'fileName', 'lineNumber', 'name', 'message', 'number'];
	  
	  Marionette.Error = Marionette.extend.call(Error, {
	    urlRoot: 'http://marionettejs.com/docs/v' + Marionette.VERSION + '/',
	  
	    constructor: function(message, options) {
	      if (_.isObject(message)) {
	        options = message;
	        message = options.message;
	      } else if (!options) {
	        options = {};
	      }
	  
	      var error = Error.call(this, message);
	      _.extend(this, _.pick(error, errorProps), _.pick(options, errorProps));
	  
	      this.captureStackTrace();
	  
	      if (options.url) {
	        this.url = this.urlRoot + options.url;
	      }
	    },
	  
	    captureStackTrace: function() {
	      if (Error.captureStackTrace) {
	        Error.captureStackTrace(this, Marionette.Error);
	      }
	    },
	  
	    toString: function() {
	      return this.name + ': ' + this.message + (this.url ? ' See: ' + this.url : '');
	    }
	  });
	  
	  Marionette.Error.extend = Marionette.extend;
	  
	  // Callbacks
	  // ---------
	  
	  // A simple way of managing a collection of callbacks
	  // and executing them at a later point in time, using jQuery's
	  // `Deferred` object.
	  Marionette.Callbacks = function() {
	    this._deferred = Marionette.Deferred();
	    this._callbacks = [];
	  };
	  
	  _.extend(Marionette.Callbacks.prototype, {
	  
	    // Add a callback to be executed. Callbacks added here are
	    // guaranteed to execute, even if they are added after the
	    // `run` method is called.
	    add: function(callback, contextOverride) {
	      var promise = _.result(this._deferred, 'promise');
	  
	      this._callbacks.push({cb: callback, ctx: contextOverride});
	  
	      promise.then(function(args) {
	        if (contextOverride) { args.context = contextOverride; }
	        callback.call(args.context, args.options);
	      });
	    },
	  
	    // Run all registered callbacks with the context specified.
	    // Additional callbacks can be added after this has been run
	    // and they will still be executed.
	    run: function(options, context) {
	      this._deferred.resolve({
	        options: options,
	        context: context
	      });
	    },
	  
	    // Resets the list of callbacks to be run, allowing the same list
	    // to be run multiple times - whenever the `run` method is called.
	    reset: function() {
	      var callbacks = this._callbacks;
	      this._deferred = Marionette.Deferred();
	      this._callbacks = [];
	  
	      _.each(callbacks, function(cb) {
	        this.add(cb.cb, cb.ctx);
	      }, this);
	    }
	  });
	  
	  // Controller
	  // ----------
	  
	  // A multi-purpose object to use as a controller for
	  // modules and routers, and as a mediator for workflow
	  // and coordination of other objects, views, and more.
	  Marionette.Controller = function(options) {
	    this.options = options || {};
	  
	    if (_.isFunction(this.initialize)) {
	      this.initialize(this.options);
	    }
	  };
	  
	  Marionette.Controller.extend = Marionette.extend;
	  
	  // Controller Methods
	  // --------------
	  
	  // Ensure it can trigger events with Backbone.Events
	  _.extend(Marionette.Controller.prototype, Backbone.Events, {
	    destroy: function() {
	      Marionette._triggerMethod(this, 'before:destroy', arguments);
	      Marionette._triggerMethod(this, 'destroy', arguments);
	  
	      this.stopListening();
	      this.off();
	      return this;
	    },
	  
	    // import the `triggerMethod` to trigger events with corresponding
	    // methods if the method exists
	    triggerMethod: Marionette.triggerMethod,
	  
	    // A handy way to merge options onto the instance
	    mergeOptions: Marionette.mergeOptions,
	  
	    // Proxy `getOption` to enable getting options from this or this.options by name.
	    getOption: Marionette.proxyGetOption
	  
	  });
	  
	  // Object
	  // ------
	  
	  // A Base Class that other Classes should descend from.
	  // Object borrows many conventions and utilities from Backbone.
	  Marionette.Object = function(options) {
	    this.options = _.extend({}, _.result(this, 'options'), options);
	  
	    this.initialize.apply(this, arguments);
	  };
	  
	  Marionette.Object.extend = Marionette.extend;
	  
	  // Object Methods
	  // --------------
	  
	  // Ensure it can trigger events with Backbone.Events
	  _.extend(Marionette.Object.prototype, Backbone.Events, {
	  
	    //this is a noop method intended to be overridden by classes that extend from this base
	    initialize: function() {},
	  
	    destroy: function() {
	      this.triggerMethod('before:destroy');
	      this.triggerMethod('destroy');
	      this.stopListening();
	  
	      return this;
	    },
	  
	    // Import the `triggerMethod` to trigger events with corresponding
	    // methods if the method exists
	    triggerMethod: Marionette.triggerMethod,
	  
	    // A handy way to merge options onto the instance
	    mergeOptions: Marionette.mergeOptions,
	  
	    // Proxy `getOption` to enable getting options from this or this.options by name.
	    getOption: Marionette.proxyGetOption,
	  
	    // Proxy `bindEntityEvents` to enable binding view's events from another entity.
	    bindEntityEvents: Marionette.proxyBindEntityEvents,
	  
	    // Proxy `unbindEntityEvents` to enable unbinding view's events from another entity.
	    unbindEntityEvents: Marionette.proxyUnbindEntityEvents
	  });
	  
	  /* jshint maxcomplexity: 16, maxstatements: 45, maxlen: 120 */
	  
	  // Region
	  // ------
	  
	  // Manage the visual regions of your composite application. See
	  // http://lostechies.com/derickbailey/2011/12/12/composite-js-apps-regions-and-region-managers/
	  
	  Marionette.Region = Marionette.Object.extend({
	    constructor: function(options) {
	  
	      // set options temporarily so that we can get `el`.
	      // options will be overriden by Object.constructor
	      this.options = options || {};
	      this.el = this.getOption('el');
	  
	      // Handle when this.el is passed in as a $ wrapped element.
	      this.el = this.el instanceof Backbone.$ ? this.el[0] : this.el;
	  
	      if (!this.el) {
	        throw new Marionette.Error({
	          name: 'NoElError',
	          message: 'An "el" must be specified for a region.'
	        });
	      }
	  
	      this.$el = this.getEl(this.el);
	      Marionette.Object.call(this, options);
	    },
	  
	    // Displays a backbone view instance inside of the region.
	    // Handles calling the `render` method for you. Reads content
	    // directly from the `el` attribute. Also calls an optional
	    // `onShow` and `onDestroy` method on your view, just after showing
	    // or just before destroying the view, respectively.
	    // The `preventDestroy` option can be used to prevent a view from
	    // the old view being destroyed on show.
	    // The `forceShow` option can be used to force a view to be
	    // re-rendered if it's already shown in the region.
	    show: function(view, options) {
	      if (!this._ensureElement()) {
	        return;
	      }
	  
	      this._ensureViewIsIntact(view);
	      Marionette.MonitorDOMRefresh(view);
	  
	      var showOptions     = options || {};
	      var isDifferentView = view !== this.currentView;
	      var preventDestroy  = !!showOptions.preventDestroy;
	      var forceShow       = !!showOptions.forceShow;
	  
	      // We are only changing the view if there is a current view to change to begin with
	      var isChangingView = !!this.currentView;
	  
	      // Only destroy the current view if we don't want to `preventDestroy` and if
	      // the view given in the first argument is different than `currentView`
	      var _shouldDestroyView = isDifferentView && !preventDestroy;
	  
	      // Only show the view given in the first argument if it is different than
	      // the current view or if we want to re-show the view. Note that if
	      // `_shouldDestroyView` is true, then `_shouldShowView` is also necessarily true.
	      var _shouldShowView = isDifferentView || forceShow;
	  
	      if (isChangingView) {
	        this.triggerMethod('before:swapOut', this.currentView, this, options);
	      }
	  
	      if (this.currentView) {
	        delete this.currentView._parent;
	      }
	  
	      if (_shouldDestroyView) {
	        this.empty();
	  
	      // A `destroy` event is attached to the clean up manually removed views.
	      // We need to detach this event when a new view is going to be shown as it
	      // is no longer relevant.
	      } else if (isChangingView && _shouldShowView) {
	        this.currentView.off('destroy', this.empty, this);
	      }
	  
	      if (_shouldShowView) {
	  
	        // We need to listen for if a view is destroyed
	        // in a way other than through the region.
	        // If this happens we need to remove the reference
	        // to the currentView since once a view has been destroyed
	        // we can not reuse it.
	        view.once('destroy', this.empty, this);
	  
	        this._renderView(view);
	  
	        view._parent = this;
	  
	        if (isChangingView) {
	          this.triggerMethod('before:swap', view, this, options);
	        }
	  
	        this.triggerMethod('before:show', view, this, options);
	        Marionette.triggerMethodOn(view, 'before:show', view, this, options);
	  
	        if (isChangingView) {
	          this.triggerMethod('swapOut', this.currentView, this, options);
	        }
	  
	        // An array of views that we're about to display
	        var attachedRegion = Marionette.isNodeAttached(this.el);
	  
	        // The views that we're about to attach to the document
	        // It's important that we prevent _getNestedViews from being executed unnecessarily
	        // as it's a potentially-slow method
	        var displayedViews = [];
	  
	        var attachOptions = _.extend({
	          triggerBeforeAttach: this.triggerBeforeAttach,
	          triggerAttach: this.triggerAttach
	        }, showOptions);
	  
	        if (attachedRegion && attachOptions.triggerBeforeAttach) {
	          displayedViews = this._displayedViews(view);
	          this._triggerAttach(displayedViews, 'before:');
	        }
	  
	        this.attachHtml(view);
	        this.currentView = view;
	  
	        if (attachedRegion && attachOptions.triggerAttach) {
	          displayedViews = this._displayedViews(view);
	          this._triggerAttach(displayedViews);
	        }
	  
	        if (isChangingView) {
	          this.triggerMethod('swap', view, this, options);
	        }
	  
	        this.triggerMethod('show', view, this, options);
	        Marionette.triggerMethodOn(view, 'show', view, this, options);
	  
	        return this;
	      }
	  
	      return this;
	    },
	  
	    triggerBeforeAttach: true,
	    triggerAttach: true,
	  
	    _triggerAttach: function(views, prefix) {
	      var eventName = (prefix || '') + 'attach';
	      _.each(views, function(view) {
	        Marionette.triggerMethodOn(view, eventName, view, this);
	      }, this);
	    },
	  
	    _displayedViews: function(view) {
	      return _.union([view], _.result(view, '_getNestedViews') || []);
	    },
	  
	    _renderView: function(view) {
	      if (!view.supportsRenderLifecycle) {
	        Marionette.triggerMethodOn(view, 'before:render', view);
	      }
	      view.render();
	      if (!view.supportsRenderLifecycle) {
	        Marionette.triggerMethodOn(view, 'render', view);
	      }
	    },
	  
	    _ensureElement: function() {
	      if (!_.isObject(this.el)) {
	        this.$el = this.getEl(this.el);
	        this.el = this.$el[0];
	      }
	  
	      if (!this.$el || this.$el.length === 0) {
	        if (this.getOption('allowMissingEl')) {
	          return false;
	        } else {
	          throw new Marionette.Error('An "el" ' + this.$el.selector + ' must exist in DOM');
	        }
	      }
	      return true;
	    },
	  
	    _ensureViewIsIntact: function(view) {
	      if (!view) {
	        throw new Marionette.Error({
	          name: 'ViewNotValid',
	          message: 'The view passed is undefined and therefore invalid. You must pass a view instance to show.'
	        });
	      }
	  
	      if (view.isDestroyed) {
	        throw new Marionette.Error({
	          name: 'ViewDestroyedError',
	          message: 'View (cid: "' + view.cid + '") has already been destroyed and cannot be used.'
	        });
	      }
	    },
	  
	    // Override this method to change how the region finds the DOM
	    // element that it manages. Return a jQuery selector object scoped
	    // to a provided parent el or the document if none exists.
	    getEl: function(el) {
	      return Backbone.$(el, Marionette._getValue(this.options.parentEl, this));
	    },
	  
	    // Override this method to change how the new view is
	    // appended to the `$el` that the region is managing
	    attachHtml: function(view) {
	      this.$el.contents().detach();
	  
	      this.el.appendChild(view.el);
	    },
	  
	    // Destroy the current view, if there is one. If there is no
	    // current view, it does nothing and returns immediately.
	    empty: function(options) {
	      var view = this.currentView;
	  
	      var emptyOptions = options || {};
	      var preventDestroy  = !!emptyOptions.preventDestroy;
	      // If there is no view in the region
	      // we should not remove anything
	      if (!view) { return; }
	  
	      view.off('destroy', this.empty, this);
	      this.triggerMethod('before:empty', view);
	      if (!preventDestroy) {
	        this._destroyView();
	      }
	      this.triggerMethod('empty', view);
	  
	      // Remove region pointer to the currentView
	      delete this.currentView;
	  
	      if (preventDestroy) {
	        this.$el.contents().detach();
	      }
	  
	      return this;
	    },
	  
	    // call 'destroy' or 'remove', depending on which is found
	    // on the view (if showing a raw Backbone view or a Marionette View)
	    _destroyView: function() {
	      var view = this.currentView;
	      if (view.isDestroyed) { return; }
	  
	      if (!view.supportsDestroyLifecycle) {
	        Marionette.triggerMethodOn(view, 'before:destroy', view);
	      }
	      if (view.destroy) {
	        view.destroy();
	      } else {
	        view.remove();
	  
	        // appending isDestroyed to raw Backbone View allows regions
	        // to throw a ViewDestroyedError for this view
	        view.isDestroyed = true;
	      }
	      if (!view.supportsDestroyLifecycle) {
	        Marionette.triggerMethodOn(view, 'destroy', view);
	      }
	    },
	  
	    // Attach an existing view to the region. This
	    // will not call `render` or `onShow` for the new view,
	    // and will not replace the current HTML for the `el`
	    // of the region.
	    attachView: function(view) {
	      if (this.currentView) {
	        delete this.currentView._parent;
	      }
	      view._parent = this;
	      this.currentView = view;
	      return this;
	    },
	  
	    // Checks whether a view is currently present within
	    // the region. Returns `true` if there is and `false` if
	    // no view is present.
	    hasView: function() {
	      return !!this.currentView;
	    },
	  
	    // Reset the region by destroying any existing view and
	    // clearing out the cached `$el`. The next time a view
	    // is shown via this region, the region will re-query the
	    // DOM for the region's `el`.
	    reset: function() {
	      this.empty();
	  
	      if (this.$el) {
	        this.el = this.$el.selector;
	      }
	  
	      delete this.$el;
	      return this;
	    }
	  
	  },
	  
	  // Static Methods
	  {
	  
	    // Build an instance of a region by passing in a configuration object
	    // and a default region class to use if none is specified in the config.
	    //
	    // The config object should either be a string as a jQuery DOM selector,
	    // a Region class directly, or an object literal that specifies a selector,
	    // a custom regionClass, and any options to be supplied to the region:
	    //
	    // ```js
	    // {
	    //   selector: "#foo",
	    //   regionClass: MyCustomRegion,
	    //   allowMissingEl: false
	    // }
	    // ```
	    //
	    buildRegion: function(regionConfig, DefaultRegionClass) {
	      if (_.isString(regionConfig)) {
	        return this._buildRegionFromSelector(regionConfig, DefaultRegionClass);
	      }
	  
	      if (regionConfig.selector || regionConfig.el || regionConfig.regionClass) {
	        return this._buildRegionFromObject(regionConfig, DefaultRegionClass);
	      }
	  
	      if (_.isFunction(regionConfig)) {
	        return this._buildRegionFromRegionClass(regionConfig);
	      }
	  
	      throw new Marionette.Error({
	        message: 'Improper region configuration type.',
	        url: 'marionette.region.html#region-configuration-types'
	      });
	    },
	  
	    // Build the region from a string selector like '#foo-region'
	    _buildRegionFromSelector: function(selector, DefaultRegionClass) {
	      return new DefaultRegionClass({el: selector});
	    },
	  
	    // Build the region from a configuration object
	    // ```js
	    // { selector: '#foo', regionClass: FooRegion, allowMissingEl: false }
	    // ```
	    _buildRegionFromObject: function(regionConfig, DefaultRegionClass) {
	      var RegionClass = regionConfig.regionClass || DefaultRegionClass;
	      var options = _.omit(regionConfig, 'selector', 'regionClass');
	  
	      if (regionConfig.selector && !options.el) {
	        options.el = regionConfig.selector;
	      }
	  
	      return new RegionClass(options);
	    },
	  
	    // Build the region directly from a given `RegionClass`
	    _buildRegionFromRegionClass: function(RegionClass) {
	      return new RegionClass();
	    }
	  });
	  
	  // Region Manager
	  // --------------
	  
	  // Manage one or more related `Marionette.Region` objects.
	  Marionette.RegionManager = Marionette.Controller.extend({
	    constructor: function(options) {
	      this._regions = {};
	      this.length = 0;
	  
	      Marionette.Controller.call(this, options);
	  
	      this.addRegions(this.getOption('regions'));
	    },
	  
	    // Add multiple regions using an object literal or a
	    // function that returns an object literal, where
	    // each key becomes the region name, and each value is
	    // the region definition.
	    addRegions: function(regionDefinitions, defaults) {
	      regionDefinitions = Marionette._getValue(regionDefinitions, this, arguments);
	  
	      return _.reduce(regionDefinitions, function(regions, definition, name) {
	        if (_.isString(definition)) {
	          definition = {selector: definition};
	        }
	        if (definition.selector) {
	          definition = _.defaults({}, definition, defaults);
	        }
	  
	        regions[name] = this.addRegion(name, definition);
	        return regions;
	      }, {}, this);
	    },
	  
	    // Add an individual region to the region manager,
	    // and return the region instance
	    addRegion: function(name, definition) {
	      var region;
	  
	      if (definition instanceof Marionette.Region) {
	        region = definition;
	      } else {
	        region = Marionette.Region.buildRegion(definition, Marionette.Region);
	      }
	  
	      this.triggerMethod('before:add:region', name, region);
	  
	      region._parent = this;
	      this._store(name, region);
	  
	      this.triggerMethod('add:region', name, region);
	      return region;
	    },
	  
	    // Get a region by name
	    get: function(name) {
	      return this._regions[name];
	    },
	  
	    // Gets all the regions contained within
	    // the `regionManager` instance.
	    getRegions: function() {
	      return _.clone(this._regions);
	    },
	  
	    // Remove a region by name
	    removeRegion: function(name) {
	      var region = this._regions[name];
	      this._remove(name, region);
	  
	      return region;
	    },
	  
	    // Empty all regions in the region manager, and
	    // remove them
	    removeRegions: function() {
	      var regions = this.getRegions();
	      _.each(this._regions, function(region, name) {
	        this._remove(name, region);
	      }, this);
	  
	      return regions;
	    },
	  
	    // Empty all regions in the region manager, but
	    // leave them attached
	    emptyRegions: function() {
	      var regions = this.getRegions();
	      _.invoke(regions, 'empty');
	      return regions;
	    },
	  
	    // Destroy all regions and shut down the region
	    // manager entirely
	    destroy: function() {
	      this.removeRegions();
	      return Marionette.Controller.prototype.destroy.apply(this, arguments);
	    },
	  
	    // internal method to store regions
	    _store: function(name, region) {
	      if (!this._regions[name]) {
	        this.length++;
	      }
	  
	      this._regions[name] = region;
	    },
	  
	    // internal method to remove a region
	    _remove: function(name, region) {
	      this.triggerMethod('before:remove:region', name, region);
	      region.empty();
	      region.stopListening();
	  
	      delete region._parent;
	      delete this._regions[name];
	      this.length--;
	      this.triggerMethod('remove:region', name, region);
	    }
	  });
	  
	  Marionette.actAsCollection(Marionette.RegionManager.prototype, '_regions');
	  

	  // Template Cache
	  // --------------
	  
	  // Manage templates stored in `<script>` blocks,
	  // caching them for faster access.
	  Marionette.TemplateCache = function(templateId) {
	    this.templateId = templateId;
	  };
	  
	  // TemplateCache object-level methods. Manage the template
	  // caches from these method calls instead of creating
	  // your own TemplateCache instances
	  _.extend(Marionette.TemplateCache, {
	    templateCaches: {},
	  
	    // Get the specified template by id. Either
	    // retrieves the cached version, or loads it
	    // from the DOM.
	    get: function(templateId, options) {
	      var cachedTemplate = this.templateCaches[templateId];
	  
	      if (!cachedTemplate) {
	        cachedTemplate = new Marionette.TemplateCache(templateId);
	        this.templateCaches[templateId] = cachedTemplate;
	      }
	  
	      return cachedTemplate.load(options);
	    },
	  
	    // Clear templates from the cache. If no arguments
	    // are specified, clears all templates:
	    // `clear()`
	    //
	    // If arguments are specified, clears each of the
	    // specified templates from the cache:
	    // `clear("#t1", "#t2", "...")`
	    clear: function() {
	      var i;
	      var args = _.toArray(arguments);
	      var length = args.length;
	  
	      if (length > 0) {
	        for (i = 0; i < length; i++) {
	          delete this.templateCaches[args[i]];
	        }
	      } else {
	        this.templateCaches = {};
	      }
	    }
	  });
	  
	  // TemplateCache instance methods, allowing each
	  // template cache object to manage its own state
	  // and know whether or not it has been loaded
	  _.extend(Marionette.TemplateCache.prototype, {
	  
	    // Internal method to load the template
	    load: function(options) {
	      // Guard clause to prevent loading this template more than once
	      if (this.compiledTemplate) {
	        return this.compiledTemplate;
	      }
	  
	      // Load the template and compile it
	      var template = this.loadTemplate(this.templateId, options);
	      this.compiledTemplate = this.compileTemplate(template, options);
	  
	      return this.compiledTemplate;
	    },
	  
	    // Load a template from the DOM, by default. Override
	    // this method to provide your own template retrieval
	    // For asynchronous loading with AMD/RequireJS, consider
	    // using a template-loader plugin as described here:
	    // https://github.com/marionettejs/backbone.marionette/wiki/Using-marionette-with-requirejs
	    loadTemplate: function(templateId, options) {
	      var $template = Backbone.$(templateId);
	  
	      if (!$template.length) {
	        throw new Marionette.Error({
	          name: 'NoTemplateError',
	          message: 'Could not find template: "' + templateId + '"'
	        });
	      }
	      return $template.html();
	    },
	  
	    // Pre-compile the template before caching it. Override
	    // this method if you do not need to pre-compile a template
	    // (JST / RequireJS for example) or if you want to change
	    // the template engine used (Handebars, etc).
	    compileTemplate: function(rawTemplate, options) {
	      return _.template(rawTemplate, options);
	    }
	  });
	  
	  // Renderer
	  // --------
	  
	  // Render a template with data by passing in the template
	  // selector and the data to render.
	  Marionette.Renderer = {
	  
	    // Render a template with data. The `template` parameter is
	    // passed to the `TemplateCache` object to retrieve the
	    // template function. Override this method to provide your own
	    // custom rendering and template handling for all of Marionette.
	    render: function(template, data) {
	      if (!template) {
	        throw new Marionette.Error({
	          name: 'TemplateNotFoundError',
	          message: 'Cannot render the template since its false, null or undefined.'
	        });
	      }
	  
	      var templateFunc = _.isFunction(template) ? template : Marionette.TemplateCache.get(template);
	  
	      return templateFunc(data);
	    }
	  };
	  

	  /* jshint maxlen: 114, nonew: false */
	  // View
	  // ----
	  
	  // The core view class that other Marionette views extend from.
	  Marionette.View = Backbone.View.extend({
	    isDestroyed: false,
	    supportsRenderLifecycle: true,
	    supportsDestroyLifecycle: true,
	  
	    constructor: function(options) {
	      this.render = _.bind(this.render, this);
	  
	      options = Marionette._getValue(options, this);
	  
	      // this exposes view options to the view initializer
	      // this is a backfill since backbone removed the assignment
	      // of this.options
	      // at some point however this may be removed
	      this.options = _.extend({}, _.result(this, 'options'), options);
	  
	      this._behaviors = Marionette.Behaviors(this);
	  
	      Backbone.View.call(this, this.options);
	  
	      Marionette.MonitorDOMRefresh(this);
	    },
	  
	    // Get the template for this view
	    // instance. You can set a `template` attribute in the view
	    // definition or pass a `template: "whatever"` parameter in
	    // to the constructor options.
	    getTemplate: function() {
	      return this.getOption('template');
	    },
	  
	    // Serialize a model by returning its attributes. Clones
	    // the attributes to allow modification.
	    serializeModel: function(model) {
	      return model.toJSON.apply(model, _.rest(arguments));
	    },
	  
	    // Mix in template helper methods. Looks for a
	    // `templateHelpers` attribute, which can either be an
	    // object literal, or a function that returns an object
	    // literal. All methods and attributes from this object
	    // are copies to the object passed in.
	    mixinTemplateHelpers: function(target) {
	      target = target || {};
	      var templateHelpers = this.getOption('templateHelpers');
	      templateHelpers = Marionette._getValue(templateHelpers, this);
	      return _.extend(target, templateHelpers);
	    },
	  
	    // normalize the keys of passed hash with the views `ui` selectors.
	    // `{"@ui.foo": "bar"}`
	    normalizeUIKeys: function(hash) {
	      var uiBindings = _.result(this, '_uiBindings');
	      return Marionette.normalizeUIKeys(hash, uiBindings || _.result(this, 'ui'));
	    },
	  
	    // normalize the values of passed hash with the views `ui` selectors.
	    // `{foo: "@ui.bar"}`
	    normalizeUIValues: function(hash, properties) {
	      var ui = _.result(this, 'ui');
	      var uiBindings = _.result(this, '_uiBindings');
	      return Marionette.normalizeUIValues(hash, uiBindings || ui, properties);
	    },
	  
	    // Configure `triggers` to forward DOM events to view
	    // events. `triggers: {"click .foo": "do:foo"}`
	    configureTriggers: function() {
	      if (!this.triggers) { return; }
	  
	      // Allow `triggers` to be configured as a function
	      var triggers = this.normalizeUIKeys(_.result(this, 'triggers'));
	  
	      // Configure the triggers, prevent default
	      // action and stop propagation of DOM events
	      return _.reduce(triggers, function(events, value, key) {
	        events[key] = this._buildViewTrigger(value);
	        return events;
	      }, {}, this);
	    },
	  
	    // Overriding Backbone.View's delegateEvents to handle
	    // the `triggers`, `modelEvents`, and `collectionEvents` configuration
	    delegateEvents: function(events) {
	      this._delegateDOMEvents(events);
	      this.bindEntityEvents(this.model, this.getOption('modelEvents'));
	      this.bindEntityEvents(this.collection, this.getOption('collectionEvents'));
	  
	      _.each(this._behaviors, function(behavior) {
	        behavior.bindEntityEvents(this.model, behavior.getOption('modelEvents'));
	        behavior.bindEntityEvents(this.collection, behavior.getOption('collectionEvents'));
	      }, this);
	  
	      return this;
	    },
	  
	    // internal method to delegate DOM events and triggers
	    _delegateDOMEvents: function(eventsArg) {
	      var events = Marionette._getValue(eventsArg || this.events, this);
	  
	      // normalize ui keys
	      events = this.normalizeUIKeys(events);
	      if (_.isUndefined(eventsArg)) {this.events = events;}
	  
	      var combinedEvents = {};
	  
	      // look up if this view has behavior events
	      var behaviorEvents = _.result(this, 'behaviorEvents') || {};
	      var triggers = this.configureTriggers();
	      var behaviorTriggers = _.result(this, 'behaviorTriggers') || {};
	  
	      // behavior events will be overriden by view events and or triggers
	      _.extend(combinedEvents, behaviorEvents, events, triggers, behaviorTriggers);
	  
	      Backbone.View.prototype.delegateEvents.call(this, combinedEvents);
	    },
	  
	    // Overriding Backbone.View's undelegateEvents to handle unbinding
	    // the `triggers`, `modelEvents`, and `collectionEvents` config
	    undelegateEvents: function() {
	      Backbone.View.prototype.undelegateEvents.apply(this, arguments);
	  
	      this.unbindEntityEvents(this.model, this.getOption('modelEvents'));
	      this.unbindEntityEvents(this.collection, this.getOption('collectionEvents'));
	  
	      _.each(this._behaviors, function(behavior) {
	        behavior.unbindEntityEvents(this.model, behavior.getOption('modelEvents'));
	        behavior.unbindEntityEvents(this.collection, behavior.getOption('collectionEvents'));
	      }, this);
	  
	      return this;
	    },
	  
	    // Internal helper method to verify whether the view hasn't been destroyed
	    _ensureViewIsIntact: function() {
	      if (this.isDestroyed) {
	        throw new Marionette.Error({
	          name: 'ViewDestroyedError',
	          message: 'View (cid: "' + this.cid + '") has already been destroyed and cannot be used.'
	        });
	      }
	    },
	  
	    // Default `destroy` implementation, for removing a view from the
	    // DOM and unbinding it. Regions will call this method
	    // for you. You can specify an `onDestroy` method in your view to
	    // add custom code that is called after the view is destroyed.
	    destroy: function() {
	      if (this.isDestroyed) { return this; }
	  
	      var args = _.toArray(arguments);
	  
	      this.triggerMethod.apply(this, ['before:destroy'].concat(args));
	  
	      // mark as destroyed before doing the actual destroy, to
	      // prevent infinite loops within "destroy" event handlers
	      // that are trying to destroy other views
	      this.isDestroyed = true;
	      this.triggerMethod.apply(this, ['destroy'].concat(args));
	  
	      // unbind UI elements
	      this.unbindUIElements();
	  
	      this.isRendered = false;
	  
	      // remove the view from the DOM
	      this.remove();
	  
	      // Call destroy on each behavior after
	      // destroying the view.
	      // This unbinds event listeners
	      // that behaviors have registered for.
	      _.invoke(this._behaviors, 'destroy', args);
	  
	      return this;
	    },
	  
	    bindUIElements: function() {
	      this._bindUIElements();
	      _.invoke(this._behaviors, this._bindUIElements);
	    },
	  
	    // This method binds the elements specified in the "ui" hash inside the view's code with
	    // the associated jQuery selectors.
	    _bindUIElements: function() {
	      if (!this.ui) { return; }
	  
	      // store the ui hash in _uiBindings so they can be reset later
	      // and so re-rendering the view will be able to find the bindings
	      if (!this._uiBindings) {
	        this._uiBindings = this.ui;
	      }
	  
	      // get the bindings result, as a function or otherwise
	      var bindings = _.result(this, '_uiBindings');
	  
	      // empty the ui so we don't have anything to start with
	      this.ui = {};
	  
	      // bind each of the selectors
	      _.each(bindings, function(selector, key) {
	        this.ui[key] = this.$(selector);
	      }, this);
	    },
	  
	    // This method unbinds the elements specified in the "ui" hash
	    unbindUIElements: function() {
	      this._unbindUIElements();
	      _.invoke(this._behaviors, this._unbindUIElements);
	    },
	  
	    _unbindUIElements: function() {
	      if (!this.ui || !this._uiBindings) { return; }
	  
	      // delete all of the existing ui bindings
	      _.each(this.ui, function($el, name) {
	        delete this.ui[name];
	      }, this);
	  
	      // reset the ui element to the original bindings configuration
	      this.ui = this._uiBindings;
	      delete this._uiBindings;
	    },
	  
	    // Internal method to create an event handler for a given `triggerDef` like
	    // 'click:foo'
	    _buildViewTrigger: function(triggerDef) {
	  
	      var options = _.defaults({}, triggerDef, {
	        preventDefault: true,
	        stopPropagation: true
	      });
	  
	      var eventName = _.isObject(triggerDef) ? options.event : triggerDef;
	  
	      return function(e) {
	        if (e) {
	          if (e.preventDefault && options.preventDefault) {
	            e.preventDefault();
	          }
	  
	          if (e.stopPropagation && options.stopPropagation) {
	            e.stopPropagation();
	          }
	        }
	  
	        var args = {
	          view: this,
	          model: this.model,
	          collection: this.collection
	        };
	  
	        this.triggerMethod(eventName, args);
	      };
	    },
	  
	    setElement: function() {
	      var ret = Backbone.View.prototype.setElement.apply(this, arguments);
	  
	      // proxy behavior $el to the view's $el.
	      // This is needed because a view's $el proxy
	      // is not set until after setElement is called.
	      _.invoke(this._behaviors, 'proxyViewProperties', this);
	  
	      return ret;
	    },
	  
	    // import the `triggerMethod` to trigger events with corresponding
	    // methods if the method exists
	    triggerMethod: function() {
	      var ret = Marionette._triggerMethod(this, arguments);
	  
	      this._triggerEventOnBehaviors(arguments);
	      this._triggerEventOnParentLayout(arguments[0], _.rest(arguments));
	  
	      return ret;
	    },
	  
	    _triggerEventOnBehaviors: function(args) {
	      var triggerMethod = Marionette._triggerMethod;
	      var behaviors = this._behaviors;
	      // Use good ol' for as this is a very hot function
	      for (var i = 0, length = behaviors && behaviors.length; i < length; i++) {
	        triggerMethod(behaviors[i], args);
	      }
	    },
	  
	    _triggerEventOnParentLayout: function(eventName, args) {
	      var layoutView = this._parentLayoutView();
	      if (!layoutView) {
	        return;
	      }
	  
	      // invoke triggerMethod on parent view
	      var eventPrefix = Marionette.getOption(layoutView, 'childViewEventPrefix');
	      var prefixedEventName = eventPrefix + ':' + eventName;
	      var callArgs = [this].concat(args);
	  
	      Marionette._triggerMethod(layoutView, prefixedEventName, callArgs);
	  
	      // call the parent view's childEvents handler
	      var childEvents = Marionette.getOption(layoutView, 'childEvents');
	      var normalizedChildEvents = layoutView.normalizeMethods(childEvents);
	  
	      if (normalizedChildEvents && _.isFunction(normalizedChildEvents[eventName])) {
	        normalizedChildEvents[eventName].apply(layoutView, callArgs);
	      }
	    },
	  
	    // This method returns any views that are immediate
	    // children of this view
	    _getImmediateChildren: function() {
	      return [];
	    },
	  
	    // Returns an array of every nested view within this view
	    _getNestedViews: function() {
	      var children = this._getImmediateChildren();
	  
	      if (!children.length) { return children; }
	  
	      return _.reduce(children, function(memo, view) {
	        if (!view._getNestedViews) { return memo; }
	        return memo.concat(view._getNestedViews());
	      }, children);
	    },
	  
	    // Internal utility for building an ancestor
	    // view tree list.
	    _getAncestors: function() {
	      var ancestors = [];
	      var parent  = this._parent;
	  
	      while (parent) {
	        ancestors.push(parent);
	        parent = parent._parent;
	      }
	  
	      return ancestors;
	    },
	  
	    // Returns the containing parent view.
	    _parentLayoutView: function() {
	      var ancestors = this._getAncestors();
	      return _.find(ancestors, function(parent) {
	        return parent instanceof Marionette.LayoutView;
	      });
	    },
	  
	    // Imports the "normalizeMethods" to transform hashes of
	    // events=>function references/names to a hash of events=>function references
	    normalizeMethods: Marionette.normalizeMethods,
	  
	    // A handy way to merge passed-in options onto the instance
	    mergeOptions: Marionette.mergeOptions,
	  
	    // Proxy `getOption` to enable getting options from this or this.options by name.
	    getOption: Marionette.proxyGetOption,
	  
	    // Proxy `bindEntityEvents` to enable binding view's events from another entity.
	    bindEntityEvents: Marionette.proxyBindEntityEvents,
	  
	    // Proxy `unbindEntityEvents` to enable unbinding view's events from another entity.
	    unbindEntityEvents: Marionette.proxyUnbindEntityEvents
	  });
	  
	  // Item View
	  // ---------
	  
	  // A single item view implementation that contains code for rendering
	  // with underscore.js templates, serializing the view's model or collection,
	  // and calling several methods on extended views, such as `onRender`.
	  Marionette.ItemView = Marionette.View.extend({
	  
	    // Setting up the inheritance chain which allows changes to
	    // Marionette.View.prototype.constructor which allows overriding
	    constructor: function() {
	      Marionette.View.apply(this, arguments);
	    },
	  
	    // Serialize the model or collection for the view. If a model is
	    // found, the view's `serializeModel` is called. If a collection is found,
	    // each model in the collection is serialized by calling
	    // the view's `serializeCollection` and put into an `items` array in
	    // the resulting data. If both are found, defaults to the model.
	    // You can override the `serializeData` method in your own view definition,
	    // to provide custom serialization for your view's data.
	    serializeData: function() {
	      if (!this.model && !this.collection) {
	        return {};
	      }
	  
	      var args = [this.model || this.collection];
	      if (arguments.length) {
	        args.push.apply(args, arguments);
	      }
	  
	      if (this.model) {
	        return this.serializeModel.apply(this, args);
	      } else {
	        return {
	          items: this.serializeCollection.apply(this, args)
	        };
	      }
	    },
	  
	    // Serialize a collection by serializing each of its models.
	    serializeCollection: function(collection) {
	      return collection.toJSON.apply(collection, _.rest(arguments));
	    },
	  
	    // Render the view, defaulting to underscore.js templates.
	    // You can override this in your view definition to provide
	    // a very specific rendering for your view. In general, though,
	    // you should override the `Marionette.Renderer` object to
	    // change how Marionette renders views.
	    render: function() {
	      this._ensureViewIsIntact();
	  
	      this.triggerMethod('before:render', this);
	  
	      this._renderTemplate();
	      this.isRendered = true;
	      this.bindUIElements();
	  
	      this.triggerMethod('render', this);
	  
	      return this;
	    },
	  
	    // Internal method to render the template with the serialized data
	    // and template helpers via the `Marionette.Renderer` object.
	    // Throws an `UndefinedTemplateError` error if the template is
	    // any falsely value but literal `false`.
	    _renderTemplate: function() {
	      var template = this.getTemplate();
	  
	      // Allow template-less item views
	      if (template === false) {
	        return;
	      }
	  
	      if (!template) {
	        throw new Marionette.Error({
	          name: 'UndefinedTemplateError',
	          message: 'Cannot render the template since it is null or undefined.'
	        });
	      }
	  
	      // Add in entity data and template helpers
	      var data = this.mixinTemplateHelpers(this.serializeData());
	  
	      // Render and add to el
	      var html = Marionette.Renderer.render(template, data, this);
	      this.attachElContent(html);
	  
	      return this;
	    },
	  
	    // Attaches the content of a given view.
	    // This method can be overridden to optimize rendering,
	    // or to render in a non standard way.
	    //
	    // For example, using `innerHTML` instead of `$el.html`
	    //
	    // ```js
	    // attachElContent: function(html) {
	    //   this.el.innerHTML = html;
	    //   return this;
	    // }
	    // ```
	    attachElContent: function(html) {
	      this.$el.html(html);
	  
	      return this;
	    }
	  });
	  
	  /* jshint maxstatements: 20, maxcomplexity: 7 */
	  
	  // Collection View
	  // ---------------
	  
	  // A view that iterates over a Backbone.Collection
	  // and renders an individual child view for each model.
	  Marionette.CollectionView = Marionette.View.extend({
	  
	    // used as the prefix for child view events
	    // that are forwarded through the collectionview
	    childViewEventPrefix: 'childview',
	  
	    // flag for maintaining the sorted order of the collection
	    sort: true,
	  
	    // constructor
	    // option to pass `{sort: false}` to prevent the `CollectionView` from
	    // maintaining the sorted order of the collection.
	    // This will fallback onto appending childView's to the end.
	    //
	    // option to pass `{comparator: compFunction()}` to allow the `CollectionView`
	    // to use a custom sort order for the collection.
	    constructor: function(options) {
	      this.once('render', this._initialEvents);
	      this._initChildViewStorage();
	  
	      Marionette.View.apply(this, arguments);
	  
	      this.on({
	        'before:show':   this._onBeforeShowCalled,
	        'show':          this._onShowCalled,
	        'before:attach': this._onBeforeAttachCalled,
	        'attach':        this._onAttachCalled
	      });
	      this.initRenderBuffer();
	    },
	  
	    // Instead of inserting elements one by one into the page,
	    // it's much more performant to insert elements into a document
	    // fragment and then insert that document fragment into the page
	    initRenderBuffer: function() {
	      this._bufferedChildren = [];
	    },
	  
	    startBuffering: function() {
	      this.initRenderBuffer();
	      this.isBuffering = true;
	    },
	  
	    endBuffering: function() {
	      // Only trigger attach if already shown and attached, otherwise Region#show() handles this.
	      var canTriggerAttach = this._isShown && Marionette.isNodeAttached(this.el);
	      var nestedViews;
	  
	      this.isBuffering = false;
	  
	      if (this._isShown) {
	        this._triggerMethodMany(this._bufferedChildren, this, 'before:show');
	      }
	      if (canTriggerAttach && this._triggerBeforeAttach) {
	        nestedViews = this._getNestedViews();
	        this._triggerMethodMany(nestedViews, this, 'before:attach');
	      }
	  
	      this.attachBuffer(this, this._createBuffer());
	  
	      if (canTriggerAttach && this._triggerAttach) {
	        nestedViews = this._getNestedViews();
	        this._triggerMethodMany(nestedViews, this, 'attach');
	      }
	      if (this._isShown) {
	        this._triggerMethodMany(this._bufferedChildren, this, 'show');
	      }
	      this.initRenderBuffer();
	    },
	  
	    _triggerMethodMany: function(targets, source, eventName) {
	      var args = _.drop(arguments, 3);
	  
	      _.each(targets, function(target) {
	        Marionette.triggerMethodOn.apply(target, [target, eventName, target, source].concat(args));
	      });
	    },
	  
	    // Configured the initial events that the collection view
	    // binds to.
	    _initialEvents: function() {
	      if (this.collection) {
	        this.listenTo(this.collection, 'add', this._onCollectionAdd);
	        this.listenTo(this.collection, 'remove', this._onCollectionRemove);
	        this.listenTo(this.collection, 'reset', this.render);
	  
	        if (this.getOption('sort')) {
	          this.listenTo(this.collection, 'sort', this._sortViews);
	        }
	      }
	    },
	  
	    // Handle a child added to the collection
	    _onCollectionAdd: function(child, collection, opts) {
	      // `index` is present when adding with `at` since BB 1.2; indexOf fallback for < 1.2
	      var index = opts.at !== undefined && (opts.index || collection.indexOf(child));
	  
	      // When filtered or when there is no initial index, calculate index.
	      if (this.getOption('filter') || index === false) {
	        index = _.indexOf(this._filteredSortedModels(index), child);
	      }
	  
	      if (this._shouldAddChild(child, index)) {
	        this.destroyEmptyView();
	        var ChildView = this.getChildView(child);
	        this.addChild(child, ChildView, index);
	      }
	    },
	  
	    // get the child view by model it holds, and remove it
	    _onCollectionRemove: function(model) {
	      var view = this.children.findByModel(model);
	      this.removeChildView(view);
	      this.checkEmpty();
	    },
	  
	    _onBeforeShowCalled: function() {
	      // Reset attach event flags at the top of the Region#show() event lifecycle; if the Region's
	      // show() options permit onBeforeAttach/onAttach events, these flags will be set true again.
	      this._triggerBeforeAttach = this._triggerAttach = false;
	      this.children.each(function(childView) {
	        Marionette.triggerMethodOn(childView, 'before:show', childView);
	      });
	    },
	  
	    _onShowCalled: function() {
	      this.children.each(function(childView) {
	        Marionette.triggerMethodOn(childView, 'show', childView);
	      });
	    },
	  
	    // If during Region#show() onBeforeAttach was fired, continue firing it for child views
	    _onBeforeAttachCalled: function() {
	      this._triggerBeforeAttach = true;
	    },
	  
	    // If during Region#show() onAttach was fired, continue firing it for child views
	    _onAttachCalled: function() {
	      this._triggerAttach = true;
	    },
	  
	    // Render children views. Override this method to
	    // provide your own implementation of a render function for
	    // the collection view.
	    render: function() {
	      this._ensureViewIsIntact();
	      this.triggerMethod('before:render', this);
	      this._renderChildren();
	      this.isRendered = true;
	      this.triggerMethod('render', this);
	      return this;
	    },
	  
	    // Reorder DOM after sorting. When your element's rendering
	    // do not use their index, you can pass reorderOnSort: true
	    // to only reorder the DOM after a sort instead of rendering
	    // all the collectionView
	    reorder: function() {
	      var children = this.children;
	      var models = this._filteredSortedModels();
	      var modelsChanged = _.find(models, function(model) {
	        return !children.findByModel(model);
	      });
	  
	      // If the models we're displaying have changed due to filtering
	      // We need to add and/or remove child views
	      // So render as normal
	      if (modelsChanged) {
	        this.render();
	      } else {
	        // get the DOM nodes in the same order as the models
	        var els = _.map(models, function(model, index) {
	          var view = children.findByModel(model);
	          view._index = index;
	          return view.el;
	        });
	  
	        // since append moves elements that are already in the DOM,
	        // appending the elements will effectively reorder them
	        this.triggerMethod('before:reorder');
	        this._appendReorderedChildren(els);
	        this.triggerMethod('reorder');
	      }
	    },
	  
	    // Render view after sorting. Override this method to
	    // change how the view renders after a `sort` on the collection.
	    // An example of this would be to only `renderChildren` in a `CompositeView`
	    // rather than the full view.
	    resortView: function() {
	      if (Marionette.getOption(this, 'reorderOnSort')) {
	        this.reorder();
	      } else {
	        this.render();
	      }
	    },
	  
	    // Internal method. This checks for any changes in the order of the collection.
	    // If the index of any view doesn't match, it will render.
	    _sortViews: function() {
	      var models = this._filteredSortedModels();
	  
	      // check for any changes in sort order of views
	      var orderChanged = _.find(models, function(item, index) {
	        var view = this.children.findByModel(item);
	        return !view || view._index !== index;
	      }, this);
	  
	      if (orderChanged) {
	        this.resortView();
	      }
	    },
	  
	    // Internal reference to what index a `emptyView` is.
	    _emptyViewIndex: -1,
	  
	    // Internal method. Separated so that CompositeView can append to the childViewContainer
	    // if necessary
	    _appendReorderedChildren: function(children) {
	      this.$el.append(children);
	    },
	  
	    // Internal method. Separated so that CompositeView can have
	    // more control over events being triggered, around the rendering
	    // process
	    _renderChildren: function() {
	      this.destroyEmptyView();
	      this.destroyChildren({checkEmpty: false});
	  
	      if (this.isEmpty(this.collection)) {
	        this.showEmptyView();
	      } else {
	        this.triggerMethod('before:render:collection', this);
	        this.startBuffering();
	        this.showCollection();
	        this.endBuffering();
	        this.triggerMethod('render:collection', this);
	  
	        // If we have shown children and none have passed the filter, show the empty view
	        if (this.children.isEmpty() && this.getOption('filter')) {
	          this.showEmptyView();
	        }
	      }
	    },
	  
	    // Internal method to loop through collection and show each child view.
	    showCollection: function() {
	      var ChildView;
	  
	      var models = this._filteredSortedModels();
	  
	      _.each(models, function(child, index) {
	        ChildView = this.getChildView(child);
	        this.addChild(child, ChildView, index);
	      }, this);
	    },
	  
	    // Allow the collection to be sorted by a custom view comparator
	    _filteredSortedModels: function(addedAt) {
	      var viewComparator = this.getViewComparator();
	      var models = this.collection.models;
	      addedAt = Math.min(Math.max(addedAt, 0), models.length - 1);
	  
	      if (viewComparator) {
	        var addedModel;
	        // Preserve `at` location, even for a sorted view
	        if (addedAt) {
	          addedModel = models[addedAt];
	          models = models.slice(0, addedAt).concat(models.slice(addedAt + 1));
	        }
	        models = this._sortModelsBy(models, viewComparator);
	        if (addedModel) {
	          models.splice(addedAt, 0, addedModel);
	        }
	      }
	  
	      // Filter after sorting in case the filter uses the index
	      if (this.getOption('filter')) {
	        models = _.filter(models, function(model, index) {
	          return this._shouldAddChild(model, index);
	        }, this);
	      }
	  
	      return models;
	    },
	  
	    _sortModelsBy: function(models, comparator) {
	      if (typeof comparator === 'string') {
	        return _.sortBy(models, function(model) {
	          return model.get(comparator);
	        }, this);
	      } else if (comparator.length === 1) {
	        return _.sortBy(models, comparator, this);
	      } else {
	        return models.sort(_.bind(comparator, this));
	      }
	    },
	  
	    // Internal method to show an empty view in place of
	    // a collection of child views, when the collection is empty
	    showEmptyView: function() {
	      var EmptyView = this.getEmptyView();
	  
	      if (EmptyView && !this._showingEmptyView) {
	        this.triggerMethod('before:render:empty');
	  
	        this._showingEmptyView = true;
	        var model = new Backbone.Model();
	        this.addEmptyView(model, EmptyView);
	  
	        this.triggerMethod('render:empty');
	      }
	    },
	  
	    // Internal method to destroy an existing emptyView instance
	    // if one exists. Called when a collection view has been
	    // rendered empty, and then a child is added to the collection.
	    destroyEmptyView: function() {
	      if (this._showingEmptyView) {
	        this.triggerMethod('before:remove:empty');
	  
	        this.destroyChildren();
	        delete this._showingEmptyView;
	  
	        this.triggerMethod('remove:empty');
	      }
	    },
	  
	    // Retrieve the empty view class
	    getEmptyView: function() {
	      return this.getOption('emptyView');
	    },
	  
	    // Render and show the emptyView. Similar to addChild method
	    // but "add:child" events are not fired, and the event from
	    // emptyView are not forwarded
	    addEmptyView: function(child, EmptyView) {
	      // Only trigger attach if already shown, attached, and not buffering, otherwise endBuffer() or
	      // Region#show() handles this.
	      var canTriggerAttach = this._isShown && !this.isBuffering && Marionette.isNodeAttached(this.el);
	      var nestedViews;
	  
	      // get the emptyViewOptions, falling back to childViewOptions
	      var emptyViewOptions = this.getOption('emptyViewOptions') ||
	                            this.getOption('childViewOptions');
	  
	      if (_.isFunction(emptyViewOptions)) {
	        emptyViewOptions = emptyViewOptions.call(this, child, this._emptyViewIndex);
	      }
	  
	      // build the empty view
	      var view = this.buildChildView(child, EmptyView, emptyViewOptions);
	  
	      view._parent = this;
	  
	      // Proxy emptyView events
	      this.proxyChildEvents(view);
	  
	      view.once('render', function() {
	        // trigger the 'before:show' event on `view` if the collection view has already been shown
	        if (this._isShown) {
	          Marionette.triggerMethodOn(view, 'before:show', view);
	        }
	  
	        // Trigger `before:attach` following `render` to avoid adding logic and event triggers
	        // to public method `renderChildView()`.
	        if (canTriggerAttach && this._triggerBeforeAttach) {
	          nestedViews = this._getViewAndNested(view);
	          this._triggerMethodMany(nestedViews, this, 'before:attach');
	        }
	      }, this);
	  
	      // Store the `emptyView` like a `childView` so we can properly remove and/or close it later
	      this.children.add(view);
	      this.renderChildView(view, this._emptyViewIndex);
	  
	      // Trigger `attach`
	      if (canTriggerAttach && this._triggerAttach) {
	        nestedViews = this._getViewAndNested(view);
	        this._triggerMethodMany(nestedViews, this, 'attach');
	      }
	      // call the 'show' method if the collection view has already been shown
	      if (this._isShown) {
	        Marionette.triggerMethodOn(view, 'show', view);
	      }
	    },
	  
	    // Retrieve the `childView` class, either from `this.options.childView`
	    // or from the `childView` in the object definition. The "options"
	    // takes precedence.
	    // This method receives the model that will be passed to the instance
	    // created from this `childView`. Overriding methods may use the child
	    // to determine what `childView` class to return.
	    getChildView: function(child) {
	      var childView = this.getOption('childView');
	  
	      if (!childView) {
	        throw new Marionette.Error({
	          name: 'NoChildViewError',
	          message: 'A "childView" must be specified'
	        });
	      }
	  
	      return childView;
	    },
	  
	    // Render the child's view and add it to the
	    // HTML for the collection view at a given index.
	    // This will also update the indices of later views in the collection
	    // in order to keep the children in sync with the collection.
	    addChild: function(child, ChildView, index) {
	      var childViewOptions = this.getOption('childViewOptions');
	      childViewOptions = Marionette._getValue(childViewOptions, this, [child, index]);
	  
	      var view = this.buildChildView(child, ChildView, childViewOptions);
	  
	      // increment indices of views after this one
	      this._updateIndices(view, true, index);
	  
	      this.triggerMethod('before:add:child', view);
	      this._addChildView(view, index);
	      this.triggerMethod('add:child', view);
	  
	      view._parent = this;
	  
	      return view;
	    },
	  
	    // Internal method. This decrements or increments the indices of views after the
	    // added/removed view to keep in sync with the collection.
	    _updateIndices: function(view, increment, index) {
	      if (!this.getOption('sort')) {
	        return;
	      }
	  
	      if (increment) {
	        // assign the index to the view
	        view._index = index;
	      }
	  
	      // update the indexes of views after this one
	      this.children.each(function(laterView) {
	        if (laterView._index >= view._index) {
	          laterView._index += increment ? 1 : -1;
	        }
	      });
	    },
	  
	    // Internal Method. Add the view to children and render it at
	    // the given index.
	    _addChildView: function(view, index) {
	      // Only trigger attach if already shown, attached, and not buffering, otherwise endBuffer() or
	      // Region#show() handles this.
	      var canTriggerAttach = this._isShown && !this.isBuffering && Marionette.isNodeAttached(this.el);
	      var nestedViews;
	  
	      // set up the child view event forwarding
	      this.proxyChildEvents(view);
	  
	      view.once('render', function() {
	        // trigger the 'before:show' event on `view` if the collection view has already been shown
	        if (this._isShown && !this.isBuffering) {
	          Marionette.triggerMethodOn(view, 'before:show', view);
	        }
	  
	        // Trigger `before:attach` following `render` to avoid adding logic and event triggers
	        // to public method `renderChildView()`.
	        if (canTriggerAttach && this._triggerBeforeAttach) {
	          nestedViews = this._getViewAndNested(view);
	          this._triggerMethodMany(nestedViews, this, 'before:attach');
	        }
	      }, this);
	  
	      // Store the child view itself so we can properly remove and/or destroy it later
	      this.children.add(view);
	      this.renderChildView(view, index);
	  
	      // Trigger `attach`
	      if (canTriggerAttach && this._triggerAttach) {
	        nestedViews = this._getViewAndNested(view);
	        this._triggerMethodMany(nestedViews, this, 'attach');
	      }
	      // Trigger `show`
	      if (this._isShown && !this.isBuffering) {
	        Marionette.triggerMethodOn(view, 'show', view);
	      }
	    },
	  
	    // render the child view
	    renderChildView: function(view, index) {
	      if (!view.supportsRenderLifecycle) {
	        Marionette.triggerMethodOn(view, 'before:render', view);
	      }
	      view.render();
	      if (!view.supportsRenderLifecycle) {
	        Marionette.triggerMethodOn(view, 'render', view);
	      }
	      this.attachHtml(this, view, index);
	      return view;
	    },
	  
	    // Build a `childView` for a model in the collection.
	    buildChildView: function(child, ChildViewClass, childViewOptions) {
	      var options = _.extend({model: child}, childViewOptions);
	      var childView = new ChildViewClass(options);
	      Marionette.MonitorDOMRefresh(childView);
	      return childView;
	    },
	  
	    // Remove the child view and destroy it.
	    // This function also updates the indices of
	    // later views in the collection in order to keep
	    // the children in sync with the collection.
	    removeChildView: function(view) {
	      if (!view) { return view; }
	  
	      this.triggerMethod('before:remove:child', view);
	  
	      if (!view.supportsDestroyLifecycle) {
	        Marionette.triggerMethodOn(view, 'before:destroy', view);
	      }
	      // call 'destroy' or 'remove', depending on which is found
	      if (view.destroy) {
	        view.destroy();
	      } else {
	        view.remove();
	      }
	      if (!view.supportsDestroyLifecycle) {
	        Marionette.triggerMethodOn(view, 'destroy', view);
	      }
	  
	      delete view._parent;
	      this.stopListening(view);
	      this.children.remove(view);
	      this.triggerMethod('remove:child', view);
	  
	      // decrement the index of views after this one
	      this._updateIndices(view, false);
	  
	      return view;
	    },
	  
	    // check if the collection is empty
	    isEmpty: function() {
	      return !this.collection || this.collection.length === 0;
	    },
	  
	    // If empty, show the empty view
	    checkEmpty: function() {
	      if (this.isEmpty(this.collection)) {
	        this.showEmptyView();
	      }
	    },
	  
	    // You might need to override this if you've overridden attachHtml
	    attachBuffer: function(collectionView, buffer) {
	      collectionView.$el.append(buffer);
	    },
	  
	    // Create a fragment buffer from the currently buffered children
	    _createBuffer: function() {
	      var elBuffer = document.createDocumentFragment();
	      _.each(this._bufferedChildren, function(b) {
	        elBuffer.appendChild(b.el);
	      });
	      return elBuffer;
	    },
	  
	    // Append the HTML to the collection's `el`.
	    // Override this method to do something other
	    // than `.append`.
	    attachHtml: function(collectionView, childView, index) {
	      if (collectionView.isBuffering) {
	        // buffering happens on reset events and initial renders
	        // in order to reduce the number of inserts into the
	        // document, which are expensive.
	        collectionView._bufferedChildren.splice(index, 0, childView);
	      } else {
	        // If we've already rendered the main collection, append
	        // the new child into the correct order if we need to. Otherwise
	        // append to the end.
	        if (!collectionView._insertBefore(childView, index)) {
	          collectionView._insertAfter(childView);
	        }
	      }
	    },
	  
	    // Internal method. Check whether we need to insert the view into
	    // the correct position.
	    _insertBefore: function(childView, index) {
	      var currentView;
	      var findPosition = this.getOption('sort') && (index < this.children.length - 1);
	      if (findPosition) {
	        // Find the view after this one
	        currentView = this.children.find(function(view) {
	          return view._index === index + 1;
	        });
	      }
	  
	      if (currentView) {
	        currentView.$el.before(childView.el);
	        return true;
	      }
	  
	      return false;
	    },
	  
	    // Internal method. Append a view to the end of the $el
	    _insertAfter: function(childView) {
	      this.$el.append(childView.el);
	    },
	  
	    // Internal method to set up the `children` object for
	    // storing all of the child views
	    _initChildViewStorage: function() {
	      this.children = new Backbone.ChildViewContainer();
	    },
	  
	    // Handle cleanup and other destroying needs for the collection of views
	    destroy: function() {
	      if (this.isDestroyed) { return this; }
	  
	      this.triggerMethod('before:destroy:collection');
	      this.destroyChildren({checkEmpty: false});
	      this.triggerMethod('destroy:collection');
	  
	      return Marionette.View.prototype.destroy.apply(this, arguments);
	    },
	  
	    // Destroy the child views that this collection view
	    // is holding on to, if any
	    destroyChildren: function(options) {
	      var destroyOptions = options || {};
	      var shouldCheckEmpty = true;
	      var childViews = this.children.map(_.identity);
	  
	      if (!_.isUndefined(destroyOptions.checkEmpty)) {
	        shouldCheckEmpty = destroyOptions.checkEmpty;
	      }
	  
	      this.children.each(this.removeChildView, this);
	  
	      if (shouldCheckEmpty) {
	        this.checkEmpty();
	      }
	      return childViews;
	    },
	  
	    // Return true if the given child should be shown
	    // Return false otherwise
	    // The filter will be passed (child, index, collection)
	    // Where
	    //  'child' is the given model
	    //  'index' is the index of that model in the collection
	    //  'collection' is the collection referenced by this CollectionView
	    _shouldAddChild: function(child, index) {
	      var filter = this.getOption('filter');
	      return !_.isFunction(filter) || filter.call(this, child, index, this.collection);
	    },
	  
	    // Set up the child view event forwarding. Uses a "childview:"
	    // prefix in front of all forwarded events.
	    proxyChildEvents: function(view) {
	      var prefix = this.getOption('childViewEventPrefix');
	  
	      // Forward all child view events through the parent,
	      // prepending "childview:" to the event name
	      this.listenTo(view, 'all', function() {
	        var args = _.toArray(arguments);
	        var rootEvent = args[0];
	        var childEvents = this.normalizeMethods(_.result(this, 'childEvents'));
	  
	        args[0] = prefix + ':' + rootEvent;
	        args.splice(1, 0, view);
	  
	        // call collectionView childEvent if defined
	        if (typeof childEvents !== 'undefined' && _.isFunction(childEvents[rootEvent])) {
	          childEvents[rootEvent].apply(this, args.slice(1));
	        }
	  
	        this.triggerMethod.apply(this, args);
	      });
	    },
	  
	    _getImmediateChildren: function() {
	      return _.values(this.children._views);
	    },
	  
	    _getViewAndNested: function(view) {
	      // This will not fail on Backbone.View which does not have #_getNestedViews.
	      return [view].concat(_.result(view, '_getNestedViews') || []);
	    },
	  
	    getViewComparator: function() {
	      return this.getOption('viewComparator');
	    }
	  });
	  
	  /* jshint maxstatements: 17, maxlen: 117 */
	  
	  // Composite View
	  // --------------
	  
	  // Used for rendering a branch-leaf, hierarchical structure.
	  // Extends directly from CollectionView and also renders an
	  // a child view as `modelView`, for the top leaf
	  Marionette.CompositeView = Marionette.CollectionView.extend({
	  
	    // Setting up the inheritance chain which allows changes to
	    // Marionette.CollectionView.prototype.constructor which allows overriding
	    // option to pass '{sort: false}' to prevent the CompositeView from
	    // maintaining the sorted order of the collection.
	    // This will fallback onto appending childView's to the end.
	    constructor: function() {
	      Marionette.CollectionView.apply(this, arguments);
	    },
	  
	    // Configured the initial events that the composite view
	    // binds to. Override this method to prevent the initial
	    // events, or to add your own initial events.
	    _initialEvents: function() {
	  
	      // Bind only after composite view is rendered to avoid adding child views
	      // to nonexistent childViewContainer
	  
	      if (this.collection) {
	        this.listenTo(this.collection, 'add', this._onCollectionAdd);
	        this.listenTo(this.collection, 'remove', this._onCollectionRemove);
	        this.listenTo(this.collection, 'reset', this._renderChildren);
	  
	        if (this.getOption('sort')) {
	          this.listenTo(this.collection, 'sort', this._sortViews);
	        }
	      }
	    },
	  
	    // Retrieve the `childView` to be used when rendering each of
	    // the items in the collection. The default is to return
	    // `this.childView` or Marionette.CompositeView if no `childView`
	    // has been defined
	    getChildView: function(child) {
	      var childView = this.getOption('childView') || this.constructor;
	  
	      return childView;
	    },
	  
	    // Serialize the model for the view.
	    // You can override the `serializeData` method in your own view
	    // definition, to provide custom serialization for your view's data.
	    serializeData: function() {
	      var data = {};
	  
	      if (this.model) {
	        data = _.partial(this.serializeModel, this.model).apply(this, arguments);
	      }
	  
	      return data;
	    },
	  
	    // Renders the model and the collection.
	    render: function() {
	      this._ensureViewIsIntact();
	      this._isRendering = true;
	      this.resetChildViewContainer();
	  
	      this.triggerMethod('before:render', this);
	  
	      this._renderTemplate();
	      this._renderChildren();
	  
	      this._isRendering = false;
	      this.isRendered = true;
	      this.triggerMethod('render', this);
	      return this;
	    },
	  
	    _renderChildren: function() {
	      if (this.isRendered || this._isRendering) {
	        Marionette.CollectionView.prototype._renderChildren.call(this);
	      }
	    },
	  
	    // Render the root template that the children
	    // views are appended to
	    _renderTemplate: function() {
	      var data = {};
	      data = this.serializeData();
	      data = this.mixinTemplateHelpers(data);
	  
	      this.triggerMethod('before:render:template');
	  
	      var template = this.getTemplate();
	      var html = Marionette.Renderer.render(template, data, this);
	      this.attachElContent(html);
	  
	      // the ui bindings is done here and not at the end of render since they
	      // will not be available until after the model is rendered, but should be
	      // available before the collection is rendered.
	      this.bindUIElements();
	      this.triggerMethod('render:template');
	    },
	  
	    // Attaches the content of the root.
	    // This method can be overridden to optimize rendering,
	    // or to render in a non standard way.
	    //
	    // For example, using `innerHTML` instead of `$el.html`
	    //
	    // ```js
	    // attachElContent: function(html) {
	    //   this.el.innerHTML = html;
	    //   return this;
	    // }
	    // ```
	    attachElContent: function(html) {
	      this.$el.html(html);
	  
	      return this;
	    },
	  
	    // You might need to override this if you've overridden attachHtml
	    attachBuffer: function(compositeView, buffer) {
	      var $container = this.getChildViewContainer(compositeView);
	      $container.append(buffer);
	    },
	  
	    // Internal method. Append a view to the end of the $el.
	    // Overidden from CollectionView to ensure view is appended to
	    // childViewContainer
	    _insertAfter: function(childView) {
	      var $container = this.getChildViewContainer(this, childView);
	      $container.append(childView.el);
	    },
	  
	    // Internal method. Append reordered childView'.
	    // Overidden from CollectionView to ensure reordered views
	    // are appended to childViewContainer
	    _appendReorderedChildren: function(children) {
	      var $container = this.getChildViewContainer(this);
	      $container.append(children);
	    },
	  
	    // Internal method to ensure an `$childViewContainer` exists, for the
	    // `attachHtml` method to use.
	    getChildViewContainer: function(containerView, childView) {
	      if (!!containerView.$childViewContainer) {
	        return containerView.$childViewContainer;
	      }
	  
	      var container;
	      var childViewContainer = Marionette.getOption(containerView, 'childViewContainer');
	      if (childViewContainer) {
	  
	        var selector = Marionette._getValue(childViewContainer, containerView);
	  
	        if (selector.charAt(0) === '@' && containerView.ui) {
	          container = containerView.ui[selector.substr(4)];
	        } else {
	          container = containerView.$(selector);
	        }
	  
	        if (container.length <= 0) {
	          throw new Marionette.Error({
	            name: 'ChildViewContainerMissingError',
	            message: 'The specified "childViewContainer" was not found: ' + containerView.childViewContainer
	          });
	        }
	  
	      } else {
	        container = containerView.$el;
	      }
	  
	      containerView.$childViewContainer = container;
	      return container;
	    },
	  
	    // Internal method to reset the `$childViewContainer` on render
	    resetChildViewContainer: function() {
	      if (this.$childViewContainer) {
	        this.$childViewContainer = undefined;
	      }
	    }
	  });
	  
	  // Layout View
	  // -----------
	  
	  // Used for managing application layoutViews, nested layoutViews and
	  // multiple regions within an application or sub-application.
	  //
	  // A specialized view class that renders an area of HTML and then
	  // attaches `Region` instances to the specified `regions`.
	  // Used for composite view management and sub-application areas.
	  Marionette.LayoutView = Marionette.ItemView.extend({
	    regionClass: Marionette.Region,
	  
	    options: {
	      destroyImmediate: false
	    },
	  
	    // used as the prefix for child view events
	    // that are forwarded through the layoutview
	    childViewEventPrefix: 'childview',
	  
	    // Ensure the regions are available when the `initialize` method
	    // is called.
	    constructor: function(options) {
	      options = options || {};
	  
	      this._firstRender = true;
	      this._initializeRegions(options);
	  
	      Marionette.ItemView.call(this, options);
	    },
	  
	    // LayoutView's render will use the existing region objects the
	    // first time it is called. Subsequent calls will destroy the
	    // views that the regions are showing and then reset the `el`
	    // for the regions to the newly rendered DOM elements.
	    render: function() {
	      this._ensureViewIsIntact();
	  
	      if (this._firstRender) {
	        // if this is the first render, don't do anything to
	        // reset the regions
	        this._firstRender = false;
	      } else {
	        // If this is not the first render call, then we need to
	        // re-initialize the `el` for each region
	        this._reInitializeRegions();
	      }
	  
	      return Marionette.ItemView.prototype.render.apply(this, arguments);
	    },
	  
	    // Handle destroying regions, and then destroy the view itself.
	    destroy: function() {
	      if (this.isDestroyed) { return this; }
	      // #2134: remove parent element before destroying the child views, so
	      // removing the child views doesn't retrigger repaints
	      if (this.getOption('destroyImmediate') === true) {
	        this.$el.remove();
	      }
	      this.regionManager.destroy();
	      return Marionette.ItemView.prototype.destroy.apply(this, arguments);
	    },
	  
	    showChildView: function(regionName, view) {
	      return this.getRegion(regionName).show(view);
	    },
	  
	    getChildView: function(regionName) {
	      return this.getRegion(regionName).currentView;
	    },
	  
	    // Add a single region, by name, to the layoutView
	    addRegion: function(name, definition) {
	      var regions = {};
	      regions[name] = definition;
	      return this._buildRegions(regions)[name];
	    },
	  
	    // Add multiple regions as a {name: definition, name2: def2} object literal
	    addRegions: function(regions) {
	      this.regions = _.extend({}, this.regions, regions);
	      return this._buildRegions(regions);
	    },
	  
	    // Remove a single region from the LayoutView, by name
	    removeRegion: function(name) {
	      delete this.regions[name];
	      return this.regionManager.removeRegion(name);
	    },
	  
	    // Provides alternative access to regions
	    // Accepts the region name
	    // getRegion('main')
	    getRegion: function(region) {
	      return this.regionManager.get(region);
	    },
	  
	    // Get all regions
	    getRegions: function() {
	      return this.regionManager.getRegions();
	    },
	  
	    // internal method to build regions
	    _buildRegions: function(regions) {
	      var defaults = {
	        regionClass: this.getOption('regionClass'),
	        parentEl: _.partial(_.result, this, 'el')
	      };
	  
	      return this.regionManager.addRegions(regions, defaults);
	    },
	  
	    // Internal method to initialize the regions that have been defined in a
	    // `regions` attribute on this layoutView.
	    _initializeRegions: function(options) {
	      var regions;
	      this._initRegionManager();
	  
	      regions = Marionette._getValue(this.regions, this, [options]) || {};
	  
	      // Enable users to define `regions` as instance options.
	      var regionOptions = this.getOption.call(options, 'regions');
	  
	      // enable region options to be a function
	      regionOptions = Marionette._getValue(regionOptions, this, [options]);
	  
	      _.extend(regions, regionOptions);
	  
	      // Normalize region selectors hash to allow
	      // a user to use the @ui. syntax.
	      regions = this.normalizeUIValues(regions, ['selector', 'el']);
	  
	      this.addRegions(regions);
	    },
	  
	    // Internal method to re-initialize all of the regions by updating the `el` that
	    // they point to
	    _reInitializeRegions: function() {
	      this.regionManager.invoke('reset');
	    },
	  
	    // Enable easy overriding of the default `RegionManager`
	    // for customized region interactions and business specific
	    // view logic for better control over single regions.
	    getRegionManager: function() {
	      return new Marionette.RegionManager();
	    },
	  
	    // Internal method to initialize the region manager
	    // and all regions in it
	    _initRegionManager: function() {
	      this.regionManager = this.getRegionManager();
	      this.regionManager._parent = this;
	  
	      this.listenTo(this.regionManager, 'before:add:region', function(name) {
	        this.triggerMethod('before:add:region', name);
	      });
	  
	      this.listenTo(this.regionManager, 'add:region', function(name, region) {
	        this[name] = region;
	        this.triggerMethod('add:region', name, region);
	      });
	  
	      this.listenTo(this.regionManager, 'before:remove:region', function(name) {
	        this.triggerMethod('before:remove:region', name);
	      });
	  
	      this.listenTo(this.regionManager, 'remove:region', function(name, region) {
	        delete this[name];
	        this.triggerMethod('remove:region', name, region);
	      });
	    },
	  
	    _getImmediateChildren: function() {
	      return _.chain(this.regionManager.getRegions())
	        .pluck('currentView')
	        .compact()
	        .value();
	    }
	  });
	  

	  // Behavior
	  // --------
	  
	  // A Behavior is an isolated set of DOM /
	  // user interactions that can be mixed into any View.
	  // Behaviors allow you to blackbox View specific interactions
	  // into portable logical chunks, keeping your views simple and your code DRY.
	  
	  Marionette.Behavior = Marionette.Object.extend({
	    constructor: function(options, view) {
	      // Setup reference to the view.
	      // this comes in handle when a behavior
	      // wants to directly talk up the chain
	      // to the view.
	      this.view = view;
	      this.defaults = _.result(this, 'defaults') || {};
	      this.options  = _.extend({}, this.defaults, options);
	      // Construct an internal UI hash using
	      // the views UI hash and then the behaviors UI hash.
	      // This allows the user to use UI hash elements
	      // defined in the parent view as well as those
	      // defined in the given behavior.
	      this.ui = _.extend({}, _.result(view, 'ui'), _.result(this, 'ui'));
	  
	      Marionette.Object.apply(this, arguments);
	    },
	  
	    // proxy behavior $ method to the view
	    // this is useful for doing jquery DOM lookups
	    // scoped to behaviors view.
	    $: function() {
	      return this.view.$.apply(this.view, arguments);
	    },
	  
	    // Stops the behavior from listening to events.
	    // Overrides Object#destroy to prevent additional events from being triggered.
	    destroy: function() {
	      this.stopListening();
	  
	      return this;
	    },
	  
	    proxyViewProperties: function(view) {
	      this.$el = view.$el;
	      this.el = view.el;
	    }
	  });
	  
	  /* jshint maxlen: 143 */
	  // Behaviors
	  // ---------
	  
	  // Behaviors is a utility class that takes care of
	  // gluing your behavior instances to their given View.
	  // The most important part of this class is that you
	  // **MUST** override the class level behaviorsLookup
	  // method for things to work properly.
	  
	  Marionette.Behaviors = (function(Marionette, _) {
	    // Borrow event splitter from Backbone
	    var delegateEventSplitter = /^(\S+)\s*(.*)$/;
	  
	    function Behaviors(view, behaviors) {
	  
	      if (!_.isObject(view.behaviors)) {
	        return {};
	      }
	  
	      // Behaviors defined on a view can be a flat object literal
	      // or it can be a function that returns an object.
	      behaviors = Behaviors.parseBehaviors(view, behaviors || _.result(view, 'behaviors'));
	  
	      // Wraps several of the view's methods
	      // calling the methods first on each behavior
	      // and then eventually calling the method on the view.
	      Behaviors.wrap(view, behaviors, _.keys(methods));
	      return behaviors;
	    }
	  
	    var methods = {
	      behaviorTriggers: function(behaviorTriggers, behaviors) {
	        var triggerBuilder = new BehaviorTriggersBuilder(this, behaviors);
	        return triggerBuilder.buildBehaviorTriggers();
	      },
	  
	      behaviorEvents: function(behaviorEvents, behaviors) {
	        var _behaviorsEvents = {};
	  
	        _.each(behaviors, function(b, i) {
	          var _events = {};
	          var behaviorEvents = _.clone(_.result(b, 'events')) || {};
	  
	          // Normalize behavior events hash to allow
	          // a user to use the @ui. syntax.
	          behaviorEvents = Marionette.normalizeUIKeys(behaviorEvents, getBehaviorsUI(b));
	  
	          var j = 0;
	          _.each(behaviorEvents, function(behaviour, key) {
	            var match     = key.match(delegateEventSplitter);
	  
	            // Set event name to be namespaced using the view cid,
	            // the behavior index, and the behavior event index
	            // to generate a non colliding event namespace
	            // http://api.jquery.com/event.namespace/
	            var eventName = match[1] + '.' + [this.cid, i, j++, ' '].join('');
	            var selector  = match[2];
	  
	            var eventKey  = eventName + selector;
	            var handler   = _.isFunction(behaviour) ? behaviour : b[behaviour];
	  
	            _events[eventKey] = _.bind(handler, b);
	          }, this);
	  
	          _behaviorsEvents = _.extend(_behaviorsEvents, _events);
	        }, this);
	  
	        return _behaviorsEvents;
	      }
	    };
	  
	    _.extend(Behaviors, {
	  
	      // Placeholder method to be extended by the user.
	      // The method should define the object that stores the behaviors.
	      // i.e.
	      //
	      // ```js
	      // Marionette.Behaviors.behaviorsLookup: function() {
	      //   return App.Behaviors
	      // }
	      // ```
	      behaviorsLookup: function() {
	        throw new Marionette.Error({
	          message: 'You must define where your behaviors are stored.',
	          url: 'marionette.behaviors.html#behaviorslookup'
	        });
	      },
	  
	      // Takes care of getting the behavior class
	      // given options and a key.
	      // If a user passes in options.behaviorClass
	      // default to using that. Otherwise delegate
	      // the lookup to the users `behaviorsLookup` implementation.
	      getBehaviorClass: function(options, key) {
	        if (options.behaviorClass) {
	          return options.behaviorClass;
	        }
	  
	        // Get behavior class can be either a flat object or a method
	        return Marionette._getValue(Behaviors.behaviorsLookup, this, [options, key])[key];
	      },
	  
	      // Iterate over the behaviors object, for each behavior
	      // instantiate it and get its grouped behaviors.
	      parseBehaviors: function(view, behaviors) {
	        return _.chain(behaviors).map(function(options, key) {
	          var BehaviorClass = Behaviors.getBehaviorClass(options, key);
	  
	          var behavior = new BehaviorClass(options, view);
	          var nestedBehaviors = Behaviors.parseBehaviors(view, _.result(behavior, 'behaviors'));
	  
	          return [behavior].concat(nestedBehaviors);
	        }).flatten().value();
	      },
	  
	      // Wrap view internal methods so that they delegate to behaviors. For example,
	      // `onDestroy` should trigger destroy on all of the behaviors and then destroy itself.
	      // i.e.
	      //
	      // `view.delegateEvents = _.partial(methods.delegateEvents, view.delegateEvents, behaviors);`
	      wrap: function(view, behaviors, methodNames) {
	        _.each(methodNames, function(methodName) {
	          view[methodName] = _.partial(methods[methodName], view[methodName], behaviors);
	        });
	      }
	    });
	  
	    // Class to build handlers for `triggers` on behaviors
	    // for views
	    function BehaviorTriggersBuilder(view, behaviors) {
	      this._view      = view;
	      this._behaviors = behaviors;
	      this._triggers  = {};
	    }
	  
	    _.extend(BehaviorTriggersBuilder.prototype, {
	      // Main method to build the triggers hash with event keys and handlers
	      buildBehaviorTriggers: function() {
	        _.each(this._behaviors, this._buildTriggerHandlersForBehavior, this);
	        return this._triggers;
	      },
	  
	      // Internal method to build all trigger handlers for a given behavior
	      _buildTriggerHandlersForBehavior: function(behavior, i) {
	        var triggersHash = _.clone(_.result(behavior, 'triggers')) || {};
	  
	        triggersHash = Marionette.normalizeUIKeys(triggersHash, getBehaviorsUI(behavior));
	  
	        _.each(triggersHash, _.bind(this._setHandlerForBehavior, this, behavior, i));
	      },
	  
	      // Internal method to create and assign the trigger handler for a given
	      // behavior
	      _setHandlerForBehavior: function(behavior, i, eventName, trigger) {
	        // Unique identifier for the `this._triggers` hash
	        var triggerKey = trigger.replace(/^\S+/, function(triggerName) {
	          return triggerName + '.' + 'behaviortriggers' + i;
	        });
	  
	        this._triggers[triggerKey] = this._view._buildViewTrigger(eventName);
	      }
	    });
	  
	    function getBehaviorsUI(behavior) {
	      return behavior._uiBindings || behavior.ui;
	    }
	  
	    return Behaviors;
	  
	  })(Marionette, _);
	  

	  // App Router
	  // ----------
	  
	  // Reduce the boilerplate code of handling route events
	  // and then calling a single method on another object.
	  // Have your routers configured to call the method on
	  // your object, directly.
	  //
	  // Configure an AppRouter with `appRoutes`.
	  //
	  // App routers can only take one `controller` object.
	  // It is recommended that you divide your controller
	  // objects in to smaller pieces of related functionality
	  // and have multiple routers / controllers, instead of
	  // just one giant router and controller.
	  //
	  // You can also add standard routes to an AppRouter.
	  
	  Marionette.AppRouter = Backbone.Router.extend({
	  
	    constructor: function(options) {
	      this.options = options || {};
	  
	      Backbone.Router.apply(this, arguments);
	  
	      var appRoutes = this.getOption('appRoutes');
	      var controller = this._getController();
	      this.processAppRoutes(controller, appRoutes);
	      this.on('route', this._processOnRoute, this);
	    },
	  
	    // Similar to route method on a Backbone Router but
	    // method is called on the controller
	    appRoute: function(route, methodName) {
	      var controller = this._getController();
	      this._addAppRoute(controller, route, methodName);
	    },
	  
	    // process the route event and trigger the onRoute
	    // method call, if it exists
	    _processOnRoute: function(routeName, routeArgs) {
	      // make sure an onRoute before trying to call it
	      if (_.isFunction(this.onRoute)) {
	        // find the path that matches the current route
	        var routePath = _.invert(this.getOption('appRoutes'))[routeName];
	        this.onRoute(routeName, routePath, routeArgs);
	      }
	    },
	  
	    // Internal method to process the `appRoutes` for the
	    // router, and turn them in to routes that trigger the
	    // specified method on the specified `controller`.
	    processAppRoutes: function(controller, appRoutes) {
	      if (!appRoutes) { return; }
	  
	      var routeNames = _.keys(appRoutes).reverse(); // Backbone requires reverted order of routes
	  
	      _.each(routeNames, function(route) {
	        this._addAppRoute(controller, route, appRoutes[route]);
	      }, this);
	    },
	  
	    _getController: function() {
	      return this.getOption('controller');
	    },
	  
	    _addAppRoute: function(controller, route, methodName) {
	      var method = controller[methodName];
	  
	      if (!method) {
	        throw new Marionette.Error('Method "' + methodName + '" was not found on the controller');
	      }
	  
	      this.route(route, methodName, _.bind(method, controller));
	    },
	  
	    mergeOptions: Marionette.mergeOptions,
	  
	    // Proxy `getOption` to enable getting options from this or this.options by name.
	    getOption: Marionette.proxyGetOption,
	  
	    triggerMethod: Marionette.triggerMethod,
	  
	    bindEntityEvents: Marionette.proxyBindEntityEvents,
	  
	    unbindEntityEvents: Marionette.proxyUnbindEntityEvents
	  });
	  
	  // Application
	  // -----------
	  
	  // Contain and manage the composite application as a whole.
	  // Stores and starts up `Region` objects, includes an
	  // event aggregator as `app.vent`
	  Marionette.Application = Marionette.Object.extend({
	    constructor: function(options) {
	      this._initializeRegions(options);
	      this._initCallbacks = new Marionette.Callbacks();
	      this.submodules = {};
	      _.extend(this, options);
	      this._initChannel();
	      Marionette.Object.apply(this, arguments);
	    },
	  
	    // Command execution, facilitated by Backbone.Wreqr.Commands
	    execute: function() {
	      this.commands.execute.apply(this.commands, arguments);
	    },
	  
	    // Request/response, facilitated by Backbone.Wreqr.RequestResponse
	    request: function() {
	      return this.reqres.request.apply(this.reqres, arguments);
	    },
	  
	    // Add an initializer that is either run at when the `start`
	    // method is called, or run immediately if added after `start`
	    // has already been called.
	    addInitializer: function(initializer) {
	      this._initCallbacks.add(initializer);
	    },
	  
	    // kick off all of the application's processes.
	    // initializes all of the regions that have been added
	    // to the app, and runs all of the initializer functions
	    start: function(options) {
	      this.triggerMethod('before:start', options);
	      this._initCallbacks.run(options, this);
	      this.triggerMethod('start', options);
	    },
	  
	    // Add regions to your app.
	    // Accepts a hash of named strings or Region objects
	    // addRegions({something: "#someRegion"})
	    // addRegions({something: Region.extend({el: "#someRegion"}) });
	    addRegions: function(regions) {
	      return this._regionManager.addRegions(regions);
	    },
	  
	    // Empty all regions in the app, without removing them
	    emptyRegions: function() {
	      return this._regionManager.emptyRegions();
	    },
	  
	    // Removes a region from your app, by name
	    // Accepts the regions name
	    // removeRegion('myRegion')
	    removeRegion: function(region) {
	      return this._regionManager.removeRegion(region);
	    },
	  
	    // Provides alternative access to regions
	    // Accepts the region name
	    // getRegion('main')
	    getRegion: function(region) {
	      return this._regionManager.get(region);
	    },
	  
	    // Get all the regions from the region manager
	    getRegions: function() {
	      return this._regionManager.getRegions();
	    },
	  
	    // Create a module, attached to the application
	    module: function(moduleNames, moduleDefinition) {
	  
	      // Overwrite the module class if the user specifies one
	      var ModuleClass = Marionette.Module.getClass(moduleDefinition);
	  
	      var args = _.toArray(arguments);
	      args.unshift(this);
	  
	      // see the Marionette.Module object for more information
	      return ModuleClass.create.apply(ModuleClass, args);
	    },
	  
	    // Enable easy overriding of the default `RegionManager`
	    // for customized region interactions and business-specific
	    // view logic for better control over single regions.
	    getRegionManager: function() {
	      return new Marionette.RegionManager();
	    },
	  
	    // Internal method to initialize the regions that have been defined in a
	    // `regions` attribute on the application instance
	    _initializeRegions: function(options) {
	      var regions = _.isFunction(this.regions) ? this.regions(options) : this.regions || {};
	  
	      this._initRegionManager();
	  
	      // Enable users to define `regions` in instance options.
	      var optionRegions = Marionette.getOption(options, 'regions');
	  
	      // Enable region options to be a function
	      if (_.isFunction(optionRegions)) {
	        optionRegions = optionRegions.call(this, options);
	      }
	  
	      // Overwrite current regions with those passed in options
	      _.extend(regions, optionRegions);
	  
	      this.addRegions(regions);
	  
	      return this;
	    },
	  
	    // Internal method to set up the region manager
	    _initRegionManager: function() {
	      this._regionManager = this.getRegionManager();
	      this._regionManager._parent = this;
	  
	      this.listenTo(this._regionManager, 'before:add:region', function() {
	        Marionette._triggerMethod(this, 'before:add:region', arguments);
	      });
	  
	      this.listenTo(this._regionManager, 'add:region', function(name, region) {
	        this[name] = region;
	        Marionette._triggerMethod(this, 'add:region', arguments);
	      });
	  
	      this.listenTo(this._regionManager, 'before:remove:region', function() {
	        Marionette._triggerMethod(this, 'before:remove:region', arguments);
	      });
	  
	      this.listenTo(this._regionManager, 'remove:region', function(name) {
	        delete this[name];
	        Marionette._triggerMethod(this, 'remove:region', arguments);
	      });
	    },
	  
	    // Internal method to setup the Wreqr.radio channel
	    _initChannel: function() {
	      this.channelName = _.result(this, 'channelName') || 'global';
	      this.channel = _.result(this, 'channel') || Backbone.Wreqr.radio.channel(this.channelName);
	      this.vent = _.result(this, 'vent') || this.channel.vent;
	      this.commands = _.result(this, 'commands') || this.channel.commands;
	      this.reqres = _.result(this, 'reqres') || this.channel.reqres;
	    }
	  });
	  
	  /* jshint maxparams: 9 */
	  
	  // Module
	  // ------
	  
	  // A simple module system, used to create privacy and encapsulation in
	  // Marionette applications
	  Marionette.Module = function(moduleName, app, options) {
	    this.moduleName = moduleName;
	    this.options = _.extend({}, this.options, options);
	    // Allow for a user to overide the initialize
	    // for a given module instance.
	    this.initialize = options.initialize || this.initialize;
	  
	    // Set up an internal store for sub-modules.
	    this.submodules = {};
	  
	    this._setupInitializersAndFinalizers();
	  
	    // Set an internal reference to the app
	    // within a module.
	    this.app = app;
	  
	    if (_.isFunction(this.initialize)) {
	      this.initialize(moduleName, app, this.options);
	    }
	  };
	  
	  Marionette.Module.extend = Marionette.extend;
	  
	  // Extend the Module prototype with events / listenTo, so that the module
	  // can be used as an event aggregator or pub/sub.
	  _.extend(Marionette.Module.prototype, Backbone.Events, {
	  
	    // By default modules start with their parents.
	    startWithParent: true,
	  
	    // Initialize is an empty function by default. Override it with your own
	    // initialization logic when extending Marionette.Module.
	    initialize: function() {},
	  
	    // Initializer for a specific module. Initializers are run when the
	    // module's `start` method is called.
	    addInitializer: function(callback) {
	      this._initializerCallbacks.add(callback);
	    },
	  
	    // Finalizers are run when a module is stopped. They are used to teardown
	    // and finalize any variables, references, events and other code that the
	    // module had set up.
	    addFinalizer: function(callback) {
	      this._finalizerCallbacks.add(callback);
	    },
	  
	    // Start the module, and run all of its initializers
	    start: function(options) {
	      // Prevent re-starting a module that is already started
	      if (this._isInitialized) { return; }
	  
	      // start the sub-modules (depth-first hierarchy)
	      _.each(this.submodules, function(mod) {
	        // check to see if we should start the sub-module with this parent
	        if (mod.startWithParent) {
	          mod.start(options);
	        }
	      });
	  
	      // run the callbacks to "start" the current module
	      this.triggerMethod('before:start', options);
	  
	      this._initializerCallbacks.run(options, this);
	      this._isInitialized = true;
	  
	      this.triggerMethod('start', options);
	    },
	  
	    // Stop this module by running its finalizers and then stop all of
	    // the sub-modules for this module
	    stop: function() {
	      // if we are not initialized, don't bother finalizing
	      if (!this._isInitialized) { return; }
	      this._isInitialized = false;
	  
	      this.triggerMethod('before:stop');
	  
	      // stop the sub-modules; depth-first, to make sure the
	      // sub-modules are stopped / finalized before parents
	      _.invoke(this.submodules, 'stop');
	  
	      // run the finalizers
	      this._finalizerCallbacks.run(undefined, this);
	  
	      // reset the initializers and finalizers
	      this._initializerCallbacks.reset();
	      this._finalizerCallbacks.reset();
	  
	      this.triggerMethod('stop');
	    },
	  
	    // Configure the module with a definition function and any custom args
	    // that are to be passed in to the definition function
	    addDefinition: function(moduleDefinition, customArgs) {
	      this._runModuleDefinition(moduleDefinition, customArgs);
	    },
	  
	    // Internal method: run the module definition function with the correct
	    // arguments
	    _runModuleDefinition: function(definition, customArgs) {
	      // If there is no definition short circut the method.
	      if (!definition) { return; }
	  
	      // build the correct list of arguments for the module definition
	      var args = _.flatten([
	        this,
	        this.app,
	        Backbone,
	        Marionette,
	        Backbone.$, _,
	        customArgs
	      ]);
	  
	      definition.apply(this, args);
	    },
	  
	    // Internal method: set up new copies of initializers and finalizers.
	    // Calling this method will wipe out all existing initializers and
	    // finalizers.
	    _setupInitializersAndFinalizers: function() {
	      this._initializerCallbacks = new Marionette.Callbacks();
	      this._finalizerCallbacks = new Marionette.Callbacks();
	    },
	  
	    // import the `triggerMethod` to trigger events with corresponding
	    // methods if the method exists
	    triggerMethod: Marionette.triggerMethod
	  });
	  
	  // Class methods to create modules
	  _.extend(Marionette.Module, {
	  
	    // Create a module, hanging off the app parameter as the parent object.
	    create: function(app, moduleNames, moduleDefinition) {
	      var module = app;
	  
	      // get the custom args passed in after the module definition and
	      // get rid of the module name and definition function
	      var customArgs = _.drop(arguments, 3);
	  
	      // Split the module names and get the number of submodules.
	      // i.e. an example module name of `Doge.Wow.Amaze` would
	      // then have the potential for 3 module definitions.
	      moduleNames = moduleNames.split('.');
	      var length = moduleNames.length;
	  
	      // store the module definition for the last module in the chain
	      var moduleDefinitions = [];
	      moduleDefinitions[length - 1] = moduleDefinition;
	  
	      // Loop through all the parts of the module definition
	      _.each(moduleNames, function(moduleName, i) {
	        var parentModule = module;
	        module = this._getModule(parentModule, moduleName, app, moduleDefinition);
	        this._addModuleDefinition(parentModule, module, moduleDefinitions[i], customArgs);
	      }, this);
	  
	      // Return the last module in the definition chain
	      return module;
	    },
	  
	    _getModule: function(parentModule, moduleName, app, def, args) {
	      var options = _.extend({}, def);
	      var ModuleClass = this.getClass(def);
	  
	      // Get an existing module of this name if we have one
	      var module = parentModule[moduleName];
	  
	      if (!module) {
	        // Create a new module if we don't have one
	        module = new ModuleClass(moduleName, app, options);
	        parentModule[moduleName] = module;
	        // store the module on the parent
	        parentModule.submodules[moduleName] = module;
	      }
	  
	      return module;
	    },
	  
	    // ## Module Classes
	    //
	    // Module classes can be used as an alternative to the define pattern.
	    // The extend function of a Module is identical to the extend functions
	    // on other Backbone and Marionette classes.
	    // This allows module lifecyle events like `onStart` and `onStop` to be called directly.
	    getClass: function(moduleDefinition) {
	      var ModuleClass = Marionette.Module;
	  
	      if (!moduleDefinition) {
	        return ModuleClass;
	      }
	  
	      // If all of the module's functionality is defined inside its class,
	      // then the class can be passed in directly. `MyApp.module("Foo", FooModule)`.
	      if (moduleDefinition.prototype instanceof ModuleClass) {
	        return moduleDefinition;
	      }
	  
	      return moduleDefinition.moduleClass || ModuleClass;
	    },
	  
	    // Add the module definition and add a startWithParent initializer function.
	    // This is complicated because module definitions are heavily overloaded
	    // and support an anonymous function, module class, or options object
	    _addModuleDefinition: function(parentModule, module, def, args) {
	      var fn = this._getDefine(def);
	      var startWithParent = this._getStartWithParent(def, module);
	  
	      if (fn) {
	        module.addDefinition(fn, args);
	      }
	  
	      this._addStartWithParent(parentModule, module, startWithParent);
	    },
	  
	    _getStartWithParent: function(def, module) {
	      var swp;
	  
	      if (_.isFunction(def) && (def.prototype instanceof Marionette.Module)) {
	        swp = module.constructor.prototype.startWithParent;
	        return _.isUndefined(swp) ? true : swp;
	      }
	  
	      if (_.isObject(def)) {
	        swp = def.startWithParent;
	        return _.isUndefined(swp) ? true : swp;
	      }
	  
	      return true;
	    },
	  
	    _getDefine: function(def) {
	      if (_.isFunction(def) && !(def.prototype instanceof Marionette.Module)) {
	        return def;
	      }
	  
	      if (_.isObject(def)) {
	        return def.define;
	      }
	  
	      return null;
	    },
	  
	    _addStartWithParent: function(parentModule, module, startWithParent) {
	      module.startWithParent = module.startWithParent && startWithParent;
	  
	      if (!module.startWithParent || !!module.startWithParentIsConfigured) {
	        return;
	      }
	  
	      module.startWithParentIsConfigured = true;
	  
	      parentModule.addInitializer(function(options) {
	        if (module.startWithParent) {
	          module.start(options);
	        }
	      });
	    }
	  });
	  

	  return Marionette;
	}));


/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/* WEBPACK VAR INJECTION */(function(global) {//     Backbone.js 1.2.3

	//     (c) 2010-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
	//     Backbone may be freely distributed under the MIT license.
	//     For all details and documentation:
	//     http://backbonejs.org

	(function(factory) {

	  // Establish the root object, `window` (`self`) in the browser, or `global` on the server.
	  // We use `self` instead of `window` for `WebWorker` support.
	  var root = (typeof self == 'object' && self.self == self && self) ||
	            (typeof global == 'object' && global.global == global && global);

	  // Set up Backbone appropriately for the environment. Start with AMD.
	  if (true) {
	    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(2), __webpack_require__(1), exports], __WEBPACK_AMD_DEFINE_RESULT__ = function(_, $, exports) {
	      // Export global even in AMD case in case this script is loaded with
	      // others that may still expect a global Backbone.
	      root.Backbone = factory(root, exports, _, $);
	    }.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

	  // Next for Node.js or CommonJS. jQuery may not be needed as a module.
	  } else if (typeof exports !== 'undefined') {
	    var _ = require('underscore'), $;
	    try { $ = require('jquery'); } catch(e) {}
	    factory(root, exports, _, $);

	  // Finally, as a browser global.
	  } else {
	    root.Backbone = factory(root, {}, root._, (root.jQuery || root.Zepto || root.ender || root.$));
	  }

	}(function(root, Backbone, _, $) {

	  // Initial Setup
	  // -------------

	  // Save the previous value of the `Backbone` variable, so that it can be
	  // restored later on, if `noConflict` is used.
	  var previousBackbone = root.Backbone;

	  // Create a local reference to a common array method we'll want to use later.
	  var slice = Array.prototype.slice;

	  // Current version of the library. Keep in sync with `package.json`.
	  Backbone.VERSION = '1.2.3';

	  // For Backbone's purposes, jQuery, Zepto, Ender, or My Library (kidding) owns
	  // the `$` variable.
	  Backbone.$ = $;

	  // Runs Backbone.js in *noConflict* mode, returning the `Backbone` variable
	  // to its previous owner. Returns a reference to this Backbone object.
	  Backbone.noConflict = function() {
	    root.Backbone = previousBackbone;
	    return this;
	  };

	  // Turn on `emulateHTTP` to support legacy HTTP servers. Setting this option
	  // will fake `"PATCH"`, `"PUT"` and `"DELETE"` requests via the `_method` parameter and
	  // set a `X-Http-Method-Override` header.
	  Backbone.emulateHTTP = false;

	  // Turn on `emulateJSON` to support legacy servers that can't deal with direct
	  // `application/json` requests ... this will encode the body as
	  // `application/x-www-form-urlencoded` instead and will send the model in a
	  // form param named `model`.
	  Backbone.emulateJSON = false;

	  // Proxy Backbone class methods to Underscore functions, wrapping the model's
	  // `attributes` object or collection's `models` array behind the scenes.
	  //
	  // collection.filter(function(model) { return model.get('age') > 10 });
	  // collection.each(this.addView);
	  //
	  // `Function#apply` can be slow so we use the method's arg count, if we know it.
	  var addMethod = function(length, method, attribute) {
	    switch (length) {
	      case 1: return function() {
	        return _[method](this[attribute]);
	      };
	      case 2: return function(value) {
	        return _[method](this[attribute], value);
	      };
	      case 3: return function(iteratee, context) {
	        return _[method](this[attribute], cb(iteratee, this), context);
	      };
	      case 4: return function(iteratee, defaultVal, context) {
	        return _[method](this[attribute], cb(iteratee, this), defaultVal, context);
	      };
	      default: return function() {
	        var args = slice.call(arguments);
	        args.unshift(this[attribute]);
	        return _[method].apply(_, args);
	      };
	    }
	  };
	  var addUnderscoreMethods = function(Class, methods, attribute) {
	    _.each(methods, function(length, method) {
	      if (_[method]) Class.prototype[method] = addMethod(length, method, attribute);
	    });
	  };

	  // Support `collection.sortBy('attr')` and `collection.findWhere({id: 1})`.
	  var cb = function(iteratee, instance) {
	    if (_.isFunction(iteratee)) return iteratee;
	    if (_.isObject(iteratee) && !instance._isModel(iteratee)) return modelMatcher(iteratee);
	    if (_.isString(iteratee)) return function(model) { return model.get(iteratee); };
	    return iteratee;
	  };
	  var modelMatcher = function(attrs) {
	    var matcher = _.matches(attrs);
	    return function(model) {
	      return matcher(model.attributes);
	    };
	  };

	  // Backbone.Events
	  // ---------------

	  // A module that can be mixed in to *any object* in order to provide it with
	  // a custom event channel. You may bind a callback to an event with `on` or
	  // remove with `off`; `trigger`-ing an event fires all callbacks in
	  // succession.
	  //
	  //     var object = {};
	  //     _.extend(object, Backbone.Events);
	  //     object.on('expand', function(){ alert('expanded'); });
	  //     object.trigger('expand');
	  //
	  var Events = Backbone.Events = {};

	  // Regular expression used to split event strings.
	  var eventSplitter = /\s+/;

	  // Iterates over the standard `event, callback` (as well as the fancy multiple
	  // space-separated events `"change blur", callback` and jQuery-style event
	  // maps `{event: callback}`).
	  var eventsApi = function(iteratee, events, name, callback, opts) {
	    var i = 0, names;
	    if (name && typeof name === 'object') {
	      // Handle event maps.
	      if (callback !== void 0 && 'context' in opts && opts.context === void 0) opts.context = callback;
	      for (names = _.keys(name); i < names.length ; i++) {
	        events = eventsApi(iteratee, events, names[i], name[names[i]], opts);
	      }
	    } else if (name && eventSplitter.test(name)) {
	      // Handle space separated event names by delegating them individually.
	      for (names = name.split(eventSplitter); i < names.length; i++) {
	        events = iteratee(events, names[i], callback, opts);
	      }
	    } else {
	      // Finally, standard events.
	      events = iteratee(events, name, callback, opts);
	    }
	    return events;
	  };

	  // Bind an event to a `callback` function. Passing `"all"` will bind
	  // the callback to all events fired.
	  Events.on = function(name, callback, context) {
	    return internalOn(this, name, callback, context);
	  };

	  // Guard the `listening` argument from the public API.
	  var internalOn = function(obj, name, callback, context, listening) {
	    obj._events = eventsApi(onApi, obj._events || {}, name, callback, {
	        context: context,
	        ctx: obj,
	        listening: listening
	    });

	    if (listening) {
	      var listeners = obj._listeners || (obj._listeners = {});
	      listeners[listening.id] = listening;
	    }

	    return obj;
	  };

	  // Inversion-of-control versions of `on`. Tell *this* object to listen to
	  // an event in another object... keeping track of what it's listening to
	  // for easier unbinding later.
	  Events.listenTo =  function(obj, name, callback) {
	    if (!obj) return this;
	    var id = obj._listenId || (obj._listenId = _.uniqueId('l'));
	    var listeningTo = this._listeningTo || (this._listeningTo = {});
	    var listening = listeningTo[id];

	    // This object is not listening to any other events on `obj` yet.
	    // Setup the necessary references to track the listening callbacks.
	    if (!listening) {
	      var thisId = this._listenId || (this._listenId = _.uniqueId('l'));
	      listening = listeningTo[id] = {obj: obj, objId: id, id: thisId, listeningTo: listeningTo, count: 0};
	    }

	    // Bind callbacks on obj, and keep track of them on listening.
	    internalOn(obj, name, callback, this, listening);
	    return this;
	  };

	  // The reducing API that adds a callback to the `events` object.
	  var onApi = function(events, name, callback, options) {
	    if (callback) {
	      var handlers = events[name] || (events[name] = []);
	      var context = options.context, ctx = options.ctx, listening = options.listening;
	      if (listening) listening.count++;

	      handlers.push({ callback: callback, context: context, ctx: context || ctx, listening: listening });
	    }
	    return events;
	  };

	  // Remove one or many callbacks. If `context` is null, removes all
	  // callbacks with that function. If `callback` is null, removes all
	  // callbacks for the event. If `name` is null, removes all bound
	  // callbacks for all events.
	  Events.off =  function(name, callback, context) {
	    if (!this._events) return this;
	    this._events = eventsApi(offApi, this._events, name, callback, {
	        context: context,
	        listeners: this._listeners
	    });
	    return this;
	  };

	  // Tell this object to stop listening to either specific events ... or
	  // to every object it's currently listening to.
	  Events.stopListening =  function(obj, name, callback) {
	    var listeningTo = this._listeningTo;
	    if (!listeningTo) return this;

	    var ids = obj ? [obj._listenId] : _.keys(listeningTo);

	    for (var i = 0; i < ids.length; i++) {
	      var listening = listeningTo[ids[i]];

	      // If listening doesn't exist, this object is not currently
	      // listening to obj. Break out early.
	      if (!listening) break;

	      listening.obj.off(name, callback, this);
	    }
	    if (_.isEmpty(listeningTo)) this._listeningTo = void 0;

	    return this;
	  };

	  // The reducing API that removes a callback from the `events` object.
	  var offApi = function(events, name, callback, options) {
	    if (!events) return;

	    var i = 0, listening;
	    var context = options.context, listeners = options.listeners;

	    // Delete all events listeners and "drop" events.
	    if (!name && !callback && !context) {
	      var ids = _.keys(listeners);
	      for (; i < ids.length; i++) {
	        listening = listeners[ids[i]];
	        delete listeners[listening.id];
	        delete listening.listeningTo[listening.objId];
	      }
	      return;
	    }

	    var names = name ? [name] : _.keys(events);
	    for (; i < names.length; i++) {
	      name = names[i];
	      var handlers = events[name];

	      // Bail out if there are no events stored.
	      if (!handlers) break;

	      // Replace events if there are any remaining.  Otherwise, clean up.
	      var remaining = [];
	      for (var j = 0; j < handlers.length; j++) {
	        var handler = handlers[j];
	        if (
	          callback && callback !== handler.callback &&
	            callback !== handler.callback._callback ||
	              context && context !== handler.context
	        ) {
	          remaining.push(handler);
	        } else {
	          listening = handler.listening;
	          if (listening && --listening.count === 0) {
	            delete listeners[listening.id];
	            delete listening.listeningTo[listening.objId];
	          }
	        }
	      }

	      // Update tail event if the list has any events.  Otherwise, clean up.
	      if (remaining.length) {
	        events[name] = remaining;
	      } else {
	        delete events[name];
	      }
	    }
	    if (_.size(events)) return events;
	  };

	  // Bind an event to only be triggered a single time. After the first time
	  // the callback is invoked, its listener will be removed. If multiple events
	  // are passed in using the space-separated syntax, the handler will fire
	  // once for each event, not once for a combination of all events.
	  Events.once =  function(name, callback, context) {
	    // Map the event into a `{event: once}` object.
	    var events = eventsApi(onceMap, {}, name, callback, _.bind(this.off, this));
	    return this.on(events, void 0, context);
	  };

	  // Inversion-of-control versions of `once`.
	  Events.listenToOnce =  function(obj, name, callback) {
	    // Map the event into a `{event: once}` object.
	    var events = eventsApi(onceMap, {}, name, callback, _.bind(this.stopListening, this, obj));
	    return this.listenTo(obj, events);
	  };

	  // Reduces the event callbacks into a map of `{event: onceWrapper}`.
	  // `offer` unbinds the `onceWrapper` after it has been called.
	  var onceMap = function(map, name, callback, offer) {
	    if (callback) {
	      var once = map[name] = _.once(function() {
	        offer(name, once);
	        callback.apply(this, arguments);
	      });
	      once._callback = callback;
	    }
	    return map;
	  };

	  // Trigger one or many events, firing all bound callbacks. Callbacks are
	  // passed the same arguments as `trigger` is, apart from the event name
	  // (unless you're listening on `"all"`, which will cause your callback to
	  // receive the true name of the event as the first argument).
	  Events.trigger =  function(name) {
	    if (!this._events) return this;

	    var length = Math.max(0, arguments.length - 1);
	    var args = Array(length);
	    for (var i = 0; i < length; i++) args[i] = arguments[i + 1];

	    eventsApi(triggerApi, this._events, name, void 0, args);
	    return this;
	  };

	  // Handles triggering the appropriate event callbacks.
	  var triggerApi = function(objEvents, name, cb, args) {
	    if (objEvents) {
	      var events = objEvents[name];
	      var allEvents = objEvents.all;
	      if (events && allEvents) allEvents = allEvents.slice();
	      if (events) triggerEvents(events, args);
	      if (allEvents) triggerEvents(allEvents, [name].concat(args));
	    }
	    return objEvents;
	  };

	  // A difficult-to-believe, but optimized internal dispatch function for
	  // triggering events. Tries to keep the usual cases speedy (most internal
	  // Backbone events have 3 arguments).
	  var triggerEvents = function(events, args) {
	    var ev, i = -1, l = events.length, a1 = args[0], a2 = args[1], a3 = args[2];
	    switch (args.length) {
	      case 0: while (++i < l) (ev = events[i]).callback.call(ev.ctx); return;
	      case 1: while (++i < l) (ev = events[i]).callback.call(ev.ctx, a1); return;
	      case 2: while (++i < l) (ev = events[i]).callback.call(ev.ctx, a1, a2); return;
	      case 3: while (++i < l) (ev = events[i]).callback.call(ev.ctx, a1, a2, a3); return;
	      default: while (++i < l) (ev = events[i]).callback.apply(ev.ctx, args); return;
	    }
	  };

	  // Aliases for backwards compatibility.
	  Events.bind   = Events.on;
	  Events.unbind = Events.off;

	  // Allow the `Backbone` object to serve as a global event bus, for folks who
	  // want global "pubsub" in a convenient place.
	  _.extend(Backbone, Events);

	  // Backbone.Model
	  // --------------

	  // Backbone **Models** are the basic data object in the framework --
	  // frequently representing a row in a table in a database on your server.
	  // A discrete chunk of data and a bunch of useful, related methods for
	  // performing computations and transformations on that data.

	  // Create a new model with the specified attributes. A client id (`cid`)
	  // is automatically generated and assigned for you.
	  var Model = Backbone.Model = function(attributes, options) {
	    var attrs = attributes || {};
	    options || (options = {});
	    this.cid = _.uniqueId(this.cidPrefix);
	    this.attributes = {};
	    if (options.collection) this.collection = options.collection;
	    if (options.parse) attrs = this.parse(attrs, options) || {};
	    attrs = _.defaults({}, attrs, _.result(this, 'defaults'));
	    this.set(attrs, options);
	    this.changed = {};
	    this.initialize.apply(this, arguments);
	  };

	  // Attach all inheritable methods to the Model prototype.
	  _.extend(Model.prototype, Events, {

	    // A hash of attributes whose current and previous value differ.
	    changed: null,

	    // The value returned during the last failed validation.
	    validationError: null,

	    // The default name for the JSON `id` attribute is `"id"`. MongoDB and
	    // CouchDB users may want to set this to `"_id"`.
	    idAttribute: 'id',

	    // The prefix is used to create the client id which is used to identify models locally.
	    // You may want to override this if you're experiencing name clashes with model ids.
	    cidPrefix: 'c',

	    // Initialize is an empty function by default. Override it with your own
	    // initialization logic.
	    initialize: function(){},

	    // Return a copy of the model's `attributes` object.
	    toJSON: function(options) {
	      return _.clone(this.attributes);
	    },

	    // Proxy `Backbone.sync` by default -- but override this if you need
	    // custom syncing semantics for *this* particular model.
	    sync: function() {
	      return Backbone.sync.apply(this, arguments);
	    },

	    // Get the value of an attribute.
	    get: function(attr) {
	      return this.attributes[attr];
	    },

	    // Get the HTML-escaped value of an attribute.
	    escape: function(attr) {
	      return _.escape(this.get(attr));
	    },

	    // Returns `true` if the attribute contains a value that is not null
	    // or undefined.
	    has: function(attr) {
	      return this.get(attr) != null;
	    },

	    // Special-cased proxy to underscore's `_.matches` method.
	    matches: function(attrs) {
	      return !!_.iteratee(attrs, this)(this.attributes);
	    },

	    // Set a hash of model attributes on the object, firing `"change"`. This is
	    // the core primitive operation of a model, updating the data and notifying
	    // anyone who needs to know about the change in state. The heart of the beast.
	    set: function(key, val, options) {
	      if (key == null) return this;

	      // Handle both `"key", value` and `{key: value}` -style arguments.
	      var attrs;
	      if (typeof key === 'object') {
	        attrs = key;
	        options = val;
	      } else {
	        (attrs = {})[key] = val;
	      }

	      options || (options = {});

	      // Run validation.
	      if (!this._validate(attrs, options)) return false;

	      // Extract attributes and options.
	      var unset      = options.unset;
	      var silent     = options.silent;
	      var changes    = [];
	      var changing   = this._changing;
	      this._changing = true;

	      if (!changing) {
	        this._previousAttributes = _.clone(this.attributes);
	        this.changed = {};
	      }

	      var current = this.attributes;
	      var changed = this.changed;
	      var prev    = this._previousAttributes;

	      // For each `set` attribute, update or delete the current value.
	      for (var attr in attrs) {
	        val = attrs[attr];
	        if (!_.isEqual(current[attr], val)) changes.push(attr);
	        if (!_.isEqual(prev[attr], val)) {
	          changed[attr] = val;
	        } else {
	          delete changed[attr];
	        }
	        unset ? delete current[attr] : current[attr] = val;
	      }

	      // Update the `id`.
	      this.id = this.get(this.idAttribute);

	      // Trigger all relevant attribute changes.
	      if (!silent) {
	        if (changes.length) this._pending = options;
	        for (var i = 0; i < changes.length; i++) {
	          this.trigger('change:' + changes[i], this, current[changes[i]], options);
	        }
	      }

	      // You might be wondering why there's a `while` loop here. Changes can
	      // be recursively nested within `"change"` events.
	      if (changing) return this;
	      if (!silent) {
	        while (this._pending) {
	          options = this._pending;
	          this._pending = false;
	          this.trigger('change', this, options);
	        }
	      }
	      this._pending = false;
	      this._changing = false;
	      return this;
	    },

	    // Remove an attribute from the model, firing `"change"`. `unset` is a noop
	    // if the attribute doesn't exist.
	    unset: function(attr, options) {
	      return this.set(attr, void 0, _.extend({}, options, {unset: true}));
	    },

	    // Clear all attributes on the model, firing `"change"`.
	    clear: function(options) {
	      var attrs = {};
	      for (var key in this.attributes) attrs[key] = void 0;
	      return this.set(attrs, _.extend({}, options, {unset: true}));
	    },

	    // Determine if the model has changed since the last `"change"` event.
	    // If you specify an attribute name, determine if that attribute has changed.
	    hasChanged: function(attr) {
	      if (attr == null) return !_.isEmpty(this.changed);
	      return _.has(this.changed, attr);
	    },

	    // Return an object containing all the attributes that have changed, or
	    // false if there are no changed attributes. Useful for determining what
	    // parts of a view need to be updated and/or what attributes need to be
	    // persisted to the server. Unset attributes will be set to undefined.
	    // You can also pass an attributes object to diff against the model,
	    // determining if there *would be* a change.
	    changedAttributes: function(diff) {
	      if (!diff) return this.hasChanged() ? _.clone(this.changed) : false;
	      var old = this._changing ? this._previousAttributes : this.attributes;
	      var changed = {};
	      for (var attr in diff) {
	        var val = diff[attr];
	        if (_.isEqual(old[attr], val)) continue;
	        changed[attr] = val;
	      }
	      return _.size(changed) ? changed : false;
	    },

	    // Get the previous value of an attribute, recorded at the time the last
	    // `"change"` event was fired.
	    previous: function(attr) {
	      if (attr == null || !this._previousAttributes) return null;
	      return this._previousAttributes[attr];
	    },

	    // Get all of the attributes of the model at the time of the previous
	    // `"change"` event.
	    previousAttributes: function() {
	      return _.clone(this._previousAttributes);
	    },

	    // Fetch the model from the server, merging the response with the model's
	    // local attributes. Any changed attributes will trigger a "change" event.
	    fetch: function(options) {
	      options = _.extend({parse: true}, options);
	      var model = this;
	      var success = options.success;
	      options.success = function(resp) {
	        var serverAttrs = options.parse ? model.parse(resp, options) : resp;
	        if (!model.set(serverAttrs, options)) return false;
	        if (success) success.call(options.context, model, resp, options);
	        model.trigger('sync', model, resp, options);
	      };
	      wrapError(this, options);
	      return this.sync('read', this, options);
	    },

	    // Set a hash of model attributes, and sync the model to the server.
	    // If the server returns an attributes hash that differs, the model's
	    // state will be `set` again.
	    save: function(key, val, options) {
	      // Handle both `"key", value` and `{key: value}` -style arguments.
	      var attrs;
	      if (key == null || typeof key === 'object') {
	        attrs = key;
	        options = val;
	      } else {
	        (attrs = {})[key] = val;
	      }

	      options = _.extend({validate: true, parse: true}, options);
	      var wait = options.wait;

	      // If we're not waiting and attributes exist, save acts as
	      // `set(attr).save(null, opts)` with validation. Otherwise, check if
	      // the model will be valid when the attributes, if any, are set.
	      if (attrs && !wait) {
	        if (!this.set(attrs, options)) return false;
	      } else {
	        if (!this._validate(attrs, options)) return false;
	      }

	      // After a successful server-side save, the client is (optionally)
	      // updated with the server-side state.
	      var model = this;
	      var success = options.success;
	      var attributes = this.attributes;
	      options.success = function(resp) {
	        // Ensure attributes are restored during synchronous saves.
	        model.attributes = attributes;
	        var serverAttrs = options.parse ? model.parse(resp, options) : resp;
	        if (wait) serverAttrs = _.extend({}, attrs, serverAttrs);
	        if (serverAttrs && !model.set(serverAttrs, options)) return false;
	        if (success) success.call(options.context, model, resp, options);
	        model.trigger('sync', model, resp, options);
	      };
	      wrapError(this, options);

	      // Set temporary attributes if `{wait: true}` to properly find new ids.
	      if (attrs && wait) this.attributes = _.extend({}, attributes, attrs);

	      var method = this.isNew() ? 'create' : (options.patch ? 'patch' : 'update');
	      if (method === 'patch' && !options.attrs) options.attrs = attrs;
	      var xhr = this.sync(method, this, options);

	      // Restore attributes.
	      this.attributes = attributes;

	      return xhr;
	    },

	    // Destroy this model on the server if it was already persisted.
	    // Optimistically removes the model from its collection, if it has one.
	    // If `wait: true` is passed, waits for the server to respond before removal.
	    destroy: function(options) {
	      options = options ? _.clone(options) : {};
	      var model = this;
	      var success = options.success;
	      var wait = options.wait;

	      var destroy = function() {
	        model.stopListening();
	        model.trigger('destroy', model, model.collection, options);
	      };

	      options.success = function(resp) {
	        if (wait) destroy();
	        if (success) success.call(options.context, model, resp, options);
	        if (!model.isNew()) model.trigger('sync', model, resp, options);
	      };

	      var xhr = false;
	      if (this.isNew()) {
	        _.defer(options.success);
	      } else {
	        wrapError(this, options);
	        xhr = this.sync('delete', this, options);
	      }
	      if (!wait) destroy();
	      return xhr;
	    },

	    // Default URL for the model's representation on the server -- if you're
	    // using Backbone's restful methods, override this to change the endpoint
	    // that will be called.
	    url: function() {
	      var base =
	        _.result(this, 'urlRoot') ||
	        _.result(this.collection, 'url') ||
	        urlError();
	      if (this.isNew()) return base;
	      var id = this.get(this.idAttribute);
	      return base.replace(/[^\/]$/, '$&/') + encodeURIComponent(id);
	    },

	    // **parse** converts a response into the hash of attributes to be `set` on
	    // the model. The default implementation is just to pass the response along.
	    parse: function(resp, options) {
	      return resp;
	    },

	    // Create a new model with identical attributes to this one.
	    clone: function() {
	      return new this.constructor(this.attributes);
	    },

	    // A model is new if it has never been saved to the server, and lacks an id.
	    isNew: function() {
	      return !this.has(this.idAttribute);
	    },

	    // Check if the model is currently in a valid state.
	    isValid: function(options) {
	      return this._validate({}, _.defaults({validate: true}, options));
	    },

	    // Run validation against the next complete set of model attributes,
	    // returning `true` if all is well. Otherwise, fire an `"invalid"` event.
	    _validate: function(attrs, options) {
	      if (!options.validate || !this.validate) return true;
	      attrs = _.extend({}, this.attributes, attrs);
	      var error = this.validationError = this.validate(attrs, options) || null;
	      if (!error) return true;
	      this.trigger('invalid', this, error, _.extend(options, {validationError: error}));
	      return false;
	    }

	  });

	  // Underscore methods that we want to implement on the Model, mapped to the
	  // number of arguments they take.
	  var modelMethods = { keys: 1, values: 1, pairs: 1, invert: 1, pick: 0,
	      omit: 0, chain: 1, isEmpty: 1 };

	  // Mix in each Underscore method as a proxy to `Model#attributes`.
	  addUnderscoreMethods(Model, modelMethods, 'attributes');

	  // Backbone.Collection
	  // -------------------

	  // If models tend to represent a single row of data, a Backbone Collection is
	  // more analogous to a table full of data ... or a small slice or page of that
	  // table, or a collection of rows that belong together for a particular reason
	  // -- all of the messages in this particular folder, all of the documents
	  // belonging to this particular author, and so on. Collections maintain
	  // indexes of their models, both in order, and for lookup by `id`.

	  // Create a new **Collection**, perhaps to contain a specific type of `model`.
	  // If a `comparator` is specified, the Collection will maintain
	  // its models in sort order, as they're added and removed.
	  var Collection = Backbone.Collection = function(models, options) {
	    options || (options = {});
	    if (options.model) this.model = options.model;
	    if (options.comparator !== void 0) this.comparator = options.comparator;
	    this._reset();
	    this.initialize.apply(this, arguments);
	    if (models) this.reset(models, _.extend({silent: true}, options));
	  };

	  // Default options for `Collection#set`.
	  var setOptions = {add: true, remove: true, merge: true};
	  var addOptions = {add: true, remove: false};

	  // Splices `insert` into `array` at index `at`.
	  var splice = function(array, insert, at) {
	    at = Math.min(Math.max(at, 0), array.length);
	    var tail = Array(array.length - at);
	    var length = insert.length;
	    for (var i = 0; i < tail.length; i++) tail[i] = array[i + at];
	    for (i = 0; i < length; i++) array[i + at] = insert[i];
	    for (i = 0; i < tail.length; i++) array[i + length + at] = tail[i];
	  };

	  // Define the Collection's inheritable methods.
	  _.extend(Collection.prototype, Events, {

	    // The default model for a collection is just a **Backbone.Model**.
	    // This should be overridden in most cases.
	    model: Model,

	    // Initialize is an empty function by default. Override it with your own
	    // initialization logic.
	    initialize: function(){},

	    // The JSON representation of a Collection is an array of the
	    // models' attributes.
	    toJSON: function(options) {
	      return this.map(function(model) { return model.toJSON(options); });
	    },

	    // Proxy `Backbone.sync` by default.
	    sync: function() {
	      return Backbone.sync.apply(this, arguments);
	    },

	    // Add a model, or list of models to the set. `models` may be Backbone
	    // Models or raw JavaScript objects to be converted to Models, or any
	    // combination of the two.
	    add: function(models, options) {
	      return this.set(models, _.extend({merge: false}, options, addOptions));
	    },

	    // Remove a model, or a list of models from the set.
	    remove: function(models, options) {
	      options = _.extend({}, options);
	      var singular = !_.isArray(models);
	      models = singular ? [models] : _.clone(models);
	      var removed = this._removeModels(models, options);
	      if (!options.silent && removed) this.trigger('update', this, options);
	      return singular ? removed[0] : removed;
	    },

	    // Update a collection by `set`-ing a new list of models, adding new ones,
	    // removing models that are no longer present, and merging models that
	    // already exist in the collection, as necessary. Similar to **Model#set**,
	    // the core operation for updating the data contained by the collection.
	    set: function(models, options) {
	      if (models == null) return;

	      options = _.defaults({}, options, setOptions);
	      if (options.parse && !this._isModel(models)) models = this.parse(models, options);

	      var singular = !_.isArray(models);
	      models = singular ? [models] : models.slice();

	      var at = options.at;
	      if (at != null) at = +at;
	      if (at < 0) at += this.length + 1;

	      var set = [];
	      var toAdd = [];
	      var toRemove = [];
	      var modelMap = {};

	      var add = options.add;
	      var merge = options.merge;
	      var remove = options.remove;

	      var sort = false;
	      var sortable = this.comparator && (at == null) && options.sort !== false;
	      var sortAttr = _.isString(this.comparator) ? this.comparator : null;

	      // Turn bare objects into model references, and prevent invalid models
	      // from being added.
	      var model;
	      for (var i = 0; i < models.length; i++) {
	        model = models[i];

	        // If a duplicate is found, prevent it from being added and
	        // optionally merge it into the existing model.
	        var existing = this.get(model);
	        if (existing) {
	          if (merge && model !== existing) {
	            var attrs = this._isModel(model) ? model.attributes : model;
	            if (options.parse) attrs = existing.parse(attrs, options);
	            existing.set(attrs, options);
	            if (sortable && !sort) sort = existing.hasChanged(sortAttr);
	          }
	          if (!modelMap[existing.cid]) {
	            modelMap[existing.cid] = true;
	            set.push(existing);
	          }
	          models[i] = existing;

	        // If this is a new, valid model, push it to the `toAdd` list.
	        } else if (add) {
	          model = models[i] = this._prepareModel(model, options);
	          if (model) {
	            toAdd.push(model);
	            this._addReference(model, options);
	            modelMap[model.cid] = true;
	            set.push(model);
	          }
	        }
	      }

	      // Remove stale models.
	      if (remove) {
	        for (i = 0; i < this.length; i++) {
	          model = this.models[i];
	          if (!modelMap[model.cid]) toRemove.push(model);
	        }
	        if (toRemove.length) this._removeModels(toRemove, options);
	      }

	      // See if sorting is needed, update `length` and splice in new models.
	      var orderChanged = false;
	      var replace = !sortable && add && remove;
	      if (set.length && replace) {
	        orderChanged = this.length != set.length || _.some(this.models, function(model, index) {
	          return model !== set[index];
	        });
	        this.models.length = 0;
	        splice(this.models, set, 0);
	        this.length = this.models.length;
	      } else if (toAdd.length) {
	        if (sortable) sort = true;
	        splice(this.models, toAdd, at == null ? this.length : at);
	        this.length = this.models.length;
	      }

	      // Silently sort the collection if appropriate.
	      if (sort) this.sort({silent: true});

	      // Unless silenced, it's time to fire all appropriate add/sort events.
	      if (!options.silent) {
	        for (i = 0; i < toAdd.length; i++) {
	          if (at != null) options.index = at + i;
	          model = toAdd[i];
	          model.trigger('add', model, this, options);
	        }
	        if (sort || orderChanged) this.trigger('sort', this, options);
	        if (toAdd.length || toRemove.length) this.trigger('update', this, options);
	      }

	      // Return the added (or merged) model (or models).
	      return singular ? models[0] : models;
	    },

	    // When you have more items than you want to add or remove individually,
	    // you can reset the entire set with a new list of models, without firing
	    // any granular `add` or `remove` events. Fires `reset` when finished.
	    // Useful for bulk operations and optimizations.
	    reset: function(models, options) {
	      options = options ? _.clone(options) : {};
	      for (var i = 0; i < this.models.length; i++) {
	        this._removeReference(this.models[i], options);
	      }
	      options.previousModels = this.models;
	      this._reset();
	      models = this.add(models, _.extend({silent: true}, options));
	      if (!options.silent) this.trigger('reset', this, options);
	      return models;
	    },

	    // Add a model to the end of the collection.
	    push: function(model, options) {
	      return this.add(model, _.extend({at: this.length}, options));
	    },

	    // Remove a model from the end of the collection.
	    pop: function(options) {
	      var model = this.at(this.length - 1);
	      return this.remove(model, options);
	    },

	    // Add a model to the beginning of the collection.
	    unshift: function(model, options) {
	      return this.add(model, _.extend({at: 0}, options));
	    },

	    // Remove a model from the beginning of the collection.
	    shift: function(options) {
	      var model = this.at(0);
	      return this.remove(model, options);
	    },

	    // Slice out a sub-array of models from the collection.
	    slice: function() {
	      return slice.apply(this.models, arguments);
	    },

	    // Get a model from the set by id.
	    get: function(obj) {
	      if (obj == null) return void 0;
	      var id = this.modelId(this._isModel(obj) ? obj.attributes : obj);
	      return this._byId[obj] || this._byId[id] || this._byId[obj.cid];
	    },

	    // Get the model at the given index.
	    at: function(index) {
	      if (index < 0) index += this.length;
	      return this.models[index];
	    },

	    // Return models with matching attributes. Useful for simple cases of
	    // `filter`.
	    where: function(attrs, first) {
	      return this[first ? 'find' : 'filter'](attrs);
	    },

	    // Return the first model with matching attributes. Useful for simple cases
	    // of `find`.
	    findWhere: function(attrs) {
	      return this.where(attrs, true);
	    },

	    // Force the collection to re-sort itself. You don't need to call this under
	    // normal circumstances, as the set will maintain sort order as each item
	    // is added.
	    sort: function(options) {
	      var comparator = this.comparator;
	      if (!comparator) throw new Error('Cannot sort a set without a comparator');
	      options || (options = {});

	      var length = comparator.length;
	      if (_.isFunction(comparator)) comparator = _.bind(comparator, this);

	      // Run sort based on type of `comparator`.
	      if (length === 1 || _.isString(comparator)) {
	        this.models = this.sortBy(comparator);
	      } else {
	        this.models.sort(comparator);
	      }
	      if (!options.silent) this.trigger('sort', this, options);
	      return this;
	    },

	    // Pluck an attribute from each model in the collection.
	    pluck: function(attr) {
	      return _.invoke(this.models, 'get', attr);
	    },

	    // Fetch the default set of models for this collection, resetting the
	    // collection when they arrive. If `reset: true` is passed, the response
	    // data will be passed through the `reset` method instead of `set`.
	    fetch: function(options) {
	      options = _.extend({parse: true}, options);
	      var success = options.success;
	      var collection = this;
	      options.success = function(resp) {
	        var method = options.reset ? 'reset' : 'set';
	        collection[method](resp, options);
	        if (success) success.call(options.context, collection, resp, options);
	        collection.trigger('sync', collection, resp, options);
	      };
	      wrapError(this, options);
	      return this.sync('read', this, options);
	    },

	    // Create a new instance of a model in this collection. Add the model to the
	    // collection immediately, unless `wait: true` is passed, in which case we
	    // wait for the server to agree.
	    create: function(model, options) {
	      options = options ? _.clone(options) : {};
	      var wait = options.wait;
	      model = this._prepareModel(model, options);
	      if (!model) return false;
	      if (!wait) this.add(model, options);
	      var collection = this;
	      var success = options.success;
	      options.success = function(model, resp, callbackOpts) {
	        if (wait) collection.add(model, callbackOpts);
	        if (success) success.call(callbackOpts.context, model, resp, callbackOpts);
	      };
	      model.save(null, options);
	      return model;
	    },

	    // **parse** converts a response into a list of models to be added to the
	    // collection. The default implementation is just to pass it through.
	    parse: function(resp, options) {
	      return resp;
	    },

	    // Create a new collection with an identical list of models as this one.
	    clone: function() {
	      return new this.constructor(this.models, {
	        model: this.model,
	        comparator: this.comparator
	      });
	    },

	    // Define how to uniquely identify models in the collection.
	    modelId: function (attrs) {
	      return attrs[this.model.prototype.idAttribute || 'id'];
	    },

	    // Private method to reset all internal state. Called when the collection
	    // is first initialized or reset.
	    _reset: function() {
	      this.length = 0;
	      this.models = [];
	      this._byId  = {};
	    },

	    // Prepare a hash of attributes (or other model) to be added to this
	    // collection.
	    _prepareModel: function(attrs, options) {
	      if (this._isModel(attrs)) {
	        if (!attrs.collection) attrs.collection = this;
	        return attrs;
	      }
	      options = options ? _.clone(options) : {};
	      options.collection = this;
	      var model = new this.model(attrs, options);
	      if (!model.validationError) return model;
	      this.trigger('invalid', this, model.validationError, options);
	      return false;
	    },

	    // Internal method called by both remove and set.
	    _removeModels: function(models, options) {
	      var removed = [];
	      for (var i = 0; i < models.length; i++) {
	        var model = this.get(models[i]);
	        if (!model) continue;

	        var index = this.indexOf(model);
	        this.models.splice(index, 1);
	        this.length--;

	        if (!options.silent) {
	          options.index = index;
	          model.trigger('remove', model, this, options);
	        }

	        removed.push(model);
	        this._removeReference(model, options);
	      }
	      return removed.length ? removed : false;
	    },

	    // Method for checking whether an object should be considered a model for
	    // the purposes of adding to the collection.
	    _isModel: function (model) {
	      return model instanceof Model;
	    },

	    // Internal method to create a model's ties to a collection.
	    _addReference: function(model, options) {
	      this._byId[model.cid] = model;
	      var id = this.modelId(model.attributes);
	      if (id != null) this._byId[id] = model;
	      model.on('all', this._onModelEvent, this);
	    },

	    // Internal method to sever a model's ties to a collection.
	    _removeReference: function(model, options) {
	      delete this._byId[model.cid];
	      var id = this.modelId(model.attributes);
	      if (id != null) delete this._byId[id];
	      if (this === model.collection) delete model.collection;
	      model.off('all', this._onModelEvent, this);
	    },

	    // Internal method called every time a model in the set fires an event.
	    // Sets need to update their indexes when models change ids. All other
	    // events simply proxy through. "add" and "remove" events that originate
	    // in other collections are ignored.
	    _onModelEvent: function(event, model, collection, options) {
	      if ((event === 'add' || event === 'remove') && collection !== this) return;
	      if (event === 'destroy') this.remove(model, options);
	      if (event === 'change') {
	        var prevId = this.modelId(model.previousAttributes());
	        var id = this.modelId(model.attributes);
	        if (prevId !== id) {
	          if (prevId != null) delete this._byId[prevId];
	          if (id != null) this._byId[id] = model;
	        }
	      }
	      this.trigger.apply(this, arguments);
	    }

	  });

	  // Underscore methods that we want to implement on the Collection.
	  // 90% of the core usefulness of Backbone Collections is actually implemented
	  // right here:
	  var collectionMethods = { forEach: 3, each: 3, map: 3, collect: 3, reduce: 4,
	      foldl: 4, inject: 4, reduceRight: 4, foldr: 4, find: 3, detect: 3, filter: 3,
	      select: 3, reject: 3, every: 3, all: 3, some: 3, any: 3, include: 3, includes: 3,
	      contains: 3, invoke: 0, max: 3, min: 3, toArray: 1, size: 1, first: 3,
	      head: 3, take: 3, initial: 3, rest: 3, tail: 3, drop: 3, last: 3,
	      without: 0, difference: 0, indexOf: 3, shuffle: 1, lastIndexOf: 3,
	      isEmpty: 1, chain: 1, sample: 3, partition: 3, groupBy: 3, countBy: 3,
	      sortBy: 3, indexBy: 3};

	  // Mix in each Underscore method as a proxy to `Collection#models`.
	  addUnderscoreMethods(Collection, collectionMethods, 'models');

	  // Backbone.View
	  // -------------

	  // Backbone Views are almost more convention than they are actual code. A View
	  // is simply a JavaScript object that represents a logical chunk of UI in the
	  // DOM. This might be a single item, an entire list, a sidebar or panel, or
	  // even the surrounding frame which wraps your whole app. Defining a chunk of
	  // UI as a **View** allows you to define your DOM events declaratively, without
	  // having to worry about render order ... and makes it easy for the view to
	  // react to specific changes in the state of your models.

	  // Creating a Backbone.View creates its initial element outside of the DOM,
	  // if an existing element is not provided...
	  var View = Backbone.View = function(options) {
	    this.cid = _.uniqueId('view');
	    _.extend(this, _.pick(options, viewOptions));
	    this._ensureElement();
	    this.initialize.apply(this, arguments);
	  };

	  // Cached regex to split keys for `delegate`.
	  var delegateEventSplitter = /^(\S+)\s*(.*)$/;

	  // List of view options to be set as properties.
	  var viewOptions = ['model', 'collection', 'el', 'id', 'attributes', 'className', 'tagName', 'events'];

	  // Set up all inheritable **Backbone.View** properties and methods.
	  _.extend(View.prototype, Events, {

	    // The default `tagName` of a View's element is `"div"`.
	    tagName: 'div',

	    // jQuery delegate for element lookup, scoped to DOM elements within the
	    // current view. This should be preferred to global lookups where possible.
	    $: function(selector) {
	      return this.$el.find(selector);
	    },

	    // Initialize is an empty function by default. Override it with your own
	    // initialization logic.
	    initialize: function(){},

	    // **render** is the core function that your view should override, in order
	    // to populate its element (`this.el`), with the appropriate HTML. The
	    // convention is for **render** to always return `this`.
	    render: function() {
	      return this;
	    },

	    // Remove this view by taking the element out of the DOM, and removing any
	    // applicable Backbone.Events listeners.
	    remove: function() {
	      this._removeElement();
	      this.stopListening();
	      return this;
	    },

	    // Remove this view's element from the document and all event listeners
	    // attached to it. Exposed for subclasses using an alternative DOM
	    // manipulation API.
	    _removeElement: function() {
	      this.$el.remove();
	    },

	    // Change the view's element (`this.el` property) and re-delegate the
	    // view's events on the new element.
	    setElement: function(element) {
	      this.undelegateEvents();
	      this._setElement(element);
	      this.delegateEvents();
	      return this;
	    },

	    // Creates the `this.el` and `this.$el` references for this view using the
	    // given `el`. `el` can be a CSS selector or an HTML string, a jQuery
	    // context or an element. Subclasses can override this to utilize an
	    // alternative DOM manipulation API and are only required to set the
	    // `this.el` property.
	    _setElement: function(el) {
	      this.$el = el instanceof Backbone.$ ? el : Backbone.$(el);
	      this.el = this.$el[0];
	    },

	    // Set callbacks, where `this.events` is a hash of
	    //
	    // *{"event selector": "callback"}*
	    //
	    //     {
	    //       'mousedown .title':  'edit',
	    //       'click .button':     'save',
	    //       'click .open':       function(e) { ... }
	    //     }
	    //
	    // pairs. Callbacks will be bound to the view, with `this` set properly.
	    // Uses event delegation for efficiency.
	    // Omitting the selector binds the event to `this.el`.
	    delegateEvents: function(events) {
	      events || (events = _.result(this, 'events'));
	      if (!events) return this;
	      this.undelegateEvents();
	      for (var key in events) {
	        var method = events[key];
	        if (!_.isFunction(method)) method = this[method];
	        if (!method) continue;
	        var match = key.match(delegateEventSplitter);
	        this.delegate(match[1], match[2], _.bind(method, this));
	      }
	      return this;
	    },

	    // Add a single event listener to the view's element (or a child element
	    // using `selector`). This only works for delegate-able events: not `focus`,
	    // `blur`, and not `change`, `submit`, and `reset` in Internet Explorer.
	    delegate: function(eventName, selector, listener) {
	      this.$el.on(eventName + '.delegateEvents' + this.cid, selector, listener);
	      return this;
	    },

	    // Clears all callbacks previously bound to the view by `delegateEvents`.
	    // You usually don't need to use this, but may wish to if you have multiple
	    // Backbone views attached to the same DOM element.
	    undelegateEvents: function() {
	      if (this.$el) this.$el.off('.delegateEvents' + this.cid);
	      return this;
	    },

	    // A finer-grained `undelegateEvents` for removing a single delegated event.
	    // `selector` and `listener` are both optional.
	    undelegate: function(eventName, selector, listener) {
	      this.$el.off(eventName + '.delegateEvents' + this.cid, selector, listener);
	      return this;
	    },

	    // Produces a DOM element to be assigned to your view. Exposed for
	    // subclasses using an alternative DOM manipulation API.
	    _createElement: function(tagName) {
	      return document.createElement(tagName);
	    },

	    // Ensure that the View has a DOM element to render into.
	    // If `this.el` is a string, pass it through `$()`, take the first
	    // matching element, and re-assign it to `el`. Otherwise, create
	    // an element from the `id`, `className` and `tagName` properties.
	    _ensureElement: function() {
	      if (!this.el) {
	        var attrs = _.extend({}, _.result(this, 'attributes'));
	        if (this.id) attrs.id = _.result(this, 'id');
	        if (this.className) attrs['class'] = _.result(this, 'className');
	        this.setElement(this._createElement(_.result(this, 'tagName')));
	        this._setAttributes(attrs);
	      } else {
	        this.setElement(_.result(this, 'el'));
	      }
	    },

	    // Set attributes from a hash on this view's element.  Exposed for
	    // subclasses using an alternative DOM manipulation API.
	    _setAttributes: function(attributes) {
	      this.$el.attr(attributes);
	    }

	  });

	  // Backbone.sync
	  // -------------

	  // Override this function to change the manner in which Backbone persists
	  // models to the server. You will be passed the type of request, and the
	  // model in question. By default, makes a RESTful Ajax request
	  // to the model's `url()`. Some possible customizations could be:
	  //
	  // * Use `setTimeout` to batch rapid-fire updates into a single request.
	  // * Send up the models as XML instead of JSON.
	  // * Persist models via WebSockets instead of Ajax.
	  //
	  // Turn on `Backbone.emulateHTTP` in order to send `PUT` and `DELETE` requests
	  // as `POST`, with a `_method` parameter containing the true HTTP method,
	  // as well as all requests with the body as `application/x-www-form-urlencoded`
	  // instead of `application/json` with the model in a param named `model`.
	  // Useful when interfacing with server-side languages like **PHP** that make
	  // it difficult to read the body of `PUT` requests.
	  Backbone.sync = function(method, model, options) {
	    var type = methodMap[method];

	    // Default options, unless specified.
	    _.defaults(options || (options = {}), {
	      emulateHTTP: Backbone.emulateHTTP,
	      emulateJSON: Backbone.emulateJSON
	    });

	    // Default JSON-request options.
	    var params = {type: type, dataType: 'json'};

	    // Ensure that we have a URL.
	    if (!options.url) {
	      params.url = _.result(model, 'url') || urlError();
	    }

	    // Ensure that we have the appropriate request data.
	    if (options.data == null && model && (method === 'create' || method === 'update' || method === 'patch')) {
	      params.contentType = 'application/json';
	      params.data = JSON.stringify(options.attrs || model.toJSON(options));
	    }

	    // For older servers, emulate JSON by encoding the request into an HTML-form.
	    if (options.emulateJSON) {
	      params.contentType = 'application/x-www-form-urlencoded';
	      params.data = params.data ? {model: params.data} : {};
	    }

	    // For older servers, emulate HTTP by mimicking the HTTP method with `_method`
	    // And an `X-HTTP-Method-Override` header.
	    if (options.emulateHTTP && (type === 'PUT' || type === 'DELETE' || type === 'PATCH')) {
	      params.type = 'POST';
	      if (options.emulateJSON) params.data._method = type;
	      var beforeSend = options.beforeSend;
	      options.beforeSend = function(xhr) {
	        xhr.setRequestHeader('X-HTTP-Method-Override', type);
	        if (beforeSend) return beforeSend.apply(this, arguments);
	      };
	    }

	    // Don't process data on a non-GET request.
	    if (params.type !== 'GET' && !options.emulateJSON) {
	      params.processData = false;
	    }

	    // Pass along `textStatus` and `errorThrown` from jQuery.
	    var error = options.error;
	    options.error = function(xhr, textStatus, errorThrown) {
	      options.textStatus = textStatus;
	      options.errorThrown = errorThrown;
	      if (error) error.call(options.context, xhr, textStatus, errorThrown);
	    };

	    // Make the request, allowing the user to override any Ajax options.
	    var xhr = options.xhr = Backbone.ajax(_.extend(params, options));
	    model.trigger('request', model, xhr, options);
	    return xhr;
	  };

	  // Map from CRUD to HTTP for our default `Backbone.sync` implementation.
	  var methodMap = {
	    'create': 'POST',
	    'update': 'PUT',
	    'patch':  'PATCH',
	    'delete': 'DELETE',
	    'read':   'GET'
	  };

	  // Set the default implementation of `Backbone.ajax` to proxy through to `$`.
	  // Override this if you'd like to use a different library.
	  Backbone.ajax = function() {
	    return Backbone.$.ajax.apply(Backbone.$, arguments);
	  };

	  // Backbone.Router
	  // ---------------

	  // Routers map faux-URLs to actions, and fire events when routes are
	  // matched. Creating a new one sets its `routes` hash, if not set statically.
	  var Router = Backbone.Router = function(options) {
	    options || (options = {});
	    if (options.routes) this.routes = options.routes;
	    this._bindRoutes();
	    this.initialize.apply(this, arguments);
	  };

	  // Cached regular expressions for matching named param parts and splatted
	  // parts of route strings.
	  var optionalParam = /\((.*?)\)/g;
	  var namedParam    = /(\(\?)?:\w+/g;
	  var splatParam    = /\*\w+/g;
	  var escapeRegExp  = /[\-{}\[\]+?.,\\\^$|#\s]/g;

	  // Set up all inheritable **Backbone.Router** properties and methods.
	  _.extend(Router.prototype, Events, {

	    // Initialize is an empty function by default. Override it with your own
	    // initialization logic.
	    initialize: function(){},

	    // Manually bind a single named route to a callback. For example:
	    //
	    //     this.route('search/:query/p:num', 'search', function(query, num) {
	    //       ...
	    //     });
	    //
	    route: function(route, name, callback) {
	      if (!_.isRegExp(route)) route = this._routeToRegExp(route);
	      if (_.isFunction(name)) {
	        callback = name;
	        name = '';
	      }
	      if (!callback) callback = this[name];
	      var router = this;
	      Backbone.history.route(route, function(fragment) {
	        var args = router._extractParameters(route, fragment);
	        if (router.execute(callback, args, name) !== false) {
	          router.trigger.apply(router, ['route:' + name].concat(args));
	          router.trigger('route', name, args);
	          Backbone.history.trigger('route', router, name, args);
	        }
	      });
	      return this;
	    },

	    // Execute a route handler with the provided parameters.  This is an
	    // excellent place to do pre-route setup or post-route cleanup.
	    execute: function(callback, args, name) {
	      if (callback) callback.apply(this, args);
	    },

	    // Simple proxy to `Backbone.history` to save a fragment into the history.
	    navigate: function(fragment, options) {
	      Backbone.history.navigate(fragment, options);
	      return this;
	    },

	    // Bind all defined routes to `Backbone.history`. We have to reverse the
	    // order of the routes here to support behavior where the most general
	    // routes can be defined at the bottom of the route map.
	    _bindRoutes: function() {
	      if (!this.routes) return;
	      this.routes = _.result(this, 'routes');
	      var route, routes = _.keys(this.routes);
	      while ((route = routes.pop()) != null) {
	        this.route(route, this.routes[route]);
	      }
	    },

	    // Convert a route string into a regular expression, suitable for matching
	    // against the current location hash.
	    _routeToRegExp: function(route) {
	      route = route.replace(escapeRegExp, '\\$&')
	                   .replace(optionalParam, '(?:$1)?')
	                   .replace(namedParam, function(match, optional) {
	                     return optional ? match : '([^/?]+)';
	                   })
	                   .replace(splatParam, '([^?]*?)');
	      return new RegExp('^' + route + '(?:\\?([\\s\\S]*))?$');
	    },

	    // Given a route, and a URL fragment that it matches, return the array of
	    // extracted decoded parameters. Empty or unmatched parameters will be
	    // treated as `null` to normalize cross-browser behavior.
	    _extractParameters: function(route, fragment) {
	      var params = route.exec(fragment).slice(1);
	      return _.map(params, function(param, i) {
	        // Don't decode the search params.
	        if (i === params.length - 1) return param || null;
	        return param ? decodeURIComponent(param) : null;
	      });
	    }

	  });

	  // Backbone.History
	  // ----------------

	  // Handles cross-browser history management, based on either
	  // [pushState](http://diveintohtml5.info/history.html) and real URLs, or
	  // [onhashchange](https://developer.mozilla.org/en-US/docs/DOM/window.onhashchange)
	  // and URL fragments. If the browser supports neither (old IE, natch),
	  // falls back to polling.
	  var History = Backbone.History = function() {
	    this.handlers = [];
	    this.checkUrl = _.bind(this.checkUrl, this);

	    // Ensure that `History` can be used outside of the browser.
	    if (typeof window !== 'undefined') {
	      this.location = window.location;
	      this.history = window.history;
	    }
	  };

	  // Cached regex for stripping a leading hash/slash and trailing space.
	  var routeStripper = /^[#\/]|\s+$/g;

	  // Cached regex for stripping leading and trailing slashes.
	  var rootStripper = /^\/+|\/+$/g;

	  // Cached regex for stripping urls of hash.
	  var pathStripper = /#.*$/;

	  // Has the history handling already been started?
	  History.started = false;

	  // Set up all inheritable **Backbone.History** properties and methods.
	  _.extend(History.prototype, Events, {

	    // The default interval to poll for hash changes, if necessary, is
	    // twenty times a second.
	    interval: 50,

	    // Are we at the app root?
	    atRoot: function() {
	      var path = this.location.pathname.replace(/[^\/]$/, '$&/');
	      return path === this.root && !this.getSearch();
	    },

	    // Does the pathname match the root?
	    matchRoot: function() {
	      var path = this.decodeFragment(this.location.pathname);
	      var root = path.slice(0, this.root.length - 1) + '/';
	      return root === this.root;
	    },

	    // Unicode characters in `location.pathname` are percent encoded so they're
	    // decoded for comparison. `%25` should not be decoded since it may be part
	    // of an encoded parameter.
	    decodeFragment: function(fragment) {
	      return decodeURI(fragment.replace(/%25/g, '%2525'));
	    },

	    // In IE6, the hash fragment and search params are incorrect if the
	    // fragment contains `?`.
	    getSearch: function() {
	      var match = this.location.href.replace(/#.*/, '').match(/\?.+/);
	      return match ? match[0] : '';
	    },

	    // Gets the true hash value. Cannot use location.hash directly due to bug
	    // in Firefox where location.hash will always be decoded.
	    getHash: function(window) {
	      var match = (window || this).location.href.match(/#(.*)$/);
	      return match ? match[1] : '';
	    },

	    // Get the pathname and search params, without the root.
	    getPath: function() {
	      var path = this.decodeFragment(
	        this.location.pathname + this.getSearch()
	      ).slice(this.root.length - 1);
	      return path.charAt(0) === '/' ? path.slice(1) : path;
	    },

	    // Get the cross-browser normalized URL fragment from the path or hash.
	    getFragment: function(fragment) {
	      if (fragment == null) {
	        if (this._usePushState || !this._wantsHashChange) {
	          fragment = this.getPath();
	        } else {
	          fragment = this.getHash();
	        }
	      }
	      return fragment.replace(routeStripper, '');
	    },

	    // Start the hash change handling, returning `true` if the current URL matches
	    // an existing route, and `false` otherwise.
	    start: function(options) {
	      if (History.started) throw new Error('Backbone.history has already been started');
	      History.started = true;

	      // Figure out the initial configuration. Do we need an iframe?
	      // Is pushState desired ... is it available?
	      this.options          = _.extend({root: '/'}, this.options, options);
	      this.root             = this.options.root;
	      this._wantsHashChange = this.options.hashChange !== false;
	      this._hasHashChange   = 'onhashchange' in window && (document.documentMode === void 0 || document.documentMode > 7);
	      this._useHashChange   = this._wantsHashChange && this._hasHashChange;
	      this._wantsPushState  = !!this.options.pushState;
	      this._hasPushState    = !!(this.history && this.history.pushState);
	      this._usePushState    = this._wantsPushState && this._hasPushState;
	      this.fragment         = this.getFragment();

	      // Normalize root to always include a leading and trailing slash.
	      this.root = ('/' + this.root + '/').replace(rootStripper, '/');

	      // Transition from hashChange to pushState or vice versa if both are
	      // requested.
	      if (this._wantsHashChange && this._wantsPushState) {

	        // If we've started off with a route from a `pushState`-enabled
	        // browser, but we're currently in a browser that doesn't support it...
	        if (!this._hasPushState && !this.atRoot()) {
	          var root = this.root.slice(0, -1) || '/';
	          this.location.replace(root + '#' + this.getPath());
	          // Return immediately as browser will do redirect to new url
	          return true;

	        // Or if we've started out with a hash-based route, but we're currently
	        // in a browser where it could be `pushState`-based instead...
	        } else if (this._hasPushState && this.atRoot()) {
	          this.navigate(this.getHash(), {replace: true});
	        }

	      }

	      // Proxy an iframe to handle location events if the browser doesn't
	      // support the `hashchange` event, HTML5 history, or the user wants
	      // `hashChange` but not `pushState`.
	      if (!this._hasHashChange && this._wantsHashChange && !this._usePushState) {
	        this.iframe = document.createElement('iframe');
	        this.iframe.src = 'javascript:0';
	        this.iframe.style.display = 'none';
	        this.iframe.tabIndex = -1;
	        var body = document.body;
	        // Using `appendChild` will throw on IE < 9 if the document is not ready.
	        var iWindow = body.insertBefore(this.iframe, body.firstChild).contentWindow;
	        iWindow.document.open();
	        iWindow.document.close();
	        iWindow.location.hash = '#' + this.fragment;
	      }

	      // Add a cross-platform `addEventListener` shim for older browsers.
	      var addEventListener = window.addEventListener || function (eventName, listener) {
	        return attachEvent('on' + eventName, listener);
	      };

	      // Depending on whether we're using pushState or hashes, and whether
	      // 'onhashchange' is supported, determine how we check the URL state.
	      if (this._usePushState) {
	        addEventListener('popstate', this.checkUrl, false);
	      } else if (this._useHashChange && !this.iframe) {
	        addEventListener('hashchange', this.checkUrl, false);
	      } else if (this._wantsHashChange) {
	        this._checkUrlInterval = setInterval(this.checkUrl, this.interval);
	      }

	      if (!this.options.silent) return this.loadUrl();
	    },

	    // Disable Backbone.history, perhaps temporarily. Not useful in a real app,
	    // but possibly useful for unit testing Routers.
	    stop: function() {
	      // Add a cross-platform `removeEventListener` shim for older browsers.
	      var removeEventListener = window.removeEventListener || function (eventName, listener) {
	        return detachEvent('on' + eventName, listener);
	      };

	      // Remove window listeners.
	      if (this._usePushState) {
	        removeEventListener('popstate', this.checkUrl, false);
	      } else if (this._useHashChange && !this.iframe) {
	        removeEventListener('hashchange', this.checkUrl, false);
	      }

	      // Clean up the iframe if necessary.
	      if (this.iframe) {
	        document.body.removeChild(this.iframe);
	        this.iframe = null;
	      }

	      // Some environments will throw when clearing an undefined interval.
	      if (this._checkUrlInterval) clearInterval(this._checkUrlInterval);
	      History.started = false;
	    },

	    // Add a route to be tested when the fragment changes. Routes added later
	    // may override previous routes.
	    route: function(route, callback) {
	      this.handlers.unshift({route: route, callback: callback});
	    },

	    // Checks the current URL to see if it has changed, and if it has,
	    // calls `loadUrl`, normalizing across the hidden iframe.
	    checkUrl: function(e) {
	      var current = this.getFragment();

	      // If the user pressed the back button, the iframe's hash will have
	      // changed and we should use that for comparison.
	      if (current === this.fragment && this.iframe) {
	        current = this.getHash(this.iframe.contentWindow);
	      }

	      if (current === this.fragment) return false;
	      if (this.iframe) this.navigate(current);
	      this.loadUrl();
	    },

	    // Attempt to load the current URL fragment. If a route succeeds with a
	    // match, returns `true`. If no defined routes matches the fragment,
	    // returns `false`.
	    loadUrl: function(fragment) {
	      // If the root doesn't match, no routes can match either.
	      if (!this.matchRoot()) return false;
	      fragment = this.fragment = this.getFragment(fragment);
	      return _.some(this.handlers, function(handler) {
	        if (handler.route.test(fragment)) {
	          handler.callback(fragment);
	          return true;
	        }
	      });
	    },

	    // Save a fragment into the hash history, or replace the URL state if the
	    // 'replace' option is passed. You are responsible for properly URL-encoding
	    // the fragment in advance.
	    //
	    // The options object can contain `trigger: true` if you wish to have the
	    // route callback be fired (not usually desirable), or `replace: true`, if
	    // you wish to modify the current URL without adding an entry to the history.
	    navigate: function(fragment, options) {
	      if (!History.started) return false;
	      if (!options || options === true) options = {trigger: !!options};

	      // Normalize the fragment.
	      fragment = this.getFragment(fragment || '');

	      // Don't include a trailing slash on the root.
	      var root = this.root;
	      if (fragment === '' || fragment.charAt(0) === '?') {
	        root = root.slice(0, -1) || '/';
	      }
	      var url = root + fragment;

	      // Strip the hash and decode for matching.
	      fragment = this.decodeFragment(fragment.replace(pathStripper, ''));

	      if (this.fragment === fragment) return;
	      this.fragment = fragment;

	      // If pushState is available, we use it to set the fragment as a real URL.
	      if (this._usePushState) {
	        this.history[options.replace ? 'replaceState' : 'pushState']({}, document.title, url);

	      // If hash changes haven't been explicitly disabled, update the hash
	      // fragment to store history.
	      } else if (this._wantsHashChange) {
	        this._updateHash(this.location, fragment, options.replace);
	        if (this.iframe && (fragment !== this.getHash(this.iframe.contentWindow))) {
	          var iWindow = this.iframe.contentWindow;

	          // Opening and closing the iframe tricks IE7 and earlier to push a
	          // history entry on hash-tag change.  When replace is true, we don't
	          // want this.
	          if (!options.replace) {
	            iWindow.document.open();
	            iWindow.document.close();
	          }

	          this._updateHash(iWindow.location, fragment, options.replace);
	        }

	      // If you've told us that you explicitly don't want fallback hashchange-
	      // based history, then `navigate` becomes a page refresh.
	      } else {
	        return this.location.assign(url);
	      }
	      if (options.trigger) return this.loadUrl(fragment);
	    },

	    // Update the hash location, either replacing the current entry, or adding
	    // a new one to the browser history.
	    _updateHash: function(location, fragment, replace) {
	      if (replace) {
	        var href = location.href.replace(/(javascript:|#).*$/, '');
	        location.replace(href + '#' + fragment);
	      } else {
	        // Some browsers require that `hash` contains a leading #.
	        location.hash = '#' + fragment;
	      }
	    }

	  });

	  // Create the default Backbone.history.
	  Backbone.history = new History;

	  // Helpers
	  // -------

	  // Helper function to correctly set up the prototype chain for subclasses.
	  // Similar to `goog.inherits`, but uses a hash of prototype properties and
	  // class properties to be extended.
	  var extend = function(protoProps, staticProps) {
	    var parent = this;
	    var child;

	    // The constructor function for the new subclass is either defined by you
	    // (the "constructor" property in your `extend` definition), or defaulted
	    // by us to simply call the parent constructor.
	    if (protoProps && _.has(protoProps, 'constructor')) {
	      child = protoProps.constructor;
	    } else {
	      child = function(){ return parent.apply(this, arguments); };
	    }

	    // Add static properties to the constructor function, if supplied.
	    _.extend(child, parent, staticProps);

	    // Set the prototype chain to inherit from `parent`, without calling
	    // `parent` constructor function.
	    var Surrogate = function(){ this.constructor = child; };
	    Surrogate.prototype = parent.prototype;
	    child.prototype = new Surrogate;

	    // Add prototype properties (instance properties) to the subclass,
	    // if supplied.
	    if (protoProps) _.extend(child.prototype, protoProps);

	    // Set a convenience property in case the parent's prototype is needed
	    // later.
	    child.__super__ = parent.prototype;

	    return child;
	  };

	  // Set up inheritance for the model, collection, router, view and history.
	  Model.extend = Collection.extend = Router.extend = View.extend = History.extend = extend;

	  // Throw an error when a URL is needed, and none is supplied.
	  var urlError = function() {
	    throw new Error('A "url" property or function must be specified');
	  };

	  // Wrap an optional error callback with a fallback error event.
	  var wrapError = function(model, options) {
	    var error = options.error;
	    options.error = function(resp) {
	      if (error) error.call(options.context, model, resp, options);
	      model.trigger('error', model, resp, options);
	    };
	  };

	  return Backbone;

	}));

	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [
		__webpack_require__(2),
		__webpack_require__(3),
		__webpack_require__(6),
		__webpack_require__(7),
		__webpack_require__(12),
		__webpack_require__(13),
		__webpack_require__(17)
	], __WEBPACK_AMD_DEFINE_RESULT__ = function(_,Marionette, template,TabComponentViews,SessionEntity,VerticalListComponentViews,SessionDetail){
		
		AppLayout = Marionette.LayoutView.extend({
		  template: template,
		  el: 'body',
		  initialize: function(opts){
		  	this.tabCollections = opts.collection.groupByTitle();
		  	
		  	//Generate Tab configs dynamically based on session title grouping
		  	_self = this;
		  	this.tabConfigs = _.map(Object.keys(this.tabCollections),function(key){
		  		return {
		  			title: key,
		  			collection: new SessionEntity.SessionCollection(_self.tabCollections[key]), 
		  			view: VerticalListComponentViews.View,
		  			viewOpts: {view: SessionDetail.View}  
		  		}
		  	});

		  },
		  regions: {
		  	'tabRegion': '.tab-region'
		  },

		  onRender: function(){
		  	var tabComponent = new TabComponentViews.View({config:this.tabConfigs});
		  	this.tabRegion.show(tabComponent);
		  }

		});

		return AppLayout;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_RESULT__ = function(){
	  var template = function(__obj) {
	  var _safe = function(value) {
	    if (typeof value === 'undefined' && value == null)
	      value = '';
	    var result = new String(value);
	    result.ecoSafe = true;
	    return result;
	  };
	  return (function() {
	    var __out = [], __self = this, _print = function(value) {
	      if (typeof value !== 'undefined' && value != null)
	        __out.push(value.ecoSafe ? value : __self.escape(value));
	    }, _capture = function(callback) {
	      var out = __out, result;
	      __out = [];
	      callback.call(this);
	      result = __out.join('');
	      __out = out;
	      return _safe(result);
	    };
	    (function() {
	      _print(_safe('<div class="container-fluid">\n  <div class="row nav">\n    <div class="col-md-12">\n      <div class="row body">\n        <img src="images/logo.png"/>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class="container">\n  <div class="row page-header-logo">\n    <div class="col-md-12">\n      <img src="images/summit-logo.png"/>\n    </div>\n  </div>\n\n  <div class="row page-header-logo">\n    <div class="col-md-12">\n      <h3>Video Archive</h3>\n    </div>\n  </div>\n\n  <div class="row body">\n    <div class="col-md-12 tab-region"></div> \n  </div>\n</div>'));
	    
	    }).call(this);
	    
	    return __out.join('');
	  }).call((function() {
	    var obj = {
	      escape: function(value) {
	        return ('' + value)
	          .replace(/&/g, '&amp;')
	          .replace(/</g, '&lt;')
	          .replace(/>/g, '&gt;')
	          .replace(/"/g, '&quot;');
	      },
	      safe: _safe
	    }, key;
	    for (key in __obj) obj[key] = __obj[key];
	    return obj;
	  })());
	};
	  return template;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [
		__webpack_require__(2),
		__webpack_require__(3),
		__webpack_require__(8),
		__webpack_require__(9),
		__webpack_require__(10)
	], __WEBPACK_AMD_DEFINE_RESULT__ = function(_,Marionette, tab_component_layout_template, tab_view_template, TabEntity){
		
		var TabComponent = {}
		var _this = TabComponent;

		TabComponent.View = Marionette.LayoutView.extend({
		  template: tab_component_layout_template,
		  className: 'tab-component',
		  initialize: function(opts){
		  	this.tabs = opts.config
		  },
		  regions:{
		  	tabHeader: '.tab-header-region',
		  	tabContent: '.tab-content-region'
		  },
		  onShow: function(){
		  	tabCollection = new TabEntity.TabCollection(_.map(this.tabs,function(tab){
		  		return new TabEntity.Tab(tab);
		  	}));

		   	var tabCollectionView = new _this.TabCollectionView({collection:tabCollection});
		   	this.listenTo(tabCollection, "collection:chose:one",this.tabChosen);
		   	tabCollection.listenTo(tabCollectionView, "show", this.setDefaultTab);
		   	this.tabHeader.show(tabCollectionView);
		  },
		  tabChosen: function(model){
		  	var tabView = new (model.get('view'))({model: model});
		  	this.tabContent.show(tabView);
		  },
		  setDefaultTab: function(){
		   	//Select first tab by default
		   	this.choose(this.at(0));
		  }
		});

		TabComponent.TabView = Marionette.ItemView.extend({
			template: tab_view_template,
			tagName: 'li',
			modelEvents:{
				'change chosen':'onTabChanged'
			},
			triggers:{
				'click a' : 'tab:chosen'
			},
			onTabChanged: function(model){
				if (model.get('chosen')){
					this.$el.addClass('current');	
				}else{
					this.$el.removeClass('current');
				}
			}
		});

		TabComponent.TabCollectionView = Marionette.CollectionView.extend({
			childView: _this.TabView,
			tagName: 'ul',
			className: 'tabs col-md-12',
			childEvents:{
				'tab:chosen': 'onTabChosen'
			},
			onTabChosen: function(view){
				this.collection.choose(view.model);
			}
		});

		return TabComponent;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_RESULT__ = function(){
	  var template = function(__obj) {
	  var _safe = function(value) {
	    if (typeof value === 'undefined' && value == null)
	      value = '';
	    var result = new String(value);
	    result.ecoSafe = true;
	    return result;
	  };
	  return (function() {
	    var __out = [], __self = this, _print = function(value) {
	      if (typeof value !== 'undefined' && value != null)
	        __out.push(value.ecoSafe ? value : __self.escape(value));
	    }, _capture = function(callback) {
	      var out = __out, result;
	      __out = [];
	      callback.call(this);
	      result = __out.join('');
	      __out = out;
	      return _safe(result);
	    };
	    (function() {
	      _print(_safe('<div class="tab-header-region row"></div>\n<div class="tab-content-region row"></div>'));
	    
	    }).call(this);
	    
	    return __out.join('');
	  }).call((function() {
	    var obj = {
	      escape: function(value) {
	        return ('' + value)
	          .replace(/&/g, '&amp;')
	          .replace(/</g, '&lt;')
	          .replace(/>/g, '&gt;')
	          .replace(/"/g, '&quot;');
	      },
	      safe: _safe
	    }, key;
	    for (key in __obj) obj[key] = __obj[key];
	    return obj;
	  })());
	};
	  return template;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_RESULT__ = function(){
	  var template = function(__obj) {
	  var _safe = function(value) {
	    if (typeof value === 'undefined' && value == null)
	      value = '';
	    var result = new String(value);
	    result.ecoSafe = true;
	    return result;
	  };
	  return (function() {
	    var __out = [], __self = this, _print = function(value) {
	      if (typeof value !== 'undefined' && value != null)
	        __out.push(value.ecoSafe ? value : __self.escape(value));
	    }, _capture = function(callback) {
	      var out = __out, result;
	      __out = [];
	      callback.call(this);
	      result = __out.join('');
	      __out = out;
	      return _safe(result);
	    };
	    (function() {
	      _print(_safe('<a href="javascript:void(0)">'));
	    
	      _print(this.title);
	    
	      _print(_safe('</a>'));
	    
	    }).call(this);
	    
	    return __out.join('');
	  }).call((function() {
	    var obj = {
	      escape: function(value) {
	        return ('' + value)
	          .replace(/&/g, '&amp;')
	          .replace(/</g, '&lt;')
	          .replace(/>/g, '&gt;')
	          .replace(/"/g, '&quot;');
	      },
	      safe: _safe
	    }, key;
	    for (key in __obj) obj[key] = __obj[key];
	    return obj;
	  })());
	};
	  return template;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [
		__webpack_require__(4)
	], __WEBPACK_AMD_DEFINE_RESULT__ = function(Backbone){
		//TODO: Clean up explicit require since backbone.chooser does not appear to support AMD loading
		window._ = __webpack_require__(2);
		__webpack_require__(11);

		var TabEntity = {}

		TabEntity.Tab = Backbone.Model.extend({
			defaults: {},
			initialize: function(){
				new Backbone.Chooser(this);
			}
		});

		TabEntity.TabCollection = Backbone.Collection.extend({
			model: TabEntity.Tab,
			initialize: function(){
				new Backbone.SingleChooser(this)
			}
		});

		return TabEntity;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ },
/* 11 */
/***/ function(module, exports) {

	var __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; },
	  __hasProp = {}.hasOwnProperty,
	  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  __slice = [].slice;

	(function(Backbone) {
	  var BaseChooser, _ref;
	  Backbone.Chooser = (function() {
	    function Chooser(model) {
	      var method, _i, _len, _ref;
	      this.model = model;
	      this.model._chooser = this;
	      _ref = this._publicMethods();
	      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
	        method = _ref[_i];
	        this.model[method] = _.bind(this[method], this);
	      }
	      this.chosen = false;
	      this.model.set({
	        chosen: false
	      });
	    }

	    Chooser.prototype._publicMethods = function() {
	      return ["choose", "unchoose", "toggleChoose", "isChosen"];
	    };

	    Chooser.prototype.isChosen = function() {
	      return !!this.chosen;
	    };

	    Chooser.prototype.choose = function(options) {
	      var _ref;
	      if (options == null) {
	        options = {};
	      }
	      if (this.isChosen()) {
	        return;
	      }
	      this.chosen = true;
	      this.model.set({
	        chosen: true
	      }, options);
	      if (options.silent !== true) {
	        this.model.trigger("model:chosen", this.model);
	      }
	      return (_ref = this.model.collection) != null ? typeof _ref.choose === "function" ? _ref.choose(this.model, options) : void 0 : void 0;
	    };

	    Chooser.prototype.unchoose = function(options) {
	      var _ref;
	      if (options == null) {
	        options = {};
	      }
	      if (!this.isChosen()) {
	        return;
	      }
	      this.chosen = false;
	      this.model.set({
	        chosen: false
	      }, options);
	      if (options.silent !== true) {
	        this.model.trigger("model:unchosen", this.model);
	      }
	      return (_ref = this.model.collection) != null ? typeof _ref.unchoose === "function" ? _ref.unchoose(this.model, options) : void 0 : void 0;
	    };

	    Chooser.prototype.toggleChoose = function() {
	      if (this.isChosen()) {
	        return this.unchoose();
	      } else {
	        return this.choose();
	      }
	    };

	    return Chooser;

	  })();
	  BaseChooser = (function() {
	    function BaseChooser(collection) {
	      var method, _i, _len, _ref;
	      this.collection = collection;
	      this.collection._chooser = this;
	      this.collection._chooser.chosen = {};
	      _ref = this._publicMethods();
	      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
	        method = _ref[_i];
	        this.collection[method] = _.bind(this[method], this);
	      }
	    }

	    BaseChooser.prototype._publicMethods = function() {
	      return ["choose", "unchoose", "getChosen", "getFirstChosen", "chooseById"];
	    };

	    BaseChooser.prototype.getChosen = function() {
	      return _.toArray(this.chosen);
	    };

	    BaseChooser.prototype.getFirstChosen = function() {
	      return this.getChosen()[0];
	    };

	    BaseChooser.prototype.modelInChosen = function(model) {
	      var _ref;
	      return _ref = model.cid, __indexOf.call(_.keys(this.chosen), _ref) >= 0;
	    };

	    BaseChooser.prototype.addModel = function(model, options) {
	      if (options == null) {
	        options = {};
	      }
	      this.chosen[model.cid] = model;
	      return typeof model.choose === "function" ? model.choose(options) : void 0;
	    };

	    BaseChooser.prototype.removeModels = function(model) {
	      var _i, _len, _ref, _results;
	      if (model == null) {
	        model = false;
	      }
	      _ref = _.flatten([model || this.getChosen()]);
	      _results = [];
	      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
	        model = _ref[_i];
	        delete this.chosen[model.cid];
	        _results.push(typeof model.unchoose === "function" ? model.unchoose() : void 0);
	      }
	      return _results;
	    };

	    BaseChooser.prototype.triggerEvent = function(event, options) {
	      if (event == null) {
	        event = false;
	      }
	      if (options == null) {
	        options = {};
	      }
	      _.defaults(options, {
	        silent: false
	      });
	      if (options.silent === true) {
	        return;
	      }
	      event || (event = this._getEvent());
	      return this.collection.trigger(event, this._eventArg());
	    };

	    BaseChooser.prototype.chooseById = function(id, options) {
	      var model;
	      if (options == null) {
	        options = {};
	      }
	      model = this.collection.get(id);
	      if (model) {
	        return this.choose(model, options);
	      }
	    };

	    return BaseChooser;

	  })();
	  Backbone.SingleChooser = (function(_super) {
	    __extends(SingleChooser, _super);

	    function SingleChooser() {
	      _ref = SingleChooser.__super__.constructor.apply(this, arguments);
	      return _ref;
	    }

	    SingleChooser.prototype._eventArg = function() {
	      return this.getFirstChosen();
	    };

	    SingleChooser.prototype.choose = function(model, options) {
	      if (this.modelInChosen(model)) {
	        return;
	      }
	      this.removeModels();
	      this.addModel(model);
	      return this.triggerEvent("collection:chose:one", options);
	    };

	    SingleChooser.prototype.unchoose = function(model, options) {
	      if (!this.modelInChosen(model)) {
	        return;
	      }
	      this.removeModels(model);
	      return this.triggerEvent("collection:unchose:one", options);
	    };

	    return SingleChooser;

	  })(BaseChooser);
	  return Backbone.MultiChooser = (function(_super) {
	    __extends(MultiChooser, _super);

	    function MultiChooser() {
	      var method, _i, _len, _ref1;
	      MultiChooser.__super__.constructor.apply(this, arguments);
	      _ref1 = ["chooseAll", "chooseNone", "chooseByIds"];
	      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
	        method = _ref1[_i];
	        this.collection[method] = _.bind(this[method], this);
	      }
	    }

	    MultiChooser.prototype._eventArg = function() {
	      return this.getChosen();
	    };

	    MultiChooser.prototype.choose = function() {
	      var args, eventShouldTrigger, model, options, _i, _len, _ref1;
	      args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
	      options = !(_.chain(args).flatten().last().value() instanceof Backbone.Model) ? args.pop() : {};
	      eventShouldTrigger = false;
	      _ref1 = _([args]).flatten();
	      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
	        model = _ref1[_i];
	        if (this.modelInChosen(model)) {
	          break;
	        }
	        eventShouldTrigger || (eventShouldTrigger = true);
	        this.addModel(model, options);
	      }
	      if (eventShouldTrigger) {
	        return this.triggerEvent(false, options);
	      }
	    };

	    MultiChooser.prototype.unchoose = function() {
	      var args, eventShouldTrigger, model, options, _i, _len, _ref1;
	      args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
	      options = !(_.chain(args).flatten().last().value() instanceof Backbone.Model) ? args.pop() : {};
	      eventShouldTrigger = false;
	      _ref1 = _([args]).flatten();
	      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
	        model = _ref1[_i];
	        if (!this.modelInChosen(model)) {
	          break;
	        }
	        eventShouldTrigger || (eventShouldTrigger = true);
	        this.removeModels(model, options);
	      }
	      if (eventShouldTrigger) {
	        return this.triggerEvent(false, options);
	      }
	    };

	    MultiChooser.prototype.chooseAll = function(options) {
	      var model, _i, _len, _ref1;
	      if (options == null) {
	        options = {};
	      }
	      if (!_.difference(this.collection.models, this.getChosen()).length) {
	        return;
	      }
	      _ref1 = this.collection.models;
	      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
	        model = _ref1[_i];
	        this.addModel(model);
	      }
	      return this.triggerEvent(false, options);
	    };

	    MultiChooser.prototype.chooseNone = function(options) {
	      if (options == null) {
	        options = {};
	      }
	      if (this.getChosen().length === 0) {
	        return;
	      }
	      this.removeModels();
	      return this.triggerEvent(false, options);
	    };

	    MultiChooser.prototype.chooseByIds = function(ids, options) {
	      var id, _i, _len, _ref1, _results;
	      if (ids == null) {
	        ids = [];
	      }
	      if (options == null) {
	        options = {};
	      }
	      _.defaults(options, {
	        chooseNone: true
	      });
	      if (options.chooseNone) {
	        this.chooseNone(options);
	      }
	      _ref1 = _([ids]).flatten();
	      _results = [];
	      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
	        id = _ref1[_i];
	        _results.push(this.chooseById(id, options));
	      }
	      return _results;
	    };

	    MultiChooser.prototype._getEvent = function() {
	      if (this.collection.length === this.getChosen().length) {
	        return "collection:chose:all";
	      }
	      if (this.getChosen().length === 0) {
	        return "collection:chose:none";
	      }
	      return "collection:chose:some";
	    };

	    return MultiChooser;

	  })(BaseChooser);
	})(Backbone);

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [
		__webpack_require__(2),
		__webpack_require__(4)
	], __WEBPACK_AMD_DEFINE_RESULT__ = function(_,Backbone){

		var SessionEntity = {}

		SessionEntity.Session = Backbone.Model.extend({
			defaults: {}
		});

		SessionEntity.SessionCollection = Backbone.Collection.extend({
			model: SessionEntity.Session,

			groupByTitle: function(){
				//Group sessions by title
				var groups = _.groupBy(this.models,function(session){
					return session.get('Track').Title
				});

				/*
					Lets remove Activities and Training Talks. Ideally, we'd do this more intelligently
					by filtering if certain conditions were met
				*/
				delete groups.Activities;
				delete groups.Training;

				return groups;
			}
		});

		return SessionEntity;

	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [
		__webpack_require__(2),
		__webpack_require__(3),
		__webpack_require__(14),
		__webpack_require__(15),
		__webpack_require__(16)
	], __WEBPACK_AMD_DEFINE_RESULT__ = function(_,Marionette, vertical_list_component_layout, vertical_item_view_template, VerticalItemEntity){
		
		var VerticalListComponent = {}
		var _this=VerticalListComponent;

		VerticalListComponent.View = Marionette.LayoutView.extend({
		  template: vertical_list_component_layout,
		  className: 'vertical-list-component row',
		  regions:{
		  	verticalList: '.vertical-list-region',
		  	listContent: '.vertical-list-content-region'
		  },
		  onShow: function(){
		  	itemCollection = new VerticalItemEntity.ItemCollection(_.map(this.model.get('collection').models,function(model){
		  		return new VerticalItemEntity.Item(_.clone(model.attributes));
		  	}));

		   	var itemCollectionView = new _this.ItemCollectionView({collection:itemCollection});
		   	this.listenTo(itemCollection, "collection:chose:one",this.itemChosen);
		   	itemCollection.listenTo(itemCollectionView, "show", this.setDefaultItem);
		   	this.verticalList.show(itemCollectionView);
		  },
		  itemChosen: function(model){
		  	var contentView = new (this.model.get('viewOpts').view)({model: model});
		  	this.listContent.show(contentView);
		  },
	  	  setDefaultItem: function(){
		   	//Select first tab by default
		   	this.choose(this.at(0));
		  }
		});

		VerticalListComponent.ItemView = Marionette.ItemView.extend({
			template: vertical_item_view_template,
			tagName: 'li',
			modelEvents:{
				'change chosen':'onItemChanged'
			},
			triggers:{
				'click a' : 'item:chosen'
			},
			onItemChanged: function(model){
				if (model.get('chosen')){
					this.$el.addClass('current');	
				}else{
					this.$el.removeClass('current');
				}
			}
		});

		VerticalListComponent.ItemCollectionView = Marionette.CollectionView.extend({
			childView: _this.ItemView,
			tagName: 'ul',
			className: 'items col-md-12',
			childEvents:{
				'item:chosen': 'onItemChosen'
			},
			onItemChosen: function(view){
				this.collection.choose(view.model);
			}
		});

		return VerticalListComponent;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_RESULT__ = function(){
	  var template = function(__obj) {
	  var _safe = function(value) {
	    if (typeof value === 'undefined' && value == null)
	      value = '';
	    var result = new String(value);
	    result.ecoSafe = true;
	    return result;
	  };
	  return (function() {
	    var __out = [], __self = this, _print = function(value) {
	      if (typeof value !== 'undefined' && value != null)
	        __out.push(value.ecoSafe ? value : __self.escape(value));
	    }, _capture = function(callback) {
	      var out = __out, result;
	      __out = [];
	      callback.call(this);
	      result = __out.join('');
	      __out = out;
	      return _safe(result);
	    };
	    (function() {
	      _print(_safe('<div class="col-md-4">\n\t<h4>'));
	    
	      _print(this.title);
	    
	      _print(_safe('</h4>\n\t<div class="vertical-list-region"></div>\n</div>\n\n<div class="vertical-list-content-region col-md-8"></div>'));
	    
	    }).call(this);
	    
	    return __out.join('');
	  }).call((function() {
	    var obj = {
	      escape: function(value) {
	        return ('' + value)
	          .replace(/&/g, '&amp;')
	          .replace(/</g, '&lt;')
	          .replace(/>/g, '&gt;')
	          .replace(/"/g, '&quot;');
	      },
	      safe: _safe
	    }, key;
	    for (key in __obj) obj[key] = __obj[key];
	    return obj;
	  })());
	};
	  return template;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_RESULT__ = function(){
	  var template = function(__obj) {
	  var _safe = function(value) {
	    if (typeof value === 'undefined' && value == null)
	      value = '';
	    var result = new String(value);
	    result.ecoSafe = true;
	    return result;
	  };
	  return (function() {
	    var __out = [], __self = this, _print = function(value) {
	      if (typeof value !== 'undefined' && value != null)
	        __out.push(value.ecoSafe ? value : __self.escape(value));
	    }, _capture = function(callback) {
	      var out = __out, result;
	      __out = [];
	      callback.call(this);
	      result = __out.join('');
	      __out = out;
	      return _safe(result);
	    };
	    (function() {
	      _print(_safe('<a href="javascript:void(0)">\n\t<span>'));
	    
	      _print(this.Title);
	    
	      _print(_safe(' </span>\n\t'));
	    
	      if (this.Speakers != null) {
	        _print(_safe('\n\t\t<cite><p>'));
	        _print(this.Speakers[0].FirstName + " " + this.Speakers[0].LastName + ", " + this.Speakers[0].Company);
	        _print(_safe('</p></cite>\n\t'));
	      }
	    
	      _print(_safe('\n</a>'));
	    
	    }).call(this);
	    
	    return __out.join('');
	  }).call((function() {
	    var obj = {
	      escape: function(value) {
	        return ('' + value)
	          .replace(/&/g, '&amp;')
	          .replace(/</g, '&lt;')
	          .replace(/>/g, '&gt;')
	          .replace(/"/g, '&quot;');
	      },
	      safe: _safe
	    }, key;
	    for (key in __obj) obj[key] = __obj[key];
	    return obj;
	  })());
	};
	  return template;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [
		__webpack_require__(4)
	], __WEBPACK_AMD_DEFINE_RESULT__ = function(Backbone){
		//TODO: Clean up explicit require since backbone.chooser does not appear to support AMD loading
		window._ = __webpack_require__(2);
		__webpack_require__(11);

		var VerticalItemEntity = {}

		VerticalItemEntity.Item = Backbone.Model.extend({
			defaults: {},
			initialize: function(){
				new Backbone.Chooser(this);
			}
		});

		VerticalItemEntity.ItemCollection = Backbone.Collection.extend({
			model: VerticalItemEntity.Item,
			initialize: function(){
				new Backbone.SingleChooser(this)
			}
		});

		return VerticalItemEntity;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [
		__webpack_require__(3),
		__webpack_require__(18),
	], __WEBPACK_AMD_DEFINE_RESULT__ = function(Marionette,session_detail_template){
		
		var SessionDetail = {}


		SessionDetail.View = Marionette.LayoutView.extend({
		  template: session_detail_template,
		  className: 'session-detail-view'
		});


		return SessionDetail;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_RESULT__ = function(){
	  var template = function(__obj) {
	  var _safe = function(value) {
	    if (typeof value === 'undefined' && value == null)
	      value = '';
	    var result = new String(value);
	    result.ecoSafe = true;
	    return result;
	  };
	  return (function() {
	    var __out = [], __self = this, _print = function(value) {
	      if (typeof value !== 'undefined' && value != null)
	        __out.push(value.ecoSafe ? value : __self.escape(value));
	    }, _capture = function(callback) {
	      var out = __out, result;
	      __out = [];
	      callback.call(this);
	      result = __out.join('');
	      __out = out;
	      return _safe(result);
	    };
	    (function() {
	      var i, len, ref, ref1, speaker;
	    
	      _print(_safe('<h2>'));
	    
	      _print(this.Title);
	    
	      _print(_safe('</h2>\n\n'));
	    
	      if (this.Speakers != null) {
	        _print(_safe('\n\t<cite><p>'));
	        _print(this.Speakers[0].FirstName + " " + this.Speakers[0].LastName + ", " + this.Speakers[0].Company);
	        _print(_safe('</p></cite>\n'));
	      }
	    
	      _print(_safe('\n\n<p>'));
	    
	      _print(this.Description);
	    
	      _print(_safe('</p>\n\n'));
	    
	      if ((this.Speakers != null) && ((ref = this.Speakers[0].Biography) != null ? ref.length : void 0) !== 0) {
	        _print(_safe('\n<h3>About the speaker'));
	        _print(this.Speakers.length > 1 ? "s" : void 0);
	        _print(_safe('</h3>\n\n\t'));
	        ref1 = this.Speakers;
	        for (i = 0, len = ref1.length; i < len; i++) {
	          speaker = ref1[i];
	          _print(_safe('\n\t<p>\n\t\t<strong>'));
	          _print(speaker.FirstName + " " + speaker.LastName + ",");
	          _print(_safe('</strong> \n\t\t'));
	          _print("" + speaker.Company);
	          _print(_safe('\n\t\t'));
	          debugger;
	          _print(_safe('\n\t</p>\n\t<p>'));
	          _print(speaker.Biography);
	          _print(_safe('</p>\n\n\t'));
	        }
	        _print(_safe('\n\n'));
	      }
	    
	      _print(_safe(' '));
	    
	    }).call(this);
	    
	    return __out.join('');
	  }).call((function() {
	    var obj = {
	      escape: function(value) {
	        return ('' + value)
	          .replace(/&/g, '&amp;')
	          .replace(/</g, '&lt;')
	          .replace(/>/g, '&gt;')
	          .replace(/"/g, '&quot;');
	      },
	      safe: _safe
	    }, key;
	    for (key in __obj) obj[key] = __obj[key];
	    return obj;
	  })());
	};
	  return template;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ },
/* 19 */
/***/ function(module, exports) {

	module.exports = {
		"Items": [
			{
				"Room": {
					"Name": "Golden Gate B",
					"Capacity": "575"
				},
				"Id": "27829",
				"Description": "Tractors, sprayers, and combines have more technology than you think – and they're getting smarter every day. Join this session to learn how cutting-edge technology is being used in a manufacturing company. Diego Lucas will walk through AGCO's three-year journey from a traditional, ad-hoc set of technologies and processes for the development lifecycle to a modern, agile automation-obsessed devops culture, using Atlassian tools in conjunction with technologies such as Heroku and Docker. He'll also discuss cultural aspects of change as well as go into lessons learned and provide a glimpse of what lies ahead.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 7:35:00 PM",
					"Label": "Breakout 7",
					"StartTime": "11/5/2015 6:50:00 PM"
				},
				"Track": {
					"Title": "Build",
					"Description": "Build"
				},
				"LastModified": "9/22/2015 4:07:06 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Basic",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "Tractors and DevOps: Harvesting the Fruits of Automation",
				"Speakers": [
					{
						"AttendeeID": "bMedMbpxRnZpdXhYv7y8",
						"Biography": "",
						"LastName": "Lucas",
						"EmailAddress": "diogo.lucas@agcocorp.com",
						"Title": "",
						"AvailableforMeeting": "False",
						"FirstName": "Diogo",
						"Industry": "",
						"Roles": [
							"Speaker"
						],
						"Company": "AGCO",
						"LastUpdated": "9/22/2015 4:07:06 AM",
						"ID": "111417"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate A",
					"Capacity": "525"
				},
				"Id": "27832",
				"Description": "Project Managers often face an identity crisis when companies transition to agile: either become a scrum master, move to another role entirely, or another company!\r\n\r\nThis discussion will demonstrate how project management can not only co-exist with your agile process, but thrive. Plus, you'll learn how Gilt blends agile principles with traditional project management techniques to bring their strategic initiatives to life.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 10:30:00 PM",
					"Label": "Breakout 4",
					"StartTime": "11/4/2015 10:00:00 PM"
				},
				"Track": {
					"Title": "Plan",
					"Description": "Plan"
				},
				"LastModified": "9/22/2015 4:06:10 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Basic",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					},
					{
						"Value": "JIRA,JIRA Agile",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "The Project Management Comeback",
				"Speakers": [
					{
						"AttendeeID": "7Lrnswmej2jQY7Q7ckQy",
						"Biography": "As Senior Director of the Program Management Organization at Gilt, Heather Fleming oversees the execution of strategic technology initiatives across the company’s 170-member tech team. Her chief aim: to challenge the status quo of project management best practices by continually experimenting with agile project management methodologies. Since joining Gilt in 2010, Heather has directed a wide variety of customer-facing, international, and back office projects instrumental to the ecommerce leader’s continued growth and success. She is also a certified facilitator in the Myers-Briggs Type Indicator® (MBTI) and coaches individuals on how to improve team dynamics and performance by adopting techniques meant to increase self-awareness and empathy. ",
						"LastName": "Fleming",
						"Interests": "Interests are private",
						"EmailAddress": "hfleming@gilt.com",
						"Title": "Senior Director, PMO",
						"AvailableforMeeting": "True",
						"FirstName": "Heather",
						"Industry": "Fashion Tech",
						"Roles": [
							"Speaker"
						],
						"Website": "http://www.gilt.com",
						"Twitter": "https://twitter.com/hfleming",
						"LinkedIn": "https://www.linkedin.com/in/hhfleming",
						"Blog": "http://tech.gilt.com",
						"Company": "GILT",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/111420",
						"LastUpdated": "9/22/2015 4:06:10 AM",
						"ID": "111420"
					},
					{
						"AttendeeID": "88776655",
						"Biography": "Justin Riservato has been an integral part of Gilt’s growth since 2009. Justin has worked with just about every team and department at Gilt, developing a direct, “tell the truth” approach and philosophy to project communication that has improved both collaboration and trust across teams. From his past life of living and working on sailboats, Justin has been able to draw numerous parallels between program management and working on a ship—at least the way he does it!",
						"LastName": "Riservato",
						"EmailAddress": "jriservato@gilt.com",
						"Title": "Director, Program Management",
						"AvailableforMeeting": "True",
						"FirstName": "Justin",
						"Industry": "Ecommerce",
						"Roles": [
							"Speaker"
						],
						"Website": "http://www.gilt.com",
						"Twitter": "twitter.com/sharksnmermaids",
						"LinkedIn": "http://www.linkedin.com/pub/justin-riservato/3/8a8/103",
						"Blog": "http://tech.gilt.com",
						"Company": "Gilt",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/111437",
						"LastUpdated": "9/22/2015 4:06:10 AM",
						"ID": "111437"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate C",
					"Capacity": "475"
				},
				"Id": "27834",
				"Description": "As a developer you use Confluence and JIRA because they improve your proficiency in processes, knowledge transfer, training peers, and situational awareness in projects. But what about the non-technical people outside your team?\r\n\r\nKick-start your whole company's adoption of JIRA and Confluence with some tips from James Hilving of Masergy Communications. He'll talk about add-ons, page templates, simple workflows, and terminology that'll bridge the gap and get everyone on the same page. (A Confluence page, that is!)",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 6:45:00 PM",
					"Label": "Breakout 1",
					"StartTime": "11/4/2015 6:15:00 PM"
				},
				"Track": {
					"Title": "Innovate",
					"Description": "Innovate"
				},
				"LastModified": "10/2/2015 3:06:45 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Intermediate",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					},
					{
						"Value": "JIRA,Confluence",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "How to Encourage Non-Development Teams to Use JIRA and Confluence",
				"Speakers": [
					{
						"AttendeeID": "hhZ4W4JztjVPrQaCWlgd",
						"Biography": "I have ~20 years in the industry and have had several roles in technology. My foundation is Network Engineering but I have found a passion in applying Software Solutions to solve problems, increase productivity, and improve teamwork among multiple types of teams. I am very excited to be speaking at the Summit this year to share my experiences related to Confluence and Jira among the workplace. ",
						"LastName": "Hilving",
						"Interests": "Programming, Networking, Capitalizing on existing Technology to make things better. ",
						"EmailAddress": "james.hilving@masergy.com",
						"Title": "Software Architect",
						"AvailableforMeeting": "True",
						"FirstName": "James",
						"Industry": "Telecommunications",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/jnetnix",
						"LinkedIn": "https://www.linkedin.com/in/jameshilving",
						"Company": "Masergy Communications",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/111422",
						"LastUpdated": "4/21/2015 5:19:41 PM",
						"ID": "111422"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 1-3",
					"Capacity": "225"
				},
				"Id": "28142",
				"Description": "HipChat is the backbone of communication and interconnectivity at Code.org. Join Brendan Reville as he shares how the team at Code.org communicates across regions, ships projects, monitors its systems, and centralizes error reporting using HipChat rooms. You’ll leave with a checklist that will help you make sure your organization is getting the most out of HipChat.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 11:15:00 PM",
					"Label": "Breakout 5",
					"StartTime": "11/4/2015 10:45:00 PM"
				},
				"Track": {
					"Title": "Interact",
					"Description": "Interact"
				},
				"LastModified": "9/18/2015 1:24:48 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Intermediate",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					},
					{
						"Value": "HipChat",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "10 Ways Your Organization Should be Using HipChat",
				"Speakers": [
					{
						"AttendeeID": "15865135",
						"Biography": "Brendan is a software engineer who loves creating user-facing experiences. Since moving to Seattle from Sydney, he has worked at Microsoft on the Xbox 360, volunteered in a low-income after-school tutoring program, and played some ice hockey.",
						"LastName": "Reville",
						"Interests": "Brendan is passionate about creating great user experiences, and building team culture that is efficient and effective.",
						"EmailAddress": "brendan@code.org",
						"AvailableforMeeting": "True",
						"FirstName": "Brendan",
						"Industry": "Industry is private",
						"Roles": [
							"Speaker"
						],
						"Website": "https://code.org",
						"Twitter": "https://twitter.com/brendanreville",
						"LinkedIn": "https://www.linkedin.com/in/brendanreville",
						"Facebook": "https://www.facebook.com/brendanreville",
						"Blog": "http://brendanreville.com",
						"Company": "Code.org",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/112328",
						"LastUpdated": "6/27/2015 2:01:43 AM",
						"ID": "112328"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate C",
					"Capacity": "475"
				},
				"Id": "28238",
				"Description": "A comedian, a scientist and a software developer walk into a room to discuss why work sucks and how teams could use fun as a creative multiplier. The software developer took fun and made a job out of it. The comedian took fun and created a business around it. The scientist took fun and wrote a white paper. This panel will explore how improv and humor in the workplace are accelerating the creative process and making work more fun. Once you try it, humor spreads like a virus so use protection. Yes, that kind of protection.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 7:35:00 PM",
					"Label": "Breakout 7",
					"StartTime": "11/5/2015 6:50:00 PM"
				},
				"Track": {
					"Title": "Innovate",
					"Description": "Innovate"
				},
				"LastModified": "10/2/2015 10:40:39 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"Title": "Bizsnatch! Why Fun is Good Business",
				"Speakers": [
					{
						"AttendeeID": "5k3xRkivR6mBicDEwbta",
						"LastName": "Halvorson",
						"EmailAddress": "mhalvorson@atlassian.com",
						"AvailableforMeeting": "False",
						"FirstName": "Mark",
						"Roles": [
							"Speaker"
						],
						"Company": "Atlassian",
						"LastUpdated": "10/2/2015 10:40:39 PM",
						"ID": "112579"
					},
					{
						"AttendeeID": "TEMP-528777782",
						"LastName": "White",
						"EmailAddress": "kenny@howfunworks.com",
						"AvailableforMeeting": "False",
						"FirstName": "Kenny",
						"Roles": [
							"Speaker"
						],
						"Company": "Funworks",
						"LastUpdated": "10/2/2015 10:40:39 PM",
						"ID": "121120"
					},
					{
						"AttendeeID": "TEMP-700466320",
						"LastName": "Charney",
						"EmailAddress": "paul@howfunworks.com",
						"AvailableforMeeting": "False",
						"FirstName": "Paul",
						"Roles": [
							"Speaker"
						],
						"Company": "Funworks",
						"LastUpdated": "10/2/2015 10:40:39 PM",
						"ID": "121121"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 1-3",
					"Capacity": "225"
				},
				"Id": "28244",
				"Description": "Join Atlassian veterans Dominic and Ben for the story of how and why we created a little thing called the Team Health Monitor. They show you how you can use it to harness the power and increase the confidence of your own healthy teams.",
				"IsFeatured": "False",
				"Links": "<br>",
				"TimeSlot": {
					"EndTime": "11/5/2015 7:35:00 PM",
					"Label": "Breakout 7",
					"StartTime": "11/5/2015 6:50:00 PM"
				},
				"Track": {
					"Title": "Interact",
					"Description": "Interact"
				},
				"LastModified": "9/18/2015 1:38:06 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Basic",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "What Awesome Sauce Tastes Like: Getting & Keeping Your Teams Healthy... the Atlassian Way",
				"Speakers": [
					{
						"AttendeeID": "epGdFnoeictHQ7AEFeWd",
						"Biography": "Born to Joy in the harsh Manchester winter of '77, Dom has traversed many countries and many industries.\r\n\r\nHis work experience spans technology consulting and business advisory in industries such as telco, consumer goods, manufacturing and software. He also spent 3 years helping a global gaming company improve their product development muscle, including a 7 month stint in India.\r\n\r\nNow Dom calls Atlassian home.",
						"LastName": "Price",
						"EmailAddress": "dprice@atlassian.com",
						"Title": "Head of Program Management",
						"AvailableforMeeting": "True",
						"FirstName": "Dominic",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/domprice",
						"LinkedIn": "https://au.linkedin.com/pub/dominic-price/3/224/89",
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/112839",
						"LastUpdated": "5/6/2015 9:39:14 PM",
						"ID": "112839"
					},
					{
						"AttendeeID": "18561352135",
						"Biography": "If robots are going to do all the regular work for us, what will we do? Great work, that's what. Rethinking. Exploring. Questioning. Creating. And helping each other do all that. That's Ben's jam. Ben is a design strategist, mentor and team leader. He's been in the online industry from when Netscape Navigator ruled the www, and at Atlassian for the last 2 years. He enjoys helping take business strategy into well-designed experiences with research, creativity techniques, sketching, prototyping and testing. When not doing these things, Ben engages in extreme sports like painting, brewing beer, and cooking mexican. He probably quotes Marshall McLuhan too much, and eats too many salt and vinegar chips.",
						"LastName": "Crothers",
						"Interests": "Design thinking, graphic facilitation, art and painting, beer brewing, playing guitar, learning French, trying to cook Mexican food.",
						"EmailAddress": "bcrothers@atlassian.com",
						"Title": "Principal Design Strategist",
						"AvailableforMeeting": "True",
						"FirstName": "Ben",
						"Industry": "Software and internet",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/bencrothers",
						"LinkedIn": "https://www.linkedin.com/in/bencrothers",
						"Blog": "http://bencrothers.com/",
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114217",
						"LastUpdated": "6/12/2015 5:50:04 PM",
						"ID": "114217"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 4-6",
					"Capacity": "225"
				},
				"Id": "28246",
				"Description": "Continuous integration is the lifeblood of any software house and is extremely important in a fast growing organization like Atlassian. Join team lead Peter Leschev to hear how the Build Engineering team at Atlassian have scaled the infrastructure, team, and Bamboo over their 4-year journey of continuous improvement to provide a build platform that supports the 5000+ builds Atlassian developers run every day.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 12:00:00 AM",
					"Label": "Breakout 6",
					"StartTime": "11/4/2015 11:30:00 PM"
				},
				"Track": {
					"Title": "Scale",
					"Description": "Scale"
				},
				"LastModified": "9/18/2015 1:58:43 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Bamboo",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Scaling to 150,000 Builds a Month... and Beyond",
				"Speakers": [
					{
						"AttendeeID": "vcx5fQgD3I3QPazxGJFb",
						"Biography": "Father of 3, Husband, Atlassian",
						"LastName": "Leschev",
						"EmailAddress": "pleschev@atlassian.com",
						"Title": "Senior Development Team Lead",
						"AvailableforMeeting": "False",
						"FirstName": "Peter",
						"Roles": [
							"Speaker"
						],
						"Company": "Atlassian",
						"LastUpdated": "8/25/2015 9:38:59 PM",
						"ID": "112841"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 13-15",
					"Capacity": "225"
				},
				"Id": "28279",
				"Description": "Spotify is one of the largest online music streaming company in the world and they've been growing extremely fast. Like, \"600% growth in three years\" fast. So put your headphones away and listen to Rick Wacey tell the story of how his team went from managing requests by email to using JIRA and JIRA Service Desk. You'll get tips on coping with increased demand from employees (while still providing great service, of course), and see how to set up your own internal Genius Bar kiosk for IT issues. Get here early and grab a seat – this one goes to 11.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 9:45:00 PM",
					"Label": "Breakout 3",
					"StartTime": "11/4/2015 9:15:00 PM"
				},
				"Track": {
					"Title": "Service",
					"Description": "Service"
				},
				"LastModified": "9/18/2015 1:43:49 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Service Desk,Confluence",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					},
					{
						"Value": "Intermediate",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "How Spotify IT Supports a Company in Hypergrowth",
				"Speakers": [
					{
						"AttendeeID": "9dJ1rw7sfzp18CbkO9qY",
						"Biography": "Rick Wacey moved to Stockholm, Sweden from the UK in November 2011 to be part of Spotify’s growing internal IT team. As a service owner, Rick is responsible for upholding service levels within the IT support team, which has proved a challenging task during Spotify’s substantial growth.",
						"LastName": "Wacey",
						"Interests": "Gadgets, Video Games, Travelling ",
						"EmailAddress": "rick@spotify.com",
						"Title": "Service Owner",
						"AvailableforMeeting": "True",
						"FirstName": "Rick",
						"Industry": "Tech",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/rickwacey",
						"LinkedIn": "https://se.linkedin.com/in/rickwacey",
						"Company": "Spotify",
						"LastUpdated": "6/15/2015 7:50:36 AM",
						"ID": "113057"
					},
					{
						"AttendeeID": "pWuh7GTL7bUeeQ2xqec4",
						"Biography": "Daniel Vazquez is part of Spotify’s internal IT team based in Stockholm, after moving to Sweden from Spain in June 2014. He helps employees with tricky technical support problems and drives forward new ideas and initiatives to keep the company moving at speed during phenomenal growth.\r\n",
						"LastName": "Vázquez",
						"Interests": "Technology, soccer, traveling, food, good beer and of course, music! ",
						"EmailAddress": "vazquez@spotify.com",
						"Title": "IT Engineer",
						"AvailableforMeeting": "True",
						"FirstName": "Daniel",
						"Industry": "Music, Technology",
						"Roles": [
							"Speaker"
						],
						"LinkedIn": "https://se.linkedin.com/in/danielvazquezr",
						"Company": "Spotify",
						"LastUpdated": "5/26/2015 7:00:54 AM",
						"ID": "113061"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 13-15",
					"Capacity": "225"
				},
				"Id": "28419",
				"Description": "Learn how the Electric Reliability Council of Texas was able to replace multiple tools with a complete IT change management system running on JIRA. It provides end users a single point of entry for any hardware or software change, as well as saves the company millions by eliminating maintenance contracts on the old tools. What you learn may \"shock\" you – in the best possible way, of course.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 10:30:00 PM",
					"Label": "Breakout 4",
					"StartTime": "11/4/2015 10:00:00 PM"
				},
				"Track": {
					"Title": "Service",
					"Description": "Service"
				},
				"LastModified": "9/18/2015 1:44:17 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "IT Change Management Using JIRA",
				"Speakers": [
					{
						"AttendeeID": "47jZTWceoalFiS8rQxad",
						"Biography": "With more than fifteen years of system design, development, and support experience in the\r\ninformation technology field, Mike Whitlock provides a great deal of experience and knowledge to ERCOT.  He is a graduate of Indiana University.",
						"LastName": "Whitlock",
						"EmailAddress": "mwhitlock@ercot.com",
						"Title": "Lead Applications Developer",
						"AvailableforMeeting": "True",
						"FirstName": "Mike",
						"Roles": [
							"Speaker"
						],
						"Company": "Electric Reliability Council of Texas (ERCOT)",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/113237",
						"LastUpdated": "6/17/2015 3:31:05 PM",
						"ID": "113237"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 4-6",
					"Capacity": "225"
				},
				"Id": "28492",
				"Description": "When you have a JIRA instance with thousands of users and multiple plugins, you want your users to have seamless experience. Discover how our log analyzer tool gives you crystal ball-like insight into potential issues and overall performance, and how you can interpret performance graphs to make informed decisions and plan ahead.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 9:45:00 PM",
					"Label": "Breakout 3",
					"StartTime": "11/4/2015 9:15:00 PM"
				},
				"Track": {
					"Title": "Scale",
					"Description": "Scale"
				},
				"LastModified": "9/18/2015 1:57:42 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Knowledge is Power: Visualizing JIRA's Performance Data",
				"Speakers": [
					{
						"AttendeeID": "kY5TZlA4OFX1HGK6bXTu",
						"Biography": "Luke Durkan has only recently begun his journey at Atlassian as a Graduate Software Developer, jumping straight into looking at Enterprise scale issues in JIRA and improving tools to help determine potential performance issues for customers. ",
						"LastName": "Durkan",
						"EmailAddress": "ldurkan@atlassian.com",
						"Title": "Graduate Software Developer",
						"AvailableforMeeting": "False",
						"FirstName": "Luke",
						"Industry": "Information Technology",
						"Roles": [
							"Speaker"
						],
						"Company": "Atlassian",
						"LastUpdated": "6/15/2015 5:11:12 AM",
						"ID": "113697"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate C",
					"Capacity": "475"
				},
				"Id": "28497",
				"Description": "Atlassian has been in hyper-growth for the last 5 years, exploding from 200 employees to over 1700. We've worked tirelessly to implement strategic planning while staying true to our agile roots and upholding our culture and values. To the surprise of no-one, it ain't easy.\r\n\r\nLearn about three practices we developed – and scaled – to help our teams deliver more compelling stories, and the strategic framework they all feed into.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 9:30:00 PM",
					"Label": "Breakout 8",
					"StartTime": "11/5/2015 9:00:00 PM"
				},
				"Track": {
					"Title": "Innovate",
					"Description": "Innovate"
				},
				"LastModified": "9/18/2015 1:13:28 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Agile,Confluence,HipChat",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Balancing Act: Large-Scale Strategy vs. Agile Methodologies at Atlassian",
				"Speakers": [
					{
						"AttendeeID": "1hva3WYkmh5xzY6lVfWv",
						"Biography": "Wendell Keuneman has 20 years of experience in software engineering and product management. He is currently the Group Product Manager (Collaboration) for Atlassian that includes overseeing Confluence and HipChat used and loved by over 10 million users worldwide. Previously he was the Business Manager for OnDemand at Atlassian. He has expertise in product strategy, user experience, ecosystems, mobile, cloud, and technology commercialisation through startup engagements and coaching. He holds a Masters of Information Technology.\r\nWendell has an entrepreneurial background with a passion to translate customer and market insights to shape the creative process. This has resulted in several creative product roles including co-founding a multi-product startup whose applications are deployed by hundreds of carriers to millions of users around the world and later acquired by BroadSoft (NASDAQ:BSFT) a global provider of Unified Communications and Collaboration (UCC) services to operators.",
						"LastName": "Keuneman",
						"EmailAddress": "wkeuneman@atlassian.com",
						"Title": "Group Product Manager",
						"AvailableforMeeting": "True",
						"FirstName": "Wendell",
						"Industry": "Computer Software",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/wendo",
						"LinkedIn": "https://www.linkedin.com/in/wendellk",
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/113701",
						"LastUpdated": "6/13/2015 3:42:54 AM",
						"ID": "113701"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate B",
					"Capacity": "575"
				},
				"Id": "28498",
				"Description": "Atlassian faces the same issues as any other software company in the world. The battle for continuous integration glory is fought every day, and at stake is nothing less than our development and delivery speed. Join us to find out how we do it at Atlassian, powered by Bamboo. Because in the Game of Codes, you win... or you die.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 11:15:00 PM",
					"Label": "Breakout 5",
					"StartTime": "11/4/2015 10:45:00 PM"
				},
				"Track": {
					"Title": "Build",
					"Description": "Build"
				},
				"LastModified": "9/22/2015 4:07:06 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Intermediate",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "Game of Codes: the Battle for CI",
				"Speakers": [
					{
						"AttendeeID": "aQtiTasZoedQOQkayhVe",
						"Biography": "I was born in Spain, and spent almost all my life there enjoying the relaxing lifestyle of my beautiful Andalusia. I decided to move to London due to work, and while I was there Atlassian came on a bus to Steal 15 Developers in 15 Days, one of our most successful recruitment campaigns ever. Lucky me, I was one of them. I got on the bus and moved to Sydney with my husband, around 3 years ago - where I have been enjoying the relaxing Aussie lifestyle. I guess it's normal I feel like home here.\r\nI am a Bamboo developer, and I am passionate about CI and CD, and about collaboration in general when building software.",
						"LastName": "Asenjo",
						"Interests": "Photography, Street Art, Interior Design.",
						"EmailAddress": "easenjo@atlassian.com",
						"Title": "Software Developer",
						"AvailableforMeeting": "True",
						"FirstName": "Esther",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/Sthreo",
						"LinkedIn": "https://www.linkedin.com/in/easenjo",
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/113702",
						"LastUpdated": "9/22/2015 4:07:06 AM",
						"ID": "113702"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 1-3",
					"Capacity": "225"
				},
				"Id": "28507",
				"Description": "Think agile is just for software shops? Think again. NetApp will show you how they helped a digital marketing department at a Fortune 500 company move from waterfall to agile – with Atlassian tools, of course. The result? The department pulled off a complete overhaul of their website in just seven weeks (a project that was originally forecasted at 3-6 months)!\r\n\r\nJim and Samuel will share how they built a case for agile, and how the transition provided a more efficient workflow for the rank n' file while giving management more visibility in to projects. ",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 10:15:00 PM",
					"Label": "Breakout 9",
					"StartTime": "11/5/2015 9:45:00 PM"
				},
				"Track": {
					"Title": "Interact",
					"Description": "Interact"
				},
				"LastModified": "9/18/2015 1:35:57 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Agile,Confluence,Marketplace add-ons",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					},
					{
						"Value": "Basic",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "How to Take a Business Team from Waterfall to Agile",
				"Speakers": [
					{
						"AttendeeID": "WLjbSIjVHcHH21fXrMDk",
						"Biography": "Jim Rabon is a Senior Manager at NetApp, a Fortune 500 Computer Storage and Data Management company based in Sunnyvale, California.  Jim works out of the Research Triangle Park offices in Raleigh, NC.  Shortly after joining NetApp, Jim was inspired by the potential of Agile and Atlassian tools and how they could be leveraged to improve project process and operations.  Jim is a leader within NetApp's Digital Marketing team, who championed iterative and collaborative approaches to rebrand and redesign the global netapp.com Website in seven weeks. Jim lead the team in making technology and project process improvements that would influence all of Digital marketing. Prior to joining NetApp, Jim worked in various capacities with the Web at IBM, Lenovo, and Cisco.",
						"LastName": "Rabon",
						"EmailAddress": "rabon@netapp.com",
						"Title": "Senior Manager, Web Development, Digital Marketing",
						"AvailableforMeeting": "False",
						"FirstName": "Jim",
						"Industry": "Computer Storage and Data Management",
						"Roles": [
							"Speaker"
						],
						"Website": "http://www.netapp.com",
						"Company": "NetApp, Inc.",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/113947",
						"LastUpdated": "6/13/2015 1:23:23 AM",
						"ID": "113947"
					},
					{
						"AttendeeID": "TEMP-625385629",
						"LastName": "Wong",
						"EmailAddress": "samuel.wong@netapp.com",
						"AvailableforMeeting": "False",
						"FirstName": "Samuel",
						"Roles": [
							"Speaker"
						],
						"Company": "NetApp, Inc",
						"LastUpdated": "8/14/2015 9:29:04 PM",
						"ID": "118481"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 1-3",
					"Capacity": "225"
				},
				"Id": "28509",
				"Description": "Don't you love those \"gifts that keep on giving\"? JAMF Software does. They moved their 400+ employees from Microsoft Lync to HipChat for ease of use and UX design. Only after making the switch did they discover the transformative impact of HipChat’s open API. (boom) JAMF's Bryson Tyrrell will share his experience building custom plug-ins that meet his team's needs for internal support via HipChat and JIRA Service Desk, and show how you can do it, too.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 6:45:00 PM",
					"Label": "Breakout 1",
					"StartTime": "11/4/2015 6:15:00 PM"
				},
				"Track": {
					"Title": "Interact",
					"Description": "Interact"
				},
				"LastModified": "9/18/2015 1:36:57 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Service Desk,HipChat",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Usability, Extensibility & Fun: the Switch from Lync to HipChat",
				"Speakers": [
					{
						"AttendeeID": "Eu1LkVe9feZq9IPcDT8P",
						"Biography": "Bryson Tyrrell is an IT professional from the Twin Cities. He works at JAMF Software, creators of the Casper Suite, and uses their product internally to manage and support the organization's OS X and IOS devices. ",
						"LastName": "Tyrrell",
						"EmailAddress": "bryson.tyrrell@jamfsoftware.com",
						"Title": "Desktop Services Specialist",
						"AvailableforMeeting": "True",
						"FirstName": "Bryson",
						"Industry": "Software",
						"Roles": [
							"Speaker"
						],
						"Website": "https://bryson3gps.wordpress.com/",
						"Twitter": "https://twitter.com/bryson3gps/",
						"LinkedIn": "https://www.linkedin.com/in/brysontyrrell",
						"Blog": "https://bryson3gps.wordpress.com/",
						"Company": "JAMF Software",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/113948",
						"LastUpdated": "6/22/2015 8:20:39 PM",
						"ID": "113948"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 13-15",
					"Capacity": "225"
				},
				"Id": "28512",
				"Description": "Faced with a challenge of not getting the right information from email exchanges, Sotheby's decided to invest in a new service desk tool. Now here's the plot twist: they use JIRA Service Desk for anything from hanging a multi-million dollar painting in one of their galleries to replenishing the coffee in the kitchen. If that blows your mind a little bit, you won't want to miss this session! Join Nathan Smith as he shows how Sotheby's set up JIRA Service Desk to support 500 employees across the globe and how your team can take their use of JIRA Service Desk from conventional to sensational. ",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 11:00:00 PM",
					"Label": "Breakout 10",
					"StartTime": "11/5/2015 10:30:00 PM"
				},
				"Track": {
					"Title": "Service",
					"Description": "Service"
				},
				"LastModified": "9/18/2015 1:43:22 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Basic",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					},
					{
						"Value": "JIRA,JIRA Service Desk",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "How Sotheby's Uses JIRA Service Desk to Sell Million-Dollar Artwork",
				"Speakers": [
					{
						"AttendeeID": "yTMxpvXjDZFfvo8hB9oI",
						"Biography": "An Englishman in New York, I have been working in IT for close to 20 years.\r\n\r\nA specialist in SAP, but became heavily involved in the deployment of Jira Service Desk to Operations and Facilities departments at Sotheby's",
						"LastName": "Smith",
						"Interests": "Football (soccer), music (primarily The Wheels On The Bus) ",
						"EmailAddress": "nathan.smith@sothebys.com",
						"Title": "Assistant Vice President, IT Application Development",
						"AvailableforMeeting": "False",
						"FirstName": "Nathan",
						"Industry": "Auction",
						"Roles": [
							"Speaker"
						],
						"Company": "Sothebys",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/113951",
						"LastUpdated": "5/13/2015 5:00:59 AM",
						"ID": "113951"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 10-12",
					"Capacity": "225"
				},
				"Id": "28566",
				"Description": "From simple integrations to rich interactions, HipChat's integration platform, powered by Atlassian Connect, supports it all! Tanguy will walk you through HipChat's integration capabilities and how they're used in real life, as well as give you a glimpse into the future of messaging-powered apps. You'll learn how the HipChat team reimagined its integration platform and get a look at some of the 60+ 3rd party integrations that turn HipChat into your team's notification center. ",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 7:45:00 PM",
					"Label": "Breakout 2",
					"StartTime": "11/4/2015 7:00:00 PM"
				},
				"Track": {
					"Title": "Enhance",
					"Description": "Enhance"
				},
				"LastModified": "9/18/2015 2:05:28 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "HipChat,Marketplace add-ons",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Connecting HipChat to (allthethings)",
				"Speakers": [
					{
						"AttendeeID": "WCgkjtQwGJVnnhobPCPR",
						"Biography": "Tanguy Crusson is a Product Manager for HipChat. He is popular as a presenter/educator in the API space, and Atlassian's customer centric approach to Product Management. \r\n\r\nHis teams get nervous whenever he starts hacking code - but not for the reasons he thinks.\r\nA geek at heart, in his free time you'll find him scaling cliffs or deep Freediving.",
						"LastName": "Crusson",
						"EmailAddress": "tcrusson@atlassian.com",
						"Title": "Product Manager, HipChat",
						"AvailableforMeeting": "False",
						"FirstName": "Tanguy",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/tc_saas",
						"LinkedIn": "https://www.linkedin.com/profile/view?id=AAMAAAAMTwQBX8gMBDwwJ_2ROWmQBJA2yWAbbRA",
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114052",
						"LastUpdated": "6/12/2015 7:41:40 AM",
						"ID": "114052"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 13-15",
					"Capacity": "225"
				},
				"Id": "28567",
				"Description": "In 2015 BAE Systems implemented JIRA Service Desk for 4,700 staff and 250 agents across IT, human resources and business improvement. But a successful project isn't just about technology – that's actually the easy part! Since this affected every employee at every level, they had to nail the people, process, and internal marketing, too. JIRA Service Desk is saving BAE Systems $600,000 this year and $1.62m over 5 years. Join Greg Warner as he takes you through the journey from the initial pitch to ongoing support, and highlights what your team needs to know for a successful implementation. ",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 11:15:00 PM",
					"Label": "Breakout 5",
					"StartTime": "11/4/2015 10:45:00 PM"
				},
				"Track": {
					"Title": "Service",
					"Description": "Service"
				},
				"LastModified": "9/18/2015 1:42:48 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Service Desk,Confluence,Confluence Questions",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "From Go to Whoa: How to Make a Difference with JIRA Service Desk",
				"Speakers": [
					{
						"AttendeeID": "RBaXlLRacxqb7kz2GSZg",
						"Biography": "Greg has over 15 years experience in the IT industry, 10 of those in the defence. Greg leads a team responsible for the support and development of software and systems engineering tools. Recently that included implementing JIRA Service Desk across the Australian business. A daunting but most rewarding project. Greg is passionate to see that his customers are productive and making awesome products with the systems and tools supported by his team.",
						"LastName": "Warner",
						"EmailAddress": "greg.warner@baesystems.com",
						"Title": "Team Leader, Information Management & Technology",
						"AvailableforMeeting": "True",
						"FirstName": "Greg",
						"Industry": "Defense, security and aerospace.",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/sabre1817",
						"LinkedIn": "http://au.linkedin.com/in/warnergreg",
						"Company": "BAE Systems",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114053",
						"LastUpdated": "6/13/2015 2:17:27 AM",
						"ID": "114053"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate C",
					"Capacity": "475"
				},
				"Id": "28569",
				"Description": "Join ten-year Atlassian veteran Chris Mountford as he takes you behind the scenes for an inside look at how we make software. You'll get a sneak peek into the culture, methods and techniques that Atlassian uses to develop software across three continents and more than a dozen products. Ask your toughest questions and get tips and tricks to take back to your team.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 11:15:00 PM",
					"Label": "Breakout 5",
					"StartTime": "11/4/2015 10:45:00 PM"
				},
				"Track": {
					"Title": "Innovate",
					"Description": "Innovate"
				},
				"LastModified": "10/6/2015 1:22:29 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Agile,Confluence,HipChat,Bitbucket / Stash / SourceTree,Bamboo",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "The Inside Story of how Atlassian Makes Software",
				"Speakers": [
					{
						"AttendeeID": "dgfrV5Vjpei2ryJ6qAfw",
						"Biography": "",
						"LastName": "Mountford",
						"EmailAddress": "chris@atlassian.com",
						"Title": "",
						"AvailableforMeeting": "False",
						"FirstName": "Chris",
						"Industry": "",
						"Roles": [
							"Speaker"
						],
						"Company": "Atlassian",
						"LastUpdated": "6/9/2015 3:04:23 PM",
						"ID": "114055"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate B",
					"Capacity": "575"
				},
				"Id": "28571",
				"Description": "Your team has adopted Git, and are happily coding along. But is that all? Can you do more with it? You bet! Join the always-animated Nicola Paolucci to learn advanced techniques for grafting multiple repositories, managing project dependencies with git subtree, splitting commits, and finding the best merge strategy for your staging servers. \r\n\r\nIf you've ever wondered how to collate the histories of different projects, or how to split a sub-directory into it's own project without destroying its history, this session is for you. ",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 10:15:00 PM",
					"Label": "Breakout 9",
					"StartTime": "11/5/2015 9:45:00 PM"
				},
				"Track": {
					"Title": "Build",
					"Description": "Build"
				},
				"LastModified": "9/22/2015 4:07:06 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"Title": "Advanced Git Techniques: Grafting, Octopuses and Other Fun Stuff",
				"Speakers": [
					{
						"AttendeeID": "iBKcHBgKJtkDZyUGRcG8",
						"Biography": "Nicola is an all-round hacker who loves exploring and teaching bleeding\r\nedge technologies. He writes and talks about Git, development workflows, code collaboration and more recently about Docker.\r\n\r\nPrior to his current role as Developer Instigator at Atlassian he lead software teams, built crowd sourcing applications for geo-spacial data, worked on huge e-commerce deployments.\r\n\r\nLittle known facts about Nicola: he gesticulates a lot while speaking (being\r\nItalian), lives in Amsterdam and rides a Ducati.",
						"LastName": "Paolucci",
						"EmailAddress": "Email is private",
						"Title": "Developer Advocate",
						"AvailableforMeeting": "True",
						"FirstName": "Nicola",
						"Roles": [
							"Speaker"
						],
						"Twitter": "http://twitter.com/durdn",
						"Blog": "http://developer.atlassian.com/blog",
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114056",
						"LastUpdated": "9/22/2015 4:07:06 AM",
						"ID": "114056"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate B",
					"Capacity": "575"
				},
				"Id": "28572",
				"Description": "Docker is rapidly gaining mind-share amongst both operations and development teams. One of Docker's sweet spots is in the area of rapid development and testing. This talk will show how we use Docker containers to resolve dependency problems in tests, and how to integrate this workflow into a Bamboo continuous integration and deployment pipeline. ",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 10:30:00 PM",
					"Label": "Breakout 4",
					"StartTime": "11/4/2015 10:00:00 PM"
				},
				"Track": {
					"Title": "Build",
					"Description": "Build"
				},
				"LastModified": "9/22/2015 4:07:06 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Bamboo",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Docker, Continuous Integration, and You",
				"Speakers": [
					{
						"AttendeeID": "zbod8ozspiyM3cBnwb1j",
						"Biography": "Steve Smith has worked at Atlassian for over 9 years, both as a sysadmin and a developer. Prior to that he worked on tanks and radars in the Outer Hebrides, telecoms systems in Hong Kong, and in startups in Australia. He now works out of Atlassian's UK offices, focusing on high-availability, continuous-deployment and platform migration issues.",
						"LastName": "Smith",
						"EmailAddress": "ssmith@atlassian.com",
						"Title": "Developer Advocate",
						"AvailableforMeeting": "False",
						"FirstName": "Steve",
						"Roles": [
							"Speaker"
						],
						"Company": "Atlassian",
						"LastUpdated": "9/22/2015 4:07:06 AM",
						"ID": "114058"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 4-6",
					"Capacity": "225"
				},
				"Id": "28624",
				"Description": "As a rapidly growing company, Vanderlande needed to improve how its 3,000 global employees found, shared, and contributed to information. Learn how they consolidate information, curate content, and get new staff members up to speed using Confluence. You'll walk away with tips for implementing Confluence on a large scale, avoiding the pitfalls, and getting people into the system.",
				"IsFeatured": "False",
				"Links": "<br>",
				"TimeSlot": {
					"EndTime": "11/5/2015 10:15:00 PM",
					"Label": "Breakout 9",
					"StartTime": "11/5/2015 9:45:00 PM"
				},
				"Track": {
					"Title": "Scale",
					"Description": "Scale"
				},
				"LastModified": "9/18/2015 1:56:43 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Confluence",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					},
					{
						"Value": "Basic",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "Growing Fast and Staying Sane with Confluence",
				"Speakers": [
					{
						"AttendeeID": "Iii7paiU5hhIElNhMTKI",
						"Biography": "How to get the most out of IT Tooling? Help people in doing their jobs with less efforts. Those are topics that I'm working on as a Process Information Manager.\r\nWith over 15 years of experience in Education, Communication and IT I'm able to make connections, create opportunities and help people and organisations to grow.\r\n\r\nSpecialties: Knowledge Management, Information Management, Education, E-Learning, Social Media, Communication, Creating Connections\r\n\r\nWould you like to know more? Don't hesitate to contact me!",
						"LastName": "den Blijker",
						"Interests": "Businesswise I'm interested in topics that are related to IT, Communication and Education. How to get the most out of IT tooling to help people in doing their jobs with less efforts.\r\n\r\nPrivately I like cooking, I love my bicycle (hey I'm Dutch ;-), and I hope to see a lot of countries outside the Netherlands in my life.",
						"EmailAddress": "lyset.den.blijker@vanderlande.com",
						"Title": "Process Information Manager",
						"AvailableforMeeting": "True",
						"FirstName": "Lyset",
						"Industry": "Technical: Automated Material Handling Solutions",
						"Roles": [
							"Speaker"
						],
						"Website": "http://www.vanderlande.com",
						"Twitter": "https://twitter.com/den_blijker",
						"LinkedIn": "https://nl.linkedin.com/in/lyset",
						"Company": "Vanderlande",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114151",
						"LastUpdated": "8/15/2015 11:23:33 AM",
						"ID": "114151"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 1-3",
					"Capacity": "225"
				},
				"Id": "28629",
				"Description": "Convincing a diverse group of highly-motivated people to get behind a single knowledge management strategy can be a challenge. Especially when everyone has their own opinions on how best to document a complex product.\r\n\r\nThis session will show you examples of Braintree's content management successes (and failures) to illustrate the four steps we used to kickstart a documentation-minded culture with Confluence at its core. You'll also hear what's next on the documentation team's agenda and how we plan to continue our mission of domination through documentation. ",
				"IsFeatured": "False",
				"Links": "<br>",
				"TimeSlot": {
					"EndTime": "11/4/2015 7:45:00 PM",
					"Label": "Breakout 2",
					"StartTime": "11/4/2015 7:00:00 PM"
				},
				"Track": {
					"Title": "Interact",
					"Description": "Interact"
				},
				"LastModified": "9/18/2015 1:34:45 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Confluence,Marketplace add-ons",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Documentation Domination: How to Build a Knowledge Sharing Culture in 4 Easy Steps",
				"Speakers": [
					{
						"AttendeeID": "kW7A9DsQu9UIWDBz2Dlq",
						"Biography": "Carolyn comes from an eclectic professional background, with a passion for writing, teaching, and collaboration. At Braintree, her primary focus is the maintenance and development of the public-facing support articles—spending most of her time making complex topics more digestible. In her free time, she's an actor, performer, and lover of good food.  ",
						"LastName": "Sinon",
						"Interests": "Interests are private",
						"EmailAddress": "Email is private",
						"Title": "Content Manager",
						"AvailableforMeeting": "False",
						"FirstName": "Carolyn",
						"Industry": "Industry is private",
						"Roles": [
							"Speaker"
						],
						"Website": "http://articles.braintreepayments.com",
						"Twitter": "Twitter is private",
						"Facebook": "Facebook is private",
						"Blog": "Blog is private",
						"Company": "Braintree",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114134",
						"LastUpdated": "6/16/2015 6:37:36 PM",
						"ID": "114134"
					},
					{
						"AttendeeID": "nRRRhLlaZBOjUfB1aCNM",
						"Biography": "Liz started at Braintree in 2011 and discovered her passion for good documentation as part of the support team. She now leads a group of technical communicators on a mission to improve collaboration between Operations and Product teams. She also recently earned her Master’s in Global Technology and Development.",
						"LastName": "Gray",
						"Interests": "Travel, all forms of discussion about media and technology in zones of conflict, wine, and, unsurprisingly, cats.",
						"EmailAddress": "liz@braintreepayments.com",
						"Title": "Knowledge Management Lead",
						"AvailableforMeeting": "False",
						"FirstName": "Elisabeth",
						"Industry": "Payments",
						"Roles": [
							"Speaker"
						],
						"Website": "http://elisabethgray.me/",
						"Twitter": "https://twitter.com/elcgray",
						"LinkedIn": "https://www.linkedin.com/in/elcgray",
						"Facebook": "https://www.facebook.com/elcgray",
						"Company": "Braintree",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114150",
						"LastUpdated": "6/16/2015 5:46:25 PM",
						"ID": "114150"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 10-12",
					"Capacity": "225"
				},
				"Id": "28631",
				"Description": "With over 2.5 million users, Bitbucket is an exciting platform to integrate with. Between static analysis, deployment, testing, and monitoring tools, there are amazing opportunities to create an uber-rich development experience. So join veteran Atlassian developer Tim Pettersen as he shows off Bitbucket's brand new integration framework and highlight best practices for building scalable, tightly integrated services that developers will love. ",
				"IsFeatured": "False",
				"Links": "<br>",
				"TimeSlot": {
					"EndTime": "11/4/2015 6:45:00 PM",
					"Label": "Breakout 1",
					"StartTime": "11/4/2015 6:15:00 PM"
				},
				"Track": {
					"Title": "Enhance",
					"Description": "Enhance"
				},
				"LastModified": "9/18/2015 2:04:28 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Bitbucket / Stash / SourceTree,Marketplace add-ons",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Building Kick-ass Tools for Millions of Developers with Bitbucket",
				"Speakers": [
					{
						"AttendeeID": "MN7m6QPUO514YCdyv5Vf",
						"Biography": "I served a few years as a developer on the JIRA, FishEye/Crucible and Stash teams. In early 2013 I switched to Developer Relations to get out from behind the keyboard and meet some developers. These days I speak and blog about developer workflows, git, continuous integration/deployment and Atlassian's developer tools: especially Bitbucket & Stash. I regularly tweet about these things and other Atlassian arcana as @kannonboy.",
						"LastName": "Pettersen",
						"Interests": "Bitbucket, Stash, git, Java, JavaScript, Android, IOT",
						"EmailAddress": "tim@atlassian.com",
						"Title": "Developer Provocateur",
						"AvailableforMeeting": "True",
						"FirstName": "Tim",
						"Industry": "Software",
						"Roles": [
							"Speaker",
							"Grader"
						],
						"Website": "https://www.atlassian.com",
						"Twitter": "https://twitter.com/kannonboy",
						"LinkedIn": "https://www.linkedin.com/profile/view?id=12889971",
						"Blog": "https://developer.atlassian.com/blog/authors/tpettersen",
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/113234",
						"LastUpdated": "5/26/2015 10:18:59 PM",
						"ID": "113234"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 13-15",
					"Capacity": "225"
				},
				"Id": "28635",
				"Description": "A crucial transition is taking place at Atlassian... we can feel our DNA evolving a little each day. Our focus is always on the future, and that future will mean rallying behind a cloud-first strategy. In doing so, we have the unique opportunity to re-imagine the way we run our services and get behind a modern approach to distribute our operations function and optimize for scale. \r\n\r\nThis talk will cover the steps we've taken on that journey as we build site reliability engineering, an operations approach pioneered by industry champs like Netflix. We'll talk about the concept, how it applies at Atlassian, the wins we have achieved, and learning you can bring back to your team. ",
				"IsFeatured": "False",
				"Links": "<br>",
				"TimeSlot": {
					"EndTime": "11/5/2015 10:15:00 PM",
					"Label": "Breakout 9",
					"StartTime": "11/5/2015 9:45:00 PM"
				},
				"Track": {
					"Title": "Service",
					"Description": "Service"
				},
				"LastModified": "9/18/2015 1:45:15 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Service Desk",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Love DevOps? Wait 'Till You Meet SRE",
				"Speakers": [
					{
						"AttendeeID": "OevnEellPoKIZhWvAC2e",
						"Biography": "Nick Wright is the head of Site Reliability Engineering at Atlassian. With 10 years’ experience in IT operations roles in New Zealand, UK and Australia, Nick’s focus is on making sure our cloud applications and infrastructure are performing top notch, and is keen to share how we do it while scaling fast. \r\n\r\nOutside of work, he’s an avid cyclist and gamer. He’s married and has a lovely little girl.\r\n\r\n\r\n\r\n\r\n",
						"LastName": "Wright",
						"EmailAddress": "nwright@atlassian.com",
						"Title": "Site Reliability Engineering Manager",
						"AvailableforMeeting": "False",
						"FirstName": "Nick",
						"Roles": [
							"Speaker"
						],
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114159",
						"LastUpdated": "5/26/2015 7:01:05 AM",
						"ID": "114159"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 13-15",
					"Capacity": "225"
				},
				"Id": "28637",
				"Description": "CSIRO, Australia's national science agency, embarked on a transformation program in 2013 to modernize its service centers. Their goal? Make an impact on scientific innovation throughout the country. But their IT department had an additional challenge: repair their long-damaged relationship with CSIRO's business teams. That's where JIRA Service Desk comes in.\r\n\r\nTaking an agile approach, IT built a repeatable process using self-service portals that helped business teams do their work faster. A process so successful, they've now rolled it out to nine business teams (and counting!). Join Norman and Sarah as they explain what they did, and what your team can learn from it.",
				"IsFeatured": "False",
				"Links": "<br>",
				"TimeSlot": {
					"EndTime": "11/5/2015 12:00:00 AM",
					"Label": "Breakout 6",
					"StartTime": "11/4/2015 11:30:00 PM"
				},
				"Track": {
					"Title": "Service",
					"Description": "Service"
				},
				"LastModified": "9/18/2015 1:42:16 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Service Desk,Confluence",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Fast & Furious Service Rollout: Thanks to Agile ",
				"Speakers": [
					{
						"AttendeeID": "ZYWI6eIRE6oqml3eUFFX",
						"Biography": "I work for Australia’s national science agency CSIRO. With more than 5000 people working out of 55 centres in Australia and internationally, CSIRO’s research makes a difference to industry, people and the planet. \r\n\r\nI head a group that develops software products in support of science as well as building line of business workflow solutions using platforms such JIRA, JIRA Service Desk and Confluence.\r\n\r\nI'm responsible for group strategy, execution, capability and portfolio delivery.",
						"LastName": "Spurr",
						"EmailAddress": "norman.spurr@csiro.au",
						"Title": "Executive Manager IMT",
						"AvailableforMeeting": "True",
						"FirstName": "Norman",
						"Industry": "Research",
						"Roles": [
							"Speaker"
						],
						"Website": "http://www.csiro.au",
						"Twitter": "Twitter is private",
						"LinkedIn": "https://au.linkedin.com/pub/norman-spurr/20/1a4/4a2",
						"Facebook": "Facebook is private",
						"Blog": "Blog is private",
						"Company": "CSIRO",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114158",
						"LastUpdated": "6/18/2015 3:43:00 AM",
						"ID": "114158"
					},
					{
						"AttendeeID": "E3RxRUJtRHHAHLk7W1le",
						"Biography": "Sarah Blake, Services Manager - Business Workflow Services, works for Australia’s national science agency, CSIRO. CSIRO uses science to solve real issues. Its research makes a difference to industry, people, and the planet. Her team is responsible for the end to end development of custom solutions to support CSIRO service centres. In the past six months, her team has rolled out service management to 9 teams across CSIRO (Finance, HR, Project Management, etc.) helping them get off email, scale their work and become more productive.",
						"LastName": "Blake",
						"EmailAddress": "sarah.blake@csiro.au",
						"Title": "Service Manager",
						"AvailableforMeeting": "False",
						"FirstName": "Sarah",
						"Roles": [
							"Speaker"
						],
						"Company": "CSIRO",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114164",
						"LastUpdated": "6/13/2015 7:47:49 AM",
						"ID": "114164"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 1-3",
					"Capacity": "225"
				},
				"Id": "28645",
				"Description": "You've probably wondered about it, and they proved it: state-of-the-art software engineering can be taught using the Atlassian stack.\r\n\r\nDora and Lukas will show how Munich's Technical University uses JIRA, Confluence, HipChat, Bitbucket (including Stash), and Bamboo for running an agile software engineering course with 100 participants and 10 industry partners. We'll show you how the tools work hand-in-hand to teach students and reduce our organizational overhead. They'll also show how the stack can be scaled to support interactive lectures with a large number of participants. Ever wondered what happens when 100 students start their own sprint at the same time in the lecture hall? Join this session and find out! ",
				"IsFeatured": "False",
				"Links": "<br>",
				"TimeSlot": {
					"EndTime": "11/5/2015 12:00:00 AM",
					"Label": "Breakout 6",
					"StartTime": "11/4/2015 11:30:00 PM"
				},
				"Track": {
					"Title": "Interact",
					"Description": "Interact"
				},
				"LastModified": "9/18/2015 11:56:10 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Agile,Confluence,Confluence Team Calendars,HipChat,Bitbucket / Stash / SourceTree,Bamboo",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					},
					{
						"Value": "Basic",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "Using Atlassian for State-of-the-Art Computer Science Education",
				"Speakers": [
					{
						"AttendeeID": "ki1lE5pl5idJ9jxgmnrL",
						"Biography": "Lukas Alperowitz studied Information Systems at the Technische Universität München. He has been a member of the chair for Applied Software Engineering since 2014. He is researching about agile software development with a focus on Continuous Delivery of mobile applications. \r\nHe is one of the instructors of the 100 students capstone course and loves to teach real-word software engineering to students. In his free time you can find Lukas on his bike cycling the Munich area or on the road with his backpack.\r\n",
						"LastName": "Alperowitz",
						"Interests": "Teaching, Agile Methods, Continuous Delivery",
						"EmailAddress": "alperowi@in.tum.de",
						"Title": "Doctoral Candidate",
						"AvailableforMeeting": "True",
						"FirstName": "Lukas",
						"Roles": [
							"Speaker"
						],
						"Website": "http://www1.in.tum.de/alperowitz",
						"Company": "Technical University Munich",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114126",
						"LastUpdated": "7/13/2015 4:10:38 PM",
						"ID": "114126"
					},
					{
						"AttendeeID": "boqv3hC9mhu8EFNdoQgu",
						"Biography": "Dora Dzvonyar is a doctoral candidate at the Chair of Applied Software Engineering at the Technical University Munich (TUM). As one of the instructors of a multi-project capstone course for the development of mobile applications, she teaches agile software engineering methods to 100 students in each semester. In particular, she is interested in teaching informal modeling techniques to enhance communication and feedback in software projects with ambiguous or quickly changing requirements. In her free time, she is also a hobby photographer and organizer of TEDxTUM, the local TED conference at her home university.",
						"LastName": "Dzvonyar",
						"EmailAddress": "dora.dzvonyar@tum.de",
						"Title": "Doctoral Candidate",
						"AvailableforMeeting": "True",
						"FirstName": "Dora",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/DzDorie",
						"LinkedIn": "https://linkedin.com/pub/dora-dzvonyar/5a/274/216",
						"Company": "Technical University Munich",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114139",
						"LastUpdated": "6/13/2015 7:20:24 AM",
						"ID": "114139"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate A",
					"Capacity": "525"
				},
				"Id": "28653",
				"Description": "What do software, financial reporting, and digital marketing teams have in common? Process!\r\n\r\nAll teams need to manage their work, and each have their own ways to getting it done. Learn how Janus Capital has implemented JIRA projects for their business teams to increase productivity and traceability. What started with just one team is now spreading across the entire organization!",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 9:30:00 PM",
					"Label": "Breakout 8",
					"StartTime": "11/5/2015 9:00:00 PM"
				},
				"Track": {
					"Title": "Plan",
					"Description": "Plan"
				},
				"LastModified": "9/22/2015 4:06:10 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Agile",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Be the Enabler: JIRA Workflows for Business Teams",
				"Speakers": [
					{
						"AttendeeID": "WriyYrsX6YYgQIRfklim",
						"Biography": "Jordan is responsible for the Software Configuration Management and Release Management processes at Janus. This includes managing the suite of Atlassian products they use. Jordan is a long time Atlassian advocate and loves helping to improve workplace productivity. He holds both a BS in Software Engineering and an MBA in IT Management from WGU.",
						"LastName": "Packer",
						"EmailAddress": "jordan.packer@janus.com",
						"Title": "Senior Configuration Analyst",
						"AvailableforMeeting": "False",
						"FirstName": "Jordan",
						"Industry": "Financial Services",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/Mr_JPac",
						"LinkedIn": "https://www.linkedin.com/in/jordanpacker",
						"Company": "Janus Capital Group",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114147",
						"LastUpdated": "9/22/2015 4:06:10 AM",
						"ID": "114147"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate B",
					"Capacity": "575"
				},
				"Id": "28688",
				"Description": "Nuance Communications is making the move from SVN to Git! Why? To take advantage of its strong branching and merging capabilities... and to keep their developers happy. With wild variations between each product's release model, they have multiple releases of one or more components in play at a given time. So they had to get creative with a branching model. This talk will discuss choosing the right Git branching model for each of your release streams, and managing multiple releases using Bitbucket (including Stash), JIRA, Bamboo, and Maven.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 9:45:00 PM",
					"Label": "Breakout 3",
					"StartTime": "11/4/2015 9:15:00 PM"
				},
				"Track": {
					"Title": "Build",
					"Description": "Build"
				},
				"LastModified": "9/22/2015 4:07:06 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"Title": "Creative Branching Models for Multiple Release Streams",
				"Speakers": [
					{
						"AttendeeID": "LUVIXeDypzb6bBTcbnUZ",
						"Biography": "Matt has been with Nuance since 2005, working across Technical Support and R&D for enterprise healthcare platforms. He occasionally moonlights in Operations when they let him. At present, Matt leads onshore and offshore development teams, and is the Atlassian aficianado, Apple geek, and general dev tools nerd for a good portion of the Nuance Healthcare division. Matt is a big fan of DevOps culture and tools, and has been working with some or all of the Atlassian stack since 2010.",
						"LastName": "Shelton",
						"Interests": "My wife and kids, music, Apple hardware/software, fixing stuff, tabletop games, healthy banter about most anything",
						"EmailAddress": "matt.shelton@nuance.com",
						"Title": "Engineering Manager",
						"AvailableforMeeting": "True",
						"FirstName": "Matt",
						"Industry": "Healthcare",
						"Roles": [
							"Speaker"
						],
						"Website": "http://www.mattshelton.net",
						"Twitter": "https://twitter.com/mattshelton",
						"LinkedIn": "https://www.linkedin.com/in/mattshelton",
						"Company": "Nuance Communications",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114257",
						"LastUpdated": "9/22/2015 4:07:06 AM",
						"ID": "114257"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate B",
					"Capacity": "575"
				},
				"Id": "28692",
				"Description": " The hype and excitement around micro-services and shiny new technologies means we commonly overlook the big, hairy mammoth in the room: legacy monoliths. Comprised mostly of tribal knowledge and spaghetti code, they're hard to work with and deeply understood only by a few experts. So when it's time to update legacy code, these experts become bottlenecks.\r\n\r\nJoin Atlassian developer Peggy Kuo as she provides you with practical ways to help the people on your team understand – and tame – the legacy monolith.  ",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 11:00:00 PM",
					"Label": "Breakout 10",
					"StartTime": "11/5/2015 10:30:00 PM"
				},
				"Track": {
					"Title": "Build",
					"Description": "Build"
				},
				"LastModified": "9/22/2015 4:07:06 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Basic",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "Taming the Legacy Monolith",
				"Speakers": [
					{
						"AttendeeID": "qE9KTtcGxLo1GcNDT999",
						"Biography": "Peggy is a senior developer at Atlassian who has worked across many different teams. From integration to plugins to purchasing, she's had a wide range of experience and is always hungry to learn more. She loves sharing her knowledge and has previously spoken at AtlasCamp, as well as other conferences like JSConfAsia. In her spare time, she loves creating weird and wonderful things, whether in code or in the real world.",
						"LastName": "Kuo",
						"EmailAddress": "pkuo@atlassian.com",
						"AvailableforMeeting": "False",
						"FirstName": "Peggy",
						"Roles": [
							"Speaker"
						],
						"Company": "Atlassian",
						"LastUpdated": "9/22/2015 4:07:06 AM",
						"ID": "114268"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 4-6",
					"Capacity": "225"
				},
				"Id": "28693",
				"Description": "Hans and Franz are here to pump YOU up. Well, pump your Atlassian tools up, that is. Has your JIRA slowed down? Does your Confluence fall over and can't get back up? We're here to give you some tactical tips on how to take your performance to the next level. We'll reveal best practices from Atlassian support's secret arsenal, teach you how to configure JIRA, Confluence, and other products to get the most out of your system, and focus on small and easy changes that you can implement quickly. ",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 11:00:00 PM",
					"Label": "Breakout 10",
					"StartTime": "11/5/2015 10:30:00 PM"
				},
				"Track": {
					"Title": "Scale",
					"Description": "Scale"
				},
				"LastModified": "9/18/2015 4:21:51 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,Confluence",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "10 Tips to Pump Up Your Atlassian Performance",
				"Speakers": [
					{
						"AttendeeID": "w3OLygSkN7AIgy67jIlA",
						"Biography": "Boris has 3.5 years on the front lines of Atlassian's JIRA support team where he has helped everyone from 1 man shops to multinational organizations for whom JIRA is the core of their business. He is the former administrator for Atlassian's support portal (https://support.atlassian.com), has worked over 2500 support tickets, frequently provides feedback on the Atlassian community website (https://answers.atlassian.com), and regularly contributes to Atlassian's knowledge base articles. He is the author of the Confluence Markdown Macro add-on. His career experience spans 11+ years in operations and IT. Feel free to come find him at the Support Bar where he can answer your questions.",
						"LastName": "Berenberg",
						"Interests": "Scuba diving, snowboarding, car racing, photography, and skydiving.",
						"EmailAddress": "bberenberg@atlassian.com",
						"Title": "Support Engineer",
						"AvailableforMeeting": "True",
						"FirstName": "Boris",
						"Roles": [
							"Speaker"
						],
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114229",
						"LastUpdated": "9/18/2015 4:21:51 PM",
						"ID": "114229"
					},
					{
						"AttendeeID": "w1S1w8FDMxzPmapydl8Q",
						"Biography": "Brian Tom is a Senior Support Engineer at Atlassian, and has been with the company for more than two years. With a focus on supporting Confluence and Crowd, he delights in helping customers with everything from basic usage questions, to problematic deployments, to performance improvements, and relishes the challenge that every new issue presents. A technology autodidact, he credits Atlassian for providing him with the amazing opportunity to learn as much as he can to help make customers awesome. In his free time, he practices Capoeira, reads dystopian novels, and recites Monty Python skits to no one in particular. ",
						"LastName": "Tom",
						"EmailAddress": "btom@atlassian.com",
						"Title": "Senior Support Engineer",
						"AvailableforMeeting": "False",
						"FirstName": "Brian",
						"Roles": [
							"Speaker"
						],
						"Company": "Atlassian",
						"LastUpdated": "9/18/2015 4:21:51 PM",
						"ID": "114236"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 10-12",
					"Capacity": "225"
				},
				"Id": "28700",
				"Description": "Discover JIRA Service Desk's brand new built-in automation capabilities that help IT teams work smarter and focus on the Important Stuff™. Automation will become the #1 initiative for IT teams in the next 12-24 months as teams increasingly stay on top of their workload by automating repetitive tasks and notifications with the help of tools like JIRA Service Desk.\r\n\r\nJoin Edward and Clement to learn how easy it is to create and configure rules with the WYSIWYG automation rules editor, use SMS and HipChat messaging to send alerts, and reduce repetitive tasks to get your issues resolved faster than ever.",
				"IsFeatured": "False",
				"Links": "<br>",
				"TimeSlot": {
					"EndTime": "11/5/2015 12:00:00 AM",
					"Label": "Breakout 6",
					"StartTime": "11/4/2015 11:30:00 PM"
				},
				"Track": {
					"Title": "Enhance",
					"Description": "Enhance"
				},
				"LastModified": "9/18/2015 2:04:00 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Advanced",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "Automating JIRA Service Desk : Resolving Your Issues Faster",
				"Speakers": [
					{
						"AttendeeID": "qcBTzyMdybYy99PwDLah",
						"Biography": "Clement Capiaux has 5 years of experience in software development. He is a full stack developer in the JIRA Service Desk product team - the fastest growing Atlassian product helping your IT teams serving their customers. Clement previously worked with an Atlassian Expert - Valiantys - giving him a background helping understand Atlassian customers needs and issues, particularly around SLAs. Attending his 1st Atlassian Summit, the frenchie JIRA Service De[v|sk] welcomes you to come have a chat any time.",
						"LastName": "Capiaux",
						"EmailAddress": "ccapiaux@atlassian.com",
						"Title": "Developer",
						"AvailableforMeeting": "False",
						"FirstName": "Clement",
						"Industry": "Software engineering",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/klemman_",
						"LinkedIn": "https://au.linkedin.com/in/clementcapiaux",
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114238",
						"LastUpdated": "5/27/2015 4:47:07 AM",
						"ID": "114238"
					},
					{
						"AttendeeID": "TEMP-121131390",
						"Biography": "Since starting at Atlassian in 2010 as an intern, Ed has worked on a number of different products within the JIRA ecosystem. He now leads a team in JIRA Service Desk whose goal is to make JSD the market leading IT solution. Come talk to Ed about JIRA, Service Desk, Video Games or MTG.",
						"LastName": "Zhang",
						"EmailAddress": "ezhang@atlassian.com",
						"Title": "Development Team Lead",
						"AvailableforMeeting": "False",
						"FirstName": "Edward",
						"Roles": [
							"Speaker"
						],
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/118044",
						"LastUpdated": "7/23/2015 12:19:17 AM",
						"ID": "118044"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate C",
					"Capacity": "475"
				},
				"Id": "28709",
				"Description": "A PM and a developer walk into a bar... That's a joke all on it's own!\r\n\r\nCreating a team dynamic where engineering and product management compliment each other can feel like a pipe dream. Confluence's Sherif Mansour (Principle Product Manager) and Issac Gerges (Senior Developer) will share, explore, and even attempt to re-enact the familiar anti-patterns faced when product and engineering collaborate. Come for the practical tips, stay for the on-stage antics.",
				"IsFeatured": "False",
				"Links": "<br>",
				"TimeSlot": {
					"EndTime": "11/5/2015 12:00:00 AM",
					"Label": "Breakout 6",
					"StartTime": "11/4/2015 11:30:00 PM"
				},
				"Track": {
					"Title": "Innovate",
					"Description": "Innovate"
				},
				"LastModified": "9/18/2015 1:08:51 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Agile,Confluence,Confluence Questions,HipChat,Bitbucket / Stash / SourceTree",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "A PM and a Developer Walk into a Bar",
				"Speakers": [
					{
						"AttendeeID": "HDpCCFnNQTFlJATohsfk",
						"Biography": "Issac Gerges is a Senior Developer at Atlassian. As a full stack developer on Confluence he’s helped ship key features like blueprints, tasks, and file collaboration. Since moving to HipChat, he's helped rebuild the web client from the ground up and is currently working on HipChat's next generation Desktop clients. For as long as he can remember, he's been hacking on one thing or another. From camera carrying high altitude balloons to racing microcontroller powered POWER WHEELS® cars; Issac’s torn it apart and put it back together with a few screws left over.",
						"LastName": "Gerges",
						"EmailAddress": "igerges@atlassian.com",
						"Title": "Senior Developer, HipChat",
						"AvailableforMeeting": "False",
						"FirstName": "Issac",
						"Industry": "Software",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/gerges",
						"LinkedIn": "https://www.linkedin.com/in/issacgerges",
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114245",
						"LastUpdated": "6/8/2015 5:55:33 PM",
						"ID": "114245"
					},
					{
						"AttendeeID": "6hKKTkeUcYp3jWIShYPI",
						"Biography": "Sherif Mansour has 11 years of experience in software development. He is currently a Principle Product Manager for Atlassian, responsible for Confluence, a popular social collaboration tool for product teams. Sherif also played a key role in developing one of Atlassian’s new products: Team Calendars. Previously, he served as Atlassian’s Cross Product Integration Manager, ensuring a high quality experience for customers. Sherif previously worked in software development for a Web consultancy firm, and for Optus. Areas of expertise include agile product development. Sherif thinks building simple products is hard and so is writing a simple, short bio.",
						"LastName": "Mansour",
						"EmailAddress": "smansour@atlassian.com",
						"Title": "Principle Product Manager",
						"AvailableforMeeting": "True",
						"FirstName": "Sherif",
						"Roles": [
							"Speaker",
							"Grader"
						],
						"Twitter": "http://twitter.com/sherifmansour",
						"LinkedIn": "https://www.linkedin.com/in/sherifmansour",
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114278",
						"LastUpdated": "6/21/2015 11:13:48 AM",
						"ID": "114278"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate B",
					"Capacity": "575"
				},
				"Id": "28723",
				"Description": "When adopting new patterns, we always encounter that moment where we think \"If only I knew this three months ago!\" And if your team is experimenting with microservices, this probably sounds familiar.\r\n\r\nJoin developer Vincent Kok for a sneak peak into the world of microservices at Atlassian. He'll reveal all we've learned along the way: how to arrange, configure and build your code efficiently; deployment and testing; and how to operate effectively in this environment. You'll learn how to apply five simple strategies to your environment straight away. ",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 6:45:00 PM",
					"Label": "Breakout 1",
					"StartTime": "11/4/2015 6:15:00 PM"
				},
				"Track": {
					"Title": "Build",
					"Description": "Build"
				},
				"LastModified": "9/22/2015 4:07:06 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"Title": "5 Things I Wish I'd Known about Microservices",
				"Speakers": [
					{
						"AttendeeID": "LnjOT2EZPklFnZlN5Xee",
						"Biography": "Vincent is Development Manager for Confluence Cloud, looking after performance, deployment and cloud architecture. Before that, he led the team which was responsible for moving Atlassian's identity systems towards a microservice architecture.\r\n\r\nWhen not thinking about software, Vincent likes a good game of lunchtime soccer in the park with fellow Atlassians.",
						"LastName": "Kok",
						"EmailAddress": "vkok@atlassian.com",
						"AvailableforMeeting": "False",
						"FirstName": "Vincent",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/vincentkok",
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114283",
						"LastUpdated": "9/22/2015 4:07:06 AM",
						"ID": "114283"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 13-15",
					"Capacity": "225"
				},
				"Id": "28724",
				"Description": "The Defense Media Activity’s Public web program supports more than 10,000 soldiers, sailors, airmen and Marines globally with the ability to rapidly move news, images and video to worldwide audiences via official military websites and the ability to integrate that content on social media sites.\r\n\r\nIn September 2014, DMA's CIO Leslie Benito realized that to deliver world-class service, DMA needed to transform its Public Web service desk. In this session, Leslie will explain how they completed that within weeks, under stringent security requirements, while improving IT productivity and customer satisfaction. carried out the projects and what we achieved along the way.",
				"IsFeatured": "False",
				"Links": "<br>",
				"TimeSlot": {
					"EndTime": "11/4/2015 7:45:00 PM",
					"Label": "Breakout 2",
					"StartTime": "11/4/2015 7:00:00 PM"
				},
				"Track": {
					"Title": "Service",
					"Description": "Service"
				},
				"LastModified": "10/6/2015 1:38:35 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA Service Desk",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "The Five-Week Transformation: How the Department of Defense’s Public Web Service went from Good to Great",
				"Speakers": [
					{
						"AttendeeID": "TEMP-99440834",
						"Biography": "Leslie D. Benito is the Chief Information Officer and Chief Technical Officer for the Defense Media Activity of the U. S. Department of Defense. He has been the CIO for six months, and prior to that was the director of the DMA’s Public Web Program for seven years, and with the armed forces for more than 15 years.",
						"LastName": "Benito",
						"EmailAddress": "leslie.d.benito.civ@mail.mil",
						"Title": "CIO & CTO",
						"AvailableforMeeting": "False",
						"FirstName": "Leslie",
						"Roles": [
							"Speaker"
						],
						"Company": "Defense Media Activity, US Department of Defense",
						"LastUpdated": "10/2/2015 10:35:23 PM",
						"ID": "121124"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 1-3",
					"Capacity": "225"
				},
				"Id": "28751",
				"Description": "The session gives a brief overview on the challenges that an Agile development team faced before adopting agile tools, it walks through on how adopting the Chuck Norris suite transformed an ordinary Agile team into power packed ChuckAgile Team. The suite consists of various Atlassian tools like Confluence, Jira, Bitbucket (including Stash), Bamboo and Jira Service Desk and is referred here as \"Chuck Norris\" for obvious reasons of agility, flexibility, traceability and various other adjectives. The session highlights on how the integration between these tools contributed for a greater transparency, traceability, collaboration and increased productivity. Agility redefined by Chuck Norris.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 10:30:00 PM",
					"Label": "Breakout 4",
					"StartTime": "11/4/2015 10:00:00 PM"
				},
				"Track": {
					"Title": "Interact",
					"Description": "Interact"
				},
				"LastModified": "9/18/2015 11:49:36 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA Service Desk,Confluence,HipChat,Bitbucket / Stash / SourceTree,Bamboo",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Agile Development with Chuck Norris",
				"Speakers": [
					{
						"AttendeeID": "ErUPlipzR6Le7CfTAHO8",
						"Biography": "Entered into the earth portal from the city of Bangalore, India. Did my Bachelors of Engineering in Instrumentation Technology from Sri Jayachamarajendra College of Engineering, Mysore - India. I have over 13 yrs of Experience in IT and have performed various kinds of roles and I like my current role in the DevOps area in the banking industry in Big Apple.",
						"LastName": "Matrubai",
						"Interests": "Love to watch science documentaries, hear podcasts from NPR and read \"Calvin & Hobbes\" comics. Like to do outdoor activities like biking, camping and travelling.",
						"EmailAddress": "Email is private",
						"Title": "Equities DevOps Lead",
						"AvailableforMeeting": "True",
						"FirstName": "Madhusudhan",
						"Industry": "Investment Banking",
						"Roles": [
							"Speaker"
						],
						"Twitter": "twitter.com/madhushell",
						"LinkedIn": "http://linkedin.com/madhusudhan.matrubai",
						"Facebook": "http://facebook.com/madhusudhan.matrubai",
						"Company": "Barclays",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114608",
						"LastUpdated": "9/21/2015 9:36:09 PM",
						"ID": "114608"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 1-3",
					"Capacity": "225"
				},
				"Id": "28760",
				"Description": "Got your little Atlassian fiefdom serving a few teams, but harbouring dreams of corporate conquest? Let’s make those dreams come true!\r\n\r\nJoin HomeAway’s Michael Kuhl on an epic adventure taking JIRA and Confluence from cool tools for the geeks to the foundation of business process management and internal communication in two years and change. You’ll learn how to promote adoption and then manage the surge in usage, demands for customization, and day to day support while nurturing a (totally healthy!) addiction to plugins.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 9:30:00 PM",
					"Label": "Breakout 8",
					"StartTime": "11/5/2015 9:00:00 PM"
				},
				"Track": {
					"Title": "Interact",
					"Description": "Interact"
				},
				"LastModified": "9/18/2015 1:35:23 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Agile,JIRA Service Desk,Confluence,Confluence Team Calendars",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Go Big or Go HomeAway with JIRA and Confluence",
				"Speakers": [
					{
						"AttendeeID": "sz1RywNHComB55nShojF",
						"Biography": "Michael has been bridging the gap between Business Problems and Software Solutions for over 25 years building and leading teams spanning the software development lifecycle from design & development to sales, support, and service. \r\n\r\nLately he's been focused on enabling collaboration and communication at HomeAway.com with Confluence and Jira for over 2000 users. He's displaced a number of products with solutions that leverage the power and flexibility of Atlassian tools. \r\n\r\nIn his spare time he's an avid gardener consuming a steadily increasing footprint in his backyard.",
						"LastName": "Kuhl",
						"Interests": "Interests are private",
						"EmailAddress": "mail@michaelkuhl.com",
						"Title": "Senior Manager - IT Operations",
						"AvailableforMeeting": "True",
						"FirstName": "Michael",
						"Industry": "Travel & Leisure",
						"Roles": [
							"Speaker"
						],
						"Website": "http://www.michaelkuhl.com",
						"Twitter": "https://twitter.com/mekuhl",
						"LinkedIn": "https://www.linkedin.com/in/michaelkuhl",
						"Company": "HomeAway.com, Inc.",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114618",
						"LastUpdated": "8/13/2015 10:54:18 PM",
						"ID": "114618"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate A",
					"Capacity": "525"
				},
				"Id": "28768",
				"Description": "The tremendous flexibility in JIRA Agile is what makes it so powerful. But it can also be daunting.\r\n\r\nIn this session, Spectrum Health will share how they learned to configure JIRA Agile and JIRA Portfolio to encourage the right agile practices, manage scope, and facilitate product owner-to-stakeholder communication – and still give teams the freedom to innovate! ",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 7:35:00 PM",
					"Label": "Breakout 7",
					"StartTime": "11/5/2015 6:50:00 PM"
				},
				"Track": {
					"Title": "Plan",
					"Description": "Plan"
				},
				"LastModified": "9/22/2015 4:06:10 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Agile,JIRA Portfolio",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Divergence is not Innovation: Using JIRA Agile Governance to Drive the Right Behaviors",
				"Speakers": [
					{
						"AttendeeID": "2LdgkmOdRaCAyENc3Nq8",
						"Biography": "My Agile journey began in 1999 as a Java developer at Menlo Innovations in Ann Arbor, Michigan, using XP (Extreme Programming). In 2004 I moved to Grand Rapids to work at Spectrum Health, an integrated health care company with over 21,000 employees. Currently, as the Agile Coach, I design and deliver internal training on Agile/Scrum for the enterprise, and coach our software development teams. I am a Certified ScrumMaster and Certified Scrum Professional, and am working on my Certified Scum Coaching accreditation.\r\n\r\nMy JIRA journey began last summer when we replaced our current issue tracking applications with the JIRA suite. As admin for JIRA, JIRA Agile, and JIRA Portfolio, I focus on configuration, administration, building add-ons/plug-ins, and training users.",
						"LastName": "Limbaugh",
						"Interests": "Video editing and special effects.\r\nVolleyball, music (guitar, drums), surfing.",
						"EmailAddress": "charles.limbaugh@spectrumhealth.org",
						"Title": "Agile Coach",
						"AvailableforMeeting": "False",
						"FirstName": "Dave",
						"Industry": "Health Care",
						"Roles": [
							"Speaker"
						],
						"Company": "Spectrum Health",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114569",
						"LastUpdated": "9/22/2015 4:06:10 AM",
						"ID": "114569"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate B",
					"Capacity": "575"
				},
				"Id": "28769",
				"Description": "Dev and ops teams need constant information about the health of their service offering in order to work together effectively – not to mention the ability to communicate efficiently to issues. Featuring speakers from Twitter, Pager Duty and New Relic, this panel will cover Atlassian's view of ChatOps today, how it is evolving, how key players in the industry are using it and how to get your ops teams to collaborate in action with ChatOps.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 7:45:00 PM",
					"Label": "Breakout 2",
					"StartTime": "11/4/2015 7:00:00 PM"
				},
				"Track": {
					"Title": "Build",
					"Description": "Build"
				},
				"LastModified": "9/23/2015 9:50:28 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Basic",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "Bringing Speed and Efficiency to Operations with ChatOps",
				"Speakers": [
					{
						"AttendeeID": "TEMP-167539613",
						"Biography": "While Ian Buchanan has broad and deep experience with both Java and .NET, he is best known as a champion of agile methods in large enterprises. He is currently focused on the emerging DevOps culture and the tools for enabling better continuous integration, continuous delivery, and data analysis. During his career, he has successfully managed enterprise software development tools in all phases of their lifecycle, from cradle to grave. He has driven organization-wide process improvement with results of greater productivity, higher quality, and improved customer satisfaction. He has built multi-national teams that value self-direction and self-organization. When not speaking or coding, you are likely to find Ian indulging his passions in parsers, meta-programming, and domain-specific languages.",
						"LastName": "Buchanan",
						"Interests": "Agile, DevOps, Java, .NET, Bamboo, Bitbucket",
						"EmailAddress": "ibuchanan@atlassian.com",
						"Title": "Developer Partisan",
						"AvailableforMeeting": "True",
						"FirstName": "Ian",
						"Industry": "Software",
						"Roles": [
							"Speaker"
						],
						"Website": "https://developer.atlassian.com/blog/authors/ibuchanan",
						"Twitter": "https://twitter.com/devpartisan",
						"LinkedIn": "https://www.linkedin.com/in/ianbuchanan",
						"Blog": "https://developer.atlassian.com/blog/authors/ibuchanan",
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/118352",
						"LastUpdated": "9/22/2015 4:07:06 AM",
						"ID": "118352"
					},
					{
						"AttendeeID": "TEMP-436366604",
						"Biography": "Stevan Arychuk is a recovering Ops engineer, having spent 15+ years carrying the pager and fighting fires for both startups and large tech companies.  Currently, Stevan works at New Relic in Portland, Oregon with Product Management and Strategic Marketing teams.  He strives to make things better for future Ops/DevOps Engineers by advocating Ops use cases for New Relic products and championing DevOps at large.",
						"LastName": "Arychuk",
						"Interests": "Measuring stuff!",
						"EmailAddress": "stevan@newrelic.com",
						"Title": "Sr. Product Manager / Strategic Marketing",
						"AvailableforMeeting": "True",
						"FirstName": "Stevan",
						"Roles": [
							"Speaker"
						],
						"Website": "http://newrelic.com",
						"Twitter": "Twitter is private",
						"LinkedIn": "https://www.linkedin.com/in/sarychuk",
						"Facebook": "Facebook is private",
						"Blog": "https://blog.newrelic.com/author/stevanarychuk/",
						"Company": "New Relic",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/119878",
						"LastUpdated": "9/22/2015 4:07:06 AM",
						"ID": "119878"
					},
					{
						"AttendeeID": "TEMP-20184525",
						"LastName": "Hayes",
						"EmailAddress": "dave@pagerduty.com",
						"AvailableforMeeting": "False",
						"FirstName": "David",
						"Roles": [
							"Speaker"
						],
						"Company": "Pager Duty",
						"LastUpdated": "9/22/2015 4:07:06 AM",
						"ID": "119879"
					},
					{
						"AttendeeID": "TEMP-98142331",
						"LastName": "Chan",
						"EmailAddress": "raymond@twitter.com",
						"AvailableforMeeting": "False",
						"FirstName": "Raymond",
						"Roles": [
							"Speaker"
						],
						"Company": "Twitter",
						"LastUpdated": "9/23/2015 9:50:13 PM",
						"ID": "120697"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate A",
					"Capacity": "525"
				},
				"Id": "28791",
				"Description": "The realities of working in an enterprise (distributed teams, multiple stakeholders, etc) present a series of challenges when trying to plan and scale agile development. Learn how Rosetta Stone knit together a dozen existing JIRA Agile boards into a coherent program-level view of their Scaled Agile Framework (SAFe) release train using JIRA Portfolio – without sacrificing team autonomy.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 12:00:00 AM",
					"Label": "Breakout 6",
					"StartTime": "11/4/2015 11:30:00 PM"
				},
				"Track": {
					"Title": "Plan",
					"Description": "Plan"
				},
				"LastModified": "9/22/2015 4:06:10 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Agile,JIRA Portfolio",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					},
					{
						"Value": "Intermediate",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "Managing Scaled Agile at Rosetta Stone",
				"Speakers": [
					{
						"AttendeeID": "TEMP-467188918",
						"Biography": "Eric Hilfer has been building innovative software products for the Ed Tech and Solid Modeling industries since the early 1980s.  As a former research scientist and Special Ed teacher, Eric has championed the use of technology where it provides real value – not as a buzzword, but as a tool to empower learners, and a force multiplier for talented teachers who are scaling their practice. Eric has led software development organizations at a variety of groundbreaking companies and publishers, including PTC, Tom Snyder Productions, and Scholastic, and is currently Vice President of Software Engineering at Rosetta Stone Ltd.  A pragmatic Agile practitioner and certified Scaled Agilist, Eric has been pushing the envelope on supporting distributed teams to succeed with enterprise-level Agile implementations.",
						"LastName": "Hilfer",
						"EmailAddress": "ehilfer@rosettastone.com",
						"Title": "Vice President of Software Engineering",
						"AvailableforMeeting": "False",
						"FirstName": "Eric",
						"Industry": "eLearning",
						"Roles": [
							"Speaker"
						],
						"Website": "http://www.rosettastone.com",
						"Twitter": "https://twitter.com/EricHilfer1",
						"LinkedIn": "http://www.linkedin.com/profile/view?id=10086446",
						"Company": "Rosetta Stone Ltd.",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/117409",
						"LastUpdated": "9/22/2015 4:06:10 AM",
						"ID": "117409"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate A",
					"Capacity": "525"
				},
				"Id": "28792",
				"Description": "JIRA Portfolio’s 1st year, in short, has been one hell of a ride.\r\n\r\nJoin product manager Martin Suntinger as he talks about data-driven planning no-one will secretly shake their heads at, how to maintain your innovation mojo at scale, and how to report beyond just facts and figures. Plus, see what's in store for the next generation of JIRA Portfolio. No \"terrible twos\" here!",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 6:45:00 PM",
					"Label": "Breakout 1",
					"StartTime": "11/4/2015 6:15:00 PM"
				},
				"Track": {
					"Title": "Plan",
					"Description": "Plan"
				},
				"LastModified": "9/22/2015 4:06:10 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Agile,JIRA Portfolio",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "A Year Full of Learnings from JIRA Portfolio's First Year",
				"Speakers": [
					{
						"AttendeeID": "35gi3ixeYID6OPzxxOG9",
						"Biography": "Martin Suntinger works as a principal product manager at Atlassian and is responsible for the fast-growing JIRA Portfolio add-on. He built the product from the ground up as the founder and CEO of Vienna based Radiant Minds Software before the startup was acquired by Atlassian and he moved to Sydney with the entire team. Outside the office, you’ll likely meet him running, kayaking, or chasing down the best coffee in town.",
						"LastName": "Suntinger",
						"EmailAddress": "msuntinger@atlassian.com",
						"Title": "Principal Product Manager",
						"AvailableforMeeting": "True",
						"FirstName": "Martin",
						"Roles": [
							"Speaker",
							"Grader"
						],
						"LinkedIn": "http://au.linkedin.com/in/martinsuntinger",
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114623",
						"LastUpdated": "9/22/2015 4:06:10 AM",
						"ID": "114623"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 4-6",
					"Capacity": "225"
				},
				"Id": "28794",
				"Description": "Splunk decided to switch from Perforce to Git, and needed a scalable solution for repository management. Since they were already knee-deep in the Atlassian stack, they chose Stash Data Center. Join Kurt and Christopher as they tell Splunk's story – from assessment and architecture, to configuration plans and results. If your team is switching to Git on any scale and looking to speed up your development lifecycle, this session is for you!",
				"IsFeatured": "False",
				"Links": "<br>",
				"TimeSlot": {
					"EndTime": "11/5/2015 9:30:00 PM",
					"Label": "Breakout 8",
					"StartTime": "11/5/2015 9:00:00 PM"
				},
				"Track": {
					"Title": "Scale",
					"Description": "Scale"
				},
				"LastModified": "9/18/2015 1:59:15 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Advanced",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					},
					{
						"Value": "Bitbucket / Stash / SourceTree",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Slam Dunk with Splunk and Stash Data Center",
				"Speakers": [
					{
						"AttendeeID": "FQRD7JqstquvClRXZs3Q",
						"Biography": "I am a results-driven engineering leader with a BS in mathematics and computer science allied with more than two decades of consistent advance from hands-on software development to strategic national and international leadership roles. I have extensive experience creating strong shared-responsibility teams that produce world class software on time and on budget. I also have an in-depth technical skill set from my previous roles in software development. I currently work at Splunk as Director of Release Engineering. In this role I lead a high performing, engaged team of engineers chartered with delivering high quality products through scaled build, test, and deployment platforms that delight our customers and set Splunk apart in the marketplace.",
						"LastName": "Chase",
						"Interests": "Family, fishing and producing world class software",
						"EmailAddress": "kchase@splunk.com",
						"Title": "Director, Release Engineering",
						"AvailableforMeeting": "False",
						"FirstName": "Kurt",
						"Industry": "High-Technology: Operational Intelligence, Enterprise Security and Big Data",
						"Roles": [
							"Speaker"
						],
						"LinkedIn": "https://www.linkedin.com/profile/view?id=1955602",
						"Company": "Splunk",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114602",
						"LastUpdated": "5/29/2015 3:58:40 PM",
						"ID": "114602"
					},
					{
						"AttendeeID": "o28MFhggoL5Ef753Nuzs",
						"Biography": "With an extensive background in software engineering and nearly ten years of Atlassian experience, not only does Christopher provide innovative solutions to some of the most difficult problems but he also puts a smile on clients' faces.",
						"LastName": "Pepe",
						"Interests": "Fishing, foraging, food",
						"EmailAddress": "pepeca@praecipio.com",
						"Title": "Dragon of the West",
						"AvailableforMeeting": "False",
						"FirstName": "Christopher",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/cpepe",
						"Company": "Praecipio Consulting",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114625",
						"LastUpdated": "5/29/2015 3:58:40 PM",
						"ID": "114625"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate A",
					"Capacity": "525"
				},
				"Id": "28808",
				"Description": "If you're struggling to implement QA methods that fit with agile's core principles, you're not alone.\r\n\r\nJoin Giancarlo and Maurizio as they explain how their teams found a sweet spot at the intersection of agile and QA engineering. They'll share common pitfalls and how to avoid them. Plus, get tips and tricks on how to capture requirements and link JIRA to test repositories for complete traceability.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 7:45:00 PM",
					"Label": "Breakout 2",
					"StartTime": "11/4/2015 7:00:00 PM"
				},
				"Track": {
					"Title": "Plan",
					"Description": "Plan"
				},
				"LastModified": "9/22/2015 4:06:10 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Agile,JIRA Capture,Confluence,Confluence Team Calendars,HipChat,Bamboo,FishEye/Crucible",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					},
					{
						"Value": "Intermediate",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "How to Build in Quality from Day 1 using Lean QA and Agile Testing",
				"Speakers": [
					{
						"AttendeeID": "YIvlKJ4kcj9yraVwZnzf",
						"Biography": "Giancarlo has built and managed QA teams working in IID and Agile development. He has been involved in testing complex backend systems, API’s, and software that was deployed on millions of end user’s computers/mobile devices. Currently, Giancarlo is building and managing a team of 50 QA people at Yellow Pages Canada.",
						"LastName": "Bisceglia",
						"Interests": "Interests are private",
						"EmailAddress": "Giancarlo.Bisceglia@yp.ca",
						"Title": "QA Manager",
						"AvailableforMeeting": "True",
						"FirstName": "Giancarlo",
						"Industry": "Advertising",
						"Roles": [
							"Speaker"
						],
						"Website": "http://www.yp.ca",
						"Twitter": "Twitter is private",
						"LinkedIn": "https://ca.linkedin.com/in/giancarlobisceglia",
						"Facebook": "Facebook is private",
						"Blog": "Blog is private",
						"Company": "Yellow Pages Canada",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114584",
						"LastUpdated": "9/22/2015 4:06:10 AM",
						"ID": "114584"
					},
					{
						"AttendeeID": "cKAc7qz4pcdqHawercXD",
						"Biography": "Maurizio Mancini has built and managed QA teams working in Agile and Incremental & Iterative development for over 25 years. Maurizio has a sixth sense for QA, Agile, and process. He has deployed Agile across numerous teams working on software used by millions of users. Maurizio has been using Atlassian’s tools for over a decade. He is currently building and managing a team of over 100 QA people at Yellow Pages Canada.",
						"LastName": "Mancini",
						"EmailAddress": "maurizio.mancini@yp.ca",
						"Title": "QA Director/Agile Evangelist",
						"AvailableforMeeting": "True",
						"FirstName": "Maurizio",
						"Industry": "Digital Advertising",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/qaandprocessguy",
						"Company": "Yellow Pages Canada",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114613",
						"LastUpdated": "9/22/2015 4:06:10 AM",
						"ID": "114613"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate C",
					"Capacity": "475"
				},
				"Id": "28813",
				"Description": "Running experiments in your product will tell you what your customers are doing, but they don't often tell you why they're doing it. So even after experimenting, you can be left wondering: which option would be better to ship? \r\n\r\nOne way that Atlassian has tackled this is by taking the \"concept testing\" technique to a whole new level. Get a first-hand look into how we run concept tests to extract the right insights – before a single line of code has been written. You'll learn how they can work side-by-side with quantitative experimentation to help you get smarter metrics, and have greater confidence in knowing how to take your product from good to great.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 10:15:00 PM",
					"Label": "Breakout 9",
					"StartTime": "11/5/2015 9:45:00 PM"
				},
				"Track": {
					"Title": "Innovate",
					"Description": "Innovate"
				},
				"LastModified": "9/18/2015 1:15:40 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,Confluence",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Going from Good to Great with Concept Testing",
				"Speakers": [
					{
						"AttendeeID": "18561352135",
						"Biography": "If robots are going to do all the regular work for us, what will we do? Great work, that's what. Rethinking. Exploring. Questioning. Creating. And helping each other do all that. That's Ben's jam. Ben is a design strategist, mentor and team leader. He's been in the online industry from when Netscape Navigator ruled the www, and at Atlassian for the last 2 years. He enjoys helping take business strategy into well-designed experiences with research, creativity techniques, sketching, prototyping and testing. When not doing these things, Ben engages in extreme sports like painting, brewing beer, and cooking mexican. He probably quotes Marshall McLuhan too much, and eats too many salt and vinegar chips.",
						"LastName": "Crothers",
						"Interests": "Design thinking, graphic facilitation, art and painting, beer brewing, playing guitar, learning French, trying to cook Mexican food.",
						"EmailAddress": "bcrothers@atlassian.com",
						"Title": "Principal Design Strategist",
						"AvailableforMeeting": "True",
						"FirstName": "Ben",
						"Industry": "Software and internet",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/bencrothers",
						"LinkedIn": "https://www.linkedin.com/in/bencrothers",
						"Blog": "http://bencrothers.com/",
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114217",
						"LastUpdated": "6/12/2015 5:50:04 PM",
						"ID": "114217"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate B",
					"Capacity": "575"
				},
				"Id": "28820",
				"Description": "\"Measure first, then optimise\" is an oft-cited maxim for developers. But you can't optimise without first understanding what factors influence a given measurement. Only then is it worth your time to start digging around in your code and infrastructure.\r\n\r\nJoin Atlassian engineer Ian Grunert as he outlines the measurements exposed by the browser about the request lifecycle, and the areas of your web application contributing to each section. He'll also show you how the Confluence team uses Bamboo, Bitbucket (including Stash), Confluence, and JIRA to track the impact of performance improvements and reduce the chance of shipping performance regressions.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 12:00:00 AM",
					"Label": "Breakout 6",
					"StartTime": "11/4/2015 11:30:00 PM"
				},
				"Track": {
					"Title": "Build",
					"Description": "Build"
				},
				"LastModified": "9/22/2015 4:07:06 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,Confluence,Bitbucket / Stash / SourceTree,Bamboo",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					},
					{
						"Value": "Intermediate",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "Demystifying Web Performance",
				"Speakers": [
					{
						"AttendeeID": "HoaM4n4mxwLBtz9fUpqc",
						"Biography": "Ian currently works at Atlassian where he has worked on JIRA, Stash and Confluence. He previously worked for Rising Sun Pictures and Maptek, and dabbled in some research, whilst completing his degree at Adelaide University. Ian enjoys discussing code over beer, and writing in the third person in speaker bios.",
						"LastName": "Grunert",
						"Interests": "Interests are private",
						"EmailAddress": "Email is private",
						"Title": "Senior Developer",
						"AvailableforMeeting": "False",
						"FirstName": "Ian",
						"Industry": "Industry is private",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/iangrunert",
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114593",
						"LastUpdated": "9/22/2015 4:07:06 AM",
						"ID": "114593"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate A",
					"Capacity": "525"
				},
				"Id": "28822",
				"Description": "BlackRock is a firm of 12000 employees rocking about 500 software deployments per week. (Yes: you read that right.)\r\n\r\nLearn how BlackRock installed and customized JIRA to manage software enhancements from idea inception to production release. The presentation highlights challenges of the implementation, usage and key integrations between JIRA, iPad, and BlackRock's Trading Platform software. ",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 11:15:00 PM",
					"Label": "Breakout 5",
					"StartTime": "11/4/2015 10:45:00 PM"
				},
				"Track": {
					"Title": "Plan",
					"Description": "Plan"
				},
				"LastModified": "9/22/2015 4:06:10 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Basic",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					},
					{
						"Value": "JIRA,JIRA Agile",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Release Management with JIRA at BlackRock",
				"Speakers": [
					{
						"AttendeeID": "6lNhYjBDXVfRXB9Sw92e",
						"Biography": "Gabe oversees Release Management personnel, processes and technology at BlackRock. He began his career with Sun Microsystems as a Professional Service Engineer, later moving to Release Engineering for Java & Solaris. With BlackRock, Gabe is also responsible for Program Management and Business Operations for the Development Services organization.",
						"LastName": "Tiu",
						"EmailAddress": "gabriel.tiu@blackrock.com",
						"Title": "Director of Release Management",
						"AvailableforMeeting": "True",
						"FirstName": "Gabriel",
						"Industry": "Finance",
						"Roles": [
							"Speaker"
						],
						"Company": "BlackRock",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114583",
						"LastUpdated": "9/22/2015 4:06:10 AM",
						"ID": "114583"
					},
					{
						"AttendeeID": "WRunPpn2T5bIsgYOq4uU",
						"Biography": "Jean-Paul Paramor leads the Software Tools team at BlackRock focusing of delivering tools which increase the productivity of Software Development across the firm.  He has 7 years’ extensive experience of implementing the Atlassian suite of tools.  Jean-Paul began his career as an Analyst Programmer within Keane Inc prior to joining MLIM/BlackRock.",
						"LastName": "Paramor",
						"EmailAddress": "jean-paul.paramor@blackrock.com",
						"AvailableforMeeting": "False",
						"FirstName": "Jean-Paul",
						"Roles": [
							"Speaker"
						],
						"Company": "BlackRock",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114597",
						"LastUpdated": "9/22/2015 4:06:10 AM",
						"ID": "114597"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 13-15",
					"Capacity": "225"
				},
				"Id": "28825",
				"Description": "Houston, we have a problem. Too many companies are fixated on the definitions of ITIL and problem management instead of actually getting value out of them. When you don't have a bonafide problem management policy, plus the training and skills to back it up, your team is likely to spend too much energy on ineffective activities. But there's hope.\r\n\r\nIT veteran John Custy will introduce the concepts you need to understand, dispel a few misconceptions, and explain the different problem management methodologies. He'll also cover the pros and cons of each methodology, and when to use each of them. and cons of each methodology and when to use of them.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 7:35:00 PM",
					"Label": "Breakout 7",
					"StartTime": "11/5/2015 6:50:00 PM"
				},
				"Track": {
					"Title": "Service",
					"Description": "Service"
				},
				"LastModified": "9/18/2015 1:45:45 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"Title": "Making Problem Management Work for Your Organization",
				"Speakers": [
					{
						"AttendeeID": "jsWmKcy4F5picRRrSbdZ",
						"LastName": "Custy",
						"EmailAddress": "jpcgroup@outlook.com",
						"AvailableforMeeting": "False",
						"FirstName": "John",
						"Roles": [
							"Speaker"
						],
						"Company": "JPC Group",
						"LastUpdated": "5/26/2015 7:01:23 AM",
						"ID": "114600"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate B",
					"Capacity": "575"
				},
				"Id": "28920",
				"Description": "If your team has entered the brave new world of mobile app development, but aren't sure you're nailing the process, you're not alone. Orbitz faced this problem with their suite of mobile apps – and emerged victorious with the help of scrum, and Atlassian dev tools.\r\n\r\nJoin Paul and Mike from Orbitz's mobile team to learn how they integrated JIRA with Bitbucket (including Stash) and SourceTree to help them coordinate the release cycles of 7 different brands, distributed across 5 different app stores worldwide. You'll also hear how they customized their build system to meet the needs of large-scale mobile development.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 9:30:00 PM",
					"Label": "Breakout 8",
					"StartTime": "11/5/2015 9:00:00 PM"
				},
				"Track": {
					"Title": "Build",
					"Description": "Build"
				},
				"LastModified": "9/22/2015 4:07:06 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,Bitbucket / Stash / SourceTree",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					},
					{
						"Value": "Intermediate",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "Always on the Go: Developing Mobile Apps at Orbitz",
				"Speakers": [
					{
						"AttendeeID": "VbnxDeth33H4TiLqeUMh",
						"Biography": "Paul is a hands-on Android developer, consultant, trainer and speaker with deep knowledge of Android software stack.",
						"LastName": "Turchenko",
						"EmailAddress": "paul.turchenko@gmail.com",
						"Title": "Sr. Software Developer",
						"AvailableforMeeting": "True",
						"FirstName": "Paul",
						"Industry": "Travel",
						"Roles": [
							"Speaker"
						],
						"LinkedIn": "https://www.linkedin.com/in/paulturchenko",
						"Company": "Orbitz",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114771",
						"LastUpdated": "9/22/2015 4:07:06 AM",
						"ID": "114771"
					},
					{
						"AttendeeID": "MnqE6ouj7ZyGC9arBCos",
						"Biography": "Mike Kelley is a Lead Engineer on the Android team at Orbitz Worldwide. As a Lead Engineer, he helps build and maintain features for seven different Android apps within the Orbitz brand portfolio—Orbitz.com and CheapTickets.com in North America, ebookers.com in Europe, and more. When he's not working on new app features, Mike helps organize the team’s overall development efforts alongside his team manager and product managers.\r\n\r\nMike hails from the University of Michigan. He Orbitz as a member of the Air Product Engineering team before transitioning to the Android team as a founding member in 2011. On late nights and weekends, Mike can be found thinking up new Android apps, such as FastTimes, a real-time bus & train tracking app available for several US cities.",
						"LastName": "Kelley",
						"EmailAddress": "theelfismike@gmail.com",
						"Title": "Lead Software Engineer, Android",
						"AvailableforMeeting": "False",
						"FirstName": "Mike",
						"Industry": "Travel",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/theelfismike",
						"Company": "Orbitz",
						"LastUpdated": "9/22/2015 4:07:06 AM",
						"ID": "114786"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 4-6",
					"Capacity": "225"
				},
				"Id": "28922",
				"Description": "Looking to the cloud to serve Bitbucket (including Stash) to a horde of hungry developers? Netflix has the recipe for you! Take one part fresh open source platforms, two parts juicy deployment pipelines, and add a dash of gooey-rich custom bakes. Join this session to find out how Netflix used this unique combination of technologies to move Stash from their data center to the cloud and what they burned (yes, burned) along the way.",
				"IsFeatured": "False",
				"Links": "<br>",
				"TimeSlot": {
					"EndTime": "11/4/2015 7:45:00 PM",
					"Label": "Breakout 2",
					"StartTime": "11/4/2015 7:00:00 PM"
				},
				"Track": {
					"Title": "Scale",
					"Description": "Scale"
				},
				"LastModified": "9/18/2015 11:51:57 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Bitbucket / Stash / SourceTree",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					},
					{
						"Value": "Intermediate",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "Baking Stash in the AWS Cloud at Netflix",
				"Speakers": [
					{
						"AttendeeID": "nfjVqH1RoZdIHudg3JLV",
						"Biography": "Ed Bukoski has over 20 years of experience in diverse domains including defense, e-commerce, finance and streaming media. At JP Morgan Chase, he worked on the Developer Collaboration Tools team, providing custom development and support for 50,000 users of on-premise Atlassian software.  At Netflix he is a Senior Software Engineer on the Developer Tools team providing devops support for AWS, Jenkins, Simian Army, Stash, and an array of custom built internal tools.\r\n\r\nEd has a bachelor's degree in Computer Engineering from the University of Arizona and an MBA and Master of Engineering degree from Arizona State University.",
						"LastName": "Bukoski",
						"Interests": "Stash, JIRA, Confluence, Devops",
						"EmailAddress": "ebukoski@netflix.com",
						"Title": "Senior Software Engineer",
						"AvailableforMeeting": "True",
						"FirstName": "Ed",
						"Industry": "Finance, Entertainment",
						"Roles": [
							"Speaker"
						],
						"LinkedIn": "https://www.linkedin.com/in/edbukoski",
						"Company": "Netflix",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114773",
						"LastUpdated": "8/13/2015 9:09:14 PM",
						"ID": "114773"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 4-6",
					"Capacity": "225"
				},
				"Id": "28923",
				"Description": "What started in 2008 as a team of 10 developers using JIRA has grown into an enterprise solution supporting over 11,400 users. Join Brian and Michael from Cerner for tips on overcoming the challenges of scaling JIRA. You'll learn how considering configuration, usage, and hardware can prevent outages and support a rapidly growing development environment. ",
				"IsFeatured": "False",
				"Links": "<br>",
				"TimeSlot": {
					"EndTime": "11/5/2015 7:35:00 PM",
					"Label": "Breakout 7",
					"StartTime": "11/5/2015 6:50:00 PM"
				},
				"Track": {
					"Title": "Scale",
					"Description": "Scale"
				},
				"LastModified": "9/18/2015 1:58:11 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Scaling JIRA: Challenge Accepted",
				"Speakers": [
					{
						"AttendeeID": "zAblodoK43MtIVcGB9e3",
						"Biography": "Brian is a Vice President with Cerner Corporation, one of the leading healthcare IT companies worldwide.  He is currently the executive sponsor of an effort to replace all other issue management solutions at Cerner with Atlassian’s JIRA solution.  He initially introduced JIRA to Cerner in 2008.  Brian has been responsible for managing software development tools at Cerner since 2005.",
						"LastName": "Wallace",
						"EmailAddress": "bwallace@cerner.com",
						"Title": "Vice President",
						"AvailableforMeeting": "True",
						"FirstName": "Brian",
						"Industry": "Healthcare",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/wallacebrian",
						"LinkedIn": "http://www.linkedin.com/in/bwallacekc",
						"Company": "Cerner Corporation",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114774",
						"LastUpdated": "5/27/2015 6:06:47 PM",
						"ID": "114774"
					},
					{
						"AttendeeID": "bSAue2Ay7bvz8uCLNYQt",
						"Biography": "Mike is a manager at Cerner Corporation, one of the leading healthcare IT companies worldwide. Mike's team is responsible for software development tools (including JIRA and Crucible). Over the past year he has focused on JIRA stability and scalability to ensure the success of migrating all of Cerner’s development teams into JIRA. Prior to managing the team, Mike was a JAVA developer, which included developing JIRA plugins.",
						"LastName": "Damman",
						"EmailAddress": "mike.damman@cerner.com",
						"Title": "Knowledge Architect",
						"AvailableforMeeting": "True",
						"FirstName": "Mike",
						"Industry": "Health Care",
						"Roles": [
							"Speaker"
						],
						"Company": "Cerner Corporation",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114788",
						"LastUpdated": "5/27/2015 6:06:47 PM",
						"ID": "114788"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 4-6",
					"Capacity": "225"
				},
				"Id": "28930",
				"Description": "Large companies often struggle to align development with sales and marketing - how do you ensure the delivery of features and fixes lines up with the efforts to promote them? Luisa will talk about how a big organization implemented a lightweight version of SAFe, but adapted the methodology to fit their needs. Learn how you can tailor agile principles to address the unique challenges your teams face and why configuring your agile tools properly is critical to increasing agility throughout the company.",
				"IsFeatured": "False",
				"Links": "<br>",
				"TimeSlot": {
					"EndTime": "11/4/2015 11:15:00 PM",
					"Label": "Breakout 5",
					"StartTime": "11/4/2015 10:45:00 PM"
				},
				"Track": {
					"Title": "Scale",
					"Description": "Scale"
				},
				"LastModified": "9/24/2015 7:14:50 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Agile,JIRA Portfolio",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					},
					{
						"Value": "Intermediate",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "Throwing Out the Agile Rulebook",
				"Speakers": [
					{
						"AttendeeID": "jCnjXdbfHh1ZLmyoZu2U",
						"LastName": "Randon",
						"EmailAddress": "lisrandon@gmail.com",
						"AvailableforMeeting": "True",
						"FirstName": "Luisa",
						"Roles": [
							"Speaker"
						],
						"Company": "Amgen",
						"LastUpdated": "6/12/2015 3:57:03 PM",
						"ID": "114780"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate C",
					"Capacity": "475"
				},
				"Id": "28933",
				"Description": "It's generally understood that data, when used correctly, can be used to build great products that grow like bamboo; but it's never obvious how to do so.  Over the last 3 years, Atlassian has grown its Product Analytics team from 1 analyst to 12 (a 1200% increase!) and 0kb of data to over 6TB (infinite % increase).  Come find out how we've built a team, metrics, and a culture of data within engineering at Atlassian.  Most importantly, learn how to unlock the power of data in your company and set a growth trajectory for your product.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 7:45:00 PM",
					"Label": "Breakout 2",
					"StartTime": "11/4/2015 7:00:00 PM"
				},
				"Track": {
					"Title": "Innovate",
					"Description": "Innovate"
				},
				"LastModified": "9/23/2015 2:39:42 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,Confluence",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "How Atlassian Uses Analytics to Build Better Products",
				"Speakers": [
					{
						"AttendeeID": "a7WKibj6yOsrbym4sxlp",
						"Biography": "Reed Johnson joined Atlassian 4 years ago and has helped spearhead the data revolution within the company.  He's seen the company grow from 0kb to 5TB of product data, and felt the aches and pains along the way.  As a Product Analyst, he works to make sure Product People (Devs, PMs, Designers) use data correctly to build great products. ",
						"LastName": "Johnson",
						"Interests": "Data, Sports, Sports Data, Data Sports",
						"EmailAddress": "rjohnson@atlassian.com",
						"Title": "Product Analyst",
						"AvailableforMeeting": "True",
						"FirstName": "Reed",
						"Roles": [
							"Speaker"
						],
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114783",
						"LastUpdated": "9/3/2015 7:17:45 PM",
						"ID": "114783"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 4-6",
					"Capacity": "225"
				},
				"Id": "29045",
				"Description": "Autodesk has issues. Lots of them. In 2012, Autodesk broke the 200,000 issues barrier in JIRA and three years later, they've more than tripled that number. This session will discuss the challenges they faced in consolidating different systems, provide tips on how mitigating organizational challenges around consolidation, and share best practices for supporting JIRA at scale.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 10:30:00 PM",
					"Label": "Breakout 4",
					"StartTime": "11/4/2015 10:00:00 PM"
				},
				"Track": {
					"Title": "Scale",
					"Description": "Scale"
				},
				"LastModified": "9/18/2015 1:57:12 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Intermediate",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					},
					{
						"Value": "JIRA,JIRA Agile",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "How to Join the \"1M JIRA Issues\" Club",
				"Speakers": [
					{
						"AttendeeID": "TEMP-699195386",
						"Biography": "Born in Rosario, Argentina, Claudio is a System Analyst from Universidad Argentina de la Empresa and holds a Postgraduate in Communication Networks from the Swiss Federal Institute of Technology in Lausanne (www.epfl.ch) where he implemented for the first time in history the GZIP content encoding to compress traffic between browsers and web servers used today. Since 1991 he works for Autodesk, having held a number of different positions from information technology, product support, developer support, Internet development, localization engineering processes and more. Today his team supports the engineering systems and infrastructure for the Autodesk Engineering teams worldwide.\r\n",
						"LastName": "Ombrella",
						"Interests": "Technology, engineering processes, ",
						"EmailAddress": "claudio.ombrella@autodesk.com",
						"Title": "Senior Manager Engineering Systems and Infrastructure",
						"AvailableforMeeting": "True",
						"FirstName": "Claudio",
						"Industry": "Software",
						"Roles": [
							"Speaker"
						],
						"Website": "http://www.autodesk.com",
						"Twitter": "https://twitter.com/ClaudioOmbrella",
						"LinkedIn": "https://ch.linkedin.com/in/claudioombrella",
						"Blog": "https://www.linkedin.com/grp/home?gid=4454622&trk=my_groups-tile-grp",
						"Company": "Autodesk",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114950",
						"LastUpdated": "8/10/2015 7:55:57 PM",
						"ID": "114950"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 4-6",
					"Capacity": "225"
				},
				"Id": "29123",
				"Description": "Atlassian products are catching on like wildfire. What started out as a set of tools for teams has quickly grown into a worldwide enterprise phenomenon where standardization is the watchword. Join us as we dig into the products, progress, and plans Atlassian is making on behalf of companies who are \"going big\" with our stuff.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 6:45:00 PM",
					"Label": "Breakout 1",
					"StartTime": "11/4/2015 6:15:00 PM"
				},
				"Track": {
					"Title": "Scale",
					"Description": "Scale"
				},
				"LastModified": "9/24/2015 7:06:07 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,Confluence,Bitbucket / Stash / SourceTree",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Enterprise State of the Union",
				"Speakers": [
					{
						"AttendeeID": "VqxemxT7bJiEwTfOBzAf",
						"Biography": "Chris is responsible for shepherding Atlassian's enterprise products. Joining Atlassian in late 2014, Chris has spent the last 20 years delivering market-leading software development products to a broad range of customers, from teams developing the next big thing in open source, to enterprises with thousands of developers. Big or small, co-located or distributed, agile or traditional, on-premises or in the cloud, Chris offers unique insight based on extensive product delivery experience and a never-ending fascination with tools and methodologies, past, present, and future. ",
						"LastName": "Clarke",
						"EmailAddress": "cclarke@atlassian.com",
						"AvailableforMeeting": "False",
						"FirstName": "Chris",
						"Roles": [
							"Speaker",
							"Grader"
						],
						"Company": "Atlassian",
						"LastUpdated": "6/12/2015 6:39:45 PM",
						"ID": "113961"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 13-15",
					"Capacity": "225"
				},
				"Id": "29211",
				"Description": "Are you interested to find out how to transform your service desk into a lean, mean ITSM machine? Or how to create macros and templates to automate repetitive tasks so agents can focus on solving the hard stuff? Or how to automate onboarding of new staff? Based on actual customer implementations, this advanced session is for IT admins with knowledge of JIRA and JIRA Service Desk. If you want to find out how to increase the productivity of your IT teams and take your service desk to the next level, then this session is for you.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 6:45:00 PM",
					"Label": "Breakout 1",
					"StartTime": "11/4/2015 6:15:00 PM"
				},
				"Track": {
					"Title": "Service",
					"Description": "Service"
				},
				"LastModified": "10/1/2015 8:54:23 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Service Desk,Confluence",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					},
					{
						"Value": "Basic",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "Turbo-Charge Your JIRA Service Desk with ITSM & Automation Awesomeness",
				"Speakers": [
					{
						"AttendeeID": "TEMP-511793677",
						"Biography": "I work at Valiantys, one of the leading Atlassian Experts, where I head the UK office. I've been using, customizing and implementing Atlassian tools for 6 years.\r\n\r\nPrior to joining Valiantys, I was a Product Manager and before that I use to manage support teams. I'm originally from France but have been living in London for 8 years.\r\n\r\nI'm especially interested in all the subjects around service desk and ITSM in general.",
						"LastName": "Dussurget",
						"EmailAddress": "lucas.dussurget@valiantys.com",
						"Title": "Managing Director",
						"AvailableforMeeting": "True",
						"FirstName": "Lucas",
						"Roles": [
							"Speaker"
						],
						"Website": "http://www.valiantys.com",
						"Company": "Valiantys",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/115722",
						"LastUpdated": "6/15/2015 4:48:58 PM",
						"ID": "115722"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate A",
					"Capacity": "525"
				},
				"Id": "29655",
				"Description": "Meet Kiva, the social lending company that puts $700 million in the hands of people who need it the most.\r\n\r\nCome learn how Kiva's community support team creates JIRA tickets directly from Salesforce support cases, how their development process is supercharged with HipChat and Confluence integrations, and how they've amped up communications with Geckoboard integrations.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 11:00:00 PM",
					"Label": "Breakout 10",
					"StartTime": "11/5/2015 10:30:00 PM"
				},
				"Track": {
					"Title": "Plan",
					"Description": "Plan"
				},
				"LastModified": "9/22/2015 4:06:10 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Agile",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Key Takeaways from Managing an Integrated JIRA Experience",
				"Speakers": [
					{
						"AttendeeID": "TEMP-724541617",
						"Biography": "Nick has been a Product Manager at Kiva for 3+ years focusing on a myriad of internal and field partner tools.  Prior he spent 5 years at Intuit building small business web and mobile applications for domestic and global markets.  He holds a B.A. from University of Wisconsin-Madison and is an avid outdoor enthusiast.",
						"LastName": "Anderson",
						"EmailAddress": "nick@kiva.org",
						"Title": "Senior Product Manager",
						"AvailableforMeeting": "False",
						"FirstName": "Nick",
						"Industry": "Microfinance",
						"Roles": [
							"Speaker"
						],
						"Website": "http://www.kiva.org",
						"Company": "Kiva",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/119895",
						"LastUpdated": "9/22/2015 4:06:10 AM",
						"ID": "119895"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 10-12",
					"Capacity": "225"
				},
				"Id": "31068",
				"Description": "See how health care disrupters Oscar Insurance has woven Atlassian products into their infrastructure and customer workflows: from contract reviews to customer questions.\r\n \r\nEvery department at Oscar uses JIRA. Sara Wajnber will show you how they pulled this off without violating HIPPA, the ACA, or other regulations. Next, Mackenzie Kosut will show how HipChat and Bitbucket (including Stash) are used for everything from provisioning new servers, anonymizing development databases, code deploys, managing LDAP and VPN. ",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 9:30:00 PM",
					"Label": "Breakout 8",
					"StartTime": "11/5/2015 9:00:00 PM"
				},
				"Track": {
					"Title": "Enhance",
					"Description": "Enhance"
				},
				"LastModified": "9/18/2015 11:52:50 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Agile,Confluence,HipChat,Bitbucket / Stash / SourceTree,Marketplace add-ons",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					},
					{
						"Value": "Intermediate",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "ChatOps: Automating your Company with HipChat, Stash and JIRA Service Desk",
				"Speakers": [
					{
						"AttendeeID": "BtjiQOcfORUlFabHAF9l",
						"Biography": "Sara runs the Operations Infrastructure team at Oscar Insurance, a company that uses technology, data, and design to humanize healthcare. Prior to joining Oscar, she developed products for lots of ecommerce tech startups in NYC and has experience in everything from ticketing to food delivery platforms. Sara is passionate about process optimization, automation, and effective communication and works hard to improve all of the above at Oscar.  ",
						"LastName": "Wajnberg",
						"EmailAddress": "sara@hioscar.com",
						"Title": "Director of Operations Infrastructure",
						"AvailableforMeeting": "False",
						"FirstName": "Sara",
						"Industry": "Health Insurance",
						"Roles": [
							"Speaker"
						],
						"LinkedIn": "https://www.linkedin.com/profile/view?id=110346001",
						"Company": "Oscar Insurance",
						"LastUpdated": "8/7/2015 12:39:17 AM",
						"ID": "114275"
					},
					{
						"AttendeeID": "TEMP-678593991",
						"Biography": "Mackenzie is the Head of Technical Operations at Oscar Insurance, a company that uses technology, data, and design to humanize healthcare. Prior to Oscar, he was on the original SRE team at Tumblr scaling the site up to 20B page views a month. In the years leading up to Tumblr he worked at places like MTV/Viacom and DHL, spanning industries from Media, Finance, Dating Sites, International Shipping, and others. Mackenzie sits on the Customer Advisory Council of Splunk and Advisory Council for Prevoty. He current lives and works with his celebrity Pomeranian TommyPom.",
						"LastName": "Kosut",
						"EmailAddress": "mackenzie@hioscar.com",
						"Title": "Head of Technical Operations",
						"AvailableforMeeting": "True",
						"FirstName": "Mackenzie",
						"Industry": "Healthcare",
						"Roles": [
							"Speaker"
						],
						"Website": "https://mackenzie.tumblr.com",
						"Twitter": "https://twitter.com/mkosut",
						"LinkedIn": "https://www.linkedin.com/in/mackenziekosut",
						"Company": "Oscar Insurance",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/118267",
						"LastUpdated": "8/7/2015 12:39:17 AM",
						"ID": "118267"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 10-12",
					"Capacity": "225"
				},
				"Id": "31069",
				"Description": "Learn how Camerican International, one of the worlds largest food import/export companies, has integrated numerous add-ons in Confluence to build a custom CRM on a limited budget and without any in-house programmers. The Atlassian ecosystem is full of existing add-ons you can use, as well as developers looking for new ideas to build. Understanding how to manage multiple add-ons and work with the various vendors is the key to unlocking huge value. Come see what’s possible!",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 10:15:00 PM",
					"Label": "Breakout 9",
					"StartTime": "11/5/2015 9:45:00 PM"
				},
				"Track": {
					"Title": "Enhance",
					"Description": "Enhance"
				},
				"LastModified": "9/18/2015 2:07:43 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Confluence,Confluence Team Calendars",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					},
					{
						"Value": "Basic",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "Just-In-Time Business Operations (Yeah, there’s an add-on for that!)",
				"Speakers": [
					{
						"AttendeeID": "dmzrlQF84vVtx3yYm6zB",
						"Biography": "Always an IT nerd at heart Ethan started off his career in tech support, originally in dial-up modem support and eventually as a Network Security Engineer. In his early 20's he departed from this promising career track to play professional poker. 8 years later, having successfully competed at the highest levels it was time for a new challenge. In his current role, Ethan develops custom business tools by extending Confluence through add-ons. His people skills, honed through years of poker playing, help him to understand what motivates people and use that to drive adoption of those tools.\r\n\r\nEthan has an amazing wife and an energetic 5 year old son who keeps him active. Time is valuable but he still manages to find enough to help run the NYC Atlassian User Group.",
						"LastName": "Foulkes",
						"EmailAddress": "ethan.foulkes@gmail.com",
						"Title": "Business Development Manager",
						"AvailableforMeeting": "True",
						"FirstName": "Ethan",
						"Industry": "Food Importing",
						"Roles": [
							"Speaker"
						],
						"Website": "http://aug.atlassian.com/cities/newyorkny",
						"LinkedIn": "https://www.linkedin.com/pub/ethan-foulkes/2b/5ba/44",
						"Company": "Camerican International",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114142",
						"LastUpdated": "8/7/2015 12:52:52 AM",
						"ID": "114142"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 10-12",
					"Capacity": "225"
				},
				"Id": "31070",
				"Description": "See firsthand how the Atlassian administrator from Twilio, a communications API company, has customized workflows to handle employee on-boarding, off-boarding, travel, and facility requests. You'll also learn how Twilio is integrating with Atlassian to handle 2-factor authentication in JIRA, task routing in JIRA Service Desk, and SMS for missed HipChat notifications, including escalation and approvals.\r\n\r\n",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 11:00:00 PM",
					"Label": "Breakout 10",
					"StartTime": "11/5/2015 10:30:00 PM"
				},
				"Track": {
					"Title": "Enhance",
					"Description": "Enhance"
				},
				"LastModified": "9/18/2015 2:07:05 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Agile,Confluence,HipChat,Marketplace add-ons",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					},
					{
						"Value": "Basic",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "JIRA: Beyond Engineering & Computers; From 2FA to Approvals: Utilizing Atlassian From Your Thumbs",
				"Speakers": [
					{
						"AttendeeID": "TEMP-127900139",
						"Biography": "Dominique (Dom) DeGuzman is a self-taught software engineer with a background in enterprise desktop hardware support and formalized public speaking. After years in hardware, Dom used building and provisioning Linux machines as a jumping point into software and tools development. Now a Cloud Infrastructure Engineer at Twilio, she focuses on development pipeline, infrastructure monitoring, and internal communications tools. Building off of Atlassian’s products, she’s built and customized internal intranets, wikis and issue/bug tracking systems. As part of the Systems Engineering team, she’s administratively responsible for systems such as LDAP, Jenkins, Github, Cloudability and AWS. She’s utilized tools such as Puppet and Chef to manage and provision engineering infrastructure and deployment but prefers to focus on developing tools to automate cleaner and faster deployments. ",
						"LastName": "DeGuzman",
						"EmailAddress": "dom@twilio.com",
						"Title": "Infrastructure Engineer",
						"AvailableforMeeting": "True",
						"FirstName": "Dom",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/Itshella_dom",
						"LinkedIn": "https://www.linkedin.com/in/ddeguzman",
						"Company": "Twilio",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/118268",
						"LastUpdated": "8/7/2015 12:53:33 AM",
						"ID": "118268"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 10-12",
					"Capacity": "225"
				},
				"Id": "31162",
				"Description": "Get involved in open source projects that matter to you. Atlassian has several active open source projects including jit.si, Atlassian Connect, toolkits, AUI, jgitflow, and MEAT to name a few. Learn how you can share your customizations and integrations with the open source community via the Atlassian Marketplace.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 11:15:00 PM",
					"Label": "Breakout 5",
					"StartTime": "11/4/2015 10:45:00 PM"
				},
				"Track": {
					"Title": "Enhance",
					"Description": "Enhance"
				},
				"LastModified": "9/18/2015 2:05:58 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Intermediate",
						"Details": {
							"Title": "Technical Level ",
							"Options": [
								"Basic",
								"Intermediate",
								"Advanced"
							],
							"FieldType": "DropDown"
						}
					}
				],
				"Title": "Get Involved with Open Source",
				"Speakers": [
					{
						"AttendeeID": "TEMP-501876764",
						"Biography": "Joe Lopez is always moving from project to project to project trying to build great teams and products. He joined Atlassian in 2015 to lead HipChat’s video efforts and now manages the entirety of HipChat’s platform. Joe’s also currently overseas Atlassian’s open source program seeking to bring Atlassian’s commitment to open source into greater focus for the company. He has worked at a number of companies such as Evernote, Zynga, IMVU and Dell to name a few in addition to having degrees from Drexel University and University of Texas at Austin. When he’s not at work Joe splits his time between cooking (his second passion) and chasing his two young children.\r\nTwitter handle: @joepez74",
						"LastName": "Lopez",
						"Interests": "Technology, Cooking, Family, Hiking, Travel",
						"EmailAddress": "jlopez@atlassian.com",
						"Title": "Senior Dev Mgr for HipChat Platform",
						"AvailableforMeeting": "True",
						"FirstName": "Joe",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/joepez74",
						"LinkedIn": "https://www.linkedin.com/in/joepez",
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/118380",
						"LastUpdated": "8/8/2015 12:12:52 AM",
						"ID": "118380"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate C",
					"Capacity": "475"
				},
				"Id": "31323",
				"Description": "There's a thing called \"time to value\": how long it takes a team to uncover and realize value from a product. Atlassian learned this the hard way, discovering that more than half of new customers tried its products for less than 30 minutes – far too short a time to fully unlock their value.  \r\n\r\nWe approached and solved this problem using data-informed design – a combination of growth hacking, user research, data analytics, and A/B testing at scale – to dramatically increase customer engagement with our products. Come hear lead designer Alastair Simpson describe the variety of approaches we started with and how we learned which ones to pursue and which ones to discard. You'll learn how to design and centralise improved onboarding experiences that can be spread across all your products.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 9:45:00 PM",
					"Label": "Breakout 3",
					"StartTime": "11/4/2015 9:15:00 PM"
				},
				"Track": {
					"Title": "Innovate",
					"Description": "Innovate"
				},
				"LastModified": "9/18/2015 2:17:15 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Confluence",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Building Fast Growth Into Your Products Using Data-Informed Design",
				"Speakers": [
					{
						"AttendeeID": "TEMP-607480352",
						"Biography": "Alastair has 10 years of experience in user experience design at large growth focussed businesses. Alastair is currently Head of Design for Confluence. He is a passionate advocate of using lean customer centric design and design thinking methodologies to solve complex business problems and has helped many teams see its benefits and transition to leaner approaches to product development. Previously Alastair worked at global publishers Reed Business Information as well as a digital consultancy firm consulting to Qantas, FOXTEL and Telstra. His areas of expertise include design thinking, lean customer centric design, and program management. ",
						"LastName": "Simpson",
						"EmailAddress": "asimpson@atlassian.com",
						"Title": "Head of Design for Confluence",
						"AvailableforMeeting": "True",
						"FirstName": "Alastair",
						"Roles": [
							"Speaker"
						],
						"Website": "http://clunky.com.au/",
						"Twitter": "https://twitter.com/alanstairs",
						"Blog": "https://medium.com/designing-atlassian",
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/118683",
						"LastUpdated": "8/20/2015 11:13:08 PM",
						"ID": "118683"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate C",
					"Capacity": "475"
				},
				"Id": "31450",
				"Description": "Innovation sounds sexy. But behind every great innovator is an even better goal-setter who can marry collective focus, a careful balance of structure and agility, and a strong vision. Learn how Atlassian teams use the OKR (objectives and key results) framework to realize their great ideas, and JIRA and Confluence to track their goals.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 11:00:00 PM",
					"Label": "Breakout 10",
					"StartTime": "11/5/2015 10:30:00 PM"
				},
				"Track": {
					"Title": "Innovate",
					"Description": "Innovate"
				},
				"LastModified": "10/1/2015 8:50:51 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Agile,Confluence",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Bringing Ideas to Life: Setting Goals with JIRA & Confluence",
				"Speakers": [
					{
						"AttendeeID": "TEMP-944503726",
						"Biography": "Mary has worn many hats in her career, but the one that keeps on sticking seems to be problem solver. Two years at fast-moving Atlassian has given Mary the opportunity to be part of solving the most intriguing strategy questions of her career, first as part of the Corporate Marketing team and now as the Strategy and Operations Manager for our newest product family, JIRA Service Desk. Prior to Atlassian, Mary was a Management Consultant, Community Organizer, Entertainment Marketer, Personal Stylist, and on her good days, an aspiring Entrepreneur. When not solving problems at work, Mary is known for planning dinner parties, planning trips around the world and planning what she is going to wear next Wednesday. ",
						"LastName": "Raleigh",
						"EmailAddress": "mraleigh@atlassian.com",
						"Title": "Strategy & Operations Manager",
						"AvailableforMeeting": "False",
						"FirstName": "Mary",
						"Roles": [
							"Speaker"
						],
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/117408",
						"LastUpdated": "7/10/2015 10:44:43 PM",
						"ID": "117408"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Golden Gate C",
					"Capacity": "475"
				},
				"Id": "31465",
				"Description": "Jack Krawczyk was brought to Pandora in 2013 to formalize its first product management team dedicated to advertising. In his time, he's seen many ideas lead to creative solutions, and lots of ideas dead-end, as well. Join him as he discusses 6 concrete tactics product leaders can use to scale, standardize, and improve their software development cycle – including using Confluence to document and share product requirements and how to better communicate so you don't micromanage your team. ",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 10:30:00 PM",
					"Label": "Breakout 4",
					"StartTime": "11/4/2015 10:00:00 PM"
				},
				"Track": {
					"Title": "Innovate",
					"Description": "Innovate"
				},
				"LastModified": "9/18/2015 1:17:34 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Confluence",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Scaling Product Management Teams",
				"Speakers": [
					{
						"AttendeeID": "TEMP-448313773",
						"LastName": "Krawczyk",
						"EmailAddress": "jkrawczyk@pandora.com",
						"AvailableforMeeting": "False",
						"FirstName": "Jack",
						"Roles": [
							"Speaker"
						],
						"Company": "Pandora",
						"LastUpdated": "9/3/2015 7:19:40 PM",
						"ID": "119081"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 1-3",
					"Capacity": "225"
				},
				"Id": "31466",
				"Description": "As organizations grow to 100's of employees, expand to multiple offerings, or step out into multiple geographies, they can naturally develop siloed and hierarchical communication channels which hinder transparency, scale, and operational agility. Learn how Atlassian has tackled these problems by creating simple, highly visible, and cross-cutting communication channels all while helping to empower teams and encourage autonomy; built using the power of JIRA and using microservices as the vehicle to generate customised views and weekly communications tailored to each user.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 9:45:00 PM",
					"Label": "Breakout 3",
					"StartTime": "11/4/2015 9:15:00 PM"
				},
				"Track": {
					"Title": "Interact",
					"Description": "Interact"
				},
				"LastModified": "10/1/2015 8:48:31 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Cutting Through the Noise: Breaking Down Hierarchical Communication Channels and Silos",
				"Speakers": [
					{
						"AttendeeID": "TEMP-905069668",
						"Biography": "Ronald Romain is the Program Manager for Dev Tools at Atlassian with 10 years experience in the software engineering space, but don't let the job tittle mislead you. He has a dislike for complicated manual reports, red-tape, and meticulously maintained gantt charts. He loves technology and tinkering and getting the right tools into the hands of those who need it to help build fast moving, agile, autonomous teams. His passion lies in breaking down traditional patterns of project/program management and enabling small teams to operate effectively and autonomously in growing organisations while still providing much needed information and insight to leadership and stakeholders.",
						"LastName": "Romain",
						"EmailAddress": "rromain@atlassian.com",
						"Title": "Dev Tools Program Manager",
						"AvailableforMeeting": "False",
						"FirstName": "Ron",
						"Roles": [
							"Speaker"
						],
						"Company": "Atlassian",
						"LastUpdated": "9/3/2015 8:02:40 PM",
						"ID": "119084"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 10-12",
					"Capacity": "225"
				},
				"Id": "31828",
				"Description": "Are your teams used to Confluence out-of-the-box and want to take their productivity to the next level? Are you ready to extend Confluence to support the way your teams want to work? In this presentation we will reveal 3 content tailoring strategies that will free your teams' time. Included will be walkthroughs of extending Confluence with scripts, blueprints, macros and more of the latest Confluence Platform capabilities. Whether you are an eager wiki champion with scripting knowledge, an in-house software developer or an ecosystem developer, this session will get you started on creating awesome tailored solutions for your teams.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 9:45:00 PM",
					"Label": "Breakout 3",
					"StartTime": "11/4/2015 9:15:00 PM"
				},
				"Track": {
					"Title": "Enhance",
					"Description": "Enhance"
				},
				"LastModified": "9/18/2015 2:08:12 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Confluence",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Tailoring Confluence for Team Productivity",
				"Speakers": [
					{
						"AttendeeID": "ry7qwpmioVTla1qIMkgW",
						"Biography": "Steve Lancashire has been with Atlassian since 2011 and is currently a Principal Developer on the Confluence Platform Team.  Responsible for the confluence rest api, Steve has been focusing on building CQL the Confluence Query Language. When not writing code, Steve enjoys scuba diving and, when he can get his hands on a boat, sailing.",
						"LastName": "Lancashire",
						"EmailAddress": "slancashire@atlassian.com",
						"Title": "Principal Developer",
						"AvailableforMeeting": "False",
						"FirstName": "Steve",
						"Roles": [
							"Speaker"
						],
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114277",
						"LastUpdated": "6/2/2015 10:15:51 PM",
						"ID": "114277"
					},
					{
						"AttendeeID": "xi7QSTHSZ1UEbepjPmgs",
						"Biography": "Ben has over 15 years of experience building and leading collaboration and intelligence solutions for global markets. He loves growing motivated and productive teams and seeing the power of a platform deliver a consistently awesome experience for developers and customers alike. He is the Development Manager for the Confluence Platform.",
						"LastName": "Mackie",
						"EmailAddress": "bmackie@atlassian.com",
						"Title": "Development Manager",
						"AvailableforMeeting": "True",
						"FirstName": "Ben",
						"Roles": [
							"Speaker"
						],
						"Twitter": "https://twitter.com/bdmackie",
						"LinkedIn": "https://au.linkedin.com/in/bdmackie",
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/114566",
						"LastUpdated": "5/18/2015 7:54:29 PM",
						"ID": "114566"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 1-3",
					"Capacity": "225"
				},
				"Id": "31885",
				"Description": "At Atlassian we aim to create amazing experiences within our products. Harnessing the power of great feedback while in this process is hard. Feedback like, “it just doesn’t pop” doesn’t provide any value, and often feedback focusses on micro UI details. It stops us from getting to the heart of the problem.\r\n\r\nIn this talk, Nat will share how Atlassian incorporates continuous feedback via sparring to enhance our design process, and why everyone – from design to engineering to QA – needs to be involved.\r\n\r\nThis talk will cover the history of sparring, why it is so important and how to get it right. You’ll see how our design team employs HipChat and Confluence to solicit constructive feedback and ship the best version of our tools. Find out how to successfully share your work (and your soul) with your peers.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 11:00:00 PM",
					"Label": "Breakout 10",
					"StartTime": "11/5/2015 10:30:00 PM"
				},
				"Track": {
					"Title": "Interact",
					"Description": "Interact"
				},
				"LastModified": "9/18/2015 1:36:24 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "Confluence,HipChat",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Pain is Temporary, Suck is Forever: the Power of Sparring",
				"Speakers": [
					{
						"AttendeeID": "TEMP-932383076",
						"Biography": "Nat has 10 years of experience in user experience design working both client and agency side. Nat is currently a UX Designer at Atlassian, working on the customer purchasing lifecycle. Nat has a deep interest in visual design and how this can connect customers to products.",
						"LastName": "Jones",
						"EmailAddress": "njones@atlassian.com",
						"Title": "Senior Designer",
						"AvailableforMeeting": "False",
						"FirstName": "Nat",
						"Industry": "Design",
						"Roles": [
							"Speaker"
						],
						"Twitter": "http://twitter.com/natemsj",
						"LinkedIn": "https://au.linkedin.com/pub/nathaniel-jones/44/7a8/5b1",
						"Company": "Atlassian",
						"PhotoLink": "https://hubb.blob.core.windows.net/46507649-869b-4d20-9203-04ce11517482-profile/119660",
						"LastUpdated": "9/10/2015 5:31:29 PM",
						"ID": "119660"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 7-9",
					"Capacity": "2970"
				},
				"Id": "32034",
				"Description": "Behind every great human achievement, there is a team. That’s why Atlassian’s mission is to unleash the potential in every team – from software teams, to IT teams, to business teams. In this opening keynote, Atlassian’s co-CEOs will highlight key product portfolio expansions and enhancements, give an update on how the Atlassian family (that includes you!) has positively impacted the world over the last year, and provide an insider’s view into the exciting year ahead.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 6:00:00 PM",
					"Label": "Keynote 1 (Opening)",
					"StartTime": "11/4/2015 5:00:00 PM"
				},
				"Track": {
					"Title": "General",
					"Description": "General"
				},
				"LastModified": "9/18/2015 2:09:33 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Keynote"
				},
				"Title": "Opening Keynote: Smarter, Better, Faster, Stronger Teams",
				"Speakers": [
					{
						"AttendeeID": "TEMP-847939020",
						"Biography": "Scott Farquhar is the co-founder and co-CEO of Atlassian. Scott was awarded the Australian IT Professional of the Year in 2004 and, in 2006, was the youngest person ever awarded the Australian Entrepreneur of the Year by Ernst & Young alongside co‐founder and co‐CEO, Mike Cannon-Brookes. He was honored with the Corporate Social Responsibility award in 2011 by the Young Presidents’ Organization. Scott actively participates in the Sydney startup scene, contributes to Australian seed fund StartMate and venture capital fund Blackbird Ventures, and lectures on entrepreneurship at the University of New South Wales.",
						"LastName": "Farquhar",
						"EmailAddress": "sfarquhar@atlassian.com",
						"Title": "Co-Founder & Co-CEO",
						"AvailableforMeeting": "False",
						"FirstName": "Scott",
						"Roles": [
							"Speaker"
						],
						"Company": "Atlassian",
						"LastUpdated": "9/11/2015 9:42:48 PM",
						"ID": "119903"
					},
					{
						"AttendeeID": "TEMP-924911116",
						"Biography": "Mike Cannon-Brookes is the co-founder and co-CEO of Atlassian. Mike was awarded the Australian IT Professional in 2004, and in 2006 was the youngest person ever to be awarded the Australian Entrepreneur of the Year by Ernst & Young alongside co‐founder and co‐CEO, Scott Farquhar. Mike is an active angel investor and a non-executive director of Tyro, a technology focused Australian bank. Mike was honoured by the World Economic Forum as a Young Global Leader in 2009 and is currently a member of the Forum of Young Global Leaders.",
						"LastName": "Cannon-Brookes",
						"EmailAddress": "mcannon@atlassian.com",
						"Title": "Co-Founder & Co-CEO",
						"AvailableforMeeting": "False",
						"FirstName": "Mike",
						"Roles": [
							"Speaker"
						],
						"Company": "Atlassian",
						"LastUpdated": "9/11/2015 9:43:35 PM",
						"ID": "119904"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 7-9",
					"Capacity": "2970"
				},
				"Id": "32035",
				"Description": "Modern software teams are changing how they are structured, how they work, and how they communicate, because they know that the teams that adapt to and respond to customer's needs the fastest will win. Come hear about the latest improvements to JIRA, Bitbucket, HipChat and the rest of the Atlassian portfolio all designed to accelerate your software team.\r\n",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 1:00:00 AM",
					"Label": "Keynote 2 (Software)",
					"StartTime": "11/5/2015 12:15:00 AM"
				},
				"Track": {
					"Title": "General",
					"Description": "General"
				},
				"LastModified": "9/18/2015 2:10:24 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Keynote"
				},
				"Title": "Keynote: Atlassian for Software Teams",
				"Speakers": [
					{
						"AttendeeID": "TEMP-340300300",
						"Biography": "Eric Wittman is the General Manager for the developer products group at Atlassian, and is responsible for driving the strategic direction of the company's developer tools–including Bitbucket, Atlassian's enterprise Git repository management software, Bamboo and others. With more than 20 years of tech industry experience under his belt, Eric has been involved in creating some of the industry's most groundbreaking products including Director, the standard for multimedia CD-ROM development, Shockwave, the first multimedia technology for the web, Dreamweaver, the de facto standard for WYSIWYG HTML editing, and Flash.",
						"LastName": "Wittman",
						"EmailAddress": "ewittman@atlassian.com",
						"Title": "General Manager, Developer Tools",
						"AvailableforMeeting": "False",
						"FirstName": "Eric",
						"Roles": [
							"Speaker"
						],
						"Company": "Atlassian",
						"LastUpdated": "9/11/2015 9:45:57 PM",
						"ID": "119905"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 7-9",
					"Capacity": "2970"
				},
				"Id": "32036",
				"Description": "In the era of unlimited software at our fingertips, IT is under pressure to go beyond just fixing incidents to enabling teams across the business to be their best. Starting with JIRA and Confluence, we’ve been on a journey to empower IT with the best tools to deliver value. We followed up with JIRA Service Desk, an app built on JIRA for service management, but that was only the start. Come hear how we are helping IT innovate on top of the Atlassian portfolio and learn about the new goodies we’ll be delivering in JIRA Service Desk, Confluence, and HipChat.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 5:45:00 PM",
					"Label": "Keynote 3 (IT)",
					"StartTime": "11/5/2015 5:00:00 PM"
				},
				"Track": {
					"Title": "General",
					"Description": "General"
				},
				"LastModified": "9/18/2015 2:11:13 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Keynote"
				},
				"Title": "Keynote: Atlassian for IT Teams",
				"Speakers": [
					{
						"AttendeeID": "TEMP-973094801",
						"Biography": "Didier Moretti is the General Manager for the Service Desk and Confluence product families at Atlassian. An entrepreneur-at-heart, Didier enjoys building and scaling new businesses, drawing on experiences as the CEO of startups and as a VP/GM in large companies, most recently Cisco. Before venturing out west, Didier graduated from Massachusetts Institute of Technology (MIT) with two Master of Science degrees and earned a Bachelor’s Degree from Ecole Polytechnique in France.",
						"LastName": "Moretti",
						"EmailAddress": "dmoretti@atlassian.com",
						"Title": "General Manager, Service Desk",
						"AvailableforMeeting": "False",
						"FirstName": "Didier",
						"Roles": [
							"Speaker"
						],
						"Company": "Atlassian",
						"LastUpdated": "9/11/2015 9:47:38 PM",
						"ID": "119906"
					}
				],
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 7-9",
					"Capacity": "2970"
				},
				"Id": "32037",
				"Description": "Business teams are increasingly adopting the same tools that software and IT teams have been using for years. Come hear examples of how Marketing, HR, Legal, and other teams are using Atlassian products, and learn how those use cases might apply to your company…or at least spark your imagination! In the process we’ll introduce our first product dedicated to business teams, and demo the latest product advancements to HipChat and Confluence.",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 6:35:00 PM",
					"Label": "Keynote 4 (Beyond)",
					"StartTime": "11/5/2015 5:50:00 PM"
				},
				"Track": {
					"Title": "General",
					"Description": "General"
				},
				"LastModified": "9/18/2015 2:11:57 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Keynote"
				},
				"Title": "Keynote: Atlassian for Business Teams",
				"Speakers": [
					{
						"AttendeeID": "TEMP-524592517",
						"Biography": "Bryan J. Rollins is the General Manager for the JIRA Family of products at Atlassian. Originally from King Salmon, Alaska, Bryan now resides in Manly, Australia, and enjoys long distance triathlons, authentic Mexican cuisine, and the cinematic poetry of The Fast and Furious series.",
						"LastName": "Rollins",
						"EmailAddress": "brollins@atlassian.com",
						"Title": "General Manager, JIRA",
						"AvailableforMeeting": "False",
						"FirstName": "Bryan",
						"Roles": [
							"Speaker"
						],
						"Company": "Atlassian",
						"LastUpdated": "9/11/2015 9:49:46 PM",
						"ID": "119907"
					}
				],
				"Mandatory": "False"
			},
			{
				"Id": "32503",
				"Description": "Tuesday registration",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 5:00:00 AM",
					"Label": "Tuesday Registration",
					"StartTime": "11/3/2015 3:00:00 PM"
				},
				"Track": {
					"Title": "Activities",
					"Description": "Activities outside of regular sessions"
				},
				"LastModified": "9/22/2015 4:25:58 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Break"
				},
				"Title": "Registration",
				"Mandatory": "False"
			},
			{
				"Id": "32504",
				"Description": "Wednesday registration",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 3:30:00 AM",
					"Label": "Wednesday Registration",
					"StartTime": "11/4/2015 3:30:00 PM"
				},
				"Track": {
					"Title": "Activities",
					"Description": "Activities outside of regular sessions"
				},
				"LastModified": "9/22/2015 4:25:58 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Break"
				},
				"Title": "Registration",
				"Mandatory": "False"
			},
			{
				"Id": "32505",
				"Description": "Tuesday all day training",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 1:00:00 AM",
					"Label": "Tuesday Training Day",
					"StartTime": "11/3/2015 4:00:00 PM"
				},
				"Track": {
					"Title": "Training",
					"Description": "Training track for all training sessions"
				},
				"LastModified": "9/22/2015 4:29:45 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Training"
				},
				"Title": "Training",
				"Mandatory": "False"
			},
			{
				"Id": "32506",
				"Description": "Fireside chat opening keynote",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 2:30:00 AM",
					"Label": "Fireside Chat",
					"StartTime": "11/4/2015 1:30:00 AM"
				},
				"Track": {
					"Title": "Activities",
					"Description": "Activities outside of regular sessions"
				},
				"LastModified": "9/28/2015 9:23:25 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Keynote"
				},
				"Title": "Fireside Chat",
				"Mandatory": "False"
			},
			{
				"Id": "32507",
				"Description": "Opening reception on Tuesday",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 5:00:00 AM",
					"Label": "Opening Reception",
					"StartTime": "11/4/2015 2:30:00 AM"
				},
				"Track": {
					"Title": "Activities",
					"Description": "Activities outside of regular sessions"
				},
				"LastModified": "9/22/2015 4:33:53 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Break"
				},
				"Title": "Opening Reception",
				"Mandatory": "False"
			},
			{
				"Id": "32509",
				"Description": "Wednesday exhibits and Atlassian Product Hub open",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 12:15:00 AM",
					"Label": "Wednesday Day Exhibit Floor / Atlassian Product Hub",
					"StartTime": "11/4/2015 6:00:00 PM"
				},
				"Track": {
					"Title": "Activities",
					"Description": "Activities outside of regular sessions"
				},
				"LastModified": "9/22/2015 3:05:32 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Break"
				},
				"Title": "Exhibit Floor / Atlassian Product Hub",
				"Mandatory": "False"
			},
			{
				"Id": "32510",
				"Description": "Wednesday Demo Den open",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 8:50:00 PM",
					"Label": "Wednesday Demo Den",
					"StartTime": "11/4/2015 8:00:00 PM"
				},
				"Track": {
					"Title": "Activities",
					"Description": "Activities outside of regular sessions"
				},
				"LastModified": "9/22/2015 3:05:32 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Break"
				},
				"Title": "Demo Den",
				"Mandatory": "False"
			},
			{
				"Id": "32511",
				"Description": "Join us for our first-ever Women in Tech lunch at Summit! ",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 9:30:00 PM",
					"Label": "Women in Tech Lunch",
					"StartTime": "11/4/2015 8:00:00 PM"
				},
				"Track": {
					"Title": "Activities",
					"Description": "Activities outside of regular sessions"
				},
				"LastModified": "10/7/2015 6:47:25 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Break"
				},
				"Title": "Women in Tech Lunch",
				"Mandatory": "False"
			},
			{
				"Id": "32512",
				"Description": "Wednesday afternoon exhibits and Atlassian Product Hub open",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 2:00:00 AM",
					"Label": "Wednesday Afternoon Exhibit Floor / Atlassian Product Hub",
					"StartTime": "11/5/2015 1:00:00 AM"
				},
				"Track": {
					"Title": "Activities",
					"Description": "Activities outside of regular sessions"
				},
				"LastModified": "9/22/2015 3:05:32 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Break"
				},
				"Title": "Exhibit Floor / Atlassian Product Hub",
				"Mandatory": "False"
			},
			{
				"Id": "32513",
				"Description": "Epic Summit 2015 Bash!",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 7:00:00 AM",
					"Label": "Summit Bash",
					"StartTime": "11/5/2015 3:00:00 AM"
				},
				"Track": {
					"Title": "Activities",
					"Description": "Activities outside of regular sessions"
				},
				"LastModified": "9/22/2015 3:05:32 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Break"
				},
				"Title": "Summit Bash",
				"Mandatory": "False"
			},
			{
				"Id": "32515",
				"Description": "Thursday registration",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/6/2015 1:00:00 AM",
					"Label": "Thursday Registration",
					"StartTime": "11/5/2015 3:30:00 PM"
				},
				"Track": {
					"Title": "Activities",
					"Description": "Activities outside of regular sessions"
				},
				"LastModified": "9/22/2015 3:05:32 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Break"
				},
				"Title": "Registration",
				"Mandatory": "False"
			},
			{
				"Id": "32516",
				"Description": "Thursday breakfast and exhibits open",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 5:00:00 PM",
					"Label": "Thursday Breakfast / Exhibit Floor",
					"StartTime": "11/5/2015 3:30:00 PM"
				},
				"Track": {
					"Title": "Activities",
					"Description": "Activities outside of regular sessions"
				},
				"LastModified": "9/22/2015 3:05:32 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Break"
				},
				"Title": "Breakfast / Exhibit Floor",
				"Mandatory": "False"
			},
			{
				"Id": "32517",
				"Description": "Thursday exhibits and Atlassian Product Hub open",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 11:15:00 PM",
					"Label": "Thursday Exhibit Floor / Atlassian Product Hub",
					"StartTime": "11/5/2015 6:35:00 PM"
				},
				"Track": {
					"Title": "Activities",
					"Description": "Activities outside of regular sessions"
				},
				"LastModified": "9/22/2015 3:05:32 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Break"
				},
				"Title": "Exhibit Floor / Atlassian Product Hub",
				"Mandatory": "False"
			},
			{
				"Id": "32518",
				"Description": "Thursday Demo Den open",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 8:40:00 PM",
					"Label": "Thursday Demo Den",
					"StartTime": "11/5/2015 7:50:00 PM"
				},
				"Track": {
					"Title": "Activities",
					"Description": "Activities outside of regular sessions"
				},
				"LastModified": "9/28/2015 12:35:24 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Break"
				},
				"Title": "Demo Den",
				"Mandatory": "False"
			},
			{
				"Id": "32519",
				"Description": "Ship It Live 2015!",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/6/2015 12:30:00 AM",
					"Label": "Ship It Live",
					"StartTime": "11/5/2015 11:15:00 PM"
				},
				"Track": {
					"Title": "Activities",
					"Description": "Activities outside of regular sessions"
				},
				"LastModified": "9/22/2015 3:05:32 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Break"
				},
				"Title": "Ship It Live",
				"Mandatory": "False"
			},
			{
				"Id": "32536",
				"Description": "Wednesday lunch",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/4/2015 9:15:00 PM",
					"Label": "Wednesday Lunch",
					"StartTime": "11/4/2015 7:45:00 PM"
				},
				"Track": {
					"Title": "Activities",
					"Description": "Activities outside of regular sessions"
				},
				"LastModified": "9/28/2015 12:43:04 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Break"
				},
				"Title": "Lunch",
				"Mandatory": "False"
			},
			{
				"Id": "32537",
				"Description": "Thursday lunch",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 9:00:00 PM",
					"Label": "Thursday Lunch",
					"StartTime": "11/5/2015 7:35:00 PM"
				},
				"Track": {
					"Title": "Activities",
					"Description": "Activities outside of regular sessions"
				},
				"LastModified": "9/28/2015 12:43:04 PM",
				"Published": "True",
				"SessionType": {
					"Name": "Break"
				},
				"Title": "Lunch",
				"Mandatory": "False"
			},
			{
				"Room": {
					"Name": "Yerba Buena 13-15",
					"Capacity": "225"
				},
				"Id": "32572",
				"Description": "Millions of people book trips using Skyscanner's global search engine. Keeping the service is up and running 24/7 is crucial. Collaboration between software and IT teams ensures that this is the case: it’s all about finding and fixing bugs fast.\r\n\r\nIn this session, learn how the Skyscanner squad structure works, and how they improved collaboration between development and IT teams by 2x while improving IT productivity by 44%. Buckle up: you're about to take off.\r\n",
				"IsFeatured": "False",
				"TimeSlot": {
					"EndTime": "11/5/2015 9:30:00 PM",
					"Label": "Breakout 8",
					"StartTime": "11/5/2015 9:00:00 PM"
				},
				"Track": {
					"Title": "Service",
					"Description": "Service"
				},
				"LastModified": "10/6/2015 1:39:21 AM",
				"Published": "True",
				"SessionType": {
					"Name": "Breakout"
				},
				"MetadataValues": [
					{
						"Value": "JIRA,JIRA Service Desk",
						"Details": {
							"Title": "Products covered",
							"Options": [
								"JIRA",
								"JIRA Agile",
								"JIRA Service Desk",
								"JIRA Portfolio",
								"JIRA Capture",
								"Confluence",
								"Confluence Questions",
								"Confluence Team Calendars",
								"HipChat",
								"Bitbucket / Stash / SourceTree",
								"Bamboo",
								"FishEye/Crucible",
								"Clover / Crowd / Other",
								"Marketplace add-ons"
							],
							"FieldType": "MultiSelect"
						}
					}
				],
				"Title": "Better, Faster, Stronger: Improving Collaboration Between Dev and IT by 2x",
				"Speakers": [
					{
						"AttendeeID": "TEMP-915524037",
						"Biography": "Laura Haines is a Growth Operations Lead at Skyscanner, an Edinburgh based Meta Search Travel Company. She began her career as a full stack engineer in the Investment Banking industry but her passion for travel, product delivery and all things agile led her to Skyscanner. Laura manages all aspects of product delivery, engineering, commercial and marketing strategy, to focus on growing the company across the Americas, as well as coordinating multiple projects and agile coaching. Being organized inside and outside of work is important to Laura and while her work is also her passion she enjoys spending her spare time with family, friends and traveling.",
						"LastName": "Haines",
						"EmailAddress": "laura.haines@skyscanner.com",
						"Title": "Senior Project Manager",
						"AvailableforMeeting": "False",
						"FirstName": "Laura",
						"Roles": [
							"Speaker"
						],
						"Company": "Skyscanner",
						"LastUpdated": "10/2/2015 10:23:09 PM",
						"ID": "121122"
					},
					{
						"AttendeeID": "TEMP-125380975",
						"Biography": "Mike joined Skyscanner in 2013 working in Project Management and is now leading the Business Tools squad in owning, maintaining and enhancing several business applications - including JIRA and JIRA Service Desk. Right now, he and the rest of the squad are exploring how best to provide an excellent support service to a rapidly growing and innovative business as well as learning how to increase agility in service management. Before moving back to Scotland and joining Skyscanner, Mike worked with various companies and charities around the UK, Australia and New Zealand in sales, project management and operations. He spends too much of his free time adding to his rock collection and planning new trips.",
						"LastName": "Hall",
						"EmailAddress": "michael.hall@skyscanner.net",
						"AvailableforMeeting": "False",
						"FirstName": "Michael",
						"Roles": [
							"Speaker"
						],
						"Company": "Skyscanner",
						"LastUpdated": "10/2/2015 10:23:43 PM",
						"ID": "121123"
					}
				],
				"Mandatory": "False"
			}
		]
	};

/***/ }
/******/ ]);