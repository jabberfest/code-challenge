define([
	'backbone'
], function(Backbone){
	//TODO: Clean up explicit require since backbone.chooser does not appear to support AMD loading
	window._ = require('underscore');
	require('backbone.chooser');

	var TabEntity = {}

	TabEntity.Tab = Backbone.Model.extend({
		defaults: {},
		initialize: function(){
			new Backbone.Chooser(this);
		}
	});

	TabEntity.TabCollection = Backbone.Collection.extend({
		model: TabEntity.Tab,
		initialize: function(){
			new Backbone.SingleChooser(this)
		}
	});

	return TabEntity;
});