define([
	'underscore',
	'marionette',
	'templates/vertical_list_component_layout',
	'templates/vertical_item_view',
	'lib/entities/abstract/vertical_item'
], function(_,Marionette, vertical_list_component_layout, vertical_item_view_template, VerticalItemEntity){
	
	var VerticalListComponent = {}
	var _this=VerticalListComponent;

	VerticalListComponent.View = Marionette.LayoutView.extend({
	  template: vertical_list_component_layout,
	  className: 'vertical-list-component row',
	  regions:{
	  	verticalList: '.vertical-list-region',
	  	listContent: '.vertical-list-content-region'
	  },
	  onShow: function(){
	  	itemCollection = new VerticalItemEntity.ItemCollection(_.map(this.model.get('collection').models,function(model){
	  		return new VerticalItemEntity.Item(_.clone(model.attributes));
	  	}));

	   	var itemCollectionView = new _this.ItemCollectionView({collection:itemCollection});
	   	this.listenTo(itemCollection, "collection:chose:one",this.itemChosen);
	   	itemCollection.listenTo(itemCollectionView, "show", this.setDefaultItem);
	   	this.verticalList.show(itemCollectionView);
	  },
	  itemChosen: function(model){
	  	var contentView = new (this.model.get('viewOpts').view)({model: model});
	  	this.listContent.show(contentView);
	  },
  	  setDefaultItem: function(){
	   	//Select first tab by default
	   	this.choose(this.at(0));
	  }
	});

	VerticalListComponent.ItemView = Marionette.ItemView.extend({
		template: vertical_item_view_template,
		tagName: 'li',
		modelEvents:{
			'change chosen':'onItemChanged'
		},
		triggers:{
			'click a' : 'item:chosen'
		},
		onItemChanged: function(model){
			if (model.get('chosen')){
				this.$el.addClass('current');	
			}else{
				this.$el.removeClass('current');
			}
		}
	});

	VerticalListComponent.ItemCollectionView = Marionette.CollectionView.extend({
		childView: _this.ItemView,
		tagName: 'ul',
		className: 'items col-md-12',
		childEvents:{
			'item:chosen': 'onItemChosen'
		},
		onItemChosen: function(view){
			this.collection.choose(view.model);
		}
	});

	return VerticalListComponent;
});