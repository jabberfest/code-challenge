module.exports = (grunt) ->

  grunt.initConfig

    pkg: grunt.file.readJSON('package.json')

    clean:
      src: ['public//javascripts/templates']

    eco:
      compile:
        options:
          amd: true
        expand: true
        flatten:true
        cwd: 'public/'
        src: ['**/*.eco']
        dest: 'public/javascripts/templates/'
        ext: '.js'

    watch:
      files: ['**/*.eco']
      tasks: ['build']

  grunt.loadNpmTasks('grunt-eco')
  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-contrib-clean')


  grunt.registerTask('build', ['clean','eco'])
  grunt.registerTask('default', ['build'])