# README #

To view the finished code challenge clone this repo and navigate to **public/www** and open the **index.html** file.


All the source files live in the public directory

## Build steps (From project root)
### NPM
    npm install

### Bower
    bower install

### Webpack
From the javascripts directory run:
    
    webpack

### Harp
From the public directory run:
     
    harp compile